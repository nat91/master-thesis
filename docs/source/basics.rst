***********************************************
Introduction
***********************************************
This is the documentation for the "Calibration Library", developed by Anders Hansen and Jakob Lystbæk for their Master Thesis project at the University of Copenhagen.

The library contains utilities for working with multiple Intel Realsense RGB-D sensors and their video/depth streams. It contains utilities for establishing correspondences in RGB or RGB-D data between multiple cameras with known calibration objects and finally a set of tools for performing various kinds of Camera Calibration:

* Time Calibration. 
* Point Cloud alignment (extrinsic camera calibration) 

Furthermore, it can also output byproducts of the above, eg. point correspondences. It is not necessary to perform Intrinsic Calibration because that is done by Intel and its values are provided through our API or the RealSense API depending on which you prefer.

The library is written as a mix of Python and C++. The C++ code wraps the RealSense API in a minimal wrapper, which makes it easier to interface with the cameras. Furthermore, the `FileRecorder <https://gitlab.com/nat91/master-thesis/-/blob/master/code/lib/rswrapper/ku/ccal/FileRecorderThread.cpp>`_ class provides a convenient interface for recording from multiple cameras to a compact binary format.

You can use the API exposed by the code directly with your own code or you can use some of the general purpose tools we provide. See the thesis PDF file for two tutorials on how to use them.

===============================================
Requirements
===============================================
The software is developed for Linux, the following guide assumes you are using some variant of Ubuntu 18, however it should be easily adaptable to other distros. You can use it on Linux subsystems for Windows, but you will not be able to use any of the GUI applications. The library is primarily implemented in Python, uses PyQt for GUI and uses the standard template library for threading and IO, so in theory it can be ported completely to Windows with relatively little effort.

Note that the Realsense API provided by Intel does not work on Linux kernel version >= 5.0 at the moment. You will be able to record but not receive any frame meta information such as timestamps. Therefore, to use this library you need a 4.x Linux kernel. If you use Ubuntu, you can swap to one when you are booting via. the "Advanced Options for Ubuntu" menu in Grub.


===============================================
Installation
===============================================

First you must get the source code by cloning the master branch: ::

  git clone git@gitlab.com:nat91/master-thesis.git

After that, follow the instructions below to install the necessary dependencies and compile the necessary components.

-----------------------------------------------
Installing Python Dependencies
-----------------------------------------------

We use `Poetry <https://github.com/python-poetry/poetry>`_ for package management in Python, your first step is to install that: ::

  pip install poetry 

Once Poetry has been installed, enter the *code* directory and execute: ::

  poetry install
  poetry shell

The first will install all Python dependencies and set up a virtuel environment. The latter will give you a shell in the virtuel environment and allow you to run the code.

-----------------------------------------------
Installing Other Dependencies 
-----------------------------------------------
You can get almost everything you need via. the apt package manager, just run the following command: ::

  sudo apt-get install libboost-all-dev pyqt5-dev libhdf5-serial-dev

In addition you need to install Intel's RealSense software, follow the instructions `here <https://github.com/IntelRealSense/librealsense/blob/master/doc/distribution_linux.md>`_ .
Make sure to run: ::

  modinfo uvcvideo | grep "version:"

at the end to check if it contains the string "realsense", otherwise you will not be able to retrieve metadata from the cameras and time calibration will not work. If you get the error "No UVC frame data available", then your Linux kernel version is incompatible.


--------------------------------------------------------
Compiling the Realsense wrapper and its Python wrapper
--------------------------------------------------------
We have provided a *Makefile* to compile the Realsense wrapper and its Python wrapper, you do it as follows: ::

  cd code/lib/rswrapper
  make all

You may get an error if your Python installation is not located in the default directory (/usr/include/python) or if your version is different (we default to 3.6). You can change the above as follows if so: ::

  cd code/lib/rswrapper
  make python_wrapper PY_VERSION=yourPythonVersion PY_PATH=yourPythonInstallPath


===============================================
Default frame rate and resolution
===============================================
For maximum accuracy we use the maximum resolution and capture speed that can be provided by the RealSense sensors in both depth and RGB. This translates to 30 FPS, 1920x1080 RGB video and 1280x720 depth frames. If you increase the FPS, you must lower the RGB, depth resolution or both.
If you want to change these values, you need to change it in the `CameraDiscoverer <https://gitlab.com/nat91/master-thesis/-/blob/master/code/lib/rswrapper/ku/ccal/cameraDiscoverer.cpp>`_ class and recompile as described in the above. Note that if you supply a value that the RealSense API does not like, it will throw a (somewhat) meaningless exception. In addition, if you exceed the bandwidth limitations of the camera or the USB controller, you will also get a generic looking exception from the RealSense API. Thus, if you get any strange errors from the RealSense API the first thing you should do is to check that your cameras are plugged into separate USB controllers and that nothing else is connected.

.. _limitations-label:

===============================================
Limitations
===============================================
The theoretical bandwidth of a USB 3 port is 5 Gbit/s (625 MB/s). For maximum accuracy and a fluent capture, the highest resolution and FPS should be used. One full HD RGB frame uses :math:`1920*1080*3B \approx 6.2\text{MB}` and a depth frame takes up :math:`1280*720*2\text{B} \approx 1.8\text{MB}`. This gives us a bandwidth usage of approximately :math:`9\text{MB} * 30\text{FPS} = 270\text{MB/s}`. Thus at most two cameras can be connected to one USB 3 controller, and even then frames may be lost. We have found that to ensure that no frames are lost, it is best if the cameras are connected to seperate USB controllers. Note that adjacent ports often share the same USB controller. Furthermore, if you do not have a SSD drive, or if you lack USB controllers, you may need to record on multiple machines to keep up with the recording (see :ref:`multiple-machines-label`).

If you have too many cameras connected to one USB controller, or to one machine, you will get dropped frames. In the worst case, one or more of the cameras will not be able to deliver their frames and you will get a timeout error. If you do any realtime processing, it has to be faster than the camera capture frequency :math:`\frac{1}{\text{FPS}}\text{s}` otherwise frames will be dropped. To minimize the number of frames dropped, our tools are built to work on the binary format and all processing can thus be done offline. To prevent frames from being dropped, you should also ensure that no CPU heavy process is running on the machine.