**************************
The Configuration File
**************************
main.py and calibrate_frame.py will look for a file called "config.json" in the directory they are called from. The different configuration options and their default values are described in this section along with the format of the configuration file.

==========================
Configuration file format
==========================
An example file can be seen in the below: ::

  {
    "calibration": {
      "step": 10
    },
    "checkerboards": {
      "84": [4,4],
      "01": [4,4]
    }
  }

It sets the configuration parameter "calibration.step" to 10, which means "main.py" will calibrate every 10 frames when run on a video sequence. It also declares that there are two checkerboards in the scene with id "84" and "01" respectively, their value "[4,4]" indicates that they are both "4x4" checkerboards.

==========================
Configuration Parameters
==========================
The following lists the configuration parameters and their default values. The names are given in dot notation, you can enter them like that in the JSON file or nested as in the above.

-------------------------------------
cameras.auto_exposure_frames
-------------------------------------

How many frames the auto exposure should use to settle, these frames will be discarded. 

Default value is 30.

-------------------------------------
cameras.auto_ROI = false
-------------------------------------
If the camera should set its auto exposure region to a checkerboard or not. This can help with contrast in low light conditions, which in turn eases detection.
  
-------------------------------------
calibration.step
-------------------------------------
How often the calibration should be done when calibrating an entire video sequence. E.g. the default value is 10, which means it will calibrate every 10 frames.

Default value is 10

-------------------------------------
calibration.front_clipping_plane
-------------------------------------
The front clipping plane, all depth values less than this distance are removed. These can exist due to noise, even if the camera should not have data in this range. The value is in meters.

Default value is 0.3

-------------------------------------
calibration.back_clipping_plane
-------------------------------------
The back clipping plane. All depth further away than this distance will be removed. The value is in meters.

Default value is 3

-------------------------------------
calibration.minimum_correspondences
-------------------------------------
The minimum number of correspondences for a calibration to be calculated. It must be 4 at minimum for Umeyama's algorithm to work. Increasing this value can increase the robustness towards outliers and noise, but will also make it harder to find a calibration.

Default value is 4

-------------------------------------
calibration.detect_checkerboards
-------------------------------------
Whether checkerboards should be detected or not.

Default value is true

-------------------------------------
calibration.detect_spheres
-------------------------------------
Whether spheres should be detected or not.

Default value is true

-------------------------------------
calibration.detect_SIFT
-------------------------------------
Whether SIFT features should be detected or not.

Default value is true

-------------------------------------
calibration.time.num_flashes
-------------------------------------
The number of times the camera flash should flash. The value is in seconds.

Default value is 10

-------------------------------------
calibration.time.flash_frequency
-------------------------------------
How often the camera flash should flash. The one we are using cannot recharge fast enough if you set this below 3 seconds, which in turn make it hard to detect the flash events. The value is in seconds.

Default value is 3

-------------------------------------
calibration.time.baud_rate
-------------------------------------
The baud rate to be used for communicating with the flash Arduino board, we don't expect you will need to change this.

Default value is 9600

-------------------------------------
umeyama_ransac_thresh
-------------------------------------
The outlier threshold for RANSAC.

Default value is 1e-4

-------------------------------------
SIFT.dist_thresh
-------------------------------------
The SIFT feature matching threshold. The closer to 1, the more lenient it is and vice versa. It states how much, in percent, the second best match must be away in distance from the best match.

Default value is 0.7