*****************************
Examples
*****************************
The following sections show how to use parts of the API directly rather than one of the tools.

=========================
Introduction
=========================
If you are developing something outside of the package itself, you should include the repository's "lib" directory in your include path or extract it into your own project.
For instance, for a script located in the "code" directory, the following adds the directory "lib" to the include path, which allows us to use the Python wrapper for the camera API: ::

  import sys
  sys.path.append("lib/")
  from rswrapper import *

The following examples assume you are working on the project, and thus in the code directory, if you are not then you have to modify your include path.

==========================
Using the Camera API
==========================

--------------------------
Capturing a frame 
--------------------------

This example shows how you capture a single frame with the camera and display it.

First we include the necessary packages: ::

  import sys
  sys.path.append("lib/")
  from rswrapper import * # import the python wrapper for the C++ Library
  import matplotlib.pyplot as plt
  import numpy as np

Then we find any connected Realsense devices, configure them and start the stream: ::

  # find any connected cameras and configure them
  cameras = CameraDiscoverer.find()
  if not cameras.count():
    print("No cameras connected!")
    exit()
  
  # This call makes the cameras start streaming
  cameras.start()  

The `CameraDiscoverer <https://gitlab.com/nat91/master-thesis/-/blob/master/code/realsense-datalogger/ku/ccal/cameraDiscoverer.cpp>`_'s find method returns a `CameraCollection <https://gitlab.com/nat91/master-thesis/-/blob/master/code/realsense-datalogger/ku/ccal/cameraCollection.cpp>`_ object.

The Realsense sensors automatically adjust their exposure to the lighting conditions, if these are constant the depth quality is better if we wait a little for the auto exposure to settle. The below drops the first 30 frames to achieve this: ::

  # The depth data is better if we let the auto exposure settle 
  # to the lighting conditions, we do this by dropping the first 30 frames.
  cam0 = cameras[0]
  print("Settling auto exposure")
  for i in range(30):
    cam0.capture()

  print("Auto exposure settled")

Next we capture a frame from the camera, a frame contains both the RGB image and the depth frame. The memory for the frames is managed via. the STL shared pointers, they will be deallocated when they go out of scope in the Python program. The Frame object implements Python's buffer interface, which allows Python to reuse the memory allocated in the C++ code, unless you explicitly copy it. The buffer interface also allow us to construct a numpy array directly from the frame. We swap to the depth buffer by calling "enableDepthBuffer": ::

  # capture a frame object, this contains both the depth frame and the image frame.
  frame = cam0.capture()
  # get the image frame 
  image = np.array(frame)
  # enable the depth buffer
  frame.enableDepthBuffer()
  # get the depth frame
  depth = np.array(frame)
  # we want depth in meters, multiply it with the depth scale 
  depth *= cam0.getDepthScale()
  # threshold values that are far away from the camera in meters
  depth[depth >= 3.0] = 0

Thresholding the depth as is done in the above makes us able to display the image in grayscale, but it also has another purpose: At discontinuities, the cameras produce high depth values which is ultimately noise in the depth, the thresholding removes this.

Finally, we display both the RGB and the image frame, see the full code below. Note that you should end your code with: ::

  cameras.stop()

once you don't need to capture anymore frames, otherwise the camera's will keep streaming and consume power. The stop call deallocates all resources and stops the stream, hence that is the end of the lifetime for the CameraCollection object.

^^^^^^^^^^^^^^^^^^^^
The full code
^^^^^^^^^^^^^^^^^^^^

::

  import sys
  sys.path.append("lib/")
  from rswrapper import * # import the python wrapper for the C++ Library
  import matplotlib.pyplot as plt
  import numpy as np

  # find any connected cameras and configure them
  cameras = CameraDiscoverer.find()
  if not cameras.count():
    print("No cameras connected!")
    exit()
  
  # This call makes the cameras start streaming
  cameras.start()
  # The depth data is better if we let the auto exposure settle 
  # to the lighting conditions, we do this by dropping the first 30 frames.
  cam0 = cameras[0]
  print("Settling auto exposure")
  for i in range(30):
    cam0.capture()

  print("Auto exposure settled")
  # print the serial number of the first connected camera
  print(cam0.getSerialNumber())
  # capture a frame object, this contains both the depth frame and the image frame.
  frame = cam0.capture()
  # get the image frame 
  image = np.array(frame)
  # enable the depth buffer
  frame.enableDepthBuffer()
  # get the depth frame
  depth = np.array(frame)
  # we want depth in meters, multiply it with the depth scale 
  depth *= cam0.getDepthScale()
  # threshold values that are far away from the camera in meters
  depth[depth >= 3.0] = 0
  # show the depth and image frame
  plt.subplot(1, 2, 1)
  plt.title("RGB Image")
  plt.imshow(image)
  plt.subplot(1, 2, 2)
  plt.title("Depth")
  plt.imshow(depth, cmap="gray)
  plt.show()
  
  # stop the camera stream and deallocate all resources
  cameras.stop()

------------------------------------------
Accessing and using the camera metadata
------------------------------------------
This example briefly shows how to get and use the camera metadata, by applying it to align the depth frame with the RGB frame. 

Ideally, we'd like to get the depth of a RGB pixel directly from the depth frame :math:`d` by indexing into it with the RGB image coordinates :math:`(y, x)` such that we can get triples :math:`(x, y,\ d[y,x])`. However, because the Infrared sensors that produce the depth frame are slightly displaced from the RGB camera, we need to *align* the image and the depth frame before we can do that. To do that, we need to do the following steps:

1. For every :math:`(x, y, z)` in the depth frame, we reproject them into the world coordinate system defined by the IR camera. We need the Infrared Intrinsics to do this, because we need to reverse the transformation that transformed the world coordinates to the image coordinates in the Infrared sensor.
2. After reprojecting the depth coordinates to the world coordinate system, we use the Extrinsics between the Infrared and RGB camera to transform the coordinates from the world coordinate system of the Infrared camera, to that of the RGB camera.
3. Finally, we use the RGB Intrinsics to transform the world coordinates to RGB image coordinates. We may also need to do some upscaling, because the RGB image and the depth frame has different resolutions. We use nearest neighbour interpolation for that, such that no non-existant depths are created (this would be the case if we eg. used bilinear interpolation).

All of the above steps are implemented in the `util package <https://gitlab.com/nat91/master-thesis/-/blob/master/code/util/align.py>`_, the following will show how to extract the different camera intrinsics and the RGB to Infrared extrinsics from the Camera class and call the align method exposed by the util package. We assume you have followed the previous example and know how to extract the RGB and depth frame.

First, we get the camera intrinsic matrices, these are of the form: 

.. math::

  K = 
  \begin{bmatrix}
  f_x & 0 & p_x \\
  0 & f_y & p_y \\
  0 & 0 & 1 
  \end{bmatrix}

And can be retrieved as numpy 2d arrays by the following code for the IR and RGB camera respectively: ::

  # get Infrared and RGB intrinsic matrices
  ir_intrinsics = cam0.getDepthIntrinsics().getMatrix()
  rgb_intrinsics = cam0.getRGBIntrinsics().getMatrix()

After that, we retrieve the transformation between the IR and RGB camera's world coordinate system, this is a rigid transformation matrix that is usable on homogeneous coordinates:

.. math::

  T = 
  \begin{bmatrix}
  r_{11} & r_{12} & r_{13} & t_x \\
  r_{11} & r_{12} & r_{13} & t_y \\
  r_{11} & r_{12} & r_{13} & t_z \\
  0 & 0 & 0 & 1 \\
  \end{bmatrix}

It can be retrieved as a numpy 2d array as follows: ::

  depth_to_color_transform = cam.getDepthToColorExtrinsics().getTransform()

And finally we align the depth with the RGB frame: ::

  # dimensions of the RGB image, this is what the aligned depth frame will be scaled up to
  rgb_dim = (1080, 1920)

  depth = alignDepthToColor(ir_intrinsics, rgb_intrinsics, rgb_dim,
   depth_to_color_transform, depth)

The full code can be found in the below.



^^^^^^^^^^^^^^^^^^^^
The full code
^^^^^^^^^^^^^^^^^^^^

::

  import sys
  sys.path.append("lib/")
  from rswrapper import * # import the python wrapper for the C++ Library
  import matplotlib.pyplot as plt
  import numpy as np
  from util import alignDepthToColor

  # find any connected cameras and configure them
  cameras = CameraDiscoverer.find()
  if not cameras.count():
    print("No cameras connected!")
    exit()
  
  # This call makes the cameras start streaming
  cameras.start()
  # The depth data is better if we let the auto exposure settle 
  # to the lighting conditions, we do this by dropping the first 30 frames.
  cam0 = cameras[0]
  print("Settling auto exposure")
  for i in range(30):
    cam0.capture()

  print("Auto exposure settled")
  # print the serial number of the first connected camera
  print(cam0.getSerialNumber())
  # capture a frame object, this contains both the depth frame and the image frame.
  frame = cam0.capture()
  # get the image frame 
  image = np.array(frame)
  # enable the depth buffer
  frame.enableDepthBuffer()
  # get the depth frame
  depth = np.array(frame)
  # we want depth in meters, multiply it with the depth scale 
  depth *= cam0.getDepthScale()
  # threshold values that are far away from the camera in meters
  depth[depth >= 3.0] = 0

  # get Infrared and RGB intrinsic matrices
  ir_intrinsics = cam0.getDepthIntrinsics().getMatrix()
  rgb_intrinsics = cam0.getRGBIntrinsics().getMatrix()
  depth_to_color_transform = cam.getDepthToColorExtrinsics().getTransform()
  # dimensions of the RGB image, this is what the aligned depth frame will be scaled up to
  rgb_dim = (1080, 1920)

  depth = alignDepthToColor(ir_intrinsics, rgb_intrinsics, rgb_dim,
   depth_to_color_transform, depth)

  # show the depth and image frame
  plt.subplot(1, 2, 1)
  plt.title("RGB Image")
  plt.imshow(image)
  plt.subplot(1, 2, 2)
  plt.title("Aligned Depth")
  plt.imshow(depth, cmap="gray)
  plt.show()
  
  # stop the camera stream and deallocate all resources
  cameras.stop()

==================================================================================
Using the Reader API
==================================================================================

The FileReader class makes it easier to read RGB and depth data from .dat files.

Due to the lossless nature of realsense recordings, the resulting file size can be very large.
For this reason, the FileReader uses buffering, in order to save both memory usage and IO.
The size of the buffer (in frames) can be set upon instantiation of the FileReader.
The following example shows how to instantiate the FileReader and basic usage: ::

  from viewer import util
  from reader.reader import FileReader

  # Path to recdat file that needs to have recordings time-aligned
  recdata = get_camera_files_from_file(input_file)

  # Retrieve recording filepaths from recdat file
  filepaths = [elem[1] for elem in recdata]

  # Open file
  filehandler open(filepaths[0], 'rb')

  # Instantiate FileReader with 50 frames buffered
  fileReader = FileReader(50, filehandler)

  # Get current color frame and depth frame
  c = fileReader.currentFrame.colorFrame
  d = fileReader.currentFrame.depthFrame

  # Increment frame window
  fileReader.nextFrame()

  # Decrement frame window
  fileReader.prevFrame()

  # Get new color frame and depth frame
  cNew = fileReader.currentFrame.colorFrame
  dNew = fileReader.currentFrame.depthFrame

  # Skip 10 frame windows
  fileReader.skipFrames(10)

  # Get frame metadata
  frameMetadata = fileReader.currentFrame.frameMetaData

  # Get file metadata
  fileMetadata = fileReader.fileMetadata

==================================================================================
Using the Synchronization API
==================================================================================

The Synchronization API consists of a single function call, which temporally aligns
the recordings of a recdat file. Synchronization requires the recordings
to contain more than four synchronization events, ie camera flashes: ::

  from synchronization import sync
  from viewer import util

  # Path to recdat file that needs to have recordings time-aligned
  recdata = get_camera_files_from_file(input_file)

  # Retrieve recording filepaths from recdat file
  filepaths = [elem[1] for elem in recdata]

  # First file chosen as reference camera
  referenceCamera = filepaths[0]
  # Remaining cameras will be aligned to reference camera's time
  remainingCameras = filepaths[1:]

  # Perform alignement
  alignTimestamps(referenceCamera, remainingCameras)

The ``alignTimestamps`` function creates time-aligned recordings, where
the synchronization events have been stripped from the recording.


==================================================================================
More examples
==================================================================================
We have made many sample tests that we have used to test things, most of them are functional. You can see how to use the API in those and try to use them on your own.
In particular "test_cor.py" and "test_analysis.py" will be of interest.

The samples can be found at:
`https://gitlab.com/nat91/master-thesis/-/tree/master/code/samples <https://gitlab.com/nat91/master-thesis/-/tree/master/code/samples>`_