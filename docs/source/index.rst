.. Calibration Library documentation master file, created by
   sphinx-quickstart on Fri Mar 13 17:29:20 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Calibration Library's documentation!
===============================================

.. toctree::
   :maxdepth: 3

   basics
   tools
   examples
   configuration
   hdf5