**************************
Tools
**************************

==========================
The Datalogger
==========================
To start a recording go to the "code" directory and run: ::

  python3 main.py --outputFolder="recordingsFolder" --name=newRecording record

See the thesis PDF for a tutorial on how to use this tool.

The Datalogger outputs one ".recdat" file and a "*serialnumber*.dat" file pr. camera. The former is a CSV file containing the serial number and recording file name of each camera, this tells the software where to find each camera's recording. The binary recording files contain all the raw frames, their metadata and the camera metadata. The binary format is described in the next section.

--------------------------
The Binary Format
--------------------------
The code that writes the binary format can be found `here <https://gitlab.com/nat91/master-thesis/-/blob/master/code/realsense-datalogger/ku/ccal/frameFileWriter.cpp>`_. The following lists the format as a kind of grammar where we use the following notation:

* "|" is a field separator - it does **not** mean "or", the field order must be exactly as in the format.
* A name in all caps, eg. "FRAMES:", denotes a grammar production giving multiple elements.
* Each field has the format "datatype fieldName"
* "#" denotes a comment
* Every binary file starts with a HEADER section

Note that because the headers are structs written directly, if you change the format you need to make sure it is 8 byte aligned. We assume that all machines that will use the library are little endian machines, this is a reasonable assumption as big endian machines are rare.

The binary format is: ::

  # Format is little endian, header must be 8 byte aligned.
  HEADER: # 8 byte aligned
    uint16_t resolutionRGBW | 
    uint16_t resolutionRGBH | 
    uint16_t resolutionDepthW | 
    uint16_t resolutionDepthH | 
    uint64_t  FPS | 
    uint64_t number of frames | 
    float depth scale | 
    float ZERO-PADDING |
    INTRINSICS(rgb) |
    INTRINSICS(depth) |
    EXTRINSICS |
    ZERO_PADDING | # 1024 bytes reserved for future header data.
    FRAMES

  # Depth to RGB extrinsics for the camera
  # NOTE: The rotation matrix is stored in column major order
  # our API calls return it in row major order.
  EXTRINSICS: # 12*4 = 48 bytes, aligned.
    float r11 |
    float r12 |
    float r13 |
    float r21 |
    float r22 |
    float r23 |
    float r31 |
    float r32 |
    float r33 |
    float tx  |
    float ty  |
    float tz  

  # Camera intrinsic parameters
  INTRINSICS(prefix): # 9*4 = 36 bytes, not aligned - hence the trailing zero padding
    float prefix_focalLengthX |
    float prefix_focalLengthY | 
    float prefix_principalPointX | 
    float prefix_principalPointY |
    float prefix_distortionCoeff1 |
    float prefix_distortionCoeff2 |
    float prefix_distortionCoeff3 |
    float prefix_distortionCoeff4 |
    float prefix_distortionCoeff5 |
    float ZERO_PADDING

  # video frame
  FRAMES:
    uint64_t rgbtimestamp |
    uint64_t rgbFrameNo | 
    uint64_t depthtimestamp  |
    uint64_t depthFrameNo |  
    RGB data |
    depth data |
    FRAMES


.. _multiple-machines-label:

----------------------------------
Recording from multiple machines
----------------------------------
Because of the :ref:`limitations-label` previously mentioned, if you do not have enough USB controllers/CPUs on one machine, you may want to record from multiple machines. To do this, start the datalogger on multiple machines simultaneously and record as usual. After the recording has ended, copy the ".dat" files to the machine you want to do processing on and append the ".recdat" files from each machine to that of the primary machine. The ".recdat" files are CSV files, so you can do that with any text editor.

It could be a suitable distributed computing project for a future student, to make the software that can start recording simultaneously on many computers (eg. Raspberry Pi's) and potentially do distributed processing/analysis of the frame data. Most of the analysis is done on a per frame basis, so there is much to be gained from parallelization between multiple machines (or parts of it on a GPU). 

======================================
The CLI
======================================
The software provides a CLI which can used to run the different components, see the tutorials in the thesis.