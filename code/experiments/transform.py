import sys
sys.path.append("../")
sys.path.append("../lib")
import csv
import features
import numpy as np
import correspondences as corlib
from photogrammetry.absoluteOrientation import umeyama_ransac
from lib.helpers import framesetFromConnectedCameras
from scipy.spatial.transform import Rotation as R
from util import toHomogeneous, fromHomogeneous, Config


def runExperiment(cacheFile):
  config = Config("transform.json")
  # cacheFile = "transform.cache"
  frames, cameras = framesetFromConnectedCameras(cacheFile, verbose=True, setROI=False)
  det1 = features.checkerboards.CheckerboardDetector("cb", config)
  det2 = features.spheres.SphereDetector()
  det3 = features.SIFT.SIFTDetector()
  detCol = features.FeatureDetectorCollection()
  detCol.add(det1)
  detCol.add(det2)
  detCol.add(det3)

  img1 = frames[0].colorFrame
  img2 = frames[1].colorFrame
  depth1 = frames[0].depthFrame
  depth2 = frames[1].depthFrame
  K1 = cameras[0].intrinsics.getMatrix()
  K2 = cameras[1].intrinsics.getMatrix()

  f1 = detCol.getFeatures(img1, depth1, K1)
  f2 = detCol.getFeatures(img2, depth2, K2)

  cFinder = corlib.Finder(detCol)
  c3d, labels = cFinder.c3DF(img1, depth1, K1, f1, img2, depth2, K2, f2)

  [Ro, t, c, err, Rt], inliers = umeyama_ransac(c3d, t=config.get("umeyama_ransac_thresh", 1e-4))
  r = R.from_matrix((Ro.T))
  rx, ry, rz = r.as_euler("xyz", degrees=True)
  tx, ty, tz = -t
  scale = 1/c

  # Clear output-file
  # open('transform.csv', 'w').close()

  results = []
  # results.append(["rx", "ry", "rz", "tx", "ty", "tz", "scale", "err"])
  results.append([rx, ry, rz, tx, ty, tz, scale, err])


  with open('transform.csv', 'a') as f:
    writer = csv.writer(f)
    for row in results:
      writer.writerow(row)


if __name__ == "__main__":
   runExperiment(sys.argv[1])