import sys
sys.path.append("../")
sys.path.append("../lib")
from util import Config 
import datetime 
import csv
import random
from analysis import cameraCalibration
import features
from lib.helpers import leaveOneOutCalibration, framesetFromConnectedCameras


config = Config("siftLeaveOut.json")
cacheFile = "numOfCor.cache"
frames, cameras = framesetFromConnectedCameras(cacheFile, verbose=True)
det1 = features.checkerboards.CheckerboardDetector("cb", config)
det2 = features.spheres.SphereDetector()
det3 = features.SIFT.SIFTDetector(config)
detCol = features.FeatureDetectorCollection()
detCol.add(det1)
detCol.add(det2)
detCol.add(det3)

objects = ['red', 'blue', 'yellow', 'cb_05','cb_04', 'cb_01', 'cb_82', 'cb_83', 'cb_85', 'cb_14']
results = [["LeftOut", "mse", "rmse", "var", "minErr", "maxErr", "dist_var", "dist_mean", "dist_min", "dist_max", "inliers", "corsOut"]]

i = 1

while i < len(objects):
  leaveOut = [objects[i-1]]
  print('leaveOut: ', leaveOut)
  res, Rti, inliers, corsOut = leaveOneOutCalibration(detCol, leaveOut, config, cameras[0], frames[0], cameras[1], frames[1])
  var = res["var"]
  mse = res["mse"]
  rmse = res["rmse"] 
  minErr = res["min"]
  maxErr = res["max"]
  dist_var = res["dist_var"]
  dist_mean = res["dist_mean"]
  dist_min = res["dist_min"]
  dist_max = res["dist_max"]
  results.append([','.join(leaveOut), mse, rmse, var, minErr, maxErr, dist_var, dist_mean, dist_min, dist_max, inliers, corsOut])
  i += 1

with open('siftLeaveOut.csv', 'w') as f:
  writer = csv.writer(f)
  for row in results:
    writer.writerow(row)