import sys
sys.path.append("../")
sys.path.append("../lib")
from util import Config 
from analysis import cameraCalibration
import features
import datetime
import csv
from lib.photogrammetry.absoluteOrientation import umeyama_ransac
import correspondences as corlib
from lib.helpers import leaveOneOutCalibration, framesetFromConnectedCameras

if len(sys.argv) < 2:
  print("Usage: python3 span.py cacheFile")
  exit()

configSpan = Config("span.json")
#configNoSpan = Config("noSpan.json")
cacheFile = sys.argv[1]
frames, cameras = framesetFromConnectedCameras(cacheFile, verbose=True, setROI=True)

depth1 = frames[0].depthFrame
depth2 = frames[1].depthFrame
K1 = cameras[0].intrinsics.getMatrix()
K2 = cameras[1].intrinsics.getMatrix()

det1 = features.checkerboards.CheckerboardDetector("cb", configSpan)
det2 = features.spheres.SphereDetector()
detColSpan = features.FeatureDetectorCollection()
detColSpan.add(det1)
detColSpan.add(det2)

# detNoSpan1 = features.checkerboards.CheckerboardDetector("cb", configNoSpan)
# detNoSpan2 = features.spheres.SphereDetector()
# detColNoSpan = features.FeatureDetectorCollection()
# detColNoSpan.add(detNoSpan1)
# detColNoSpan.add(detNoSpan2)

leaveOut = ['green']


# Clear output-file
# open('span.csv', 'w').close()
results = []
# results.append(["LeftOut", "mse", "rmse", "var", "minErr", "maxErr", "dist_var", "dist_mean", "dist_min", "dist_max", "inliers", "corsOut"])


res, Rti, inliers, corsOut = leaveOneOutCalibration(detColSpan, leaveOut, configSpan, cameras[0], frames[0], cameras[1], frames[1])
var = res["var"]
mse = res["mse"]
rmse = res["rmse"] 
minErr = res["min"]
maxErr = res["max"]
dist_var = res["dist_var"]
dist_mean = res["dist_mean"]
dist_min = res["dist_min"]
dist_max = res["dist_max"]
results.append([','.join(leaveOut), mse, rmse, var, minErr, maxErr, dist_var, dist_mean, dist_min, dist_max, inliers, corsOut])

# print('Running noSpan\n')
# res, Rti, inliers, corsOut = leaveOneOutCalibration(detColNoSpan, leaveOut, configNoSpan, cameras[0], frames[0], cameras[1], frames[1])
# var = res["var"]
# mse = res["mse"]
# rmse = res["rmse"] 
# minErr = res["min"]
# maxErr = res["max"]
# dist_var = res["dist_var"]
# dist_mean = res["dist_mean"]
# dist_min = res["dist_min"]
# dist_max = res["dist_max"]
# results.append([','.join(leaveOut), mse, rmse, var, minErr, maxErr, dist_var, dist_mean, dist_min, dist_max, inliers, corsOut])

with open('span.csv', 'a') as f:
  writer = csv.writer(f)
  for row in results:
    writer.writerow(row)
