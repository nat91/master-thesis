import tables
import sys
import os
sys.path.append("../lib")
import features
from util import Config
from util import zInterp, deprojectPixelToPoint, alignDepthToColor, toHomogeneous
from util.pointCloud import *
import matplotlib.pyplot as plt
import numpy as np
from analysis import loadCameras
import pickle
from helpers import leaveOneOutCalibration

if len(sys.argv) < 2:
  print("Usage: python3 human_perturbation.py [path to recording]")
  exit()

path = sys.argv[1]
calFile = path.rstrip("/") + "/calibration.h5"
convFile = path.rstrip("/") + "/converted.h5"

if not os.path.exists(calFile):
  print("Error: File {} does not exist!".format(calFile))
  exit()

if not os.path.exists(convFile):
  print("Error: File {} does not exist!".format(convFile))
  exit()

config = Config("config.json")

checkerboard = features.checkerboards.CheckerboardDetector("cb")

calTbl = tables.open_file(calFile)
convTbl = tables.open_file(convFile)

cams = loadCameras(convTbl)

cam1 = cams[0]
cam2 = cams[1]

K1 = cam1.intrinsics.getMatrix()
K2 = cam2.intrinsics.getMatrix()

dScale1 = cam1.depthScale
dScale2 = cam2.depthScale

#print("recordings/cam_{}".format(cam1["serialNumber"].decode("ascii")))

cam1Tbl = convTbl.root["recordings/cam_{}".format(cam1.serialNo)]
cam2Tbl = convTbl.root["recordings/cam_{}".format(cam2.serialNo)]

rgb1 = cam1Tbl.rgb
rgb2 = cam2Tbl.rgb 

depth1 = cam1Tbl.depth
depth2 = cam2Tbl.depth

Ts = [x["transformPC"] for x in calTbl.root["transforms"] if x["fromCamera"] == 1]

numFrames = np.minimum(len(rgb1), len(rgb2))

fDet = features.FeatureDetectorCollection()
fDet.add(checkerboard)
sDet = features.SIFT.SIFTDetector(config)

res = []

for i in range(numFrames):
  print("At frame {}".format(i))
  i1 = rgb1[i]
  i2 = rgb2[i]

  res1 = checkerboard.detect(i1)
  res2 = checkerboard.detect(i2)

  if res1 is None or res2 is None:
    print("Skip {}".format(i))
    continue

  r1,_ = res1 
  r2,_ = res2


  if len(r1) != 2 or len(r2) != 2:
    print("Skip {}".format(i))
    continue

  #leaveOneOutCalibration()

  d1 = alignDepthToColor(cam1.depthIntrinsics.getMatrix(), cam1.intrinsics.getMatrix(), cam1.rgbResolution, cam1.depthToColorExtrinsics, depth1[i])

  d2 = alignDepthToColor(cam2.depthIntrinsics.getMatrix(), cam2.intrinsics.getMatrix(), cam2.rgbResolution, cam2.depthToColorExtrinsics, depth2[i])


  x1 = toHomogeneous(r1[0].coordsToArray())
  x2 = toHomogeneous(r2[0].coordsToArray())
  
  z1 = zInterp(x1, d1*dScale1)
  z2 = zInterp(x2, d2*dScale2)

  X1 = deprojectPixelToPoint(K1, x1.T, z1).T
  X2 = deprojectPixelToPoint(K2, x2.T, z2).T

  T = Ts[i]

  X2t = transformPointCloud(X2, T)
  
  if X1.shape != X2.shape:
    print("Skip {} - shape".format(i))
    continue

  res.append((i, X1, X2))

  print(i, np.mean(np.linalg.norm(X1-X2, axis=1)))

with open("human_perturbation.dat", "wb") as f:
  pickle.dump(res, f)


convTbl.close()
calTbl.close()