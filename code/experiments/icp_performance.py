import sys
sys.path.append("../lib/")
import os
import pickle
import open3d
import time
import features 
from util import Config
from util.pointCloud import *
from photogrammetry.absoluteOrientation import umeyama, umeyama_ransac
import correspondences as corlib

def preprocess_point_cloud(pcd, voxel_size):
  """
  From: http://www.open3d.org/docs/release/tutorial/Advanced/global_registration.html
  """
  #print(":: Downsample with a voxel size %.3f." % voxel_size)
  pcd_down = pcd.voxel_down_sample(voxel_size)

  radius_normal = voxel_size * 2
  #print(":: Estimate normal with search radius %.3f." % radius_normal)
  pcd_down.estimate_normals(
    o3d.geometry.KDTreeSearchParamHybrid(radius=radius_normal, max_nn=30))

  radius_feature = voxel_size * 5
  #print(":: Compute FPFH feature with search radius %.3f." % radius_feature)
  pcd_fpfh = o3d.registration.compute_fpfh_feature(
    pcd_down,
    o3d.geometry.KDTreeSearchParamHybrid(radius=radius_feature, max_nn=100))
  return pcd_down, pcd_fpfh




def execute_fast_global_registration(source_down, target_down, source_fpfh,
                                     target_fpfh, voxel_size):
  """
  From: http://www.open3d.org/docs/release/tutorial/Advanced/global_registration.html
  """                                     
  distance_threshold = voxel_size * 0.5
  #print(":: Apply fast global registration with distance threshold %.3f" \
          #% distance_threshold)
  result = o3d.registration.registration_fast_based_on_feature_matching(
    source_down, target_down, source_fpfh, target_fpfh,
    o3d.registration.FastGlobalRegistrationOption(
        maximum_correspondence_distance=distance_threshold))
  return result



def refine_registration(source, target, distThresh, T0):
  """
  Based on: http://www.open3d.org/docs/release/tutorial/Advanced/global_registration.html
  """                 
  #print(":: Point-to-plane ICP registration is applied on original point")
  #print("   clouds to refine the alignment. This time we use a strict")
  #print("   distance threshold %.3f." % distance_threshold)
  result = o3d.registration.registration_icp(
      source, target, distThresh, T0,
      o3d.registration.TransformationEstimationPointToPlane())
  return result


def coloredIcp(source, target, initialTransform, distThresh, initialRadius=0.04):
  #based on
  #http://www.open3d.org/docs/release/tutorial/Advanced/colored_pointcloud_registration.html
  scale = initialRadius
  max_iter = 100
  current_transformation = initialTransform
  result_icp = None
  last_icp = None
  #print("3. Colored point cloud registration")
  while True:
      radius = scale
      #print([iter, radius, scale])

      #print("3-1. Downsample with a voxel size %.2f" % radius)
      source_down = source.voxel_down_sample(radius)
      target_down = target.voxel_down_sample(radius)

      #print("3-2. Estimate normal.")
      source_down.estimate_normals(
          o3d.geometry.KDTreeSearchParamHybrid(radius=radius * 2, max_nn=30))
      target_down.estimate_normals(
          o3d.geometry.KDTreeSearchParamHybrid(radius=radius * 2, max_nn=30))

      #print("3-3. Applying colored point cloud registration")
      result_icp = o3d.registration.registration_colored_icp(
          source_down, target_down, radius, current_transformation,
          o3d.registration.ICPConvergenceCriteria(relative_fitness=1e-6,
                                                  relative_rmse=1e-6,
                                                  max_iteration=max_iter))
      
      if len(result_icp.correspondence_set) == 0:
        #print("Stopped at scale {}".format(scale))
        result_icp = last_icp
        break 
    

      current_transformation = result_icp.transformation
      last_icp = result_icp
      scale /= 2.0

      #print(result_icp)

  #print(result_icp)
  return result_icp

def testPointToPointICP(pc1, pc2, transform, trials, distThresh):
  res = []
  reg_p2p = None
  for i in range(trials):
    t0 = time.time()
    reg_p2p = o3d.registration.registration_icp(
        pc1, pc2, distThresh, transform,
        o3d.registration.TransformationEstimationPointToPoint())
    
    res.append(time.time()-t0)
  
  print(reg_p2p)
  print("Point to point ICP time:", np.mean(res), np.var(res), np.min(res), np.max(res))

def testPointToPointPlaneICP(pc1, pc2, transform, trials, distThresh):
  res = []
  reg_p2p = None
  for i in range(trials):
    t0 = time.time()
    reg_p2p = o3d.registration.registration_icp(
        pc1, pc2, distThresh, transform,
        o3d.registration.TransformationEstimationPointToPlane())
    
    res.append(time.time()-t0)

  print(reg_p2p)
  print("Point to plane ICP time:", np.mean(res), np.var(res), np.min(res), np.max(res))

def testColoredICP(pc1, pc2, transform, trials, distThresh):
  res = []
  reg_col = None
  for i in range(trials):
    t0 = time.time()
    reg_col = coloredIcp(pc1, pc2, transform, distThresh)
    res.append(time.time()-t0)
  
  print(reg_col)
  print("Colored ICP time:", np.mean(res), np.var(res), np.min(res), np.max(res))


if len(sys.argv) < 2:
  print("Usage: python3 icp_performance.py cacheFile")
  exit()

cacheFile = sys.argv[1]
if not os.path.exists(cacheFile):
  print("Cache file {} does not exist!".format(cacheFile))
  exit()

f = open(cacheFile, "rb")
frames, cameras = pickle.load(f)

# how far away can a correspondence at most be? 
distThresh = 0.01 # 1cm
numTrials = 25
config = Config("config.json")

pc1 = toColoredPointCloud(
  frames[0].colorFrame, 
  frames[0].depthFrame, 
  cameras[0].intrinsics.getMatrix())

pc2 = toColoredPointCloud(
  frames[1].colorFrame, 
  frames[1].depthFrame, 
  cameras[1].intrinsics.getMatrix())

pc1 = toO3DPointCloud(pc1)
pc2 = toO3DPointCloud(pc2)

pc1.estimate_normals()
pc2.estimate_normals()

# get transform from umeyama
det1 = features.checkerboards.CheckerboardDetector("cb", config)
#det2 = features.SIFT.SIFTDetector(config)
#et3 = features.spheres.SphereDetector()

detCol = features.FeatureDetectorCollection()
detCol.add(det1)
#detCol.add(det2)
#detCol.add(det3)


res = []
img1 = frames[0].colorFrame
img2 = frames[1].colorFrame
depth1 = frames[0].depthFrame
depth2 = frames[1].depthFrame
K1 = cameras[0].intrinsics.getMatrix()
K2 = cameras[1].intrinsics.getMatrix()

'''
for i in range(25):
  tStart = time.time()
  f1 = detCol.getFeatures(img1, depth1, K1)
  f2 = detCol.getFeatures(img2, depth2, K2)
  #print(1, time.time()-tStart)

  cFinder = corlib.Finder(detCol)
  c3d, labels = cFinder.c3DF(img1, depth1, K1, f1, img2, depth2, K2, f2)

  #Rt, inliers = optim_ransac(c1, c2, t=0.005, nPoints=4, c=0.999)
  #Rt = optimAll(c1, c2)
  #inliers = np.array(range(c1.shape[0]))


  (Ro, t, c, err, Rt), inliers = umeyama_ransac(c3d, t=config.get("umeyama_ransac_thresh", 1e-4))
  res.append(time.time()-tStart)
  #print(2, time.time()-tStart)

#exit()


print("Umeyama error:", err)
print("Inliers:", len(inliers))
print("Umeyama mean running time:", np.mean(res), np.min(res), np.max(res), np.var(res))
'''
print("Now benchmarking FGR")
resFGR = []
FGRT = None
voxelSize = 0.01


for i in range(25):
  tStart = time.time()
  pc1_d, pc1_fpfh = preprocess_point_cloud(pc1, voxelSize)
  pc2_d, pc2_fpfh = preprocess_point_cloud(pc2, voxelSize)

  res = execute_fast_global_registration(pc1_d, pc2_d, pc1_fpfh, pc2_fpfh, voxelSize)
  #res = execute_fast_global_registration(pc2_d, pc1_d, pc2_fpfh, pc1_fpfh, voxelSize)
  #print("Result fast global registration:", res)  
  resFGR.append(time.time()-tStart)
  print(time.time()-tStart)
  FGRT = res.transformation

print("FGR mean running time:", np.mean(resFGR), np.min(resFGR), np.max(resFGR), np.var(resFGR))

exit()

'''
print("Identity transform:")
print("-"*50)


testPointToPointICP(pc1, pc2, np.eye(4), numTrials, distThresh)
testPointToPointPlaneICP(pc1, pc2, np.eye(4), numTrials, distThresh)
testColoredICP(pc1, pc2, np.eye(4), numTrials, distThresh)

print("Umeyama transform:")
print("-"*50)
testPointToPointICP(pc1, pc2, Rt, numTrials, distThresh)
testPointToPointPlaneICP(pc1, pc2, Rt, numTrials, distThresh)
testColoredICP(pc1, pc2, Rt, numTrials, distThresh)
'''
print("FGR transform:")
print("-"*50)
testPointToPointICP(pc1, pc2, FGRT, numTrials, distThresh)
testPointToPointPlaneICP(pc1, pc2, FGRT, numTrials, distThresh)
testColoredICP(pc1, pc2, FGRT, numTrials, distThresh)