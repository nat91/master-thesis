import sys
import numpy as np
import os
from pyntcloud.io import read_ply
from mpl_toolkits.mplot3d import Axes3D
import pickle
import matplotlib.pyplot as plt
sys.path.append("../lib")
sys.path.append("../samples")
from util import Config, deprojectPixelToPoint, toHomogeneous, fromHomogeneous, pointCloud
import o3d_test
import open3d as o3d
import skimage
import scipy
from helpers import framesetFromConnectedCameras
from lib import features
from analysis import cameraCalibration

config = Config("icp.json")

useICP = True

bunny = read_ply("bunny.ply")
bunny_xyz = bunny["points"][["x", "y", "z"]].values * (1/1000)

cloud = "cloud.pickle"

# Replace with artificial
# cacheFile = "icp.cache"
# frames, cameras = framesetFromConnectedCameras(cacheFile, verbose=True, setROI=True)
# inputFile = "test_reconstruct_merged.data"
inputFile = "icp_bunny.data"
with open(inputFile, "rb") as f:
  frames, cameras = pickle.load(f)


keep = [0,1,2,3,4,5]
frames = [frames[i] for i in keep]
cameras = [cameras[i] for i in keep]

# det1 = features.checkerboards.CheckerboardDetector("cb", config)
det2 = features.spheres.SphereDetector()
det3 = features.SIFT.SIFTDetector(config)
# det4 = features.twoSidedBoards.TwoSidedDetector(config)
detCol = features.FeatureDetectorCollection()
# detCol.add(det1)
detCol.add(det2)
detCol.add(det3)
# detCol.add(det4)

# fig = plt.figure()
# # Add an axes
# ax = fig.add_subplot(111,projection='3d')
# xs = bunny_xyz[:,0]
# ys = bunny_xyz[:,1]
# zs = bunny_xyz[:,2]
# ax.scatter(xs, ys, zs, "g.")
# max_range = np.array([xs.max()-xs.min(), ys.max()-ys.min(), zs.max()-zs.min()]).max() / 2.0
# mid_x = (xs.max()+xs.min()) * 0.5
# mid_y = (ys.max()+ys.min()) * 0.5
# mid_z = (zs.max()+zs.min()) * 0.5
# ax.set_xlabel('x')
# ax.set_ylabel('y')
# ax.set_zlabel('z')
# ax.set_xlim(mid_x - max_range, mid_x + max_range)
# ax.set_ylim(mid_y - max_range, mid_y + max_range)
# ax.set_zlim(mid_z - max_range, mid_z + max_range)
# ax.set_xlabel('X axis')
# ax.set_ylabel('Y axis')
# ax.set_zlabel('Z axis')
# plt.show()


if os.path.exists(cloud):
  with open(cloud, "rb") as f:
    T = pickle.load(f)
else:
  T = cameraCalibration(cameras, frames, detCol, config)
  with open(cloud, "wb") as f:
    pickle.dump(T, f)


N = len(cameras)
# wnd = o3d_test.CalibrationWindow()

PCs = None

print('bunny_xyz: ', bunny_xyz.size)

theta = 180 
guess = np.array([[1, 0, 0, 0],
                  [0, np.cos(theta), np.sin(theta), 0],
                  [0, -np.sin(theta), np.cos(theta), 0],
                  [ 0, 0, 0, 1]])
bunny_xyz = pointCloud.transformPointCloud(bunny_xyz, guess)
bunny_xyz[:,0] = bunny_xyz[:,0]+0.01
bunny_xyz[:,1] = bunny_xyz[:,1]-0.02
bunny_xyz[:,2] = bunny_xyz[:,2]+0.82

for i in range(N):
  # if i == 4: 
  #   continue
  if i not in T and i != 0:
    print("Camera calibration for camera {} failed".format(i))
    continue

  # camera i frustrum
  (h,w) = (1080, 1920) #cameras[i].rgbResolution
  cam = np.array([
    [0.0, 0.0, 0.0],
    [w, 0.0, 0.0],
    [w, h, 0.0],
    [0.0, h, 0.0],
    [0.0, 0.0, 0.0]
  ])  

  K = cameras[i].intrinsics.getMatrix()

  coords = deprojectPixelToPoint(K, cam.T, np.array([0.1, 0.1, 0.1, 0.1, 0.0]).T).T
  # transform the camera to cam 0's coordinate system if it is not cam 0
  if i > 0:
    coords = toHomogeneous(coords)
    coords = fromHomogeneous(np.dot(T[i]["transformCam"], coords.T).T)

  # wnd.addCamera(coords, "cam{}".format(i))

  # draw camera i's point cloud
  dThresh_h = 2.0
  dThresh_l = 0.3

  depth = frames[i].depthFrame

  idx = np.where( (depth <= dThresh_h) & (depth >= dThresh_l))
  xs = idx[1]
  ys = idx[0]
  zs = depth[idx]

  X = deprojectPixelToPoint(K, np.array([xs, ys, zs]), zs).T
  # transform point cloud to cam 0's coordinate system if it is not cam 0
  if i > 0:
    X = fromHomogeneous(np.dot(T[i]["transformPC"], toHomogeneous(X).T).T)

  
  colorFrame = skimage.img_as_float(frames[i].colorFrame)
  color = colorFrame[ys, xs]
  #print(color.shape)

  X = np.hstack((X, color))


  if useICP:
    pc1 = pointCloud.toO3DPointCloud(X[:,:3])
    rows = np.where(np.logical_and(X[:,0] > -0.1, X[:,0] < 0.1))
    X = X[rows]
    rows = np.where(np.logical_and(X[:,1] > 0.04, X[:,1] < 0.10))
    X = X[rows]
    rows = np.where(np.logical_and(X[:,2] > 0.7, X[:,2] < 0.8))
    X = X[rows]
    pc1.estimate_normals()
    pc2 = pointCloud.toO3DPointCloud(bunny_xyz)
    pc2.estimate_normals()
    method = o3d.registration.TransformationEstimationPointToPoint()
    reg_p2p = o3d.registration.registration_icp(
    pc1, pc2, 0.05, np.eye(4), method, o3d.registration.ICPConvergenceCriteria(max_iteration = 2000))
  
    print('Hi')
    print('reg_p2p: ', reg_p2p.transformation)
    X = pointCloud.transformPointCloud(X, reg_p2p.transformation)


  if PCs is not None:
    PCs = np.vstack((PCs, X))
  else:
    PCs = X




PCs = PCs[:,:3]
# PCs = PCs[0::5]
rows = np.where(np.logical_and(PCs[:,0] > -0.1, PCs[:,0] < 0.1))
PCs = PCs[rows]
rows = np.where(np.logical_and(PCs[:,1] > 0.04, PCs[:,1] < 0.10))
PCs = PCs[rows]
rows = np.where(np.logical_and(PCs[:,2] > 0.7, PCs[:,2] < 0.8))
PCs = PCs[rows]
print('PCs: ', PCs.size)



fig = plt.figure()
# Add an axes
PCsDraw = PCs[0::10]
bunnyDraw = bunny_xyz[0::50]
ax = fig.add_subplot(111,projection='3d')
xs = PCsDraw[:,0]
ys = PCsDraw[:,1]
zs = PCsDraw[:,2]
# ax.scatter(bunnyDraw[:,0], bunnyDraw[:,1], bunnyDraw[:,2], c='r', s=0.002)
ax.scatter(xs, ys, zs, "g.", s=0.02)
max_range = np.array([xs.max()-xs.min(), ys.max()-ys.min(), zs.max()-zs.min()]).max() / 2.0
mid_x = (xs.max()+xs.min()) * 0.5
mid_y = (ys.max()+ys.min()) * 0.5
mid_z = (zs.max()+zs.min()) * 0.5
ax.set_xlabel('X axis')
ax.set_ylabel('Y axis')
ax.set_zlabel('Z axis')
ax.set_xticklabels([])
ax.set_yticklabels([])
ax.set_zticklabels([])
plt.show()


pc1 = pointCloud.toO3DPointCloud(PCs)
pc1.estimate_normals()
pc2 = pointCloud.toO3DPointCloud(bunny_xyz)
pc2.estimate_normals()
method = o3d.registration.TransformationEstimationPointToPoint()
reg_p2p = o3d.registration.registration_icp(
  pc1, pc2, 0.1, np.eye(4), method, o3d.registration.ICPConvergenceCriteria(max_iteration = 2000))
  
print('reg_p2p: ', reg_p2p.transformation)
print('reg_p2p: ', reg_p2p.correspondence_set)
PCs = pointCloud.transformPointCloud(PCs, reg_p2p.transformation)

# wnd.addPointCloud(PCs, "PC", color=np.array([0.0, 0.0, 1.0]))

# wnd.addPointCloud(bunny_xyz, "bunny_xyz", color=np.array([1.0, 0.0, 0.0]))
# wnd.run()    



fig = plt.figure()
# Add an axes
ax = fig.add_subplot(111,projection='3d')
PCsDraw = PCs[0::5]
bunnyDraw = bunny_xyz[0::50]
xs = PCsDraw[:,0]
ys = PCsDraw[:,1]
zs = PCsDraw[:,2]
ax.scatter(bunnyDraw[:,0], bunnyDraw[:,1], bunnyDraw[:,2], c='r', s=0.002)
ax.scatter(xs, ys, zs, "g.", s=0.002)
max_range = np.array([xs.max()-xs.min(), ys.max()-ys.min(), zs.max()-zs.min()]).max() / 2.0
mid_x = (xs.max()+xs.min()) * 0.5
mid_y = (ys.max()+ys.min()) * 0.5
mid_z = (zs.max()+zs.min()) * 0.5
ax.set_xlim(mid_x - max_range, mid_x + max_range)
ax.set_ylim(mid_y - max_range, mid_y + max_range)
ax.set_zlim(mid_z - max_range, mid_z + max_range)
ax.set_xticklabels([])
ax.set_yticklabels([])
ax.set_zticklabels([])
ax.set_xlabel('X axis')
ax.set_ylabel('Y axis')
ax.set_zlabel('Z axis')
plt.show()



print('bunny_xyz: ', bunny_xyz.shape)
print('PCs: ', PCs.shape)
hausdorff = scipy.spatial.distance.directed_hausdorff(bunny_xyz, PCs)
print('hausdorff: ', hausdorff)

runPointToPoint = False
bunnyMesh = bunny_xyz[::5]
if runPointToPoint:
  dists = []
  i = 0
  for point in PCs:
    print('i: ', (i, len(PCs)))
    shortest = np.inf
    for sndPoint in bunnyMesh:
      dist = np.linalg.norm(point-sndPoint)
      if dist < shortest:
        shortest = dist
    dists.append(shortest)
    i += 1
  
  print('dists: ', len(dists))
  print('min, max, avg:', (np.min(dists), np.max(dists), np.mean(dists)))