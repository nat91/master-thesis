import pickle
import sys
import numpy as np
import os
sys.path.append("../lib")
import features
from util import fromHomogeneous, toHomogeneous


if not os.path.exists("known.data"):
  print("known.data not found.")
  exit()

if not os.path.exists("known.cal_dat"):
  print("known.cal_dat not found.")
  exit()

cameras, frames = None, None
T = None
with open("known.data", "rb") as f:
  frames, cameras = pickle.load(f)

with open("known.cal_dat", "rb") as f:
  T = pickle.load(f)



# Stores the location of the blue sphere in the world coordinate system
# as seen from each camera after calibration
bluePoints = []

for i in range(len(frames)):
  # run sphere detector and get blue coordinate
  det = features.spheres.SphereDetector()

  K = cameras[i].intrinsics.getMatrix()
  color = frames[i].colorFrame
  depth = frames[i].depthFrame

  res = det.getFeatures(color, depth, K)
  blueIdx = res["spheres"]["descriptors"]["blue"]
  bluePos = res["spheres"]["features"][blueIdx].reshape(1, 3)

  if i > 0:
    bluePosWorld = fromHomogeneous(np.dot(T[i]["transformPC"], toHomogeneous(bluePos).T).T)
    bluePoints.append(bluePosWorld)
  else:
    bluePoints.append(bluePos)

bluePoints = np.array(bluePoints).reshape((5,3))
print(bluePoints.shape)
print(bluePoints)

reference = bluePoints[0]
rest = bluePoints[1:]
dists = []

for point in rest:
  dists.append(np.linalg.norm(reference-point))
print('dists: ', dists)
print("Mean:", np.mean(dists))
print("Var:", np.var(dists))