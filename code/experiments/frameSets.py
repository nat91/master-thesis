import os
import numpy as np
import skimage
import time
import matplotlib.pyplot as plt
import sys
import pickle
sys.path.append("../realsense-datalogger/")
sys.path.append("../")
from util import alignDepthToColor
import py_ku_ccal

def newExperiment(outputFolder, name):

  if not os.path.isdir(outputFolder):
    print("The path specified does not exist.")
    exit()

  fullPath = os.path.join(outputFolder, name) 
  if os.path.isdir(fullPath):
    print("Output directory already exists.")
    exit()

  cameras = py_ku_ccal.CameraDiscoverer.find()
  if not cameras.count():
    print("Error: No cameras connected!")
    exit()

  cameras.start()
  cam = cameras[0]
  cam.start()

  imgs = []
  time.sleep(1)
  while True:
    frame = cam.capture()
    K = cam.getRGBIntrinsics().getMatrix()
    frameCopy = skimage.img_as_float(np.array(frame))
    frame.enableDepthBuffer()

    depthCopy = np.array(frame)*cam.getDepthScale()
    # threshold points
    # depthCopy[depthCopy >= 3.0] = 0
    depthCopy = alignDepthToColor(cam.getDepthIntrinsics().getMatrix(), cam.getRGBIntrinsics().getMatrix(), (1080, 1920), cam.getDepthToColorExtrinsics(), depthCopy)


    imgs.append((frameCopy, depthCopy))
    x = input("c to continue, e to end recording\n")
    if x == 'e':
      break
    elif x == 'c':
      continue


  print('Frames: ', len(imgs))
  cam.stop()

  with open(fullPath, "wb") as f:
    pickle.dump(imgs,f)


def main(argv):
  if len(argv) < 1:
     print("Missing file path argument.\n")
  if len(argv) < 2:
     print("Missing file name argument.\n")
  elif len(argv) == 2:
    directory = argv[0]
    name = argv[1]
    newExperiment(directory, name)
  else:
    print("Too many arguments given.\n")


if __name__ == "__main__":
   main(sys.argv[1:])
