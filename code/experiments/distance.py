import sys
sys.path.append("../")
sys.path.append("../lib")
from util import Config
from analysis import cameraCalibration
import features
import datetime
import csv
from lib.photogrammetry.absoluteOrientation import umeyama_ransac
import correspondences as corlib
from lib.helpers import leaveOneOutCalibration, framesetFromConnectedCameras


closeCache = "close.cache"
farCache = "far.cache"
frames, cameras = framesetFromConnectedCameras(closeCache, verbose=True, setROI=False)
frames2, cameras2 = framesetFromConnectedCameras(farCache, verbose=True, setROI=False)

K1 = cameras[0].intrinsics.getMatrix()
K2 = cameras[1].intrinsics.getMatrix()


det1 = features.checkerboards.CheckerboardDetector("cb", None)
det2 = features.spheres.SphereDetector()

detClose = features.FeatureDetectorCollection()
detClose.add(det1)
detClose.add(det2)

detFarSphere = features.spheres.SphereDetector()
detFarCheck = features.checkerboards.CheckerboardDetector("cb", None)
detFar = features.FeatureDetectorCollection()
detFar.add(detFarSphere)
detFar.add(detFarCheck)

leaveOut = ['red']


# Clear output-file
# open('distance.csv', 'w').close()
results = []
results.append(["Distance", "mse", "rmse", "var", "minErr", "maxErr", "dist_var", "dist_mean", "dist_min", "dist_max", "inliers", "corsOut"])


res, Rti, inliers, corsOut = leaveOneOutCalibration(detClose, leaveOut, None, cameras[0], frames[0], cameras[1], frames[1])
var = res["var"]
mse = res["mse"]
rmse = res["rmse"]
minErr = res["min"]
maxErr = res["max"]
dist_var = res["dist_var"]
dist_mean = res["dist_mean"]
dist_min = res["dist_min"]
dist_max = res["dist_max"]
results.append(["Close", mse, rmse, var, minErr, maxErr, dist_var, dist_mean, dist_min, dist_max, inliers, corsOut])

print('Running far\n')
res, Rti, inliers, corsOut = leaveOneOutCalibration(detFar, leaveOut, None, cameras2[0], frames2[0], cameras2[1], frames2[1])
var = res["var"]
mse = res["mse"]
rmse = res["rmse"]
minErr = res["min"]
maxErr = res["max"]
dist_var = res["dist_var"]
dist_mean = res["dist_mean"]
dist_min = res["dist_min"]
dist_max = res["dist_max"]
results.append(["Far", mse, rmse, var, minErr, maxErr, dist_var, dist_mean, dist_min, dist_max, inliers, corsOut])

with open('distance.csv', 'a') as f:
  writer = csv.writer(f)
  for row in results:
    writer.writerow(row)
