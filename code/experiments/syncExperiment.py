import sys
import random
sys.path.append("../lib")
sys.path.append('../tools')
from synchronization import sync
from reader.reader import FileReader
from viewer.util import get_camera_files_from_file


recdata = get_camera_files_from_file('/home/jakob/master-thesis/code/experiments/flash12_2/recordingInfo.recdat')
filepaths = [elem[1] for elem in recdata]

referenceCamera = filepaths[0] # First file chosen as reference camera
remainingCameras = filepaths[1:] # Remaining cameras need alignment


# indices = list(range(0,12))
# L=6

# for _ in range(0,6):
#   sample = random.sample(indices, L)
#   sample.sort()
#   print('sample: ', sample)
#   sync.alignTimestamps(referenceCamera, remainingCameras, sample)

sync.alignTimestamps(referenceCamera, remainingCameras, [0,1,2,3,4,5])