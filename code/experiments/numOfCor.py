import sys
sys.path.append("../")
sys.path.append("../lib")
from util import Config 
import datetime 
import csv
import random
from analysis import cameraCalibration
import features
from lib.helpers import leaveOneOutCalibration, framesetFromConnectedCameras

cacheFile = "span.cache"

config = Config("numOfCor.json")
frames, cameras = framesetFromConnectedCameras(cacheFile, verbose=True)
det01 = features.checkerboards.CheckerboardDetector("cb", config)
det02 = features.spheres.SphereDetector()
detCol0 = features.FeatureDetectorCollection()
detCol0.add(det01)
detCol0.add(det02)

config1 = Config("numOfCor1.json")
frames1, cameras1 = framesetFromConnectedCameras(cacheFile, verbose=True)
det11 = features.checkerboards.CheckerboardDetector("cb", config1)
det12 = features.spheres.SphereDetector()
detCol1 = features.FeatureDetectorCollection()
detCol1.add(det11)
detCol1.add(det12)


config2 = Config("numOfCor2.json")
frames2, cameras2 = framesetFromConnectedCameras(cacheFile, verbose=True)
det21 = features.checkerboards.CheckerboardDetector("cb", config2)
det22 = features.spheres.SphereDetector()
detCol2 = features.FeatureDetectorCollection()
detCol2.add(det21)
detCol2.add(det22)


config3 = Config("numOfCor3.json")
frames3, camera3s = framesetFromConnectedCameras(cacheFile, verbose=True)
det31 = features.checkerboards.CheckerboardDetector("cb", config3)
det32 = features.spheres.SphereDetector()
detCol3 = features.FeatureDetectorCollection()
detCol3.add(det31)
detCol3.add(det32)


config4 = Config("numOfCor4.json")
frames4, cameras4 = framesetFromConnectedCameras(cacheFile, verbose=True)
det41 = features.checkerboards.CheckerboardDetector("cb", config4)
det42 = features.spheres.SphereDetector()
detCol4 = features.FeatureDetectorCollection()
detCol4.add(det41)
detCol4.add(det42)

# objects = ['cb_05','cb_04', 'cb_01', 'cb_02', 'cb_82', 'cb_83', 'cb_85', 'cb_15']
results = [["n", "mse", "rmse", "var", "minErr", "maxErr", "dist_var", "dist_mean", "dist_min", "dist_max", "inliers", "corsOut"]]


for test in [('test0', config, detCol0), ('test1', config1, detCol1), ('test2', config2, detCol2), ('test3', config3, detCol3), ('test4', config4, detCol4)]:
  print('test: ', test)
  res, Rti, inliers, corsOut = leaveOneOutCalibration(test[2], ['red'], test[1], cameras[0], frames[0], cameras[1], frames[1])
  var = res["var"]
  mse = res["mse"]
  rmse = res["rmse"] 
  minErr = res["min"]
  maxErr = res["max"]
  dist_var = res["dist_var"]
  dist_mean = res["dist_mean"]
  dist_min = res["dist_min"]
  dist_max = res["dist_max"]
  results.append([inliers, mse, rmse, var, minErr, maxErr, dist_var, dist_mean, dist_min, dist_max, inliers, corsOut])

with open('numOfCor.csv', 'w') as f:
  writer = csv.writer(f)
  for row in results:
    writer.writerow(row)