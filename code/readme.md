## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Setup](#setup)

## General info
This project is simple Lorem ipsum dolor generator.
	
## Built With
This project uses:
* Python 3.6
* c++17
* OpenCV 4.2.0
* LibRealsense2
	
# Installing Dependencies

## Installing LibRealsense
Follow the instructions at: https://github.com/IntelRealSense/librealsense/blob/master/doc/distribution_linux.md

Make sure to run: 
`modinfo uvcvideo | grep "version:"` 
at the end to check if it contains the string "realsense", otherwise you will not be able to retrieve metadata from the cameras and time calibration will not work.

If you get an error like "No UVC frame data available" and the above does contain the string "realsense", then you are running an incompatible Linux kernel. You can change kernels with grub during boot by going to advanced settings for ubuntu, and then boot with a compatible kernel - eg. 4.18.

## Other C++ dependencies
You need boost installed, on Ubuntu run:

```
sudo apt-get install libboost-all-dev
```

You need `pybind11` headers in the *lib* directory in order to compile the Python bindings for the `ku::ccal` library, you can get them here: https://github.com/pybind/pybind11. 
After downloading the repository, extract the *pybind11* directory into the *lib* directory.

## Python Dependencies
For convenience, the project uses `poetry` to manage Python packages.
To install dependencies:
```
cd code/
poetry install
```
A new shell can then be spawned with the virtual environment activated:
```
poetry shell
```

# Compiling the datalogger
On Ubuntu you simply go into the realsense datalogger directory and run the makefile
```
cd code/realsense-datalogger
make
```

# Using the CLI
The software provides a CLI which can used to run the different components.

To create a new recording (folder with .recdat and .dat files):
```
cd code/demo
python3 demo.py record -o pathToOutputDirectory -n nameOfRecording
```
To perform time synchronization of multi-camera recordings:
```
python3 demo.py sync -rd pathToRecdatFile
```
To open the recording-viewer GUI:
```
python3 demo.py view
```
To convert a recording to .AVI and .npy formats:
```
python3 demo.py convert -rd pathToRecdatFile -o pathToOutputDirectory
```