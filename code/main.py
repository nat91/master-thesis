import os
import sys
import argparse
sys.path.append("lib/")
sys.path.append("tools/")
from rswrapper import *
from viewer import util
from synchronization import sync
from convert import convert
from util import Config, readRecdatFile
from synchronization import usb
import analysis

parser = argparse.ArgumentParser(description='Record and synchronize rgb-d video using realsense cameras')

parser.add_argument('action',
                       metavar='action',
                       type=str,
                       choices=['record', 'sync', 'calibrate', 'convert', 'view'],
                       help='Possible actions: calibrate, convert, record, sync, view')

parser.add_argument('-rd',
                       '--recdat',
                       metavar='recdat',
                       type=str,
                       help='the path to the .recdat file that requires synchronization')

parser.add_argument(
  '--outputVideo',
  metavar="outputVideo",
  type=bool,
  help="Whether to output video or not",
  default=False
)

parser.add_argument(
  '--compress',
  metavar="compress",
  type=bool,
  help="Whether to compress output or not",
  default=True
)

parser.add_argument(
  '--recPath',
  metavar="recordingPath",
  type=str,
  help="The path to the directory the recording was output to"
)

parser.add_argument(
  "-disablesync",
  action="store_true",
  help="Disable time synchronization"
)

parser.add_argument(
    "--outputFolder",
    metavar='outputFolder',
    type=str,
    help='the path to the directory in which the recording folder will be created')

parser.add_argument(
  "--pname",
  metavar='name',
  type=str,
  help='the desired name of the recording')

# Execute the parse_args() method
args = parser.parse_args()

action = args.action
config = Config("config.json")

if action == 'record':
  if args.outputFolder is not None and args.pname is not None:
    outputFolder = args.outputFolder
    name = args.pname

    if not os.path.isdir(outputFolder):
      parser.error("The path specified does not exist.")

    fullPath = os.path.join(outputFolder, name)
    if os.path.isdir(fullPath):
      parser.error("Output directory already exists.")

    cameras = py_ku_ccal.CameraDiscoverer.find()
    if not cameras.count():
      print("Error: No cameras connected!")
      exit()

    recorder = None
    cameras.start()
    try:
      print("{} cameras connected".format(cameras.count()))

      recorder = py_ku_ccal.FileRecorder(cameras, fullPath)
      recorder.record()
      if not args.disablesync:
        numFlashes = config.get("calibration.time.num_flashes", 10)
        flashFrequency = config.get("calibration.time.flash_frequency", 3)
        baudRate = config.get("calibration.time.baud_rate", 9600)

        print("Now doing time calibration with {} flashes and a flash every {} seconds, ETA: {}s".format(numFlashes, flashFrequency, numFlashes*flashFrequency))
        usb.startFlash(baudRate, numFlashes, flashFrequency)
        print("Time calibration done!")

      cmd = ""
      print("Controls:")
      print("-"*100)
      print("Pause: Press 'p' and enter")
      print("Resume: Press 'p' and enter")
      print("Stop: Press 'e' and enter")
      print("-"*100)

      isPaused = False

      while True:
        cmd = input().rstrip()
        if cmd == "e":
          print("Stopped!")
          break
        if cmd == "p":
          if isPaused:
            recorder.record()
            isPaused = False
            print("Resumed!")
          else:
            recorder.pause()
            print("Paused!")
            isPaused = True
        else:
          print("Unknown command: {}".format(cmd))

    
    except Exception as e:
      print("Error: Exception: {} occured during recording, stopping...".format(e))

    finally:
      if recorder is not None:
        stats = recorder.stop()
        print("-"*50)
        print("| Recording stats")
        print("-"*50)
        i = 0
        for stat in stats:
          print("Camera: {}".format(i))
          print("Frames recorded: {}".format(stat.framesRecorded))
          print("Frames dropped: {}".format(stat.framesDropped))
          print("Bytes written: {:.2}GB".format(stat.bytesWritten/10.0**9))
          print("Duration {:.2}s".format(stat.recordingDuration/10.0**6))
          i += 1
          print("-"*50)

      cameras.stop()
  else:
    parser.error("record requires --outputFolder and --name.")

elif action == 'sync':
  if args.recPath is not None:
    originFolder = args.recPath
    recdata = readRecdatFile(originFolder + "/recordingInfo.recdat")
    tempFolder = originFolder + "_tmp"
    os.mkdir(tempFolder)
    filepaths = [elem[1] for elem in recdata]
    referenceCamera = filepaths[0] # First file chosen as reference camera
    remainingCameras = filepaths[1:] # Remaining cameras need alignment

    config = Config("config.json")
    flashFrequency = config.get("calibration.time.flash_frequency", 5)

    sync.alignTimestamps(referenceCamera, remainingCameras, 30 * flashFrequency, tempFolder)

    newRecdatFile = os.path.join(tempFolder, "recordingInfo.recdat")
    print('newRecdatFile: ', newRecdatFile)
    newRecdat = open(newRecdatFile, "w")
    for datafile in recdata:
      newRecdatContent = datafile[0] + "," + datafile[0] + ".dat\n"
      newRecdat.write(newRecdatContent)

    os.rename(originFolder, originFolder + "_old")
    os.rename(tempFolder, originFolder)

  else:
    parser.error("sync requires --recPath.")

elif action == 'calibrate':

  if args.recPath is None:
    parser.error("calibrate requires --recPath")
  else:
    analysis.run(args.recPath, config)

elif action == 'view':
  exec(open("tools/viewer/viewer.py").read())

elif action == 'convert':
  if args.recPath is not None:
    convert(args.recPath, outputVideo=args.outputVideo, compress=args.compress)

  else:
    parser.error("convert requires --recPath")


