import sys
import os

sys.path.append("../lib/")
from calview import Client
import argparse

def checkPath(path):
  if type(path) != str:
    raise argparse.ArgumentTypeError("Output folder must be a string")

  if not os.path.exists(path) or not os.path.isdir(path):
    raise argparse.ArgumentTypeError("Path: {} does not exist or is not a directory".format(path))

  return path


parser = argparse.ArgumentParser(description="Convert a recording to a sequence of images of the calibration visualizer. You must have a calibration_viewer_server running to use this tool.")
parser.add_argument("-port",
  type=int,
  default=8098
)

parser.add_argument("-host",
  type=str,
  default="localhost"
)

parser.add_argument("-outputFolder",
  "-o",
  metavar="output folder",
  type=checkPath,
  action="store",
  required=True
)

parser.add_argument("-yaw",
  "-y",
  metavar="yaw",
  type=float,
  action="store",
  default=0.0
)

parser.add_argument("-pitch",
  "-p",
  metavar="pitch",
  type=float,
  action="store",
  default=0.0
)

parser.add_argument("-tx",
  metavar="translatex",
  type=float,
  action="store",
  default=0.0
)

parser.add_argument("-ty",
  metavar="translatey",
  type=float,
  action="store",
  default=0.0
)

parser.add_argument("-tz",
  metavar="translatez",
  type=float,
  action="store",
  default=1.0
)

args = parser.parse_args()


c = Client(args.host, args.port)
c.connect()
try:
  res, state = c.getState()
  if not res:
    raise RuntimeError("Failed to get state")

  numFrames = state["numFrames"]
  curFrame = state["curFrame"]
  c.setYaw(args.yaw)
  c.setPitch(args.pitch)
  c.setTranslation([args.tx, args.ty, args.tz])

  for i in range(curFrame, numFrames):
    c.saveCurrentFrame(args.outputFolder + "/{}.png".format(i))
    if i+1 != numFrames:
      c.nextFrame()


finally:
  c.disconnect()
