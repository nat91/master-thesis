import cv2
import glob
import natsort
import argparse


parser = argparse.ArgumentParser(description="Converts a directory of frames named by their number, eg. 0.png, 1.png etc. into a avi movie.")
parser.add_argument("-imageFolder",
  "-i",
  type=str,
  required=True
)
parser.add_argument("-fps",
  "-f",
  type=int,
  default=15
)

parser.add_argument("-outputFile",
  "-o",
  type=str,
  required=True
)

args = parser.parse_args()

folder = args.imageFolder + "/*"
fps = args.fps
outputVideo = args.outputFile


if outputVideo.find(".avi") == -1:
  outputVideo += ".avi"

files = [f for f in glob.glob(folder)]
files = natsort.natsorted(files)


im0 = cv2.imread(files[0])
(h,w, _) = im0.shape

out = cv2.VideoWriter(outputVideo ,cv2.VideoWriter_fourcc('M','J','P','G'), fps, (w,h), True)


for f in files:
    img = cv2.imread(f)
    out.write(img)

out.release()

