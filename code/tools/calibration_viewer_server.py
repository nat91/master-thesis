import sys
sys.path.append("../lib/")
import os
import argparse
from calview import serverStart


parser = argparse.ArgumentParser(description="Starts a calibration visualizer server for a given calibrated recording")
parser.add_argument("-port",
  type=int,
  default=8098
)

parser.add_argument("-host",
  type=str,
  default="localhost"
)

parser.add_argument("-width",
  type=int,
  default=1280
)

parser.add_argument("-height",
  type=int,
  default=720
)
parser.add_argument("-recordingFolder",
  "-r",
  metavar="recordingFolder",
  type=str,
  action="store",
  required=True
)

args = parser.parse_args()

serverStart(args.host, args.port, args.recordingFolder, args.width, args.height)
