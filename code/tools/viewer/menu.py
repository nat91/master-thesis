from PyQt5.QtWidgets import QApplication, QLabel, QWidget, QVBoxLayout, QMainWindow, QAction, QFileDialog
import sys
import util

class AppWindow(QMainWindow):

    def __init__(self, app):
        super().__init__()

        self.statusbar = self.statusBar()
        self.statusbar.showMessage("Ready")

        self.setup_menu()

        screen_res = app.desktop().screenGeometry()
        (w, h) = screen_res.width(), screen_res.height()

        self.setGeometry(w//4, h//4, w//2, h//2)
        self.setWindowTitle("Realsense Data Viewer")
        self.show()

    def setup_menu(self):
        mainMenu = self.menuBar()
        mainMenu.setNativeMenuBar(False)
        files = mainMenu.addMenu("File")
        
        openFileAction = QAction("Open", self)
        openFileAction.setShortcut("Ctrl+O")
        openFileAction.triggered.connect(self.open_file)
        files.addAction(openFileAction)
        mainMenu.show()


    # return a list of camera filenames
    def open_file(self):
        fileName, _ = QFileDialog.getOpenFileName(self, "Select a camera output file", filter="*.recdat")

        if fileName:
          print(util.get_camera_files_from_file(fileName))
        else:
          return False          




if __name__ == "__main__":
    app = QApplication(sys.argv)
    gui = AppWindow(app)
    sys.exit(app.exec_())