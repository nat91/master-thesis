from pathlib import Path
import csv

# given a path to a camera output file eg. "test.recdat"
# it returns a list of cameras and their log files
def get_camera_files_from_file(file):
  path = Path(file).absolute()
  cameras = []
  with open(file, "r") as f:
    reader = csv.reader(f, delimiter=",")
    for row in reader:
      cameras.append((row[0], path.parent.as_posix() + '/' + row[1]))
  
  return cameras

