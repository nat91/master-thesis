import random
import sys

import matplotlib.pyplot as plt
import numpy as np
from matplotlib.backends.backend_qt5agg import \
  FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
from PyQt5.QtCore import pyqtSlot
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import (QAction, QApplication, QDialog, QFileDialog,
               QGroupBox, QHBoxLayout, QInputDialog, QLabel,
               QMainWindow, QMenu, QMessageBox, QPushButton,
               QSizePolicy, QVBoxLayout, QWidget)

from . import util

sys.path.append('../')
from reader.reader import FileReader


class App(QMainWindow):

  def __init__(self):
    super().__init__()
    self.left = 10
    self.top = 10
    self.title = 'PyQt5 matplotlib example - pythonspot.com'
    self.width = 1920
    self.height = 1080

    self.setup_menu()
    self.statusbar = self.statusBar()
    self.statusbar.showMessage("Ready")

    screen_res = app.desktop().screenGeometry()
    (w, h) = screen_res.width(), screen_res.height()

    self.setGeometry(w//4, h//4, w//2, h//2)
    self.setWindowTitle("Realsense Data Viewer")
    self.show()

  def setup_menu(self):
    mainMenu = self.menuBar()
    mainMenu.setNativeMenuBar(False)
    files = mainMenu.addMenu("File")
    
    openFileAction = QAction("Open", self)
    openFileAction.setShortcut("Ctrl+O")
    openFileAction.triggered.connect(self.open_file)
    files.addAction(openFileAction)
    mainMenu.show()

  # return a list of camera filenames
  def open_file(self):
    fileName, _ = QFileDialog.getOpenFileName(self, "Select a camera output file", filter="*.recdat")

    if fileName:
      files = util.get_camera_files_from_file(fileName)
      self.initUI(files)

  def initUI(self, files):        
    form_widget = ContainerWidget(self, files) #First file is central widget
    self.setCentralWidget(form_widget)

    self.setWindowTitle(self.title)
    self.setGeometry(self.left, self.top, self.width, self.height)
    
    self.show()

class ContainerWidget(QWidget):

  def __init__(self, parent, files):
    super(ContainerWidget, self).__init__(parent)
    mainLayout = QVBoxLayout()
    # Add remaining files
    for i in range(len(files)):
      form_widget = FormWidget(self, files[i][1]) #First file is central widget
      mainLayout.addWidget(form_widget) 
    self.setLayout(mainLayout)


class FormWidget(QWidget):

  def __init__(self, parent, file):        
    super(FormWidget, self).__init__(parent)
    self.layout = QHBoxLayout(self)

    self.cp1 = ColorPlot(self, file)
    self.layout.addWidget(self.cp1)

    self.cp2 = DepthPlot(self, file)
    self.layout.addWidget(self.cp2)

    self.setLayout(self.layout)

class ColorPlot(QWidget):
  def __init__(self, parent, file):
    super(ColorPlot, self).__init__(parent)
    self.layout = QVBoxLayout(self)
    self.camera = ColorCanvas(file, self, width=10, height=8)
    self.layout.addWidget(self.camera)
    self.btns = Controls(self)
    self.layout.addWidget(self.btns)
    self.setLayout(self.layout)

class DepthPlot(QWidget):
  def __init__(self, parent, file):
    super(DepthPlot, self).__init__(parent)
    self.layout = QVBoxLayout(self)
    self.camera = DepthCanvas(file, self, width=10, height=8)
    self.layout.addWidget(self.camera)
    self.btns = Controls(self)
    self.layout.addWidget(self.btns)
    self.setLayout(self.layout)


class Controls(QWidget):

  def __init__(self, parent):
    super(Controls, self).__init__(parent)
    self.layout = QHBoxLayout(self)

    self.prevBtn = QPushButton("Prev")
    self.prevBtn.clicked.connect(lambda: self.prev_click(parent))
    self.layout.addWidget(self.prevBtn)
    

    self.nextBtn = QPushButton("Next")
    self.nextBtn.clicked.connect(lambda: self.next_click(parent))
    self.layout.addWidget(self.nextBtn)

    self.goToBtn = QPushButton("Skip")
    self.goToBtn.clicked.connect(lambda: self.skip_click(parent))
    self.layout.addWidget(self.goToBtn)

    self.setLayout(self.layout)

  @pyqtSlot()
  def prev_click(self, parent):
    parent.camera.prev()
    parent.camera.draw()

  def next_click(self, parent):
    parent.camera.next()
    parent.camera.draw()

  def skip_click(self, parent):
    i, okPressed = QInputDialog.getInt(self, "Get integer","Frames to skip:", 10, -1000, 1000, 1)
    if okPressed:
      parent.camera.skip_frames(i)



class ColorCanvas(FigureCanvas):

  def __init__(self, file, parent=None, width=5, height=4, dpi=100):
    self.filehandler = open(file, "rb")
    fig = Figure(figsize=(width, height), dpi=dpi)
    self.axes = fig.add_subplot(111)

    FigureCanvas.__init__(self, fig)
    self.setParent(parent)

    FigureCanvas.setSizePolicy(self,
        QSizePolicy.Expanding,
        QSizePolicy.Expanding)
    FigureCanvas.updateGeometry(self)
    self.plot()


  def plot(self):
    self.fileReader = FileReader(10, self.filehandler)
    (metaData, colorFrame, _) = self.fileReader.currentFrame.frameData()
    self.ax = self.figure.add_subplot(111)
    self.ax.text(0,0,str(metaData.rgbFrameNo) + " " + str(metaData.rgbtimestamp))
    self.ax.imshow(colorFrame)
    self.ax.set_title('PyQt Matplotlib Example')
    self.draw()

  def next(self):
    (metaData, nextFrame, _) = self.fileReader.nextFrame().frameData()
    self.ax.clear()
    self.ax = self.figure.add_subplot(111)
    self.ax.text(0,0,str(metaData.rgbFrameNo) + " " + str(metaData.rgbtimestamp))
    self.ax.imshow(nextFrame)
    self.ax.set_title('PyQt Matplotlib Example')
    self.draw()

  def prev(self):
    (metaData, prevFrame, _) = self.fileReader.prevFrame().frameData()
    self.ax.clear()
    self.ax = self.figure.add_subplot(111)
    self.ax.text(0,0,str(metaData.rgbFrameNo) + " " + str(metaData.rgbtimestamp))
    self.ax.imshow(prevFrame)
    self.ax.set_title('PyQt Matplotlib Example')
    self.draw()

  def skip_frames(self, skipBy):
    (metaData, newFrame, _) = self.fileReader.skipFrames(skipBy).frameData()
    self.ax.clear()
    self.ax = self.figure.add_subplot(111)
    self.ax.text(0,0,str(metaData.rgbFrameNo) + " " + str(metaData.rgbtimestamp))
    self.ax.imshow(newFrame)
    self.ax.set_title('PyQt Matplotlib Example')
    self.draw()

class DepthCanvas(FigureCanvas):

  def __init__(self, file, parent=None, width=5, height=4, dpi=100):
    self.filehandler = open(file, "rb")
    fig = Figure(figsize=(width, height), dpi=dpi)
    self.axes = fig.add_subplot(111)

    FigureCanvas.__init__(self, fig)
    self.setParent(parent)

    FigureCanvas.setSizePolicy(self,
        QSizePolicy.Expanding,
        QSizePolicy.Expanding)
    FigureCanvas.updateGeometry(self)
    self.plot()
  
  def normalizeDepth(self, depthData):
    threshold = 3
    depthData = self.fileReader.header().dScale * depthData
    indexes = depthData > threshold
    depthData[indexes] = 0
    return depthData



  def plot(self):
    self.fileReader = FileReader(10, self.filehandler)
    (metaData, _, depthFrame) = self.fileReader.currentFrame.frameData()
    depthFrame = self.normalizeDepth(depthFrame)
    self.ax = self.figure.add_subplot(111)
    self.ax.text(0,0,str(metaData.depthFrameNo) + " " + str(metaData.depthtimestamp))
    self.ax.imshow(depthFrame,  cmap='gray')
    self.ax.set_title('PyQt Matplotlib Example')
    self.draw()

  def next(self):
    (metaData, _, nextFrame) = self.fileReader.nextFrame().frameData()
    nextFrame = self.normalizeDepth(nextFrame)
    self.ax.clear()
    self.ax = self.figure.add_subplot(111)
    self.ax.text(0,0,str(metaData.depthFrameNo) + " " + str(metaData.depthtimestamp))
    self.ax.imshow(nextFrame,  cmap='gray')
    self.ax.set_title('PyQt Matplotlib Example')
    self.draw()

  def prev(self):
    (metaData, _, prevFrame) = self.fileReader.prevFrame().frameData()
    prevFrame = self.normalizeDepth(prevFrame)
    indexes = prevFrame > 3
    prevFrame[indexes] = 0
    self.ax.clear()
    self.ax = self.figure.add_subplot(111)
    self.ax.text(0,0,str(metaData.depthFrameNo) + " " + str(metaData.depthtimestamp))
    self.ax.imshow(prevFrame,  cmap='gray')
    self.ax.set_title('PyQt Matplotlib Example')
    self.draw()

  def skip_frames(self, skibBy):
    (metaData, _, newFrame) = self.fileReader.skipFrames(skibBy).frameData()
    newFrame = self.normalizeDepth(newFrame)
    self.ax.clear()
    self.ax = self.figure.add_subplot(111)
    self.ax.text(0,0,str(metaData.depthFrameNo) + " " + str(metaData.depthtimestamp))
    self.ax.imshow(newFrame, cmap='gray')
    self.ax.set_title('PyQt Matplotlib Example')
    self.draw()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = App()
    sys.exit(app.exec_())
