import libcbgen

# distinguishable numbers: 1, 4, 5, 8
libcbgen.gen_print([1], [1, 4], sizeInCm=10, dim=5)