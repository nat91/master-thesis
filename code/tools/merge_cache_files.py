import pickle 
import sys 
import os
sys.path.append("../lib")


if len(sys.argv) < 4:
  print("Usage: python3 merge_cache_files.py inputFile1 inputFile2 outputFile")

in1 = sys.argv[1]
in2 = sys.argv[2]
outFile = sys.argv[3]

if not os.path.exists(in1):
  print("File {} does not exist".format(in1))
  exit()

if not os.path.exists(in2):
  print("File {} does not exist".format(in2))
  exit()

f1 = open(in1, "rb")
f2 = open(in2, "rb")

with open(outFile, "wb") as fOut:
  frames1, cameras1 = pickle.load(f1)
  frames2, cameras2 = pickle.load(f2)

  pickle.dump((frames1+frames2, cameras1+cameras2), fOut)