import sys
sys.path.append("../lib")
import os
from rswrapper import py_ku_ccal
import numpy as np
import matplotlib.pyplot as plt
import pickle
import skimage
from util import alignDepthToColor, Camera, Frame, Intrinsics, Extrinsics
from helpers import autoROI

def printCommands():
  print("Commands:")
  print("-"*50)
  print("exit - type exit to stop the loop and save the file")
  print("c - capture the current image, you will be prompted to save or discard after")

if len(sys.argv) < 2:
  print("Usage: python3 artificial_dataset.py outputFile [autoRoi]")
  exit()

fOut = sys.argv[1] + ".data"
if os.path.exists(fOut):
  print("File {} already exists!".format(fOut))
  exit()

cameras = py_ku_ccal.CameraDiscoverer.find()
if not cameras.count():
  print("No cameras connected!")
  exit()

print("{} cameras connected".format(cameras.count()))
cameras.start()
cam = cameras[0]

print("Settling auto exposure")
for i in range(60):
  cam.capture()

print("Auto exposure settled")
cmd = ""

Krgb = cam.getRGBIntrinsics().getMatrix()
Kdepth = cam.getDepthIntrinsics().getMatrix()
rgbToColor = cam.getDepthToColorExtrinsics().getTransform()
useAutoROI = True if len(sys.argv) == 3 else False


frames = []
cameras = []

printCommands()
while cmd != "exit":
  cmd = input("Enter command: ")

  if cmd == "c":
    if useAutoROI:
      frame = cam.capture()
      frameCopy = skimage.img_as_float(np.array(frame))    
      minX, maxX, minY, maxY = autoROI(frameCopy)
      cam.setROI(minX, minY, maxX, maxY)
      print(minX, maxX, minY, maxY)

    frame = cam.capture()
    frameCopy = np.array(frame)
    frame.enableDepthBuffer()

    depthCopy = np.array(frame)*cam.getDepthScale()

    depthCopy = alignDepthToColor(Kdepth, Krgb, (1080, 1920), rgbToColor, depthCopy)
    # threshold points
    depthCopy[depthCopy >= 3.0] = 0
    
    plt.subplot(1, 2, 1)
    plt.imshow(frameCopy)
    plt.subplot(1, 2, 2)
    plt.imshow(depthCopy, cmap="gray")
    plt.colorbar()
    plt.show()

    choice = input("Keep (y/n): ")
    if choice == "y":
      print("frame saved")
      c = Camera(frameCopy.shape, depthCopy.shape)
      c.serialNo = cam.getSerialNumber()
      c.intrinsics = Intrinsics(Krgb[0,0], Krgb[1,1], Krgb[0,2], Krgb[1,2], [])
      c.depthIntrinsics = Intrinsics(Kdepth[0,0], Kdepth[1,1], Kdepth[0,2], Kdepth[1,2], [])

      f = Frame(None, frameCopy, depthCopy)
      cameras.append(c)
      frames.append(f)
      #imgs.append((frameCopy, depthCopy))
    else:
      print("frame discarded")


with open(fOut, "wb") as f:
  pickle.dump((frames, cameras), f)
