# Installing Dependencies

## Installing LibRealsense
Follow the instructions at: https://github.com/IntelRealSense/librealsense/blob/master/doc/distribution_linux.md

Make sure to run: 
`modinfo uvcvideo | grep "version:"` 
at the end to check if it contains the string "realsense", otherwise you will not be able to retrieve metadata from the cameras and time calibration will not work.

If you get an error like "No UVC frame data available" and the above does contain the string "realsense", then you are running an incompatible Linux kernel. You can change kernels with grub during boot by going to advanced settings for ubuntu, and then boot with a compatible kernel - eg. 4.18.

## Other C++ dependencies
You need boost installed, on Ubuntu run:

`sudo apt-get install libboost-all-dev`

You need `pybind11` headers in the *lib* directory in order to compile the Python bindings for the `ku::ccal` library. They are provided in the repository, if you need to update it you can get it here: https://github.com/pybind/pybind11. After downloading the repository, extract the *pybind11* directory into the *lib* directory.

## Python Dependencies
You need the Python OpenCv package, numpy, torch, open3d and matplotlib. You can install them all via pip.
Or run:

`pip3 install -r pip_install.txt`

Install PyQt5 via apt-get:

`sudo apt-get install pyqt5-dev`

# Compiling the datalogger
On Ubuntu you simply go into the realsense datalogger directory and run `make`.

# Running the dataloggger
Plug your cameras in and call

`./realsense-datalogger "outDirectory"`

Where "outDirectory" is the folder you want files to be written to. It will produce a data file pr. camera named *cameraSerialNumber*.dat containing the recording data for the given camera and a *.recdat* telling what cameras the recordings in the directory are for.