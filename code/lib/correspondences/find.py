import numpy as np 
from features import FeatureDetectorCollection
from features import LabeledFeatureMatcher
from util import deprojectPixelToPoint, zInterp

class Finder:
  """
  Abstraction for finding correspondences in image and range data, from a pair of cameras.
  Initialize one for each pair of camera you have.
  """

  def __init__(self, detectors):
    """
    Initializer for the correspoondence finder

    Args:
      detectors (FeatureDetectorCollection): Collection of feature detectors this finder should apply to the images
    """
    # if not isinstance(detectors, FeatureDetectorCollection):
    #   raise RuntimeError("Arg 'detectors' must be an instance of FeatureDetectorCollection")

    self.detectors = detectors


  def c2D(self, img1, K1, img2, K2):
    """
    Given two images, image correspondences are established. This Output of this method is suitable for eg. traditional camera calibration.

    Args:
      img1 (np.array): Input image from first camera
      K1 (np.array): Camera intrinsics matrix for camera 1
      img2 (np.array): Input image from second camera
      K2 (np.array): Camera intrinsics matrix for camera 2
    

    Returns:
      (np.array, list): The array is a Nx4 array where each row is a correspondence pair [x1, y1, x2, y2]. The list contains the labels of each correspondence.
    """
    f1 = self.detectors.getFeatures(img1, None, K1)
    f2 = self.detectors.getFeatures(img2, None, K2)
    
    c2d = None
    labels2d = []

    for k in f1.keys():
      if k not in f2:
        continue 
      
      if f1[k]["matcher"] != f2[k]["matcher"]:
        # incompatible descriptor matcher
        continue

      matcher = f1[k]["matcher"]

      cor, labels = matcher.match(f1[k]["features"], f1[k]["descriptors"], f2[k]["features"], f2[k]["descriptors"])
      if cor.shape[1] == 4:
        if c2d is None:
          c2d = cor
          labels2d = labels 
        else:
          c2d = np.vstack((c2d, cor))
          labels2d += labels


    return c2d, labels2d
    

  def c3DF(self, img1, depth1, K1, f1, img2, depth2, K2, f2):
    """
    Same as c3D but when features have already been found.
    """

    c3ds = None
    labels3d = []

    for k in f1.keys():
      if k not in f2:
        continue 
      
      if f1[k]["matcher"] != f2[k]["matcher"]:
        # incompatible descriptor matcher
        continue

      matcher = f1[k]["matcher"]

      cor, labels = matcher.match(f1[k]["features"], f1[k]["descriptors"], f2[k]["features"], f2[k]["descriptors"])

      if cor.shape[0] == 0:
        # no correspondences for this feature type, continue.
        continue

      # if cor is 2d, make it 3d.
      if cor.shape[1] == 4:
        z1 = zInterp(cor[:, 0:2], depth1)
        z2 = zInterp(cor[:, 2:4], depth2)

        validIdx = np.where((z1 > 0) & (z2 > 0))
        if validIdx[0].shape[0] == 0:
          continue

        cor3d = np.zeros((validIdx[0].shape[0], 6))
        cor3d[:, 0:2] = cor[validIdx, 0:2]
        cor3d[:, 2] =  z1[validIdx]
        cor3d[:, 3:5] = cor[validIdx, 2:4]
        cor3d[:, 5] =  z2[validIdx]
        # reproject to 3d
        cor3d[:, 0:3] = deprojectPixelToPoint(K1, cor3d[:, 0:3].T, cor3d[:,2].T).T
        cor3d[:, 3:6] = deprojectPixelToPoint(K2, cor3d[:, 3:6].T, cor3d[:,5].T).T
        cor = cor3d
      
      if c3ds is None:
        c3ds = cor
        labels3d = labels 
      else:
        c3ds = np.vstack((c3ds, cor))
        labels3d += labels


    # return 3D correspondences + labels
    return c3ds, labels3d

      
  def c3D(self, img1, depth1, K1, img2, depth2, K2):
    """
    Given two images and two depth frames, correspondences are established. The output of this method is suitable for eg. point cloud alignment.
    3D correspondences are returned in both  camera world coordinates and image coordinates.
    The depth frames must be aligned with the RGB images, see the "alignDepthToColor" in align.py
    in the util module.


    Args:
      img1 (np.array): Input image from first camera
      depth1 (np.array): Aligned depth frame from first camera
      K1 (np.array): Camera intrinsics matrix for camera 1
      img2 (np.array): Input image from second camera
      depth2 (np.array): Aligned depth frame from second camera
      K2 (np.array): Camera intrinsics matrix for camera 2

    Returns:
      (np.array, np.array, list): The first array is a Nx6 array where each row is a correspondence pair [x1, y1, z1, x2, y2, z2] in camera world coordinates.
      The second is a Nx4 array where each row is a correspondence pair [x1, y1, x2, y2]. The third value is the labels of each correspondence.
    """
    f1 = self.detectors.getFeatures(img1, depth1, K1)
    f2 = self.detectors.getFeatures(img2, depth2, K2)

    return self.c3DF(img1, depth1, K1, f1, img2, depth2, K2, f2)
