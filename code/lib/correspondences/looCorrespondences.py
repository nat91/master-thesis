import numpy as np
import features
from . import Finder

class LooCorrespondences:
  """
  Calculate correspondences, but leave a number of calibration objects out. This class is used to conduct experiments.
  """

  def __init__(self, det, leaveOut):
    """
    Args:
      det (features.FeatureDetectorCollection): Feature detectors to use for finding features in each image.

      leaveOut (list): List of strings. If a feature descriptor matches the start of any string in this list, it will be excluded.
    """

    if not isinstance(leaveOut, list):
      raise RuntimeError("Parameter 'leaveOut' must be a list of strings")
    
    if not isinstance(det, features.FeatureDetectorCollection):
      raise RuntimeError("Parameter 'det' must be a FeatureDetectorCollection instance")
    
    self.leaveOut = leaveOut
    self.det = det

  def find(self, im1, depth1, K1, im2, depth2, K2):
    """
    Leave one out correspondences. Retrieves corresponding points for image 1 and image 2, but leaves specified calibration objects out and returns them separately.

    Args:
      im1 (np.array): Image 1
      depth1 (np.array): Depth map for image 1
      K1 (np.array): Intrinsics matrix for camera 1
      im2 (np.array): Image 2
      depth2 (np.array): Depth map for image 2
      K2 (np.array): Intrinsics matrix for camera 2

    Returns:
      (np.array, np.array, dict, dict): Returns a 4-tuple where the first and second component is an array of correspondences of dimension Nx6 where each row is a correspondence [x0, y0, z0, x1, y1, z1]. The first component is correspondences left in and the second the ones that were left out. The third and fourth component is the feature maps of the features that were left out of the calibration.     
    """
    
    f1 = self.det.getFeatures(im1, depth1, K1)
    f2 = self.det.getFeatures(im2, depth2, K2)

    f1a, f1b = features.partition(f1, self.leaveOut)
    f2a, f2b = features.partition(f2, self.leaveOut)
    
    F = Finder(self.det)
    c3da, _ = F.c3DF(im1, depth1, K1, f1a, im2, depth2, K2, f2a)
    c3db, _ = F.c3DF(im1, depth1, K1, f1b, im2, depth2, K2, f2b)

    return c3da, c3db, f1b, f2b