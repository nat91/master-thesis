import matplotlib.pyplot as plt 
import numpy as np

def plot3D(C):

  fig = plt.figure()
  ax = fig.gca(projection="3d")
  # we flip the x-axis sign everywhere because it is inverted in matplotlib
  ax.plot(-C[:,0], C[:,1], C[:,2], "r.", label="Img 1")
  ax.plot(-C[:,3], C[:,4], C[:,5], "b.", label="Img 2")

  # draw lines
  for k in range(C.shape[0]):
    ax.plot([-C[k,0], -C[k,3]], [C[k,1], C[k,4]], [C[k,2], C[k,5]], "g")

  ax.set_xlabel('X axis')
  ax.set_ylabel('Y axis')
  ax.set_zlabel('Z axis')
  ax.view_init(elev=0., azim=0)
  plt.legend()

  plt.show()

def plot2D(im1, im2, C):
  """
  This function plots 2D correspondences in image coordinates.

  Args:
    im1 (np.array): The image from the first camera
    im2 (np.array): The image from the second camera
    C (np.array): Nx4 array where each row is a correspondence of the form [x1, y1, x2, y2].
     The correspondence points must be in image coordinates.
  
  Returns:
    None
  """

  h1, w1,_ = im1.shape 
  h2, w2,_ = im2.shape

  output = np.zeros((np.maximum(h1, h2), w1+w2, 3), "int")
  output[0:h1, 0:w1] = im1 
  output[0:h2, w1:] = im2 


  plt.title("Correspondences for image pair")
  plt.imshow(output)
  for k in range(C.shape[0]):
    if C.shape[1] == 4:
      c1 = C[k,0:2]
      c2 = C[k,2:]
    elif C.shape[1] == 6:
      c1 = C[k,0:2]
      c2 = C[k,3:5]

    plt.plot([c1[0], c2[0]+w1], [c1[1], c2[1]])
  
  plt.show()
