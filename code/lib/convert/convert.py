import sys
sys.path.append('../')

import cv2
import csv
import os
import numpy as np

from reader.reader import FileReader
import tables # pytables
from util import readRecdatFile
from alive_progress import alive_bar

## pytables table descriptions
class Timestamp(tables.IsDescription):
  depthTimeStamp = tables.UInt64Col(dflt=0, pos=1)
  rgbTimeStamp = tables.UInt64Col(dflt=0, pos=2)
  isMissing = tables.BoolCol(dflt=False, pos=3)

class CameraMetadata(tables.IsDescription):
  serialNumber = tables.StringCol(100)
  colorIntrinsics = tables.Float64Col(shape=(3,3), dflt=np.zeros((3,3)), pos=0)
  depthIntrinsics = tables.Float64Col(shape=(3,3), dflt=np.zeros((3,3)), pos=1)
  depthToColorExtrinsics = tables.Float64Col(shape=(4,4), dflt=np.zeros((4,4)), pos=2)

  colorWidth = tables.UInt16Col(dflt=0, pos=3)
  colorHeight = tables.UInt16Col(dflt=0, pos=4)
  depthWidth = tables.UInt16Col(dflt=0, pos=5)
  depthHeight = tables.UInt16Col(dflt=0, pos=6)
  isPrimary = tables.BoolCol(dflt=False)
  depthScale = tables.Float64Col(dflt=0)



def convert(recordingPath, outputVideo=False, compress=True):
  recordingPath = recordingPath.rstrip("/")

  if not os.path.exists(recordingPath):
    raise RuntimeError("Path to recording {} does not exist!".format(recordingPath))

  cameras = readRecdatFile(recordingPath+"/recordingInfo.recdat")

  if not cameras:
    raise RuntimeError("Recdat file {} does not exist".format(recDatFile))
  
  N = len(cameras)
  i = 1

  ## declare HDF5 table for depth data and timestamps
  f = tables.open_file(recordingPath + "/converted.h5" , "w")  
  cMetadata = f.create_table(f.root, 'cameraMetadata', CameraMetadata)
  # create group for recordings
  recordingsGroup = f.create_group("/", 'recordings', 'Recordings for each camera'.encode("ascii"))

  for (serialNo, recFile) in cameras:
    print("Converting data for camera {}/{}".format(i, N))
    # create group for the recording
    groupRec = f.create_group(recordingsGroup, "cam_"+serialNo, "Recording for camera {}".format(serialNo).encode("ascii"))
    # Write recording to hdf5 file and/or output video for this camera
    convertRecording(recFile, serialNo, recordingPath, f, i, N, outputVideo=outputVideo, compress=compress)
    i += 1

  # close HDF5 file and flush all buffers
  f.close()


def convertRecording(inputFile, serialNo, outDir, fTbl, i, N, outputVideo=False, compress=True):

  handler = open(inputFile, 'rb')
  reader = FileReader(50, handler)
  
  # Default resolutions of the frame are obtained.The default resolutions are system dependent.
  # We convert the resolutions from float to integer.
  frame_widthC = reader.fileMetaData.resolutionRGBW
  frame_heightC = reader.fileMetaData.resolutionRGBH
  frame_widthD = reader.fileMetaData.resolutionDepthW
  frame_heightD = reader.fileMetaData.resolutionDepthH
  fps = reader.fileMetaData.fps
  lastFrameNo = reader.currentFrame.frameMetaData.rgbFrameNo
  numberOfFrames = reader.fileMetaData.numberOfFrames

  # write camera meta data
  row = fTbl.root.cameraMetadata.row
  row["colorIntrinsics"] = reader.fileMetaData.rgbIntrinsics.getMatrix()
  row["depthIntrinsics"] = reader.fileMetaData.depthIntrinsics.getMatrix()
  row["depthToColorExtrinsics"] = reader.fileMetaData.depthToColorExtrinsics.getTransform()
  row["colorWidth"] = frame_widthC
  row["colorHeight"] = frame_heightC
  row["depthWidth"] = frame_widthD
  row["depthHeight"] = frame_heightD
  row["serialNumber"] = serialNo.encode("ascii")
  row["depthScale"] = reader.fileMetaData.dScale
  row["isPrimary"] = True if i == 0 else False # make first camera the primary one

  row.append()

  # Missing frames as black frames
  blackFrame = np.zeros((frame_heightC, frame_widthC,3), dtype="uint8")
  noDepthFrame = np.zeros((frame_heightD, frame_widthD), dtype="uint16")
  
  if outputVideo:
    # Define the codec and create VideoWriter object.The output is stored in 'outpy.avi' file.
    cFileName = inputFile[:-4] + '_RGB'
    dFileName = inputFile[:-4] + '_D'
    cVideoFile = os.path.join(outDir, os.path.split(cFileName)[-1])
    dVideoFile = os.path.join(outDir, os.path.split(dFileName)[-1])
    outC = cv2.VideoWriter(cVideoFile + '.avi',cv2.VideoWriter_fourcc('Y', 'V', '1', '2'), fps, (frame_widthC,frame_heightC))
    outD = cv2.VideoWriter(dVideoFile + '.avi',cv2.VideoWriter_fourcc('Y', 'V', '1', '2'), fps, (frame_widthD,frame_heightD))


  filters = None
  if compress:
    filters = tables.Filters(complib='zlib', complevel=5, shuffle=False)

  atomDepth = tables.Atom.from_dtype(reader.currentFrame.depthFrame.dtype)
  atomRGB = tables.Atom.from_dtype(reader.currentFrame.colorFrame.dtype)

  # create tables
  path = "/recordings/cam_{}".format(serialNo)

  depthArrayTbl = fTbl.create_earray(path, "depth", atomDepth, shape=(0, frame_heightD, frame_widthD), filters=filters)
  colorArrayTbl = fTbl.create_earray(path, "rgb", atomRGB, shape=(0, frame_heightC, frame_widthC, 3), filters=filters)
  timestampTbl = fTbl.create_table(path, "timestamps", Timestamp)


  with alive_bar(numberOfFrames, bar="blocks") as bar:
    for _ in range(numberOfFrames):
      
      bar()

      if reader.currentFrame.frameMetaData.rgbFrameNo > lastFrameNo + 1:
        if outputVideo:
          outC.write(blackFrame)
          outD.write(np.uint8(cv2.cvtColor(noDepthFrame, cv2.COLOR_GRAY2BGR)))
        
        # write empty frames
        dFrame = np.expand_dims(noDepthFrame, 0)
        depthArrayTbl.append(dFrame)

        cFrame = np.expand_dims(blackFrame, 0)
        colorArrayTbl.append(cFrame)

        # write empty time stamps (0)
        row = timestampTbl.row
        row["depthTimeStamp"] = 0
        row["rgbTimeStamp"] = 0
        row["isMissing"] = True 
        row.append()

        lastFrameNo = reader.currentFrame.frameMetaData.rgbFrameNo
      else:
        colorFrame = reader.currentFrame.colorFrame
        depthFrame = reader.currentFrame.depthFrame

        dFrame = np.expand_dims(depthFrame, 0)
        depthArrayTbl.append(dFrame)
        cFrame = np.expand_dims(colorFrame, 0)
        colorArrayTbl.append(cFrame)

        if outputVideo:
          depthFrame = np.uint8(cv2.cvtColor(depthFrame, cv2.COLOR_GRAY2BGR))
          outD.write(depthFrame)
          outC.write(colorFrame)

        row = timestampTbl.row
        row["depthTimeStamp"] = reader.currentFrame.frameMetaData.depthtimestamp
        row["rgbTimeStamp"] = reader.currentFrame.frameMetaData.rgbtimestamp
        row["isMissing"] = False
        row.append()

        lastFrameNo = reader.currentFrame.frameMetaData.rgbFrameNo
      
      reader.nextFrame()
  
  if outputVideo:
    # When everything done, release the video capture and video write objects
    outC.release()
    outD.release()
  
  handler.close()


def main(argv):
  if len(sys.argv) < 2:
     print("Missing file path argument.\n")
  elif len(sys.argv) == 2:
    input_file = sys.argv[1]
    convert(input_file)
    
  else:
    print("Too many arguments given.\n")


if __name__ == "__main__":
   main(sys.argv[1:])