import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

def toWorldSpace(side, dimensions, checkerSizeInCm, borderSizeInCm):

  res = []

  if side == "front":
    for j in range(0, dimensions[0]):
      for i in range(0, dimensions[1]):
        res.append((i*checkerSizeInCm, j*checkerSizeInCm, 0))

  if side == "right":
    # Only 1 borderSizeInCm, since origo inside checkerboard, not on border
    x = (dimensions[0])*checkerSizeInCm + borderSizeInCm
    zOffset = checkerSizeInCm + borderSizeInCm
    for j in range(0, dimensions[0]):
      for i in range(0, dimensions[1]):
        res.append((x, j*checkerSizeInCm, zOffset + i*checkerSizeInCm))

  if side == "left":
    x = - checkerSizeInCm - borderSizeInCm
    zOffset = checkerSizeInCm + borderSizeInCm
    for j in range(0, dimensions[0]):
      for i in range(0, dimensions[1]):
        res.append((x, j*checkerSizeInCm, zOffset + i*checkerSizeInCm))

  if side == "back":
    z = 2*borderSizeInCm + (dimensions[0] + 1) * checkerSizeInCm
    for j in range(0, dimensions[0]):
      for i in range(0, dimensions[1]):
        res.append((i*checkerSizeInCm, j*checkerSizeInCm, z))

  if side == "top":
    y = - checkerSizeInCm - borderSizeInCm
    zOffset = checkerSizeInCm + borderSizeInCm
    for j in range(0, dimensions[0]):
      for i in range(0, dimensions[1]):
        res.append((i*checkerSizeInCm, y, zOffset + j*checkerSizeInCm))

  return np.asarray(res)


front = toWorldSpace("front", (4,4), 1, 0.15)
back = toWorldSpace("back", (4,4), 1, 0.15)
right = toWorldSpace("right", (4,4), 1, 0.15)
left = toWorldSpace("left", (4,4), 1, 0.15)
top = toWorldSpace("top", (4,4), 1, 0.15)

data = np.concatenate((front, back, right, left, top))
print('data: ', data)

fig = plt.figure()
# Add an axes
ax = fig.add_subplot(111,projection='3d')
#ax.scatter(data[:,0], data[:,1], data[:,2], "g.")
ax.scatter(front[:,0], front[:,1], front[:,2], "r.", label="Front")
ax.scatter(back[:,0], back[:,1], back[:,2], "g.", label="Back")
ax.scatter(right[:,0], right[:,1], right[:,2], "b.", label="Right")
ax.scatter(left[:,0], left[:,1], left[:,2], "y.", label="Left")
ax.scatter(top[:,0], top[:,1], top[:,2], "p.", label="Top")
ax.set_xlabel('X axis')
ax.set_ylabel('Y axis')
ax.set_zlabel('Z axis')
plt.legend()
plt.show()