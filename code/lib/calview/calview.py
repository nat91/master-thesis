import pyglet
import pyglet.gl as gl
import numpy as np
import threading
from util import deprojectPixelToPoint
from .appState import AppState
from . import draw

# Based on https://github.com/IntelRealSense/librealsense/blob/development/wrappers/python/examples/pyglet_pointcloud_viewer.py


class CalibrationWindow:

  def __init__(self, width=1280, height=720):
    self.window = pyglet.window.Window(width, height,
      config=gl.Config(
        double_buffer=True,
        samples=8 #MSAA
      ),
      resizable=True,
      vsync=True
    )

    self.window.set_caption("Camera Calibration Visualizer")

    keys = pyglet.window.key.KeyStateHandler()
    self.window.push_handlers(keys)
    self. window.on_mouse_press = self.window.on_mouse_release = self.handle_mouse_btns
    self.window.push_handlers(self.on_key_press)

    self.state = AppState()
    self.cameras = dict()
    self.pointClouds = dict()

    self.window.on_draw = self.on_draw
    self.window.on_mouse_drag = self.on_mouse_drag
    

    self.yaw_label = pyglet.text.Label("Yaw: {:.2f}".format(self.state.yaw),
                          font_size=12,
                          x=10, y=self.window.height-20)


    self.pitch_label = pyglet.text.Label("Pitch: {:.2f}".format(self.state.pitch),
                          font_size=12,
                          x=10, y=self.window.height-40)


    self.trans_label = pyglet.text.Label("",
                          font_size=12,
                          x=10, y=self.window.height-60)
    self.trans_label.text = "Translation: x: {:.2f} y: {:.2f} z: {:.2f} ".format(self.state.translation[0], self.state.translation[1], self.state.translation[2])


    self.ymax = 0.5

    self.lock = threading.Lock()
    self.commandQueue = []


  def pushQueue(self, cmd, args):
    #print("Waiting for lock")
    self.lock.acquire()
    try:
      #print("Adding command {}".format(cmd))
      self.commandQueue.append((cmd, args))
      #print("Command added")
    except Exception as e:
      print("Exception when adding command: ")
      print(e)
    finally:
      self.lock.release()
      #print("Lock released")


  def addPointCloud(self, cloud, idx, color=np.array([1.0, 0.0, 0.0])):

    if cloud.shape[1] != 6:
      cloudColored = np.zeros((cloud.shape[0], cloud.shape[1]+3))
      cloudColored[:, 0:cloud.shape[1]] = cloud 
      cloudColored[:, cloud.shape[1]:] = color
      cloud = cloudColored

    if self.ymax != 0.5:
      ymax = np.max(cloud[:,1])
      self.ymax = np.maximum(self.ymax, ymax)
    #print(cloud.shape)

    if idx not in self.pointClouds:
      vList = pyglet.graphics.vertex_list(cloud.shape[0],
        ("v3f", cloud[:,0:3].ravel()),
        ("c3B", cloud[:,3:].ravel().astype("B"))
      )
      
      self.pointClouds[idx] = vList
    else:
      # resize if necessary and update cloud
      if self.pointClouds[idx].get_size() != cloud.shape[0]:
        self.pointClouds[idx].resize(cloud.shape[0])

      self.pointClouds[idx].vertices = cloud[:,0:3].ravel()
      self.pointClouds[idx].colors = cloud[:,3:].ravel().astype("B")


  def checkCmdQueue(self, dt, args):    
    if len(self.commandQueue) == 0:
      return

    self.lock.acquire()
    try:
      print("Processing commands ({})".format(len(self.commandQueue)))
      while len(self.commandQueue) > 0:
        (cmd, args) = self.commandQueue.pop()
        if cmd == "shutdown":
          pyglet.app.exit()
          break

        method = getattr(self, cmd, None)
        if method is None:
          continue 

        if callable(method):
          method(*args)
    finally:
      self.lock.release()

  def saveFrame(self, out):
    pyglet.image.get_buffer_manager().get_color_buffer().save(out)

  def addCamera(self, pos, idx):
    """
    Adds a camera surface to be drawn where pos is an array of corner points and one center.

    Args:
      pos (np.array): A array of 3D coordinates [top left, top right, bottom right, bottom left, center]
      idx (str): The id of the camera, used for updating.
    
    Returns:
      None
    
    """
    self.cameras[idx] = pos

  def on_draw(self):
    self.window.clear()

    gl.glEnable(gl.GL_DEPTH_TEST)
    gl.glEnable(gl.GL_LINE_SMOOTH)

    width, height = self.window.get_size()
    gl.glViewport(0, 0, width, height)

    gl.glMatrixMode(gl.GL_PROJECTION)
    gl.glLoadIdentity()
    gl.gluPerspective(60, width / float(height), 0.1, 5.0)

    gl.glMatrixMode(gl.GL_MODELVIEW)
    gl.glLoadIdentity()

    # eyex, eyey, eyez, centerx, centery, centerz, upX, upY, upZ
    # sets y axis to pointing downwards
    gl.gluLookAt(0, 0, 0, 0, 0, 1, 0, -1, 0)

    gl.glRotated(self.state.pitch, 1, 0, 0)
    gl.glRotated(self.state.yaw, 0, 1, 0)

    gl.glTranslatef(*self.state.translation)

    draw.grid(self.ymax, 3.0, 30) # 3x3 meters, one cell is 10x10cm
    
    #gl.glDisable(gl.GL_MULTISAMPLE)
    draw.pointClouds(self.pointClouds)
    #gl.glEnable(gl.GL_MULTISAMPLE)

    draw.axes(0.5, 4)

    draw.cameras(self.cameras)

    # reset transformations before drawing 2d UI
    gl.glMatrixMode(gl.GL_PROJECTION)
    gl.glLoadIdentity()
    gl.glOrtho(0, width, 0, height, -1, 1)  
    gl.glMatrixMode(gl.GL_MODELVIEW)
    gl.glLoadIdentity()
    gl.glDisable(gl.GL_DEPTH_TEST)
    # draw 2d text
    self.yaw_label.text = "Yaw: {:.2f}".format(self.state.yaw)
    self.yaw_label.y = self.window.height-20
    self.yaw_label.draw()

    self.pitch_label.text = "Pitch: {:.2f}".format(self.state.pitch)
    self.pitch_label.y = self.window.height-40
    self.pitch_label.draw()

    self.trans_label.text = "Translation: x: {:.2f} y: {:.2f} z: {:.2f}".format(self.state.translation[0], self.state.translation[1], self.state.translation[2])
    self.trans_label.y = self.window.height-60
    self.trans_label.draw()


  def on_mouse_drag(self, x, y, dx, dy, buttons, modifiers):
    w, h = map(float, self.window.get_size())
    
    if buttons & pyglet.window.mouse.LEFT:
      self.state.yaw += -dx * 0.5
      self.state.pitch += -dy * 0.5

    if buttons & pyglet.window.mouse.RIGHT:
      dp = np.array((dx / w, -dy / h, 0), np.float32)
      self.state.translation += np.dot(self.state.rotation, dp)

    if buttons & pyglet.window.mouse.MIDDLE:
      dz = dy * 0.01
      self.state.translation -= (0, 0, dz)
      self.state.distance -= dz

  def handle_mouse_btns(self, x, y, button, modifiers):
    self.state.mouse_btns[0] ^= (button & pyglet.window.mouse.LEFT)
    self.state.mouse_btns[1] ^= (button & pyglet.window.mouse.RIGHT)
    self.state.mouse_btns[2] ^= (button & pyglet.window.mouse.MIDDLE)


  def on_key_press(self, symbol, modifiers):

    if symbol == pyglet.window.key.W:
      tns = np.array([0.0, 0.0, -0.05])
      self.state.translation += np.dot(self.state.rotation, tns)

    if symbol == pyglet.window.key.S:
      tns = np.array([0.0, 0.0, 0.05])
      self.state.translation += np.dot(self.state.rotation, tns)

    if symbol == pyglet.window.key.A or symbol == pyglet.window.key.LEFT:
      tns = np.array([0.05, 0.0, 0.0])
      self.state.translation += np.dot(self.state.rotation, tns)

    if symbol == pyglet.window.key.D  or symbol == pyglet.window.key.RIGHT:
      tns = np.array([-0.05, 0.0, 0.0])
      self.state.translation += np.dot(self.state.rotation, tns)

    if symbol == pyglet.window.key.UP:
      tns = np.array([0.0, 0.05, 0.0])
      self.state.translation += np.dot(self.state.rotation, tns)

    if symbol == pyglet.window.key.DOWN:
      tns = np.array([0.0, -0.05, 0.0])
      self.state.translation += np.dot(self.state.rotation, tns)


  def setYaw(self, yaw):
    self.state.yaw = yaw
    self.on_draw()
  
  def setPitch(self, pitch):
    self.state.pitch = pitch
    self.on_draw()


  def setTranslation(self, t):
    self.state.translation = t
    self.on_draw()

  def run(self):
    pyglet.clock.schedule(self.checkCmdQueue, 1.0/120.0)
    pyglet.app.run()

