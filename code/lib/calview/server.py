import socket
import threading
import os
import json
import numpy as np
import tables
import re
import traceback


from . import CalibrationWindow
from . import ServerState
from util.pointCloud import *
from util import deprojectPixelToPoint
from analysis import loadCameras, loadCameraMap

def findClosestCalibration(frameNo, transformTbl):
  transformsOut = dict()

  frameNos = np.array([row["frameNo"] for row in transformTbl])
  idx = np.argmin(np.abs(frameNos-frameNo))
  closest = frameNos[idx]

  print("Closest for {} is {}".format(frameNo, closest))

  transforms = [{
    "transformCam": row["transformCam"],
    "transformPC": row["transformPC"],
    "fromCamera": row["fromCamera"]
  } for row in transformTbl.iterrows() if row["frameNo"] == closest]

  for r in transforms:
    transformsOut[r["fromCamera"]] = r

  return transformsOut

def gotoFrame(state, alignCloud=True):
  fRoot = state.calHandle.root
  # find calibration closest to the given frame number and use that
  transforms = findClosestCalibration(state.curFrame, fRoot["transforms"])

  # draw each camera and point cloud
  for cam in state.cameras:
    grp = "cam_{}".format(cam.serialNo)
    # camera
    camCoords = np.array([
      [0.0, 0.0, 0.1],
      [1920.0, 0.0, 0.1],
      [1920.0, 1080.0, 0.1],
      [0.0, 1080.0, 0.1],
      [0.0, 0.0, 0.0]
    ])

    camId = state.camMap[cam.serialNo]
    # point cloud
    pcI = fRoot[grp+"/pointcloud_index"][state.curFrame]
    pointCloud = fRoot[grp+"/pointcloud_buffer"][pcI["bufferStart"]:pcI["bufferEnd"]]

    camCoords = deprojectPixelToPoint(cam.intrinsics.getMatrix(), camCoords.T, camCoords[:,2].T).T

    if camId > 0 and camId in transforms:
      camTransform = transforms[camId]["transformCam"]
      pcTransform = transforms[camId]["transformPC"]

      # transform camera
      camCoords = transformPointCloud(camCoords, camTransform)
      # transform point cloud
      if alignCloud:
        pointCloud = transformPointCloud(pointCloud, pcTransform)
  
    state.window.pushQueue("addCamera", [camCoords, grp])
    state.window.pushQueue("addPointCloud", [pointCloud, grp])

def sendError(clientSocket, msg):
  rsp = dict()
  rsp["status"] = "error"
  rsp["message"] = msg

  out = json.dumps(rsp)

  clientSocket.send((out + "\n").encode("ascii"))

def serverCmdLoop(clientSocket, state):
  
  fRoot = state.calHandle.root

  doRecv = True
  buf = ""
  shutdown = False

  numFrames = np.inf
  # find the maximum number of frames
  for cam in state.cameras:
    grp = "cam_{}".format(cam.serialNo)
    numFrames = int(np.minimum(numFrames, len(fRoot[grp+"/pointcloud_index"])))
  
  state.numFrames = numFrames
  
  gotoFrame(state)

  while doRecv:
    cmd = None 
    
    while not "\n" in buf:
      msg = clientSocket.recv(1024)
      if msg == b'':
        # broken pipe
        doRecv = False
        break
      
      buf += msg.decode("ascii")
    
    if not doRecv:
      break
    
    pos = buf.find("\n")
    cmd = buf[0:pos].rstrip("\r")
    buf = buf[pos+1:]

    if cmd == "shutdown":
      clientSocket.send("Bye!\n".encode("ascii"))
      shutdown = True
      doRecv = False
      state.window.pushQueue("shutdown", None)

    elif cmd == "disconnect":
      clientSocket.send("Bye!\n".encode("ascii"))
      doRecv = False
    else:
      try:
        data = json.loads(cmd)
        
        response = dict()
        response["status"] = "ok"

        if "yaw" in data:
          state.window.pushQueue("setYaw", [data["yaw"]])

        if "pitch" in data:
          state.window.pushQueue("setPitch", [data["pitch"]])

        if "translation" in data:          
          if len(data["translation"]) != 3:
            sendError(clientSocket, "Translation must be a list of 3 numbers")
            continue 

          state.window.pushQueue("setTranslation", [np.array(data["translation"])])

        if "nav" in data:
          if type(data["nav"]) == int:
            state.curFrame = data["nav"]
          elif data["nav"] == "next":
            state.curFrame += 1
          elif data["nav"] == "prev":
            state.curFrame -= 1
          else:
            sendError(clientSocket, "Unknown nav message {}".format(data["nav"]))
            continue
          
          if state.curFrame < 0 or state.curFrame > numFrames:
            sendError(clientSocket, "Frame number out of range, must be >= 0 and < {}".format(numFrames))
            continue
          
          transformCloud = True
          if "transformCloud" in data:
            transformCloud = data["transformCloud"]

          gotoFrame(state, transformCloud)
                
        if "saveFrame" in data:
          state.window.pushQueue("saveFrame", [data["saveFrame"]])

        if "getState" in data:
          response["numFrames"] = state.numFrames
          response["curFrame"] = state.curFrame
          response["yaw"] = state.window.state.yaw
          response["pitch"] = state.window.state.pitch
          response["translation"] = state.window.state.translation.tolist()
        
        clientSocket.send((json.dumps(response)+"\n").encode("ascii") )



      except Exception as e:
        print("Parse exception", e)
        traceback.print_exc()

        sendError(clientSocket, "Invalid message {}\n".format(cmd))
  
  print("Client disconnected")

  return shutdown


def serverLoop(host, port, serverState):
  print("Server: Starting on {}:{}".format(host, port))
  s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
  s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
  s.bind((host, port))
  s.listen(1) # maximum of 1 connection
  print("Server: Started")
  
  shutdown = False
  while not shutdown:
    (clientSocket, address) = s.accept()
    print("Server: Connection accepted from {}".format(address))
    
    try:
      shutdown = serverCmdLoop(clientSocket, serverState)
    except Exception as e:
      print("Exception in server loop, shutting socket down")
      traceback.print_exc()
      shutdown = True

    clientSocket.close()      

  s.shutdown(socket.SHUT_RDWR)
  s.close()
  serverState.calHandle.close()
  serverState.convHandle.close()
  print("Server: Shutdown complete")


def serverStart(host, port, calPath, width, height):
  calPath = calPath.rstrip("/")
  calFile = calPath + "/calibration.h5"
  convFile = calPath + "/converted.h5"

  if not os.path.exists(calFile):
    raise RuntimeError("Calibration file {} does not exist or is not readable".format(calFile))
  
  if not os.path.exists(convFile):
    raise RuntimeError("Converted file {} does not exist or is not readable".format(convFile))

  calHandle = tables.open_file(calFile, "r")
  convHandle = tables.open_file(convFile, "r")
  w = CalibrationWindow(width, height)

  state = ServerState(convHandle, calHandle, w)
  state.cameras = loadCameras(convHandle)
  state.camMap = loadCameraMap(calHandle)
  
  t = threading.Thread(target=serverLoop, args=(host, port, state))
  t.start()
  
  w.run()