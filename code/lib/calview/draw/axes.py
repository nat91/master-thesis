import pyglet
import pyglet.gl as gl 

def axes(size=1, width=1):
  """draw 3d axes"""
  gl.glLineWidth(width)
  pyglet.graphics.draw(6, gl.GL_LINES,
                        ('v3f', (0, 0, 0, size, 0, 0,
                                0, 0, 0, 0, size, 0,
                                0, 0, 0, 0, 0, size)),
                        ('c3f', (1, 0, 0, 1, 0, 0,
                                0, 1, 0, 0, 1, 0,
                                0, 0, 1, 0, 0, 1,
                                ))
                        )
