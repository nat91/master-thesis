import pyglet
import pyglet.gl as gl

def grid(ypos, size=1, n=10, width=1):
  """draw a grid on xz plane"""

  gl.glPushMatrix()
  gl.glTranslatef(0, ypos, 0) # move the plane down in the Y direction

  gl.glLineWidth(width)
  s = size / float(n)
  s2 = 0.5 * size
  batch = pyglet.graphics.Batch()

  for i in range(0, n + 1):
    x = -s2 + i * s
    batch.add(2, gl.GL_LINES, None, ('v3f', (x, 0, -s2, x, 0, s2)))
  
  
  for i in range(0, n + 1):
    z = -s2 + i * s
    batch.add(2, gl.GL_LINES, None, ('v3f', (-s2, 0, z, s2, 0, z)))

  batch.draw()

  gl.glPopMatrix()