import pyglet
import pyglet.gl as gl 

def pointClouds(pointClouds):

  gl.glPointSize(2.0)
  for (k, pc) in pointClouds.items():
    pc.draw(gl.GL_POINTS)