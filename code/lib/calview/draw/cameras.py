import pyglet
import pyglet.gl as gl 

def cameras(cams):

  batch = pyglet.graphics.Batch()
  for (k, v) in cams.items():
    if v.shape[0] != 5:
      print("Camera {} is invalid, 5 points must be supplied.".format(k))
      continue
    
    v = v.tolist()
    
    # draw lines around camera image plane
    batch.add(2, gl.GL_LINES, None, ('v3f', v[0] + v[1]))
    batch.add(2, gl.GL_LINES, None, ('v3f', v[1] + v[2]))
    batch.add(2, gl.GL_LINES, None, ('v3f', v[2] + v[3]))
    batch.add(2, gl.GL_LINES, None, ('v3f', v[3] + v[0]))
    # draw lines from center to corners
    batch.add(2, gl.GL_LINES, None, ('v3f', v[4] + v[1]))
    batch.add(2, gl.GL_LINES, None, ('v3f', v[4] + v[2]))
    batch.add(2, gl.GL_LINES, None, ('v3f', v[4] + v[3]))
    batch.add(2, gl.GL_LINES, None, ('v3f', v[4] + v[0]))

  batch.draw()