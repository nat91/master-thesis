import socket
import json

class Client:

  def __init__(self, host, port):
    self.host = host
    self.port = port 
    self.state = None
    self.isConnected = False
    self.buf = ""

  def connect(self):
    self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    self.socket.connect((self.host, self.port))
    self.isConnected = True

  def disconnect(self):
    self.socket.close()
    self.isConnected = False
  
  def _sendMessage(self, msg):
    msgT = json.dumps(msg) + "\n"
    ret = self.socket.sendall(msgT.encode("ascii"))
    if ret is not None:
      return False, ret
    
    # wait for server response
    while not "\n" in self.buf:
      msg = self.socket.recv(1024)
      if msg == b'':
        return False, None
      self.buf += msg.decode("ascii")
    
    pos = self.buf.find("\n")
    response = self.buf[0:pos]
    self.buf = self.buf[pos+1:]

    r = json.loads(response)

    return True, r


  def getState(self):
    status, rsp = self._sendMessage({"getState": True})
    if status and rsp["status"] == "ok":
      self.state = rsp
    
    return status,rsp

  def nextFrame(self):
    status, rsp = self._sendMessage({"nav": "next"})
    if status and rsp["status"] == "ok":
      return True, rsp

    return False, rsp

  def prevFrame(self):
    status, rsp = self._sendMessage({"nav": "prev"})
    if status and rsp["status"] == "ok":
      return True, rsp

    return False, rsp

  
  def setYaw(self, yaw):
    status, rsp = self._sendMessage({"yaw": yaw})
    if status and rsp["status"] == "ok":
      return True, rsp

    return False, rsp

  def setPitch(self, pitch):
    status, rsp = self._sendMessage({"pitch": pitch})
    if status and rsp["status"] == "ok":
      return True, rsp

    return False, rsp

  def setTranslation(self, translation):
    status, rsp = self._sendMessage({"translation": translation})
    if status and rsp["status"] == "ok":
      return True, rsp

    return False, rsp
  
  def saveCurrentFrame(self, path):
    status, rsp = self._sendMessage({"saveFrame": path})
    if status and rsp["status"] == "ok":
      return True, rsp

    return False, rsp

  def shutdown(self):
    self.socket.sendall("shutdown\n".encode("ascii"))