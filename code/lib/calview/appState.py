import numpy as np

# https://stackoverflow.com/a/6802723
def rotation_matrix(axis, theta):
    """
    Return the rotation matrix associated with counterclockwise rotation about
    the given axis by theta radians.
    """
    axis = np.asarray(axis)
    axis = axis / np.sqrt(np.dot(axis, axis))
    a = np.cos(theta / 2.0)
    b, c, d = -axis * np.sin(theta / 2.0)
    aa, bb, cc, dd = a * a, b * b, c * c, d * d
    bc, ad, ac, ab, bd, cd = b * c, a * d, a * c, a * b, b * d, c * d
    return np.array([[aa + bb - cc - dd, 2 * (bc + ad), 2 * (bd - ac)],
                     [2 * (bc - ad), aa + cc - bb - dd, 2 * (cd + ab)],
                     [2 * (bd + ac), 2 * (cd - ab), aa + dd - bb - cc]])



class AppState:

  def __init__(self):
    self.translation = np.array([0,0,1], np.float32)
    self.distance = 2
    self.mouse_btns = [False, False, False]
    self.pitch, self.yaw = np.radians(-10), np.radians(-15)

  @property
  def rotation(self):
    Rx = rotation_matrix((1, 0, 0), np.radians(-self.pitch))
    Ry = rotation_matrix((0, 1, 0), np.radians(-self.yaw))
    return np.dot(Ry, Rx).astype(np.float32)
