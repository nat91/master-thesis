#include "pybind11/pybind11.h"
#include <pybind11/stl.h>
#include <pybind11/numpy.h>
#include <iostream>
#include <librealsense2/rs.hpp>

#include "ku/ccal/frame.h"
#include "ku/ccal/camera.h"
#include "ku/ccal/cameraCollection.h"
#include "ku/ccal/cameraDiscoverer.h"
#include "ku/ccal/fileRecorderStats.h"
#include "ku/ccal/fileRecorder.h"


namespace py = pybind11;
using ku::ccal::CameraDiscoverer;
using ku::ccal::CameraCollection;
using ku::ccal::Camera;
using ku::ccal::Frame;
using ku::ccal::FileRecorder;
using ku::ccal::FileRecorderStats;

/**
* Proxy class for ku::ccal::Frame that allows us to change between buffers
* when using Python's Buffer interface.
*/
class PyFrameWrapper {
    
	private:
		std::shared_ptr<Frame> m_frame;
		bool m_isDepth = false;

	public:
		PyFrameWrapper(std::shared_ptr<Frame> frame) {
			m_frame = frame;
		}

		void enableDepthBuffer() {
			m_isDepth = true;
		}

		void enableRGBBuffer() {
			m_isDepth = false;
		}

		bool isDepthBufferEnabled() {
			return m_isDepth;
		}

		// proxy'd methods
		uint8_t* getRGB() { return m_frame->getRGB(); }
		uint16_t* getDepth() { return m_frame->getDepth(); }
		uint32_t getRGBDataSize() { return m_frame->getRGBDataSize(); }
		uint32_t getDepthDataSize() { return m_frame->getDepthDataSize(); }
		uint64_t getRGBTimestamp() { return m_frame->getRGBTimestamp(); }
		uint64_t getDepthTimestamp() { return m_frame->getDepthTimestamp(); }
		uint64_t getRGBFrameNo() { return m_frame->getRGBFrameNo(); }
		uint64_t getDepthFrameNo() { return m_frame->getDepthFrameNo(); }
		std::pair<uint16_t, uint16_t> getResolution()  { return m_frame->getResolution(); }
		std::pair<uint16_t, uint16_t> getDepthResolution() { return m_frame->getDepthResolution(); }
};

PYBIND11_MODULE(py_ku_ccal, m) {
	py::class_<CameraDiscoverer>(m, "CameraDiscoverer")
	.def("find", &CameraDiscoverer::find);

	py::class_<CameraCollection>(m, "CameraCollection")
	.def("start", &CameraCollection::start)
	.def("stop", &CameraCollection::stop)
	.def("count", &CameraCollection::count)
	.def("getCameras", &CameraCollection::getCameras)
	.def("__getitem__", &CameraCollection::operator[]);

	py::class_<Camera>(m, "Camera")
	.def("start", &Camera::start)
	.def("stop", &Camera::stop)
	.def("getSerialNumber", &Camera::getSerialNumber)
	.def("getRGBIntrinsics", &Camera::getRGBIntrinsics)
	.def("getDepthIntrinsics", &Camera::getDepthIntrinsics)
	.def("getDepthToColorExtrinsics", &Camera::getDepthToColorExtrinsics)

	.def("isStarted", &Camera::isStarted)
	.def("capture",
	 [](Camera &c) {
		 auto f = c.capture();
		 auto wrapper = PyFrameWrapper(f);
		 return wrapper;
	 	})
	.def("recordNumFrames", 
		[](Camera &c, uint32_t num) {
			auto f = c.recordNumFrames(num);
			std::vector<PyFrameWrapper> out;
			for(auto i : f) {
				out.push_back(PyFrameWrapper(i));
			}

			return out;
		}
	)
	.def("enableAutoExposure", &Camera::enableAutoExposure)
	.def("disableAutoExposure", &Camera::disableAutoExposure)
	.def("autoExposureEnabled", &Camera::autoExposureEnabled)
	.def("setExposure", &Camera::setExposure)
	.def("setGain", &Camera::setGain)
	.def("setROI", &Camera::setROI)

	.def("getResolution", &Camera::getResolution)
	.def("getDepthResolution", &Camera::getDepthResolution)
	.def("getFPS", &Camera::getFPS)
	.def("getDepthScale", &Camera::getDepthScale)

	.def("getRGBIntrinsicsMatrix", [](Camera &cam) -> py::array_t<float> {
		auto res = py::array_t<float>({3,3});
		py::buffer_info buf = res.request();
		float* resPtr = (float*)buf.ptr;

		// initialize to 0
		for(size_t i = 0; i < 9; i++) {
			resPtr[i] = 0.0;
		}

		resPtr[0] = cam.getRGBIntrinsics().fx;
		resPtr[4] = cam.getRGBIntrinsics().fy;
		resPtr[8] = 1.0;
		resPtr[2] = cam.getRGBIntrinsics().ppx;
		resPtr[5] = cam.getRGBIntrinsics().ppy;

		return res;
	});

	

	py::class_<PyFrameWrapper>(m, "Frame", py::buffer_protocol())
	.def_buffer([](PyFrameWrapper &f) -> py::buffer_info {
		
		if(f.isDepthBufferEnabled()) {
			return py::buffer_info(
				f.getDepth(),
				sizeof(uint16_t),
				py::format_descriptor<uint16_t>::format(),
				2,
				// dimensions: h*w
				{(size_t) f.getDepthResolution().second, (size_t) f.getDepthResolution().first},
				// stride pr. dim
				{sizeof(uint16_t)*f.getDepthResolution().first, sizeof(uint16_t)}
			);
		} 

		return py::buffer_info(
			f.getRGB(),
			sizeof(uint8_t),
			py::format_descriptor<uint8_t>::format(),
			3,
			// dimension h*w*3
			{(size_t)f.getResolution().second, (size_t)f.getResolution().first, 3UL},
			// stride
			{sizeof(uint8_t)*f.getResolution().first*3UL, sizeof(uint8_t)*3UL, sizeof(uint8_t)}
		);
	})
	.def("getRGBDataSize", &PyFrameWrapper::getRGBDataSize)
	.def("getDepthDataSize", &PyFrameWrapper::getDepthDataSize)
	.def("getRGBTimestamp", &PyFrameWrapper::getRGBTimestamp)
	.def("getDepthTimestamp", &PyFrameWrapper::getDepthTimestamp)
	.def("getRGBFrameNo", &PyFrameWrapper::getRGBFrameNo)
	.def("getDepthFrameNo", &PyFrameWrapper::getDepthFrameNo)
	.def("getResolution", &PyFrameWrapper::getResolution)
	.def("getDepthResolution", &PyFrameWrapper::getDepthResolution)
	.def("enableRGBBuffer", &PyFrameWrapper::enableRGBBuffer)
	.def("enableDepthBuffer", &PyFrameWrapper::enableDepthBuffer);

	py::class_<FileRecorder>(m, "FileRecorder")
	.def(py::init<CameraCollection, std::string>())
	.def("record", &FileRecorder::record)
	.def("pause", &FileRecorder::pause)
	.def("stop", &FileRecorder::stop)
	.def("isRecording", &FileRecorder::isRecording);

	py::class_<FileRecorderStats>(m, "FileRecorderStats")
	.def_readonly("framesRecorded", &FileRecorderStats::framesRecorded)
	.def_readonly("framesDropped", &FileRecorderStats::framesDropped)
	.def_readonly("bytesWritten", &FileRecorderStats::bytesWritten)
	.def_readonly("recordingDuration", &FileRecorderStats::recordingDuration);

	py::class_<rs2_intrinsics>(m, "rs2_intrinsics")
	.def_readonly("fx", &rs2_intrinsics::fx)
	.def_readonly("fy", &rs2_intrinsics::fy)
	.def_readonly("px", &rs2_intrinsics::ppx)
	.def_readonly("py", &rs2_intrinsics::ppy)
	.def("getMatrix", [](rs2_intrinsics &intr) -> py::array_t<float> {
		auto res = py::array_t<float>({3,3});
		py::buffer_info buf = res.request();
		float* resPtr = (float*)buf.ptr;

		// initialize to 0
		for(size_t i = 0; i < 9; i++) {
			resPtr[i] = 0.0;
		}

		resPtr[0] = intr.fx;
		resPtr[4] = intr.fy;
		resPtr[8] = 1.0;
		resPtr[2] = intr.ppx;
		resPtr[5] = intr.ppy;

		return res;
	})
	
	
	.def("getDistortionCoeffs", [](rs2_intrinsics &intr) -> std::vector<float> {
		auto out = std::vector<float>();
		for(size_t i = 0; i < 5; i++) {
			out.push_back(intr.coeffs[i]);
		} 

		return out;
	});
	

	py::class_<rs2_extrinsics>(m, "rs2_extrinsics")
	.def("getTransform", [](rs2_extrinsics &extr) -> py::array_t<float> {

		auto out = py::array_t<float>({4,4});
		py::buffer_info buf = out.request();
		float* resPtr = (float*)buf.ptr;

		// row 1
		resPtr[0] = extr.rotation[0];
		resPtr[1] = extr.rotation[3];
		resPtr[2] = extr.rotation[6];
		resPtr[3] = extr.translation[0];
		// row 2
		resPtr[4] = extr.rotation[1];
		resPtr[5] = extr.rotation[4];
		resPtr[6] = extr.rotation[7];
		resPtr[7] = extr.translation[1];
		// row 3
		resPtr[8] = extr.rotation[2];
		resPtr[9] = extr.rotation[5];
		resPtr[10] = extr.rotation[8];
		resPtr[11] = extr.translation[2];
		// row 4
		resPtr[12] = 0;
		resPtr[13] = 0;
		resPtr[14] = 0;
		resPtr[15] = 1.0;

		return out;
	});


}