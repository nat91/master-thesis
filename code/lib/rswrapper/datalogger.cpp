#include "ku/ccal/frame.h"
#include "ku/ccal/camera.h"
#include "ku/ccal/cameraCollection.h"
#include "ku/ccal/fileRecorder.h"
#include "ku/ccal/cameraDiscoverer.h"
#include <librealsense2/rs.hpp>
#include <boost/filesystem.hpp>
#include <iostream>
#include <stdio.h>
#include <math.h>


void usage() {
  std::cout << "Usage: ./datalogger saveDirectory" << std::endl;
}

int main(int argc, char** argv) {

  if(argc != 2) {
    usage();
    exit(EXIT_FAILURE);
  }

  std::string saveDir(argv[1]);
  if(boost::filesystem::exists(saveDir)) {
    std::cout << "Fatal error: Directory " << saveDir << " already exists!" << std::endl;
    exit(EXIT_FAILURE);
  }

  auto cams = ku::ccal::CameraDiscoverer::find();

  printf("Starting cameras... \n");
  cams.start();
  printf("Cameras started \n");

  printf("Settling auto exposure for 60 frames \n");
  auto cam0 = cams.getCamera(0);
  for(int i = 0; i < 60; i++) {
    cam0->capture();
  }
  printf("Auto exposure settled \n");

  ku::ccal::FileRecorder rec(cams, saveDir);
  printf("Starting recording...\n");
  rec.record();
  printf("Recording started \n");

  char code = 'e';

  printf("Type 'e' for stop, 'p' for pause and 'r' for record\n");

  while((code = getc(stdin))) {
    if(code == '\n') continue;
    
    if(code == 'e') {
      auto stats = rec.stop();

      float duration = 0;
      uint64_t framesRecorded = 0;
      uint64_t framesDropped = 0;
      uint64_t bytesWritten = 0;
      

      for(auto stat : stats) {
        duration += stat.recordingDuration;
        framesRecorded = std::max(framesRecorded, stat.framesRecorded);
        framesDropped += stat.framesDropped;
        bytesWritten += stat.bytesWritten;
      }

      float durationSecs = (duration/stats.size())/pow(10, 6);
      float bytesWrittenGb = bytesWritten/pow(10, 9);

      std::cout << std::string(30, '-') << std::endl;
      std::cout << "STATS:" << std::endl;
      std::cout << std::string(30, '-') << std::endl;
      std::cout << "Frames recorded: " << framesRecorded << std::endl;
      std::cout << "Frames dropped: " << framesDropped << std::endl;
      std::cout << "Duration: " << durationSecs << "s" << std::endl;
      std::cout << "Bytes written: " << bytesWrittenGb << " GB (" << bytesWrittenGb/durationSecs << " GB/s)" << std::endl;
      std::cout << std::string(30, '-') << std::endl;

      break;
    }

    if(code == 'p') {
      rec.pause();
    }

    if(code == 'r') {
      rec.record();
    }
  }

  printf("Command loop exit \n");



  /*
  auto f = c.capture();

  uint8_t* rgb = f->getRGB();

  test(c);

  printf("%d %d %d \n", rgb[0], rgb[1], rgb[2]);

  auto frames = c.recordNumFrames(30);
  printf("Done! \n");
  for(auto f : frames) {
    uint8_t* rgb = f->getRGB();

    printf("%lu %lu: %d %d %d \n", f->getRGBFrameNo(), f->getDepthFrameNo(), rgb[0], rgb[1], rgb[2]);
  }*/

  printf("Stopping cameras... \n");
  cams.stop();
  printf("Cameras stopped! \n");

  exit(EXIT_SUCCESS);
}