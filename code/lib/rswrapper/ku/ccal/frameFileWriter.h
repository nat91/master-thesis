#ifndef KU_CCAL_FRAME_FILE_WRITER_H
#define KU_CCAL_FRAME_FILE_WRITER_H
#include <stdio.h>
#include <librealsense2/rs.hpp>
#include "frame.h"
#include "camera.h"

namespace ku {
  namespace ccal {

    typedef struct {
      uint64_t RGBTimeStamp;
      uint64_t RGBFrameNo;
      uint64_t depthTimeStamp;
      uint64_t depthFrameNo;
    } FrameMetaData;

    typedef struct {
        float fx;
        float fy;
        float px;
        float py;
        float distortionCoeffs[5];
        float padding;
    } Intrinsics;

    typedef struct {
        uint16_t rgbWidth;
        uint16_t rgbHeight;
        uint16_t depthWidth;
        uint16_t depthHeight;        
        uint64_t fps; // we make this 64 bit for the sake of alignment.
        uint64_t numFrames;
        float depthScale;
        float padding;
        // intrinsics
        Intrinsics iRGB;
        Intrinsics iDepth;
        // depth to color extrinsics
        rs2_extrinsics extrinsics;
        char RESERVED[1024];
    } FileHeader;

    /**
     * Class that writes frames to disk in the binary format specified in binary-format.txt
     */
    class FrameFileWriter {
      
      private:
        uint64_t m_numFrames = 0;
        
      public:
        /**
         * Writes a single frame and its meta data to the given file pointer
         * and returns the number of bytes written
         */ 
        uint32_t writeFrame(FILE* fp, std::shared_ptr<Frame> f);
        /**
         * Writes a header to the given file pointer, the header is automatically written to the start of the file pointer. The file pointer is returned to its previous position before the function returns,
         * if restoreFp is set to true
         * 
         * the number of bytes written is returned
         */ 
        uint32_t writeHeader(FILE* fp, Camera* cam, bool restoreFp);

    };

  }
}

#endif