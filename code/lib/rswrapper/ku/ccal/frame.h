#ifndef KU_CCAL_FRAME_H
#define KU_CCAL_FRAME_H
#include <cstdint>
#include <memory>
#include <cstring>
#include <iostream>
#include <librealsense2/rs.hpp>

namespace ku {
  namespace ccal {
    
    /**
     * Wrapper for the realsense frameset, exposing a subset
     * of the functionality.
     */
    class Frame {
      private:
        uint8_t* m_rgb;
        uint16_t* m_depth;
        uint32_t m_size_rgb;
        uint32_t m_size_depth;
        uint16_t m_width;
        uint16_t m_height;
        uint16_t m_depthWidth;
        uint16_t m_depthHeight;
        uint64_t m_RGBTimestamp;
        uint64_t m_depthTimestamp;
        uint64_t m_RGBFrameNo;
        uint64_t m_depthFrameNo;


      public:
        Frame(rs2::frameset frames);
        ~Frame();
        uint8_t* getRGB();
        uint16_t* getDepth();
        uint32_t getRGBDataSize();
        uint32_t getDepthDataSize();
        uint64_t getRGBTimestamp();
        uint64_t getDepthTimestamp();
        uint64_t getRGBFrameNo();
        uint64_t getDepthFrameNo();
        std::pair<uint16_t, uint16_t> getResolution();
        std::pair<uint16_t, uint16_t> getDepthResolution();
               
    };
  }

}
#endif
