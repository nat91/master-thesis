#ifndef KU_CCAL_CAMERA_DISCOVERER_H
#define KU_CCAL_CAMERA_DISCOVERER_H
#include <librealsense2/rs.hpp>
#include "cameraCollection.h"
#include "camera.h"


namespace ku {
  namespace ccal {
    
    class CameraDiscoverer {

      public:
        static CameraCollection find(); 

    };

  }
}
#endif