#include "frameFileWriter.h"

namespace ku {
  namespace ccal {

    uint32_t FrameFileWriter::writeHeader(FILE* fp, Camera* cam, bool restoreFp) {
      long lastPos = ftell(fp);
      rewind(fp);

      FileHeader f;
      f.rgbWidth = cam->getResolution().first;
      f.rgbHeight = cam->getResolution().second;

      f.depthWidth = cam->getDepthResolution().first;
      f.depthHeight = cam->getDepthResolution().second;
      f.fps = cam->getFPS();
      f.numFrames = m_numFrames;
      f.depthScale = cam->getDepthScale();

      // set zero padding
      f.padding = 0;
      // set reserved padding
      memset(f.RESERVED, 0, sizeof(f.RESERVED));
      // set intrinsics
      f.iRGB.fx = cam->getRGBIntrinsics().fx;
      f.iRGB.fy = cam->getRGBIntrinsics().fy;
      f.iRGB.px = cam->getRGBIntrinsics().ppx;
      f.iRGB.py = cam->getRGBIntrinsics().ppy;
      memcpy(f.iRGB.distortionCoeffs, cam->getRGBIntrinsics().coeffs, sizeof(float)*5);

      f.iDepth.fx = cam->getDepthIntrinsics().fx;
      f.iDepth.fy = cam->getDepthIntrinsics().fy;
      f.iDepth.px = cam->getDepthIntrinsics().ppx;
      f.iDepth.py = cam->getDepthIntrinsics().ppy;
      memcpy(f.iDepth.distortionCoeffs, cam->getDepthIntrinsics().coeffs, sizeof(float)*5);

      f.extrinsics = cam->getDepthToColorExtrinsics();

      if(fwrite(&f, 1, sizeof(FileHeader), fp) != sizeof(FileHeader)) {
        throw std::runtime_error("Short count when writing file header data to disk!");
      }

      if(restoreFp) fseek(fp, lastPos, SEEK_SET);

      return sizeof(FileHeader);
    }

    uint32_t FrameFileWriter::writeFrame(FILE* fp, std::shared_ptr<Frame> f) {
      
      FrameMetaData m;
      // write frame meta data
      m.RGBTimeStamp = f->getRGBTimestamp();
      m.RGBFrameNo = f->getRGBFrameNo();
      m.depthTimeStamp = f->getDepthTimestamp();
      m.depthFrameNo = f->getDepthFrameNo();

      if(fwrite(&m, 1, sizeof(FrameMetaData), fp) != sizeof(FrameMetaData)) {
        throw std::runtime_error("Short count when writing frame meta data to disk!");
      }

      if(fwrite(f->getRGB(), 1, f->getRGBDataSize(), fp) != f->getRGBDataSize()) {
        throw std::runtime_error("Short count when writing frame RGB data to disk!");
      }      

      if(fwrite(f->getDepth(), 1, f->getDepthDataSize(), fp) != f->getDepthDataSize()) {
        throw std::runtime_error("Short count when writing frame depth data to disk!");
      }

      m_numFrames++;

      return sizeof(FrameMetaData) + f->getRGBDataSize() + f->getDepthDataSize();
    }

  }
}