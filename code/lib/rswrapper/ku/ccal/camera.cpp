#include "camera.h"
#include "frame.h"
#include <stdio.h>

namespace ku {
  namespace ccal {
    
    Camera::Camera(rs2::device dev, uint16_t width, uint16_t height, uint16_t depthWidth, uint16_t depthHeight, uint8_t FPS) {
      m_dev = dev;
      m_width = width;
      m_height = height;
      m_depthWidth = depthWidth;
      m_depthHeight = depthHeight;
      m_FPS = FPS;
      m_serialNo = dev.get_info(RS2_CAMERA_INFO_SERIAL_NUMBER);
      
      // initialize depth and RGB stream
      m_cnf.enable_stream(RS2_STREAM_COLOR, m_width, m_height, RS2_FORMAT_RGB8, m_FPS);
      m_cnf.enable_stream(RS2_STREAM_DEPTH, depthWidth, depthHeight, RS2_FORMAT_Z16, m_FPS);

      // Attach device to config, ensures m_pipeline will read from
      // the correct camera when multiple are attached.
      m_cnf.enable_device(m_serialNo);
    }

    Camera::~Camera() {
      stop();
      //delete align_to_color;
    }

    void Camera::start() {
      if(m_started) return;
      m_started = true;     

      m_profile = m_pipe.start(m_cnf);

      // get device depth scale
      auto sensor = m_profile.get_device().first<rs2::depth_sensor>();
      m_depthScale = sensor.get_depth_scale();

      // get stream profiles
      auto depthStream = m_profile.get_stream(RS2_STREAM_DEPTH).as<rs2::video_stream_profile>();
      auto colorStream = m_profile.get_stream(RS2_STREAM_COLOR).as<rs2::video_stream_profile>(); 
      

      // get device RGB intrinsics
      m_rgbIntrinsics = colorStream.get_intrinsics();
      // get device depth intrinsics
      m_depthIntrinsics = depthStream.get_intrinsics();
      // get depth to color extrinsics
      m_depthToColorExtrinsics = depthStream.get_extrinsics_to(colorStream);
    }

    void Camera::stop() {
      if(!m_started) return;
      m_pipe.stop();
      m_started = false;
    }

    void Camera::enableAutoExposure() {
      auto sensor = m_profile.get_device().first<rs2::color_sensor>();
      sensor.set_option(RS2_OPTION_ENABLE_AUTO_EXPOSURE, 1);
    }

    void Camera::disableAutoExposure() {
      auto sensor = m_profile.get_device().first<rs2::color_sensor>();
      sensor.set_option(RS2_OPTION_ENABLE_AUTO_EXPOSURE, 0);
    }

    bool Camera::autoExposureEnabled() {
      auto sensor = m_profile.get_device().first<rs2::color_sensor>();
      auto enabled = sensor.get_option(RS2_OPTION_ENABLE_AUTO_EXPOSURE);

      return enabled;
    }

    bool Camera::setExposure(float exposure) {
      if(autoExposureEnabled()) disableAutoExposure();

      auto sensor = m_profile.get_device().first<rs2::color_sensor>();
      auto range = sensor.get_option_range(RS2_OPTION_EXPOSURE);

      if(exposure < range.min || exposure > range.max) return false;
      sensor.set_option(RS2_OPTION_EXPOSURE, exposure);

      return true;
    }

    bool Camera::setGain(float gain) {
      if(autoExposureEnabled()) disableAutoExposure();

      auto sensor = m_profile.get_device().first<rs2::color_sensor>();
      auto range = sensor.get_option_range(RS2_OPTION_GAIN);

      if(gain < range.min || gain > range.max) return false;
      sensor.set_option(RS2_OPTION_GAIN, gain);

      return true;
    }

    bool Camera::setROI(int xmin, int ymin, int xmax, int ymax) {
      if(!autoExposureEnabled()) enableAutoExposure();

      auto sensor = m_profile.get_device().first<rs2::color_sensor>();

      if(sensor.is<rs2::roi_sensor>()) {
        auto roi2 = sensor.as<rs2::roi_sensor>().get_region_of_interest();
        roi2.min_x = xmin;
        roi2.max_x = xmax;
        roi2.min_y = ymin;
        roi2.max_y = ymax;
        sensor.as<rs2::roi_sensor>().set_region_of_interest(roi2);
        return true;
      }

      return false;
    }

    std::string Camera::getSerialNumber() {
      return m_serialNo;
    }

    bool Camera::isStarted() {
      return m_started;
    }

    
    rs2_intrinsics Camera::getRGBIntrinsics() {
      return m_rgbIntrinsics;
    }

    rs2_intrinsics Camera::getDepthIntrinsics() {
      return m_depthIntrinsics;
    }

    rs2_extrinsics Camera::getDepthToColorExtrinsics() {
      return m_depthToColorExtrinsics;
    }

    std::shared_ptr<Frame> Camera::capture() {
      rs2::frameset frames = m_pipe.wait_for_frames();
      //frames = align_to_color->process(frames);

      return std::shared_ptr<Frame>(new Frame(frames));      
    }

    std::vector<std::shared_ptr<Frame>> Camera::recordNumFrames(uint32_t num) {
      std::vector<std::shared_ptr<Frame>> out;

      uint64_t lastFrameNo = 0;
      bool firstFrame = true;
      uint32_t framesRecorded = 0;


      while(framesRecorded < num) {
        rs2::frameset frames = m_pipe.wait_for_frames();
        auto frame = std::shared_ptr<Frame>(new Frame(frames));
        // prevent duplicate frames
        if(!firstFrame) {
          if(lastFrameNo == frame->getRGBFrameNo()) {
            continue;
          }
          
        } else {
          firstFrame = false;
        }

        lastFrameNo = frame->getRGBFrameNo();

        out.push_back(frame);

        framesRecorded++;
      }

      return out;
    }

    std::pair<uint16_t, uint16_t> Camera::getResolution() {
      return std::make_pair(m_width, m_height);
    }

    std::pair<uint16_t, uint16_t> Camera::getDepthResolution() {
      return std::make_pair(m_depthWidth, m_depthHeight);
    }

    uint8_t Camera::getFPS() {
      return m_FPS;
    }

    float Camera::getDepthScale() {
      return m_depthScale;
    }

  }
}