#ifndef KU_CCAL_FILE_RECORDER_THREAD_H
#define KU_CCAL_FILE_RECORDER_THREAD_H
#include <mutex>
#include <thread>
#include <boost/circular_buffer.hpp>
#include <iostream>
#include <chrono>
#include "camera.h"
#include "stdio.h"
#include "frame.h"
#include "frameFileWriter.h"
#include "fileRecorderStats.h"

namespace ku {
  namespace ccal {

    #define BUFFER_CAPACITY 100
    /**
     * Concurrently records from a camera and writes it to a file
     */ 
    class FileRecorderThread {
      
      private:
        Camera* m_camera;
        // the file pointer this thread is writing to
        FILE* m_fp;
        // this mutex can be locked by other threads to pause the recording
        std::mutex* m_controlMutex;
        // used to signal a stop
        bool m_stopped = false;
        // used to signal that the file recorder thread is ready to record
        bool m_ready = false;
        // thread used to record in
        std::thread* m_recordThread;
        // thread used to write to file in
        std::thread* m_writeThread;
        // frame buffer
        boost::circular_buffer<std::shared_ptr<Frame>>* m_buf;
        // buffer read/write mutex
        std::mutex m_bufMutex;
        // stats
        FileRecorderStats m_stats;

      public:
        FileRecorderThread(Camera* cam, FILE* fp, std::mutex* controlMutex);
        ~FileRecorderThread();
        void runRecorder();
        void runWriter();
        void stop();
        void join();
        bool isReady();
        FileRecorderStats getStats();
    };

  }
}

#endif