#include "cameraDiscoverer.h"

namespace ku {
  namespace ccal {

    CameraCollection CameraDiscoverer::find() {
      CameraCollection cs;

      rs2::context cx;
      rs2::device_list devices = cx.query_devices();
      for(auto dev : devices) {
        Camera* c = new Camera(dev, 1920, 1080, 1280, 720, 30);
        //Camera* c = new Camera(dev, 1280, 720, 1280, 720, 30);
        cs.addCamera(c);
      }

      return cs;
    }

  }
}