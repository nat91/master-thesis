#include "fileRecorder.h"

namespace ku {
  namespace ccal {

    FileRecorder::FileRecorder(CameraCollection cams, std::string outPath) {
      m_cams = cams;

      if(m_cams.getCameras().empty()) {
        throw std::runtime_error("No cameras connected!");
      }
      
      setupDirectory(outPath);
      // set outPath to current directory so the paths are correct in the below methods
      if(outPath.empty()) outPath = ".";

      createRecDatFile(outPath);
      setupCameras(outPath);

    }

    FileRecorder::~FileRecorder() {
                  
      for(auto recThread : m_recordingThreads) {
        if(recThread != NULL) delete recThread;
      }

      for(auto m : m_controlMutexes) {
        if(m != NULL) delete m;
      }

    }

    void FileRecorder::setupDirectory(std::string outPath) {
      
      // write files into current directory if no outPath is specified
      if(outPath.empty()) return;

      if(boost::filesystem::exists(outPath)) {
        if(!boost::filesystem::is_directory(outPath)) {
          std::string err = "Path: " + outPath + " is not a directory";
          throw std::runtime_error(err);
        }
      } else {
        if(!boost::filesystem::create_directory(outPath)) {
          std::string err = "Failed to create directory " + outPath;
          throw std::runtime_error(err);
        }
      }

    }

    void FileRecorder::createRecDatFile(std::string outPath) {
      std::ofstream recdatFile;
      recdatFile.open(outPath + "/" + "recordingInfo.recdat");
      
      if(!recdatFile.is_open()) {
        throw std::runtime_error("Failed to open .recdat file for writing");
      }      

      for(auto cam : m_cams.getCameras()) {
        recdatFile << cam->getSerialNumber() << ",";
        recdatFile << cam->getSerialNumber() << ".dat" << std::endl;
      }

      recdatFile.flush();
      recdatFile.close();
      #ifdef DEBUG
      printf("Recdat written! \n");
      #endif

    }

    void FileRecorder::setupCameras(std::string outPath) {
      // create filehandles and control mutexes for each camera
      for(auto c : m_cams.getCameras()) {
        std::string path = outPath + "/" + c->getSerialNumber() + ".dat";
        FILE* recFile = fopen(path.c_str(), "wb");
        if(recFile == NULL) {
          failedToOpenFileError(path);
        }

        // setup control mutexes for each camera
        std::mutex* ctrlMutex = new std::mutex();
        m_controlMutexes.push_back(ctrlMutex);
        // recordings are initially paused
        ctrlMutex->lock();

        FileRecorderThread* recThread = new FileRecorderThread(c, recFile, ctrlMutex);
        m_recordingThreads.push_back(recThread);
      }
    }

    void FileRecorder::record() {
      if(m_stopped) {
        throw std::runtime_error("Cannot start stopped recording");
      }

      if(m_isRecording) return;
      m_isRecording = true;

      // wait for all recorder threads to signal that they are ready
      bool allReady = true;

      #ifdef DEBUG
      printf("Waiting for ready signal \n");
      #endif 

      do {

        allReady = true;

        for(auto recThread : m_recordingThreads) {
          allReady = allReady && recThread->isReady();
        }

      } while(!allReady);

      #ifdef DEBUG
      printf("Ready signal recv \n");
      #endif

      // signal all threads to start recording
      for(auto ctrlMutex : m_controlMutexes) {
        ctrlMutex->unlock();
      }

    }

    void FileRecorder::pause() {
      if(m_stopped) {
        throw std::runtime_error("Cannot pause stopped recording");
      }

      if(!m_isRecording) return;
      // signal all threads to stop recording
      for(auto ctrlMutex : m_controlMutexes) {
        ctrlMutex->lock();
      } 

      m_isRecording = false;
    }

    std::vector<FileRecorderStats> FileRecorder::stop() {

      std::vector<FileRecorderStats> out;

      if(m_stopped) return out;
      m_stopped = true;

      for(auto recThread : m_recordingThreads) {
        recThread->stop();
      }

      if(!m_isRecording) {
        // unlock mutexes so recorder threads can exit
        for(auto ctrlMutex : m_controlMutexes) {
          ctrlMutex->unlock();
        }
      }

      #ifdef DEBUG
      std::cout << "Waiting for recording threads to join" << std::endl;
      #endif
      // wait for threads to join
      for(auto recThread : m_recordingThreads) {
        recThread->join();
        out.push_back(recThread->getStats());
      }
      #ifdef DEBUG
      std::cout << "Recording threads joined!" << std::endl;
      #endif      

      return out;
    }

    bool FileRecorder::isRecording() {
      return m_isRecording;
    }

    void FileRecorder::failedToOpenFileError(std::string path) {
      std::string errMsg = "Failed to open file: " + path;
      throw std::runtime_error(errMsg);
    }


  }
}