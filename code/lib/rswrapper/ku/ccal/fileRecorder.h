#ifndef KU_CCAL_FILE_RECORDER_H
#define KU_CCAL_FILE_RECORDER_H
#include <mutex>
// for debug printf
#include <stdio.h>
#include <boost/filesystem.hpp>
#include <iostream>
#include <fstream>
// own headers
#include "cameraCollection.h"
#include "FileRecorderThread.h"
#include "fileRecorderStats.h"

namespace ku {
  namespace ccal {

    /**
     * Takes a cameraCollection and records to a file
     */ 
    class FileRecorder {
      private:
        CameraCollection m_cams;

        bool m_isRecording = false;
        bool m_stopped = false;
        // vector of mutexes used to pause the recording threads
        std::vector<std::mutex*> m_controlMutexes;
        // vector of the recording threads for each camera
        std::vector<FileRecorderThread*> m_recordingThreads;

        void setupDirectory(std::string outPath);
        void setupCameras(std::string outPath);
        void createRecDatFile(std::string outPath);
        void failedToOpenFileError(std::string path);

      public:
        FileRecorder(CameraCollection cams, std::string outPath);
        ~FileRecorder();
        void record();
        void pause();
        std::vector<FileRecorderStats> stop();
        bool isRecording();

    };

  }
}

#endif