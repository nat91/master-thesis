#ifndef KU_CCAL_FILE_RECORDER_STATS_H
#define KU_CCAL_FILE_RECORDER_STATS_H

#include <cstdint>

namespace ku {
  namespace ccal{

    class FileRecorderStats {

      public:
        uint64_t framesRecorded = 0;
        uint64_t framesDropped = 0;
        uint64_t bytesWritten = 0;
        uint64_t recordingDuration = 0;

    };

  }
}

#endif