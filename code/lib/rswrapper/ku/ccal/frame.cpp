#include "frame.h"

namespace ku {
  namespace ccal {
   

    Frame::Frame(rs2::frameset frames) {

      rs2::video_frame rgb = frames.get_color_frame();
      rs2::depth_frame depth = frames.get_depth_frame();

      m_size_rgb = rgb.get_data_size();
      m_size_depth = depth.get_data_size();

      m_rgb = (uint8_t*) malloc(m_size_rgb);
      m_depth = (uint16_t*) malloc(m_size_depth);

      if(m_rgb == NULL || m_depth == NULL) {
        throw std::runtime_error("Frame malloc failed");
      }

      memcpy(m_rgb, rgb.get_data(), m_size_rgb);
      memcpy(m_depth, depth.get_data(), m_size_depth);

      m_RGBFrameNo = rgb.get_frame_number();
      m_depthFrameNo = depth.get_frame_number();

      m_RGBTimestamp = rgb.get_frame_metadata(RS2_FRAME_METADATA_FRAME_TIMESTAMP);
      m_depthTimestamp = depth.get_frame_metadata(RS2_FRAME_METADATA_FRAME_TIMESTAMP);

      m_width = rgb.get_width();
      m_height = rgb.get_height();
      m_depthWidth = depth.get_width();
      m_depthHeight = depth.get_height();

    }
    
    uint8_t* Frame::getRGB() {
      return m_rgb;
    }

    uint16_t* Frame::getDepth() {
      return m_depth;
    }

    uint32_t Frame::getRGBDataSize() {
      return m_size_rgb;
    }

    uint32_t Frame::getDepthDataSize() {
      return m_size_depth;
    }

    uint64_t Frame::getRGBTimestamp() {
      return m_RGBTimestamp;
    }

    uint64_t Frame::getDepthTimestamp() {
      return m_depthTimestamp;
    }

    uint64_t Frame::getRGBFrameNo() {
      return m_RGBFrameNo;
    }

    uint64_t Frame::getDepthFrameNo() {
      return m_depthFrameNo;
    }


    std::pair<uint16_t, uint16_t> Frame::getResolution() {
      return std::make_pair(m_width, m_height);
    }

    std::pair<uint16_t, uint16_t> Frame::getDepthResolution() {
      return std::make_pair(m_depthWidth, m_depthHeight);
    }

    Frame::~Frame() {

      free(m_rgb);
      free(m_depth);
    }

  }


}
