#include "cameraCollection.h"


namespace ku {
  namespace ccal {

    void CameraCollection::start() {
      for(auto c : m_cams) {
        if(!c->isStarted()) c->start();
      }
    }

    void CameraCollection::stop() {
      for(auto c : m_cams) {
        if(c->isStarted()) c->stop();
      }
    }

    void CameraCollection::addCamera(Camera* c) {
      if(hasCamera(c->getSerialNumber())) {
        std::string err = "Camera with serial number " + c->getSerialNumber() + " already added";
          throw std::runtime_error(err);
      }
      m_cams.push_back(c);
    }

    bool CameraCollection::hasCamera(std::string serialNo) {
      for(auto cam : m_cams) {
        if(cam->getSerialNumber() == serialNo) {
          return true;
        }
      } 
      return false;     
    }

    Camera* CameraCollection::getCamera(uint8_t id) {
      return m_cams[id];
    }

    std::vector<Camera*> CameraCollection::getCameras() {
      return m_cams;
    }

    int CameraCollection::count() {
      return m_cams.size();
    }

    Camera* CameraCollection::operator[](int index) {
      return m_cams[index];
    }

  }
}