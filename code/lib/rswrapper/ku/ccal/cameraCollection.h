#ifndef KU_CCAL_CAMERA_COL_H
#define KU_CCAL_CAMERA_COL_H
#include <vector>
#include "camera.h"

namespace ku {
  namespace ccal {
    /**
     *  Represents a collection of multiple realsense cameras
     */ 
    class CameraCollection {
      
      private:
        std::vector<Camera*> m_cams;
      
      public:
        void start();
        void stop();
        void addCamera(Camera* c);
        bool hasCamera(std::string serialNo);
        Camera* getCamera(uint8_t id);
        std::vector<Camera*> getCameras();
        int count();
        // Overloading [] operator to access cameras with array index syntax
        Camera* operator[](int index);

    };
  }
}

#endif