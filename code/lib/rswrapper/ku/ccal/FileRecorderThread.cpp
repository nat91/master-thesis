#include "FileRecorderThread.h"

namespace ku {
  namespace ccal {

    FileRecorderThread::FileRecorderThread(Camera* cam, FILE* fp, std::mutex* controlMutex) {
      m_camera = cam;
      m_fp = fp;
      m_buf = new boost::circular_buffer<std::shared_ptr<Frame>>(BUFFER_CAPACITY);
      m_controlMutex = controlMutex;

      m_writeThread = new std::thread(&FileRecorderThread::runWriter, this);

    }

    FileRecorderThread::~FileRecorderThread() {

      if(m_buf != NULL) delete m_buf;
      if(m_recordThread != NULL) delete m_recordThread;
      if(m_writeThread != NULL) delete m_writeThread;

      if(m_fp != NULL) fclose(m_fp);
    }

    bool FileRecorderThread::isReady() {
      return m_ready;
    }

    void FileRecorderThread::stop() {
      m_stopped = true;
    }

    void FileRecorderThread::join() {
      if(m_writeThread != NULL && m_writeThread->joinable()) {
        m_writeThread->join();
      }

      if(m_recordThread != NULL && m_recordThread->joinable()) {
        m_recordThread->join();
      }

    }

    FileRecorderStats FileRecorderThread::getStats() {
      return m_stats;
    }

    void FileRecorderThread::runRecorder() {
      // signal that this thread is ready to record
      m_ready = true;
      #ifdef DEBUG
      std::cout << "Camera: " << m_camera->getSerialNumber() << " is ready!";
      std::cout << " Tid: " << std::this_thread::get_id() << std::endl;
      #endif
      bool firstFrame = true;
      uint64_t lastFrameNo = 0;
      uint64_t lastTimeStamp;

      // Loop: if not paused save frames to buffer
      while(!m_stopped) {
        bool wasPaused = false;
        auto time = std::chrono::system_clock::now();
        // acquire control mutex and release it right after
        // this acts as an external "pause" if some external actor locks the mutex
        m_controlMutex->lock();
        m_controlMutex->unlock();

        auto deltaT = std::chrono::system_clock::now() - time;
        auto dMs = std::chrono::duration_cast<std::chrono::microseconds>(deltaT);

        wasPaused = dMs.count() > 500000;

        auto frame = m_camera->capture();
        if(firstFrame) {
          lastFrameNo = frame->getRGBFrameNo();
          lastTimeStamp = frame->getRGBTimestamp();
          firstFrame = false;
        } else if(lastFrameNo == frame->getRGBFrameNo()) {
          // the realsense cameras deliver the same frame more than once sometimes
          // this filters them away.
          continue;
        }

        m_stats.framesRecorded++;
        if(!firstFrame && frame->getRGBFrameNo()-lastFrameNo > 1) {
          m_stats.framesDropped++;
        }

        uint64_t duration = frame->getRGBTimestamp() - lastTimeStamp;
        if(!wasPaused) {
          m_stats.recordingDuration += duration;
        } else {
          m_stats.recordingDuration += duration-dMs.count();
        }

        lastFrameNo = frame->getRGBFrameNo();
        lastTimeStamp = frame->getRGBTimestamp();

        #ifdef DEBUG_CAM
        printf("Camera: %s, frame no: %lu \n", m_camera->getSerialNumber().c_str(),frame->getRGBFrameNo());
        #endif

        m_bufMutex.lock();
        m_buf->push_back(frame);
        m_bufMutex.unlock();
      }
      
      #ifdef DEBUG
      std::cout << "Recorder thread Tid: " << std::this_thread::get_id() << " exited" << std::endl;
      #endif


    }

    void FileRecorderThread::runWriter() {
      // setup - write file header etc.
      FrameFileWriter writer;
      m_stats.bytesWritten += writer.writeHeader(m_fp, m_camera, false);
      
      // start recording thread
      #ifdef DEBUG
      std::cout << "Writer thread Tid: " << std::this_thread::get_id() << " started" << std::endl;      
      #endif
      m_recordThread = new std::thread(&FileRecorderThread::runRecorder, this);
      
      
      auto time = std::chrono::system_clock::now();

      bool stop = false;
      // Loop: wait for frames
      while(!stop) {

        if(m_buf->size() > 0) {
          m_bufMutex.lock();

          auto f = m_buf->front();
          m_buf->pop_front();

          m_bufMutex.unlock();
          
          m_stats.bytesWritten += writer.writeFrame(m_fp, f);

          time = std::chrono::system_clock::now();
        }

        // if stopped, keep looping till buffer is empty
        if(m_stopped) {
          stop = m_buf->size() == 0;
        }

        auto lastActive = std::chrono::system_clock::now() - time;
        auto dMs = std::chrono::duration_cast<std::chrono::milliseconds>(lastActive);

        // if no activity for 100ms, then sleep
        if(dMs.count() >= 100) {
          std::this_thread::sleep_for(std::chrono::milliseconds(100));  
        }
      }


      // Finalize: Rewrite header
      writer.writeHeader(m_fp, m_camera, true);

      #ifdef DEBUG
      std::cout << "Writer thread Tid: " << std::this_thread::get_id() << " exited" << std::endl;      
      #endif      
    }

  }
}