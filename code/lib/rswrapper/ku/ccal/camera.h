#ifndef KU_CCAL_CAMERA_H
#define KU_CCAL_CAMERA_H
#include <cstdint>
#include <string>
#include <vector>
#include <memory>
#include <librealsense2/rs.hpp>
#include "frame.h"

namespace ku {
  namespace ccal {

    /** 
     * Represents a RealSense camera with a single RGB and Depth stream
     */
    class Camera {

      private:
        // general attributes
        std::string m_serialNo;
        bool m_started = false;
        uint16_t m_width;
        uint16_t m_height;
        uint16_t m_depthWidth;
        uint16_t m_depthHeight;
        uint8_t m_FPS;
        float m_depthScale;

        // intrinsic parameters
        rs2_intrinsics m_rgbIntrinsics;
        rs2_intrinsics m_depthIntrinsics;
        // depth to color extrinsics, used for alignment
        rs2_extrinsics m_depthToColorExtrinsics;

        // realsense API object attributes
        rs2::config m_cnf;
        rs2::device m_dev;
        rs2::pipeline m_pipe;
        rs2::pipeline_profile m_profile;
        rs2::align* align_to_color;
      
      public:
        Camera(rs2::device dev, uint16_t width, uint16_t height, uint16_t m_depthWidth, uint16_t m_depthHeight, uint8_t FPS);
        ~Camera();

        void start();
        void stop();
        std::shared_ptr<Frame> capture();
        std::vector<std::shared_ptr<Frame>> recordNumFrames(uint32_t num); 
        std::string getSerialNumber();
        bool isStarted();

        void enableAutoExposure();
        void disableAutoExposure();
        bool autoExposureEnabled();

        bool setGain(float gain);
        bool setExposure(float exposure);
        bool setROI(int xmin, int ymin, int xmax, int ymax);

        std::pair<uint16_t, uint16_t> getResolution();
        std::pair<uint16_t, uint16_t> getDepthResolution();
       
        uint8_t getFPS();
        float getDepthScale();

        rs2_intrinsics getRGBIntrinsics();
        rs2_intrinsics getDepthIntrinsics();
        rs2_extrinsics getDepthToColorExtrinsics();
        
    };
  }

}
#endif