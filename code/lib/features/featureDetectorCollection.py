from inspect import signature
import sys
sys.path.append("../")
from util import Registry
import uuid

class FeatureDetectorCollection:
  """
  Represents a collection of feature detectors, a call to getFeatures will use all registered feature detectors
  to find features in an image.
  """

  def __init__(self):
    self.featureDetectors = []

  def add(self, detector):
    self.featureDetectors.append(detector)
  
  def getFeatures(self, img, depth, K):
    if len(self.featureDetectors) == 0:
      raise RuntimeError("No feature detectors registered")
  
    features = dict()

    uid = str(uuid.uuid1())
    Registry.newCamDict(uid)
    for det in self.featureDetectors:
      features.update(det.getFeatures(img, depth, K))
    
    return features
