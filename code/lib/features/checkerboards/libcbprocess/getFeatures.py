import numpy as np
import matplotlib.pyplot as plt
from skimage.color import rgb2hsv, rgb2gray
from .reorientBoard import reorientBoard
from .extractSquares import extractSquares
from .findBoundingBox import findBoundingBox
from .findOrientation import findOrientation


def getOffset(board, boardDim):
  orientBars = board.orientationBars
  offsetX = 0
  offsetY = 0

  # Case 1: Top visible
  if orientBars["top"] is not None:  
    # at most an x offset if top is visible

    # 1.a
    if board.shape[1] == boardDim[1]:
      # no offset necessary, the entire width is visible
      #print("Case 1a")
      return offsetX, offsetY
    
    # 1.b
    if orientBars["left"] is not None:
      # top and left visible, no offset needed
      #print("Case 1b")
      return offsetX, offsetY 
    
    # 1.c
    if orientBars["right"] is not None:
      # left not visible, x offset needed
      #print("Case 1c")
      offsetX = boardDim[1] - board.shape[1]
      return offsetX, offsetY
    
    # Remaining cases:
    # Top + bottom partially visible, but not left and right - or only top partially visible
    # impossible to determine from which side(s) the offset should be and by how much
    return None
  
  # Case 2: Left visible, top is not
  if orientBars["left"] is not None:
    # top bar is not visible and an y offset may be needed

    # 2.a
    if board.shape[0] == boardDim[0]:
      # entire side is visible, we can return with no offset
      #print("Case 2a")
      return offsetX, offsetY
    
    # 2.b
    if orientBars["bottom"] is not None:
      # bottom is visible and left is partially visible, top is not - y offset needed
      #print("Case 2b")
      offsetY = boardDim[0] - board.shape[0]
      return offsetX, offsetY 
    
    # Remaining cases:
    # left + right partially visible, but not top or bottom.
    # impossible to determine from which side(s) the offset should be and by how much
    return None 
  
  # Case 3: Right visible, top and left are not
  if orientBars["right"] is not None:

    # 3.a
    if board.shape[0] == boardDim[0]:
      # entire side visible, we just need an x offset
      #print("Case 3a")
      offsetX = boardDim[1] - board.shape[1] 
      return offsetX, offsetY
    
    # 3.b
    if orientBars["bottom"] is not None:
      # bottom visible, right side partially visible
      # x and y offset needed
      #print("Case 3b")
      offsetX = boardDim[1] - board.shape[1]
      offsetY = boardDim[0] - board.shape[0]
      return offsetX, offsetY
    
    # Remaining cases:
    # Right partially visible, but nothing else
    # impossible to determine the necessary y offset
    return None 
  
  # Case 4: Only bottom marker visible
  if orientBars["bottom"] is not None:
    # we can only determine an y-offset if the entire side is visible
    # 4.a
    if board.shape[1] == boardDim[1]:
      #print("Case 4a")
      offsetY = boardDim[0] - board.shape[0]
      return offsetX, offsetY

  return None
    

def getFeaturePoints(board, boardDim, identifier, ns):
  features = dict()

  if board.shape == boardDim:
    # case 1, no occlusion
    for j in range(board.shape[0]):
      for k in range(board.shape[1]):
        corId = ns + "_" + identifier + "_" +str(k) + "_" + str(j)
        features[corId] = board[j,k]
    
    return features

  else:
    # determine whether we can use the remaining sides
    # to get a coordinate offset
    offset = getOffset(board, boardDim)
    if offset is None:
      return features
    
    offsetX, offsetY = offset
    for j in range(board.shape[0]):
      for k in range(board.shape[1]):

        # we add the offsets to the labels such that they correspond to the correct place
        # in the checkerboard
        corId = ns + "_" + identifier + "_" +str(k+offsetX) + "_" + str(j+offsetY)

        features[corId] = board[j,k]

      
    return features


def getFeatures(chessboards, img, classifierFun, preprocessFun, ns, boardsInScene):
  
  hImg = rgb2hsv(img)
  gray = rgb2gray(img)
  
  i = 0

  # map of identifier => (x, y)
  features = dict()

  for board in chessboards:
    # find the orientation of the board relative to the camera orientation
    res = findOrientation(board, hImg, img)
    if res is None:
      print("No orientation found for board {}".format(i))
      i += 1
      continue
    
    i += 1
    # reorient the board such that (0,0) is relative to the orientation
    rid = res
    reorientBoard(board, rid)

    print("Board {}:  {}".format(i, rid))
    # extract corner squares for chessboard identification
    whiteSq, blackSq = extractSquares(board, gray, rid)

    votes1 = dict()
    votes2 = dict()

    for sq in whiteSq:
      s1 = preprocessFun(sq)
      vote = classifierFun(s1)
      if vote is None:
        continue

      if vote not in votes1:
        votes1[vote] = 0

      votes1[vote] += 1

    for sq in blackSq:
      s1 = preprocessFun(sq)
      vote = classifierFun(s1)
      if vote is None:
        continue

      if vote not in votes2:
        votes2[vote] = 0

      votes2[vote] += 1


    if len(votes1) == 0 or len(votes2) == 0:
      #print("Board dropped!")
      continue

    # get identifier
    c1 = list(votes1.keys())[np.argmax(list(votes1.values()))]
    c2 = list(votes2.keys())[np.argmax(list(votes2.values()))]

    # Identify board
    board.labelBoard(c1, c2)
    identifier = str(c1) + str(c2)

    if boardsInScene is not None and identifier not in boardsInScene:
      # detected board is not in the image
      print("Found board {}, but it is not in the config file - discarding".format(identifier))
      continue

    print("Found board " + identifier)

    if boardsInScene is not None:
      fp = getFeaturePoints(board, boardsInScene[identifier], identifier, ns)
      if fp is not None:
        for (k,v) in fp.items():
          features[k] = v

    else:
      # fallback in case of no board information
      for j in range(board.shape[0]):
        for k in range(board.shape[1]):
          corId = ns + "_" + identifier + "_" +str(k) + "_" + str(j)
          features[corId] = board[j,k]
  
  return features
