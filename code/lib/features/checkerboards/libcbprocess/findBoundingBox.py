import numpy as np

def findBoundingBox(cb):
  nw = cb.p(0,0)
  ne = cb.p(0, cb.width-1)
  se = cb.p(cb.height-1, cb.width-1)
  sw = cb.p(cb.height-1, 0)

  # nw
  v1 = -(cb[0, 1]-nw) 
  v2 = -(cb[1, 0]-nw) 
  v3 = v1+v2
  v3 = nw+v3
  cnw = [v3[0], v3[1]]


  # ne  
  v1 = -(cb[0, cb.width-2]-ne) 
  v2 = -(cb[1, cb.width-1]-ne) 
  v3 = v1+v2
  v3 = ne+v3
  cne = [v3[0], v3[1]]

  # sw
  v1 = -(cb[cb.height-1, 1]-sw) 
  v2 = -(cb[cb.height-2, 0]-sw) 
  v3 = v1+v2
  v3 = sw+v3  
  csw = [v3[0], v3[1]]

  # se  
  v1 = -(cb[cb.height-1, cb.width-2]-se) 
  v2 = -(cb[cb.height-2, cb.width-1]-se) 
  v3 = v1+v2
  v3 = se+v3  

  cse = [v3[0], v3[1]]

  return np.array([cnw, cne, cse, csw])
