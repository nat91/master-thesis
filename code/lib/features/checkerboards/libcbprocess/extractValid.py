import numpy as np

def extractValid(patch, bb, cval=1.0):
  """
  Extracts valid pixels in a square patch, by setting pixels outside the bounding box to cval

  Args:
    patch (np.array): An image patch
    bb (tuple): A four tuple of a 4 point boundary box as computed by getBoundingBox.
      The bounding box must have its origin at the patch's (0,0)
    cval (float): The constant value to set points to that are outside of the boundary box
  """
  # copy so we don't modify the original image
  patch = np.copy(patch)

  # Create an identity array, ie entry coords[x,y] maps to the coordinate [x,y]
  idx = np.mgrid[0:patch.shape[1], 0:patch.shape[0]]
  coords = np.zeros((patch.shape[1], patch.shape[0], 2))
  coords[:,:,0] = idx[0]
  coords[:,:,1] = idx[1]

  xb0, yb0, xb1, yb1 = bb
  for (v1, v2) in [(xb0, yb0), (yb0, xb1), (xb1, yb1), (yb1, xb0)]:
    l = v1-v2 
    n = -np.array([-l[1], l[0]])
    n /= np.linalg.norm(n)
    c = -n[0]*v1[0]-n[1]*v1[1]
    
    d = np.dot(coords, n)+c
    (xs, ys) = np.where(d <= 0)
    patch[ys, xs] = cval
  
  return patch
