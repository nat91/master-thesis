import matplotlib.pyplot as plt 
import numpy as np

def drawBoundingBox(chessboard, img, bb):
  bb = np.vstack((bb, bb[0,:]))
  plt.imshow(img, cmap="gray")
  plt.plot(bb[:,0], bb[:,1], color="blue")
  plt.show()
