import numpy as np
'''
Helper functions for computing bounding box coordinates.
'''

# triangle area using Heron's formula
def triArea(p1, p2, p3):
  a = np.linalg.norm(p1-p2)
  b = np.linalg.norm(p2-p3)
  c = np.linalg.norm(p3-p1)

  p = (a+b+c)/2.0
  return np.sqrt(p*(p-a)*(p-b)*(p-c))

def getArea(bb):
  (x0, y0, x1, y1) = bb

  return triArea(x0, y1, x1)  + triArea(x1, y0, x0)
  


def getBoundingBox(coords, dim, translate=True, squareBB=None, sortByAngle=True):
  """
   Given an array of 2D coordinates this function computes the minimal four point boundary box that contains all points in "clockwise" order.

   Args:
    coords (np.array): An array of 2D coordinates, ie. of shape (N,2)
    dim (tuple): Tuple of (height, width) - the dimensions of the image
    translate (bool): Whether to translate the returned coordinates to have origin at the square boundary box
    squareBB (tuple): 4-tuple of a precomputed square boundary box, eg. computed by getSquareBoundIngBox. 
    If nothing is passed, this function will call getSquareBoundingBox.
  
   Returns:
    (tuple): Returns four tuple of the form (x0, y0, x1, y1) each entry is a 2D boundary point
  """

  if squareBB is None:
    squareBB = getSquareBoundingBox(coords, dim)

  assert(len(dim) >= 2)
  assert(len(squareBB) == 4)

  h, w = dim[0], dim[1]
  x0, y0, x1, y1 = squareBB

  ind = np.lexsort((coords[:,1],coords[:,0]))
  s = coords[ind]
  points = np.zeros((4,2))

  # greedily select the four corner points
  idx = list(range(s.shape[0]))

  # p1 - smallest x, smallest y
  p1 = np.argmin(s[idx], axis=0)[0]

  points[0] = s[idx][p1]
  del idx[p1]

  # p3 - max x, max y. Last element of s
  p3 = -1
  points[2] = s[idx][p3]
  del idx[p3]

  # p2 - smallest y, largest x
  p2 = np.argmax(s[idx], axis=0)[0]
  points[1] = s[idx][p2]
  del idx[p2]
  
  # p4 - min x, largest y
  p4 = np.argmin(s[idx], axis=0)[-1]
  points[3] = s[idx][p4]

  centroid = np.sum(points, axis=0)/4.0


  if sortByAngle:
    # translate to center before sorting by angles
    points -= centroid
    a = np.arctan2(points[:,1], points[:,0])
    idx = np.argsort(a)
    points += centroid
    
    if translate:
      points -= np.array([x0, y0])

    

    return points[idx[0]], points[idx[1]], points[idx[2]], points[idx[3]]

  if translate:
    points -= np.array([x0, y0])

  return points[0], points[1], points[2], points[3]


def getSquareBoundingBox(coords, dim):
  """
   Given an array of 2D coordinates this function computes the minimal four point square boundary box
   that contains all points.

   Args:
    coords (np.array): An array of 2D coordinates, ie. of shape (N,2)
    dim (tuple): Tuple of (height, width) - the dimensions of the image 
  
   Returns:
    (tuple): Returns four tuple of the form (x0, y0, x1, y1) where x0 is the smallest x coordinate, x1 is the largest and likewise for y0 and y1.
  """
  assert(len(dim) >= 2)
  h, w = dim[0], dim[1]

  x0 = np.maximum(np.min(coords[:,0]), 0)
  x1 = np.minimum(np.max(coords[:,0]), w)
  y0 = np.maximum(np.min(coords[:,1]), 0)
  y1 = np.minimum(np.max(coords[:,1]), h)

  x0 = int(np.round(x0))
  x1 = int(np.round(x1))
  y0 = int(np.round(y0))
  y1 = int(np.round(y1))

  return (x0, y0, x1, y1)
