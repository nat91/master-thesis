import numpy as np
from .boundingBox import getBoundingBox, getSquareBoundingBox, getArea
from .findBoundingBox import findBoundingBox
from .extractValid import extractValid
import matplotlib.pyplot as plt
import copy

def findOrientationBar(b1, b2, imgHsv, img, colorSegFun):

  # distances to search (different scales)
  searchDist = np.array([2.0, 4.0, 8.0, 12.0])
  scores = np.zeros(searchDist.shape[0])

  for i in range(searchDist.shape[0]):
    sd = searchDist[i]
    # search away from the boundary given by points b1 and b2
    v0 = b2-b1
    v0 = v0/np.linalg.norm(v0)
    n0 = -np.array([-v0[1], v0[0]])
    # get search boundary box
    c1 = np.round(b1+n0*sd)
    c2 = np.round(b2+n0*sd)

    b1 = np.round(b1)
    b2 = np.round(b2)
    coords = np.array([c1, c2, b1, b2])

    (x0, y0, x1, y1) = getSquareBoundingBox(coords, imgHsv.shape)
    (xb0, yb0, xb1, yb1) = getBoundingBox(coords, imgHsv.shape, squareBB=(x0, y0, x1, y1))
    

    tc = coords-np.array([x0,y0])

    ## calculate score
    hue = extractValid(imgHsv[y0:y1, x0:x1,0], bb=(xb0, yb0, xb1, yb1), cval=0)
    sat = extractValid(imgHsv[y0:y1, x0:x1,1], bb=(xb0, yb0, xb1, yb1), cval=0)

    # turn into vectors
    hue = hue.flatten()
    sat = sat.flatten()

    N = getArea(bb=(xb0, yb0, xb1, yb1))
    idx = colorSegFun(hue)[0]

    # Having the hue is not enough. Eg. if the saturation is low,
    # then the hue can still be within the valid range, while there is no perceived color.
    satMean = 0.0
    if idx.shape[0] > 0:
      satMean = np.mean(sat[idx]) 

    satScore = 1.0 if satMean > 0.1 else 0.0
    #print("sat:", sat[idx], idx.shape)

    # hue score
    scores[i] = idx.shape[0]/N*satScore



  bestScale = np.argmax(scores)
  # return score
  return scores[bestScale]

def getRID(r):
  rid = "nw-ne"
  if r == 1:
    rid = "ne-se"
  elif r == 2:
    rid = "se-sw"
  elif r == 3:
    rid = "sw-nw" 
  
  return rid

def checkSidesOrderConstraint(output, positions):
  # check the sides constraint, ie. the rids must be some permutation of
  # nw-ne -> ne-se -> se-sw -> sw-nw, that potentially has holes
  validPerm = dict()
  validPerm["nw-ne"] = "ne-se"
  validPerm["ne-se"] = "se-sw"
  validPerm["se-sw"] = "sw-nw"
  validPerm["sw-nw"] = "nw-ne"

  permutationValid = True
  nextValid = None

  for i in range(4):
    
    if output[positions[i]] is None:
      # hole in the permutation
      if nextValid is not None:
        #print("Hole at {}, next {}".format(nextValid, validPerm[nextValid]))
        nextValid = validPerm[nextValid]    
      continue

    if nextValid is not None and output[positions[i]]["rid"] != nextValid:
      # invalid permutation
      permutationValid = False
      break

    curRid = output[positions[i]]["rid"]
    nextValid = validPerm[curRid]
    #print("Cur: {} Next: {}".format(curRid, nextValid))
  
  return permutationValid


def findOrientationBars(bb, imgHsv, img):
  # redish colors, 
  redSegmentFun = lambda hue: np.where( ((hue > 0) & (hue <= 0.1)) | (hue >= 0.8))
  # greenish colors (61deg to 160)
  greenSegmentFun = lambda hue: np.where( ((hue >= 0.16) & (hue <= 0.55)))
  # blueish colors (201deg to 240deg)
  blueSegmentFun = lambda hue: np.where( ((hue >= 0.55) & (hue <= 0.7)))
  # purple colors (241deg to 300deg)
  purpleSegmentFun = lambda hue: np.where( ((hue > 0.66) & (hue < 0.85)))

  bars = [redSegmentFun, greenSegmentFun, purpleSegmentFun, blueSegmentFun]
  positions = ["top", "right", "bottom", "left"]
  output = dict()

  # rows are color scores, columns are the sides
  scores = np.zeros((4,4))

  # store side scores and centroids for each color segmentation
  for i in range(len(bars)):
    hsvSegmentFun = bars[i]
    # nw ne
    s1 = findOrientationBar(bb[0], bb[1], imgHsv, img, hsvSegmentFun)
    # ne se
    s2 = findOrientationBar(bb[1], bb[2], imgHsv, img, hsvSegmentFun)
    # se sw
    s3 = findOrientationBar(bb[2], bb[3], imgHsv, img, hsvSegmentFun)
    # sw nw
    s4 = findOrientationBar(bb[3], bb[0], imgHsv, img, hsvSegmentFun)

    scores[i] = np.array([s1, s2, s3, s4])

  # initialize output
  for j in range(4):
    output[positions[j]] = None

  sidesVisited = set()
  # greedily select sides based on max score
  for k in range(4):
    # r is the id of the best scoring color for this side, j is the side
    (r, j) = np.unravel_index(np.argmax(scores), scores.shape)

    # dont pick the same side more than once
    # this can otherwise happen if multiple side scores are 0
    if j in sidesVisited:
      continue 

    sidesVisited.add(j)

    if scores[r,j] < 0.7:
      # score too low, this side was not detected
      # (less than 70% of the pixels were in the hue range)
      output[positions[r]] = None
    else:
      output[positions[r]] = {
        "rid": getRID(j),
        "score": scores[r, j]
      }

    # make sure the same row/column is not selected twice
    scores[r,:] = 0.0
    scores[:,j] = 0.0
 
  permutationValid = checkSidesOrderConstraint(output, positions)

  if not permutationValid:
    # Error permutation is invalid
    # This is most likely caused by purple and blue being exchanged, try to swap them
    left = output[positions[3]] # 3 is blue position
    bottom = output[positions[2]] # 2 is purple position

    output[positions[3]] = bottom
    output[positions[2]] = left

    permutationValid = checkSidesOrderConstraint(output, positions)
    if permutationValid:
      print("Permutation fixed it!")
      return output

    return None

  return output

def orientationFromBars(orientationBars):
  if orientationBars["top"] is not None:
    return orientationBars["top"]["rid"]
  
  if orientationBars["left"] is not None:
    # top orientation is clockwise
    rid = orientationBars["left"]["rid"]

    if rid == "nw-ne":
      return "ne-se"
    
    if rid == "ne-se":
      return "se-sw"
    
    if rid == "se-sw":
      return "sw-nw"

    if rid == "sw-nw":
      return "nw-ne"
  
  if orientationBars["right"] is not None:
    # top orientation is counter clockwise
    rid = orientationBars["right"]["rid"]

    if rid == "nw-ne":
      return "sw-nw"
    
    if rid == "ne-se":
      return "nw-ne"
    
    if rid == "se-sw":
      return "ne-se"

    if rid == "sw-nw":
      return "se-sw"

  if orientationBars["bottom"] is not None:
    # top orientation is two steps clockwise
    rid = orientationBars["bottom"]["rid"]

    if rid == "nw-ne":
      return "se-sw"
    
    if rid == "ne-se":
      return "sw-nw"
    
    if rid == "se-sw":
      return "nw-ne"

    if rid == "sw-nw":
      return "ne-se"


  return None


def findOrientation(cb, imgHsv, img):
  """
   Finds out at what side of the checkerboard the upwards pointing marker is (or would be).

   Args:
    cb (Chessboard): Chessboard object
    img (np.array): Hue channel of the image the chessboard was found in
  
   Returns:
    (str)|None: The side the upwards pointing location marker is located at, one of: nw-ne, ne-se, se-sw, sw-nw. 
    None is returned if no orientation could be found.
  """

  bb = findBoundingBox(cb)
  orientationBars = findOrientationBars(bb, imgHsv, img)

  if orientationBars is None:
    return None

  rid = orientationFromBars(orientationBars)

  # store orientation bars in checkerboard object
  cb.orientationBars = orientationBars
  cb.orientation = rid 

  return rid
