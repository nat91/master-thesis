import numpy as np

def reorientBoard(cb, rid):
  """
  Given a checkerboard, a rotation id as returned by findOrientatId, the checkerboard is rotated such that the northwest corner and northeast corner are aligned with the rotation marker.

  Args:
    cb (Chessboard): The chessboard to manipulate
    rid (string): The rotation id as returned by findOrientationId
  
  Returns:
    None: The chessboard is manipulated in place

  """
  nw = cb.p(0,0)
  ne = cb.p(0, cb.width-1)
  se = cb.p(cb.height-1, cb.width-1)
  sw = cb.p(cb.height-1, 0)
  c = (nw+ne+se+sw)/4.0

  # four cases depending on the rid
  if rid == "nw-ne":
    # orientation is correct, do nothing
    return
  
  elif rid == "ne-se":
    # right rotation
    cb.rotateRight()
  elif rid == "se-sw":
    # upside down, flip the chessboard
    cb.flip() 
  elif rid == "sw-nw":
    # left rotation 
    cb.rotateLeft()
  else:
    raise RuntimeError("Unknown rid " + str(rid))