import skimage.transform
import numpy as np
import matplotlib.pyplot as plt
import cv2
from .extractValid import extractValid
from .boundingBox import getBoundingBox, getSquareBoundingBox
import skimage.transform as transform
import time

def getSquare(pidx, nyidx, nxidx, cb, c, img, rid, isCorner=False):

  h,w = img.shape[0], img.shape[1]
  p = cb.p(*pidx)
  ny = cb.p(*nxidx)
  nx = cb.p(*nyidx)

  v1 = (nx-p) 
  if isCorner:
    v1 = -v1
  
  v2 = -(ny-p) 
  v3 = v1+v2

  # translate v1, v2 and v3 to the corner position p
  v1 += p
  v2 += p
  v3 += p

  coords = np.array([v3, p, v1, v2])

  # extract square from v3 to p, it is skewed if the checkerboard is rotated
  x0, y0, x1, y1 = getSquareBoundingBox(coords, (h,w))
  patch = img[y0:y1, x0:x1]

  # fill value for pixels outside of the square patch
  cVal = 1.0
  bb = getBoundingBox(coords, (h,w), squareBB=(x0, y0, x1, y1))

  bbA = np.array(bb)

  perm = [0, 1, 2, 3]
  if rid == "ne-se":
    # counter clockwise rotation
    perm = [1, 2, 3, 0]

  elif rid == "se-sw":
    # rotate clockwise twice
    perm = [2, 3, 0, 1]
  
  elif rid == "sw-nw":
    # rotate clockwise once
    perm = [3, 0, 1, 2]


  src_pts = np.array([
    bbA[perm[0]],
    bbA[perm[1]],
    bbA[perm[2]],
    bbA[perm[3]]
  ])

  '''
  if False:
    o = np.array([centroid[0]+5*np.cos(-np.pi/2.0-orient), centroid[1]+5*np.sin(-np.pi/2.0-orient)])
    plt.subplot(1, 2, 1)
    plt.imshow(patch, cmap="gray")
    plt.scatter(centroid[0], centroid[1], label="c")
    plt.scatter(src_pts[0][0], src_pts[0][1], label="bb0")
    plt.scatter(src_pts[1][0], src_pts[1][1], label="bb1")
    plt.scatter(src_pts[2][0], src_pts[2][1], label="bb2")
    plt.scatter(src_pts[3][0], src_pts[3][1], label="bb3")
    plt.scatter(o[0], o[1], label="o")
    plt.scatter(5*xvec[0]+centroid[0], 5*xvec[1]+centroid[1], label="x")
    plt.legend()
    plt.subplot(1, 2, 2)
    plt.imshow(patch, cmap="gray")
    plt.scatter(centroid[0], centroid[1], label="c")
    plt.scatter(bb[0][0], bb[0][1], label="bb0")
    plt.scatter(bb[1][0], bb[1][1], label="bb1")
    plt.scatter(bb[2][0], bb[2][1], label="bb2")
    plt.scatter(bb[3][0], bb[3][1], label="bb3")
    plt.scatter(o[0], o[1], label="o")
    plt.scatter(5*xvec[0]+centroid[0], 5*xvec[1]+centroid[1], label="x")
    plt.legend()
    plt.show()
    exit()
  '''


  dimSrc = np.min([
    np.linalg.norm(src_pts[0]-src_pts[1]), np.linalg.norm(src_pts[1]-src_pts[2]), 
    np.linalg.norm(src_pts[2]-src_pts[3]), np.linalg.norm(src_pts[3]-src_pts[0])]
    )
  
  dimSrc = int(np.round(dimSrc))

  dimDst = np.minimum(27, dimSrc)

  dst_pts = np.array([
    [0,0],
    [dimDst-1,0],
    [dimDst-1, dimDst-1],
    [0, dimDst-1]
  ])

  p = transform.ProjectiveTransform()
  p.estimate(src_pts, dst_pts)

  #h,_ = cv2.findHomography(src_pts, dst_pts)
  #res = cv2.warpPerspective(patch, h, (dimDst,dimDst), order=3, preserve_range=True)
  res = transform.warp(patch, p.inverse, order=3, preserve_range=True, cval=0.0, output_shape=(dimDst, dimDst))

  return res



def isBlackSquare(patch):
  # remove boundaries set by the homography when extracting the patch
  patch[patch == 1.0] = 0.0
  validIdx = np.where(patch != 0.0)  
  # the digit is centered in the patch, find out what the background color roughly is
  # by searching size*size pixels from the top left corner
  # and set the removed boundaries to that color
  y0, x0 = np.min(validIdx[0]), np.min(validIdx[1])
  size = 10
  med = np.median(patch[y0:y0+size, x0:x0+size])  
  return med < 0.3

def extractSquares(cb, img, rid):

  nw = cb.p(0,0)
  ne = cb.p(0, cb.width-1)
  se = cb.p(cb.height-1, cb.width-1)
  sw = cb.p(cb.height-1, 0)

  c = (nw+ne+se+sw)/4.0

  ch = cb.height-1
  cw = cb.width-1

  whiteSquares = []
  blackSquares = []

  # get numbers from left side
  for i in range(ch):
    p = getSquare((i,0), (i+1,0), (i,1), cb, c, img, rid)

    if isBlackSquare(p):
      blackSquares.append(p)
    else:
      whiteSquares.append(p)

  # get numbers from bottom side
  for i in range(cw):
    p = getSquare((ch, i), (ch, i+1), (ch-1, i), cb, c, img, rid)

    if isBlackSquare(p):
      blackSquares.append(p)
    else:
      whiteSquares.append(p)

  # get numbers from right side
  for i in range(ch):
    p = getSquare((i, cw), (i+1,cw), (i,cw-1), cb, c, img, rid)

    if isBlackSquare(p):
      blackSquares.append(p)
    else:
      whiteSquares.append(p)


  # get numbers from top side
  for i in range(0, cw):
    p = getSquare((0, i), (0, i+1), (1, i), cb, c, img, rid)

    if isBlackSquare(p):
      blackSquares.append(p)
    else:
      whiteSquares.append(p)

  # get numbers from the corners
  corners = [
    ((0,0), (1,0), (0,1)),
    ((0, cw), (1,cw), (0, cw-1)),
    ((ch, 0), (ch-1,0), (ch,1)),
    ((ch, cw), (ch,cw-1), (ch-1, cw))
  ]

  for (p1, p2, p3) in corners:
    p = getSquare(p1, p2, p3, cb, c, img, rid, isCorner=True)

    if isBlackSquare(p):
      blackSquares.append(p)
    else:
      whiteSquares.append(p)

  '''
  p1 = getSquare((0,0), (1,0), (0,1), cb, c, img, rid)
  p1n = getSquare((0,1), (1,1), (0,2), cb, c, img, rid)

  p2 = getSquare((0, cw), (1,cw), (0, cw-1), cb, c, img, rid)
  p2n = getSquare((0, cw-1), (1,cw-1), (0, cw-2), cb, c, img, rid)

  p3 = getSquare((ch, cw), (ch-1, cw), (ch,cw-1), cb, c, img, rid)
  p3n = getSquare((ch, cw-1), (ch-1, cw-1), (ch,cw-2), cb, c, img, rid)

  p4 = getSquare((ch, 0), (ch-1,0), (ch,1), cb,  c, img, rid)
  p4n = getSquare((ch, 1), (ch-1,1), (ch,2), cb, c, img, rid)
  '''

  return whiteSquares, blackSquares