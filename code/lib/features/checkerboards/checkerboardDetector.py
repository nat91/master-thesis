from . import pylibcbdetect as d
from . import libcbprocess as p
from .segmentDigit import segmentDigit
from .classifyDigit import classifyDigit
from ..labeledFeatureMatcher import LabeledFeatureMatcher
import sys
sys.path.append('../..')
from util import registry

class CheckerboardDetector:
  """
  Detects multiple checkerboards in an image
  """

  def __init__(self, ns, config=None):
    """
    Initializes a CheckerboardDetector

    Args:
      ns (string): feature label namespace, ie. features must be named ns_something
      config (util.Config): Config file used to read what checkerboards in the image
    """

    self.boardsInImage = None
    if config is not None:
      if "checkerboards" in config:
        self.boardsInImage = config["checkerboards"]


    self.ns = ns


  def detect(self, img):
    return d.findChessBoards(img)

  def getFeatures(self, img, _depth, _K):
    res = self.detect(img)
    if res is None:
      retD = dict()
      retD["checkerboard"] = {
        "features": [],
        "descriptors": dict(),
        "matcher": LabeledFeatureMatcher()
      }

      return retD
    
    chessboards, corners = res

    features = p.getFeatures(chessboards, img, classifyDigit, segmentDigit, self.ns, self.boardsInImage)
    for board in chessboards:
      if board.cubeId and board.boardId:
        registry.Registry.put(board.cubeId + board.boardId, board)

    descriptors = dict()
    fVals = []
    keys = list(features.keys())
    for i in range(len(keys)):
      descriptors[keys[i]] = i
      fVals.append(features[keys[i]])      

    retD = dict()
    retD["checkerboard"] = {
      "features": fVals,
      "descriptors": descriptors,
      "matcher": LabeledFeatureMatcher()
    }

    return retD
