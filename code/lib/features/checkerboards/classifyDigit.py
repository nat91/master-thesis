import matplotlib.pyplot as plt
import numpy as np

def classifyDigit(img):
    try:
      img = trimImage(img)
    except:
      return
    h, w = (img.shape[0], img.shape[1])
    if h > 2.5*w:
        guess = 1
    else:
        hMargin = int(0.2*h)
        wMargin = int(0.3*w)

        top = img[0:hMargin,wMargin:w-wMargin]
        tl = img[hMargin:int(h*0.5), 0:wMargin]
        tr = img[hMargin:int(h*0.5),w-wMargin:w]
        mid = img[int(h*.4):int(h*0.6),wMargin:w-wMargin]
        bl = img[int(h*0.5):h-hMargin,0:wMargin]
        br = img[int(h*0.5):h,w-wMargin:w]
        bot = img[h-hMargin:h,wMargin:w-wMargin]

        tolerance = 4.0
        top_ = np.count_nonzero(top) > top.size / tolerance
        tl_ = np.count_nonzero(tl) > tl.size / tolerance
        tr_= np.count_nonzero(tr) > tr.size / tolerance
        mid_ = np.count_nonzero(mid) > mid.size / tolerance
        bl_ = np.count_nonzero(bl) > bl.size / tolerance
        br_ = np.count_nonzero(br) > br.size / tolerance
        bot_ = np.count_nonzero(bot) > bot.size / tolerance

        digit = [top_, tl_, tr_, mid_, bl_, br_, bot_]

        blank = [False,False,False,False,False,False,False]
        zero = [True,True,True,False,True,True,True]
        two = [True,False,True,True,True,False,True]
        three = [True, False, True, True, False, True, True]
        four = [False,True,True,True,False,True,False]
        fourAlt = [True,True,True,True,False,True,False]
        five = [True,True,False,True,False,True,True]
        six =  [True, True, False, True, True, True, True]
        seven = [True, False, True, False, False, True, False]
        eight = [True,True,True,True,True,True,True]


        digits = [zero, blank, two, three, four, five, six, seven, eight, blank, fourAlt]

        try:
            guess = digits.index(digit)
            if guess == 10:
                guess = 4
        except:
            #print('Classification failed')
            #print('digit: ', digit)
            #plt.imshow(img, cmap='gray')
            #plt.show()
            #plt.imshow(top, cmap='gray')
            #plt.show()
            #plt.imshow(tl, cmap='gray')
            #plt.show()
            #plt.imshow(tr, cmap='gray')
            #plt.show()
            #plt.imshow(mid, cmap='gray')
            #plt.show()
            #plt.imshow(bl, cmap='gray')
            #plt.show()
            #plt.imshow(br, cmap='gray')
            #plt.show()
            #plt.imshow(bot, cmap='gray')
            #plt.show()
            guess = None

    #print(guess)
    #plt.imshow(img, cmap="gray")
    #plt.show()


    return guess

def trimImage(img):
    # argwhere will give you the coordinates of every non-zero point
    true_points = np.argwhere(img)
    # take the smallest points and use them as the top left of your crop
    top_left = true_points.min(axis=0)
    # take the largest points and use them as the bottom right of your crop
    bottom_right = true_points.max(axis=0)
    out = img[top_left[0]:bottom_right[0]+1,  # plus 1 because slice isn't
            top_left[1]:bottom_right[1]+1]  # inclusive
    return out
