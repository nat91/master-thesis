from pylibcbdetect import plotBoards, findChessBoards, plotCorners
from libcbprocess import findOrientation, findOrientationBars, findBoundingBox, reorientBoard, extractSquares
import matplotlib.pyplot as plt
import skimage.io
import skimage.color
import pickle 
import sys
import os




if len(sys.argv) < 2:
  print("Usage: python3 test.py imgNumber")
  exit()

imgNo = str(sys.argv[1])
cacheFile = "cache_{}.dat".format(imgNo)
imgFile = "test_images/{}.png".format(imgNo)

if not os.path.exists(imgFile):
  print("Image does not exist")
  exit()

img = skimage.io.imread(imgFile)

boards = None 
corners = None
if os.path.exists(cacheFile):
  with open(cacheFile, "rb") as f:
    boards, corners = pickle.load(f)
else:
  boards, corners = findChessBoards(img)
  with open(cacheFile, "wb") as f:
    pickle.dump((boards, corners), f)


if len(boards):
  hsv = skimage.color.rgb2hsv(img)
  gray = skimage.color.rgb2gray(img)
  
  '''
  plt.subplot(2, 1, 1)
  plt.imshow(hsv[:,:,0])
  plt.colorbar()
  plt.subplot(2, 1, 2)
  plt.imshow(hsv[:,:,1])
  plt.colorbar()
  plt.show()
  
  bb = findBoundingBox(boards[0])
  res = findOrientationBars(bb, hsv, img)

  for k in res.keys():
    if res[k] is not None:
      print("pos: {}, side: {}, score: {}".format(k, res[k]["rid"], res[k]["score"]))
    else:
      print("pos: {} not detected".format(k))

  '''
  #plotBoards(boards, img)
  


  board = boards[0]
  res = findOrientation(board, hsv, img)
  print(res)
  reorientBoard(board, res)

  boards[0] = board

  #blackSq, whiteSq = extractSquares(board, gray, res)
  boardDim = (4,4)
  boardId = "14"

  features = getFeaturePoints(board, boardDim, boardId, "cb")
  for (k,v) in features.items():
    print("{} = {}".format(k,v))

  plotBoards(boards, img)


  '''
  print(len(blackSq), len(whiteSq))

  i = 1
  for i1 in blackSq:
    plt.subplot(len(blackSq), 1, i)
    i += 1 
    plt.imshow(i1, cmap="gray")
  
  plt.show()

  i = 1
  for i1 in whiteSq:
    plt.subplot(len(whiteSq), 1, i)
    i += 1 
    plt.imshow(i1, cmap="gray")
  
  plt.show()
  '''


else:
  plotCorners(corners, img)