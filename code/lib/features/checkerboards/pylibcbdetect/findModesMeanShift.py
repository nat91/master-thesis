import numpy as np
from .helpers import normpdf
import matplotlib.pyplot as plt

def findModesMeanShift(hist, sigma):
  """
  Finds maxima in a histogram by approximating mean-shift via. histogram smoothing

  Args:
    hist (numpy.array): The inpuit histogram
    sigma (float): Standard deviation of Gaussian kernel used as kernel function

  Returns:
    tuple: (modes, hist_smoothed) an array of pairs [maximaIndex, maxima] and the smoothed histogram is returned
  """
  N = hist.shape[0]
  modes = []
  # the relative range to smooth in/around
  j = np.array(range(int(-np.round(2*sigma)),int(np.round(2*sigma))+1))

  if findModesMeanShift.cache is None:
    findModesMeanShift.cache = normpdf(j, 0, sigma)

  r = np.array(range(N))

  # vectorized creation of first histogram
  idxs = np.tile(j, N).reshape((N, j.shape[0]))
  idxs = (idxs.T + r).T
  idxs = np.mod(idxs, N)
  hist_smoothed = np.sum(hist[idxs]*findModesMeanShift.cache, axis=1)

  # find modes, again we wrap around the ends of the histogram because we are dealing with angles.
  # CHANGED: Removed redundant infinite loop
  for i in range(N):
    # get wraparound neighbours
    j1 = np.mod(i+1, N)
    j2 = np.mod(i-1, N)
    # get wraparound histogram values
    h = hist_smoothed[i]
    h1 = hist_smoothed[j1]
    h2 = hist_smoothed[j2]
    if h > h1 and h > h2: 
      #h is a maxima (mode)
      modes.append([i, h, 0])
  
  modes = np.array(modes)
  # sort by maxima
  descIdx = np.argsort(modes[:, 1])

  modes = modes[descIdx][::-1]

  return (modes, hist_smoothed)


findModesMeanShift.cache = None