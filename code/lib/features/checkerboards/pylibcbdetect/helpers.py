import numpy as np

# non-normalized gaussian
def G(w, h, sigma):
  out = np.zeros((h, w))
  # truncate because we 0 index
  cx, cy = w//2, h//2

  for y in range(h):
    for x in range(w):
      exp = -((x-cx)**2.0 + (y-cy)**2.0)/(2.0*sigma**2.0)
      out[y, x] = np.exp(exp)

  return out


# non-normalized gaussian x derivative
def Gx(w, h, sigma):
  out = np.zeros((h, w))
  # truncate because we 0 index
  cx, cy = w//2, h//2

  for y in range(h):
    for x in range(w):
      exp = -((x-cx)**2.0 + (y-cy)**2.0)/(2.0*sigma**2.0)
      out[y, x] = -(x-cx)*np.exp(exp)

  return np.flip(out)

# non-normalized gaussian y derivative
def Gy(w, h, sigma):
  out = np.zeros((h, w))
  # truncate because we 0 index
  cx, cy = w//2, h//2

  for y in range(h):
    for x in range(w):
      exp = -((x-cx)**2.0 + (y-cy)**2.0)/(2.0*sigma**2.0)
      out[y, x] = -(y-cy)*np.exp(exp)

  return np.flip(out)

# 1d standard deviation probability density
def normpdf(x, mu, sigma):
  return np.exp(-0.5*((x-mu)/sigma)**2.0 )/(sigma*np.sqrt(2.0*np.pi))