import numpy as np
import matplotlib.pyplot as plt
from numpy.linalg import norm

def directionalNeighbour(idx, v, chessboard, corners):
  """
  Finds the closest directional neighbour not already in the chessboard (in direction v) of idx in the corners dictionary.

  Args:
    idx (int): Index of the seed corner
    v (np.array): Direction vector of the edge we are searching along
    chessboard (np.array): Initial 3x3 chessboard hypothesis
    corners (dict): Dictionary of corners as calculated by findCorners in findCorners.py 
  """

  unused = np.array(range(corners["N"]), dtype="int")
  used = chessboard[chessboard != -1]
  unused = np.delete(unused, used)

  if unused.shape[0] == 0:
    return (-1.0, np.inf)

  ## calculate direction and distance to corners not in the chessboard
  # direction vector between the two corners
  dirx = corners["p"][unused] - corners["p"][idx]
  
  # dot product between all directions and v
  # this is positive if the vectors are pointing in the same direction
  dist = dirx[:,0]*v[0]+dirx[:,1]*v[1]
  distLen = dist.shape[0]
  # fix shapes
  distReplicated = np.reshape(np.repeat(dist, 2), (distLen,2))

  # dist*v = direction vector pointing in direction v with the length of 
  # (current_corner - potential neighbour)
  dist_edge = dirx - distReplicated*v
  dist_edge = np.sqrt(dist_edge[:,0]**2.0+dist_edge[:,1]**2.0)
  dist_point = dist
  # remove vectors not pointing in the same direction
  dist_point[dist_point<0] = np.inf

  # best neighbour, the 5* factor causes it to weight points further away less
  dist_weighted = dist_point+5.0*dist_edge
  min_idx = np.argmin(dist_weighted)

  return (unused[min_idx], dist_weighted[min_idx])



def initChessboard(corners, idx, dim=(3,3)):
  """
  Creates a "dim" chessboard hypothesis seeded with corner idx. 
  The initial seed dimensions must be > 1

  Args:
    corners (dict): Dictionary of corners as calculated by findCorners in findCorners.py
    idx (int): Index of the seed corner
    dim (int, int): Pair of the chessboard dimensions: (y,x), both must be greater than one
  
  Returns:
    np.array|None: Returns a 3x3 chessboard hypothesis. None is returned if the Hypothesis is poor.

  """
  # initial hypothesis
  chessboard = np.zeros(dim, dtype="int")*-1
  
  # v1 and v2 are the edge orientation estimates and thus the orientations we should look in for neighbours
  v1 = corners["v1"][idx]
  v2 = corners["v2"][idx]

  # dimensions
  cy, cx = dim[0]//2, dim[1]//2

  # count number of x neighbours
  xn = 0
  if cx-1 >= 0:
    xn += 1

  if cx+1 < dim[1]:
    xn += 1
  
  # count number of y neighbours

  # array of potential y neighbours, each entry is a triple of (y, x, v, sy, sx)
  # where y,x is the relative coordinate of the neighbour and v is the search direction and sy, sx
  # is the entry into chessboard it will search from
  yNeighbours = [
    (cy-1, cx, -v2, cy, cx), # top
    (cy+1, cx, v2, cy, cx),  # bottom
    (cy-1, cx-1, -v2, cy, cx-1), # top left
    (cy-1, cx+1, -v2, cy, cx+1), # top right
    (cy+1, cx-1, v2, cy, cx-1),  # bottom left
    (cy+1, cx+1, v2, cy, cx+1)] # bottom right

  validNeighbours = []
  yn = 0
  for (y, x, v, sy, sx) in yNeighbours:
    if y >= 0 and y < dim[0] and x >= 0 and x < dim[1]:
      yn += 1
      validNeighbours.append((y, x, v, sy, sx))
    

  chessboard[cy,cx] = idx # set center to be the current element
  # array of distances to neighbours in the x direction
  dist1 = np.zeros(xn)
  # array of distances to neighbours in the y direction
  dist2 = np.zeros(yn)

  # find left/right neighbours
  if cx-1 >= 0:
    chessboard[cy,cx-1], dist1[0] = directionalNeighbour(idx, -v1, chessboard, corners)
  
  if cx+1 < dim[1]:
    chessboard[cy,cx+1], dist1[1] = directionalNeighbour(idx, v1, chessboard, corners)

  for i in range(len(validNeighbours)):
    y, x, v, sy, sx = validNeighbours[i]
    chessboard[y,x], dist2[i] = directionalNeighbour(chessboard[sy, sx], v, chessboard, corners)

  '''
  chessboard[cy-1,cx], dist2[0] = directionalNeighbour(idx, -v2, chessboard, corners)
  chessboard[cy+1,cx], dist2[1] = directionalNeighbour(idx, v2, chessboard, corners)

  # find top-left/top-right/bottom-left/bottom-right neighbours
  chessboard[cy-1,cx-1], dist2[2] = directionalNeighbour(chessboard[cy,cx-1], -v2, chessboard, corners) # top of left
  chessboard[cy-1,cx+1], dist2[3] = directionalNeighbour(chessboard[cy,cx+1], -v2, chessboard, corners) # top of right
  chessboard[cy+1,cx-1], dist2[4] = directionalNeighbour(chessboard[cy,cx-1], v2, chessboard, corners) # bottom of left
  chessboard[cy+1,cx+1], dist2[5] = directionalNeighbour(chessboard[cy,cx+1], v2, chessboard, corners) # bottom of right
  '''

  # if the chessboard is not homogeneously distributed, fail.
  if np.any(np.isinf(dist1)) or np.any(np.isinf(dist2)) or np.std(dist1)/np.mean(dist1) > 0.3 or np.std(dist2)/np.mean(dist2) > 0.3:
    return None 
  
  return chessboard


# first loop in chessboardEnergy vectorized
def getRowEnergy(chessboard, corners):
  grid = np.mgrid[0:chessboard.shape[0], 0:chessboard.shape[1]-2]
  coords = np.zeros((grid.shape[1]*grid.shape[2], 2), dtype="int")
  coords[:,0] = grid[0].reshape(grid.shape[1]*grid.shape[2]) #js
  coords[:,1] = grid[1].reshape(grid.shape[1]*grid.shape[2]) #ks


  xs = np.zeros((chessboard.shape[0]*(chessboard.shape[1]-2),3), dtype="int")

  xs[:,0] = coords[:,1]
  xs[:,1] = xs[:,0]+1
  xs[:,2] = xs[:,0]+2

  ys = coords[:,0].repeat(3).reshape((coords.shape[0], 3) ).ravel()
  xs = xs.ravel()

  elms = corners["p"][chessboard[ys, xs]].reshape((coords.shape[0], 3, 2))
  E_curs = np.max(norm(elms[:,0] + elms[:,2] - 2*elms[:,1], axis=1)/norm(elms[:,0]-elms[:,2], axis=1))

  return E_curs


# second loop in chessboardEnergy vectorized
def getColumnEnergy(chessboard, corners):
  grid = np.mgrid[0:chessboard.shape[1], 0:chessboard.shape[0]-2]
  coords = np.zeros((grid.shape[1]*grid.shape[2], 2), dtype="int")
  coords[:,0] = grid[0].reshape(grid.shape[1]*grid.shape[2]) #js
  coords[:,1] = grid[1].reshape(grid.shape[1]*grid.shape[2]) #ks

  xs = np.zeros(((chessboard.shape[0]-2)*chessboard.shape[1],3), dtype="int")
    
  xs[:,0] = coords[:,1]
  xs[:,1] = xs[:,0]+1
  xs[:,2] = xs[:,0]+2

  ys = coords[:,0].repeat(3).reshape((coords.shape[0], 3) ).ravel()
  xs = xs.ravel()

  elms = corners["p"][chessboard[xs, ys]].reshape((coords.shape[0], 3, 2))  
  
  E_curs = np.max(norm(elms[:,0] + elms[:,2] - 2*elms[:,1], axis=1)/norm(elms[:,0]-elms[:,2], axis=1))

  return E_curs

def getEdgeEnergy(chessboard, corners):

  E_edge = 0.0
  
  for i in range(chessboard.shape[0]-1):
    for j in range(chessboard.shape[1]-1):
      right = norm(corners["p"][chessboard[i,j]] - corners["p"][chessboard[i,j+1]])
      down = norm(corners["p"][chessboard[i,j]] - corners["p"][chessboard[i+1,j]])

      eMin = np.minimum(right, down)
      eMax = np.maximum(right, down)

      E_edge = np.maximum(eMax/eMin, E_edge)
  
  # if the edge length ratio is acceptable
  # reduce the energy function and otherwise increase it
  if E_edge < 1.5:
    E_edge = -(1.5 - E_edge)
  
  return E_edge


def chessboardEnergy(chessboard, corners):
  """
  Calculates the structural energy for the chessboard. Smaller energy is better.

  Args:
    chessboard (np.array): Chessboard array to calculate energy for
    corners (dict): Dictionary of corners as calculated by findCorners in findCorners.py

  Returns:
    float: The energy of the chessboard
  """
  # This term ensures we prefer larger chessboards over smaller ones, if there is an overlap.
  E_corners = -chessboard.shape[0]*chessboard.shape[1]

  # the better two corners are at predicting a third chessboard corner, the smaller the energy
  # we calculate this over all rows and over all columns. E_structure is 0 if the prediction is perfect,
  # ie. if there is no distortion and the corners are equally spaced
  E_structure = 0

  # infinite energy if the chessboard has a corner more than once
  if np.unique(chessboard).shape[0] != chessboard.shape[0]*chessboard.shape[1]:
    return np.inf

  E_curs1 = 0.0
  E_curs2 = 0.0

  if chessboard.shape[1] >= 3:
    E_curs1 = getRowEnergy(chessboard, corners)

  if chessboard.shape[0] >= 3:
    E_curs2 = getColumnEnergy(chessboard, corners)
  
  E_structure = np.maximum(E_curs1, E_curs2)

  E_edge = 0
  if chessboard.shape[0] < 3 or chessboard.shape[1] < 3:
    E_edge = getEdgeEnergy(chessboard, corners)

  # if there is just one bad prediction, this becomes positive
  return E_corners + chessboard.shape[0]*chessboard.shape[1]*E_structure + E_edge


def predictCorners(p1, p2, p3):
  """
  Uses the corners in the 3 nearest rows (columns) to predict the location of a new row/column 

  Args:
    p1 (np.array): Corner points in the furthest away row(column)
    p2 (np.array): Corner points in the second furthest away row(column)
    p3 (np.array): Corner points in the closest row(column) to the predicted one
  
  Returns:
    np.array: Array of predicted corner locations for the new row(column)

  """
  v1 = p2-p1 
  v2 = p3-p2

  # predict angles
  a1 = np.arctan2(v1[:,1], v1[:,0])
  a2 = np.arctan2(v2[:,1], v2[:,0])
  a3 = 2.0*a2-a1


  # predict scales
  s1 = np.sqrt(v1[:,0]**2.0+v1[:,1]**2.0)
  s2 = np.sqrt(v2[:,0]**2.0+v2[:,1]**2.0)
  s3 = 2.0*s2-s1


  # predict the new points (the factor of 0.75 ensures that under extreme distortion)
  # the closer prediction is selected. (it makes the scale weigh less, scale is poorly preserved under extreme distortion)
  pred = 0.75*s3*np.ones((2, s3.shape[0])) * np.array([np.cos(a3),np.sin(a3)])

  return p3 + pred.T

def assignClosestCorners(cand, pred):
  """
  From a list of potential corner candidates and a list of predictions, select the corners closest to the predictions

  Args:
    cand (np.array): Array of corner coordinates
    pred (np.array): Array of corner coordinate predictions
  
  Returns:
    np.array: Array of same same as pred with the predicted closest corners
  """
  
  # if there's not enough candidates point to pick from, return
  if cand.shape[0] < pred.shape[0]:
    return None

  
  # build distance matrix
  # entry (a, b) is the distance from candidate a to prediction b
  # hence we can minimize over the columns to find the closest matches to a prediction b
  D = np.zeros((cand.shape[0], pred.shape[0]))
  
  for i in range(pred.shape[0]):
    delta = cand - np.ones((cand.shape[0], 2))*pred[i,:]
    D[:,i] = np.sqrt(delta[:,0]**2.0+delta[:,1]**2.0)
  
  # greedily search for closest match without replacement
  closest = np.zeros(pred.shape[0], dtype="int")
  for i in range(pred.shape[0]):
    r,c = np.unravel_index(np.argmin(D, axis=None), D.shape)

    closest[c] = r 
    # make sure this row/column is not picked again
    D[r,:] = np.inf
    D[:,c] = np.inf

  return np.reshape(closest, (1, pred.shape[0]))


def growChessboard(chessboard, corners, border_type):
  """
  Attempts to grow the chessboard to the west, north, south or east

  Args:
    chessboard (numpy.array): Current chessboard array
    corners (dict): Dictionary of corners as calculated by findCorners in findCorners.py
    border_type (char): The direction to grow the chessboard, takes value W, N, S or E

  Returns:
    np.array: The grown chessboard
  """
  if chessboard is None:
    return None 

  assert(border_type in range(4))
  
  p = corners["p"]

  N1 = chessboard.shape[0]-1
  N2 = chessboard.shape[1]-1

  unused = np.array(range(corners["N"]))
  used = chessboard[chessboard != -1]
  unused = np.delete(unused, used)

  candidates = p[unused,:]

  if border_type == 0:

    if N2 < 2:
      return None

    pred = predictCorners(p[chessboard[:,N2-2]], p[chessboard[:,N2-1]], p[chessboard[:,N2]])
    idx = assignClosestCorners(candidates, pred)

    if idx is not None:
      return np.hstack((chessboard, unused[idx].T))
  elif border_type == 1:

    if N1 < 2:
      return None

    pred = predictCorners(p[chessboard[N1-2,:]], p[chessboard[N1-1,:]], p[chessboard[N1,:]])
    idx = assignClosestCorners(candidates, pred)

    if idx is not None:
      return np.vstack((chessboard, unused[idx]))  
  
  elif border_type == 2:
      
      if N2 < 2:
        return None

      pred = predictCorners(p[chessboard[:,2]], p[chessboard[:,1]], p[chessboard[:,0]])
      idx = assignClosestCorners(candidates, pred)

      if idx is not None:
        return np.hstack((unused[idx].T, chessboard))
  else:

    if N1 < 2:
      return None

    pred = predictCorners(p[chessboard[2,:]], p[chessboard[1,:]], p[chessboard[0,:]])
    idx = assignClosestCorners(candidates, pred)

    if idx is not None:
      return np.vstack((unused[idx], chessboard))


def plotBoard(chessboard, corners, img):
  idx = chessboard.flatten()
  plt.imshow(img, cmap="gray")
  plt.scatter(corners["p"][idx,0], corners["p"][idx,1], facecolors="none", edgecolors="r")
  plt.show()

def chessboardsFromCorners(corners, img, verbose=True):  

  if corners["N"] < 4:
    # not enough corners
    return None

  if verbose:
    print("Structure recovery:")
  
  chessboards = []

  p = corners["p"]

  seeds = [(3,3), (2,3), (3,2)]

  for k in range(len(seeds)):
    for i in range(corners["N"]):

      #output status
      if verbose and (np.mod(i+1, 100) == 0 or i == corners["N"]-1):
        print("{}/{}".format(i+1, corners["N"]))
      
      # find checkboard hypothesis with this corner as seed
      chessboard = initChessboard(corners, i, seeds[k])

      if chessboard is None:
        #print("Invalid init")
        continue 

      E = chessboardEnergy(chessboard, corners)
      if E > 0:
        #print("Too high energy", E)
        continue

      bestEnergy = E

      while True:
        # try to grow the chessboard while the energy improves
        curBestE = bestEnergy

        for border in range(4):
          board = growChessboard(chessboard, corners, border)
          
          if board is None: continue 

          E_board = chessboardEnergy(board, corners)
          if E_board < bestEnergy:
            bestEnergy = E_board
            chessboard = board
        
        # no improvement by expansion, stop.
        if curBestE == bestEnergy:
          break
      
      
      # if chessboard has low energy, we keep it if it has lower energy
      # than any other overlapping chessboard

      # NOTE: the -10 is what causes 3x3 boards to not work, requires perfect structural energy (unrealistic)
      # FIX: BELOW
      maxPixelError = 0.2
      chessBoardElms = chessboard.shape[0]*chessboard.shape[1]
      energyThresh = -chessBoardElms + maxPixelError*chessBoardElms
      if not bestEnergy < energyThresh:
        #print("DROP: E = {}, dim = {}x{}".format(bestEnergy, chessboard.shape[0], chessboard.shape[1]))
        continue

      if len(chessboards) == 0:
        #print("First board")
        #plotBoard(chessboard, corners, img)
        chessboards.append(chessboard)
        continue 
      
      hasOverlap = False
      discard = False 
      # check for overlaps
      overlap = np.zeros((len(chessboards),2))
      curIdx = set(chessboard.flatten())
      for j in range(len(chessboards)):
        tarIdx = set(chessboards[j].flatten())
        # overlap
        if len(tarIdx & curIdx):
          overlap[j,0] = 1
          overlap[j,1] = chessboardEnergy(chessboards[j], corners)
          hasOverlap = True 

          # early stopping
          if overlap[j,1] < bestEnergy:
            discard = True
            break 
      
      if discard:
        continue

      if not hasOverlap:
        #print("No overlap")
        chessboards.append(chessboard)
        #plotBoard(chessboard, corners, img)
      else:
        #print("Better energy", bestEnergy, [chessboardEnergy(chessboards[x], corners) for x in range(len(chessboards)) if overlap[x,0]])
        chessboards = [chessboards[x] for x in range(len(chessboards)) if not overlap[x,0] ]
        chessboards.append(chessboard)
        #plotBoard(chessboard, corners, img)


  return chessboards
    


    



      


    


      