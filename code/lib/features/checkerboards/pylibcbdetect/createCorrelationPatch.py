import numpy as np
import matplotlib.pyplot as plt
from .helpers import normpdf


def createCorrelationPatch(angle_1, angle_2, radius):
  """
    Given two angles and a radius, a corner correlation patch is created.

    Args:
      angle_1 (float): The first angle in radians
      angle_2 (float): The second angle in radians
      radius (float): The radius of the correlation patch in circles

    Returns:
      dict: A dictionary containing four corner prototypes for the given angles and radius
  """
  width = int(radius*2.0)
  height = int(radius*2.0)

  template = dict()
  template["a1"] = np.zeros((height, width))
  template["a2"] = np.zeros((height, width))
  template["b1"] = np.zeros((height, width))
  template["b2"] = np.zeros((height, width))   

  # mid points
  mx = radius
  my = radius

  # normals from angles
  n1 = np.array([-np.sin(angle_1), np.cos(angle_1)])
  n2 = np.array([-np.sin(angle_2), np.cos(angle_2)])

  ## vectorized loop
  grid = np.mgrid[0:width, 0:height]
  coords = np.zeros((grid.shape[1]*grid.shape[2], 2), dtype="int")
  coords[:,0] = grid[0].reshape(grid.shape[1]*grid.shape[2])
  coords[:,1] = grid[1].reshape(grid.shape[1]*grid.shape[2])

  vecs = coords - np.array([mx, my])
  dists = np.linalg.norm(vecs, axis=1)

  s1s = np.sign(np.dot(vecs, n1))
  s2s = np.sign(np.dot(vecs, n2))

  idxA1 = np.where((s1s == -1) & (s2s == -1))[0]
  idxA2 = np.where((s1s == 1) & (s2s == 1))[0]
  idxB1 = np.where((s1s == -1) & (s2s == 1))[0]
  idxB2 = np.where((s1s == 1) & (s2s == -1))[0]

  template["a1"][coords[idxA1, 1], coords[idxA1, 0]] = normpdf(dists[idxA1], 0, radius/2.0)
  template["a2"][coords[idxA2, 1], coords[idxA2, 0]] = normpdf(dists[idxA2], 0, radius/2.0)
  template["b1"][coords[idxB1, 1], coords[idxB1, 0]] = normpdf(dists[idxB1], 0, radius/2.0)
  template["b2"][coords[idxB2, 1], coords[idxB2, 0]] = normpdf(dists[idxB2], 0, radius/2.0)

  s1 = np.sum(template["a1"])
  s2 = np.sum(template["a2"])
  s3 = np.sum(template["b1"])
  s4 = np.sum(template["b2"])


  if not np.abs(s1) <= 1e-4:
    template["a1"] = template["a1"]/s1
  
  if not np.abs(s2) <= 1e-4:
    template["a2"] = template["a2"]/s2 
  
  if not np.abs(s3) <= 1e-4:
    template["b1"] = template["b1"]/s3
  
  if not np.abs(s4) <= 1e-4:
    template["b2"] = template["b2"]/s4  

  return template