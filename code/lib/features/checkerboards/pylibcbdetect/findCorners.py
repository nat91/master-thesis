import skimage as sk
from scipy.signal import fftconvolve
from skimage.feature import corner_peaks
from skimage import exposure
import numpy as np
import time
import matplotlib.pyplot as plt
import multiprocessing
from multiprocessing import Pool
from .helpers import Gx, Gy
from .createCorrelationPatch import createCorrelationPatch
from .refineCorners import refineCorners
from .scoreCorners import scoreCorners


def ffts(args):
  img = args[0]
  template = args[1]
  return fftconvolve(img, template, mode="same")

def findCorners(img, tau, verbose=True):
  """
  Takes an image and finds potential chessboard corner points

  Args:
    img (numpy.ndarray): Image array of shape (height, width). If the image is an RGB image, it will be converted to grayscale automatically.
    tau (float): Threshold at which to remove corners with low score
    refine_corners (bool): Whether to refine corners for subpixel accuracy or not. (Default True)

  Returns:
    corners: A list of potential checkerboard corners
  """

  t1 = time.time()
  if not img.dtype in ["float", "float32", "float64"]:
    img = sk.img_as_float(img)

  if len(img.shape) == 3:
    img = sk.color.rgb2gray(img)
  
  # Corner radiuses, 3 scales is used by default
  radius = np.array([4.0, 8.0, 12.0])

  # Calculate derivatives by convolution with Gaussian derivatives (for principal axes estimation)
  # CHANGED: Use Gaussian Derivatives to calculate derivatives, this is usually better at preventing noisy derivatives than Sobel filters.
  img_dx = fftconvolve(img, Gx(7, 7, 1.0), mode="same")
  img_dy = fftconvolve(img, Gy(7, 7, 1.0), mode="same")

  img_angle = np.arctan2(img_dy, img_dx)
  # Calculate gradient magnitude
  img_weight = np.sqrt(img_dx**2.0+img_dy**2.0)

  # Correct angles to lie between [0,pi]
  img_angle[img_angle<0] = img_angle[img_angle<0]+np.pi
  img_angle[img_angle>np.pi] = img_angle[img_angle>np.pi]-np.pi


  imgBefore = img 

  # scale input image
  img_min = np.min(img)
  img_max = np.max(img)
  img = (img-img_min)/(img_max-img_min)


  # CHANGED: As a change to the original code, we do not need to scale - skimage does it for us in img_as_float

  template_props = []
  # define corner template properties at the defined scales
  for r in radius:
    # 90 degree and 45 degree corners for the given scale
    template_props.append([0, np.pi/2.0, r])
    template_props.append([np.pi/4.0, -np.pi/4.0, r])
  
  if verbose: print("Filtering ...")

  # Contains the correlation filter responses
  img_corners = np.zeros(img.shape)
  
  # spawn worker threads
  cpus = multiprocessing.cpu_count()
  threads = Pool(cpus)
  
  args = []
  for (angle_1, angle_2, r) in template_props:
    template = createCorrelationPatch(angle_1, angle_2, r)
    args += [(img, template["a1"]), (img, template["a2"]), (img, template["b1"]), (img, template["b2"])]

  res = threads.map(ffts, args)
  threads.close()

  for i in range(len(template_props)):

    img_corners_a1 = res[i*4]
    img_corners_a2 = res[i*4+1]
    img_corners_b1 = res[i*4+2]
    img_corners_b2 = res[i*4+3]
    # Compute mean responses
    img_corners_mu = (img_corners_a1+img_corners_a2+img_corners_b1+img_corners_b2)/4.0

    # in the below: keeping smallest difference from the mean => maximum response
  
    # case 1: a=white, b=black
    img_corners_a = np.minimum(img_corners_a1-img_corners_mu, img_corners_a2-img_corners_mu)
    img_corners_b = np.minimum(img_corners_mu-img_corners_b1, img_corners_mu-img_corners_b2)
    img_corners_1 = np.minimum(img_corners_a, img_corners_b)

    # case 2: a=black, b=white
    img_corners_a = np.minimum(img_corners_mu-img_corners_a1, img_corners_mu-img_corners_a2)
    img_corners_b = np.minimum(img_corners_b1-img_corners_mu, img_corners_b2-img_corners_mu)
    img_corners_2 = np.minimum(img_corners_a, img_corners_b)

    # update the corner map such that the maximum response is preserved
    img_corners = np.maximum(img_corners, img_corners_1)
    img_corners = np.maximum(img_corners, img_corners_2)

  # corner struct with fields "p" (position) and "v1", "v2" the dominant edge directions.
  corners = dict()

  # extract corner candidates via. non maximum suppression

  thresh_corner = 0.05
  #plt.imshow(img_corners, cmap="gray")
  #plt.show()

  # CHANGED: Tweaked threshold due to numerical differences between numpy/matlab function implementations
  #corners["p"] = nonMaximumSuppression(img_corners, 3, 0.2, 5)
  peaks = corner_peaks(img_corners, min_distance=3, threshold_abs=thresh_corner)
  corners["p"] = np.zeros((peaks.shape[0], 2))
  corners["p"][:,0] = peaks[:,1]
  corners["p"][:,1] = peaks[:,0]

  #plt.imshow(img, cmap="gray")
  #plt.scatter(corners["p"][:,0], corners["p"][:,1])
  #plt.show()

  N = corners["p"].shape[0]
  corners["N"] = N
  # initialize corner dominant edge orientations, if an orientation is 0 it is invalid.
  corners["v1"] = np.zeros((N, 2))
  corners["v2"] = np.zeros((N, 2))
  corners["score"] = np.zeros(N)

  if verbose: print("Refining ...")
  # CHANGED: no need to return anything, dicts are by reference in python.
  refineCorners(img_dx, img_dy, img_angle, img_weight, corners, 10)
  #print("Det:", time.time()-t1)

  # remove corners without edges
  idx = np.where((corners["v1"][:, 0] == 0) & (corners["v1"][:, 1] == 0))
  if idx[0].shape[0] > 0:
    corners["p"]  = np.delete(corners["p"], idx, axis=0)
    corners["v1"] = np.delete(corners["v1"], idx, axis=0)
    corners["v2"] = np.delete(corners["v2"], idx, axis=0)
    corners["score"] = np.delete(corners["score"], idx, axis=0)

  N = corners["p"].shape[0]
  corners["N"] = N

  #plt.imshow(img, cmap="gray")
  #plt.title("Post Refine")
  #plt.scatter(corners["p"][:,0], corners["p"][:,1], facecolors="none", edgecolors="r")
  #plt.show()

  if corners["p"].shape[0] == 0:
    return None  

  #plt.imshow(img, cmap="gray")
  #plt.scatter(corners["p"][:,0], corners["p"][:,1], edgecolors="r", facecolors="none")
  #plt.show()

  if verbose: print("Scoring ...")
  # CHANGED: no need to return anything, dicts are by reference in python.
  # CHANGED: Removed unused parameter img_angle
  scoreCorners(img, img_weight, corners, radius)

  # remove corners with low score
  idx = np.where(corners["score"] < tau)
  if idx[0].shape[0] > 0:
    corners["p"]  = np.delete(corners["p"], idx, axis=0)
    corners["v1"] = np.delete(corners["v1"], idx, axis=0)
    corners["v2"] = np.delete(corners["v2"], idx, axis=0)
    corners["score"] = np.delete(corners["score"], idx, axis=0)
  

  N = corners["p"].shape[0]
  corners["N"] = N

  #plt.imshow(img, cmap="gray")
  #plt.title("Post Score")
  #plt.scatter(corners["p"][:,0], corners["p"][:,1], edgecolors="r", facecolors="none")
  #plt.show()

  if corners["p"].shape[0] == 0:
    return None

  # make v1[0]+v1[1] positive
  idx = (corners["v1"][:,0] + corners["v1"][:,1]) < 0
  corners["v1"][idx] = -corners["v1"][idx] 

  # make all coordinate systems right-handed (reduces matching ambiguities from 8 to 4)
  corners_n1 = np.array([corners["v1"][:,1], -corners["v1"][:,0]]).T

  flip = -np.sign(corners_n1[:,0]*corners["v2"][:,0]+corners_n1[:,1]*corners["v2"][:,1])
  
  fliptwo = np.ones((flip.shape[0], 2))
  fliptwo[:,0] = flip 
  fliptwo[:,1] = flip

  corners["v2"] = corners["v2"]*fliptwo

  return corners