
import numpy as np
import copy

class Chessboard:

  def __init__(self, chessboard, corners):
    self.chessboard = chessboard
    self.corners = corners
    self.shape = chessboard.shape
    self.width = self.shape[1]
    self.height = self.shape[0]
    self._correctBoard()

    self.orientationBars = dict()
    self.orientation = None
    self.cubeId = None
    self.boardId = None

  def labelBoard(self, cubeId, boardId):
    self.cubeId = cubeId
    self.boardId = boardId

  def deepCopy(self):
    return copy.deepcopy(self)

  def rotateLeft(self):
    # need to rotate ninety degrees left
    # equivalent to transposing and flipping over y = 0  
    self.chessboard = np.flip(self.chessboard.T, axis=1)
    self._recalculateDim()
    

  def flip(self):
    # its upside down and mirrored, flip over both axes
    self.chessboard = np.flip(self.chessboard)
    self._recalculateDim()

  def flip1(self):
    # flip left to right
    self.chessboard = np.flip(self.chessboard, 1)
    self._recalculateDim()

  def rotateRight(self):
    # need to rotate 90 degrees right
    # equivalent to flipping over y = 0 and transposing
    self.chessboard = np.flip(self.chessboard, axis=1).T
    self._recalculateDim()

  def _recalculateDim(self):
    self.shape = self.chessboard.shape
    self.width = self.shape[1]
    self.height = self.shape[0]


  # this function ensures that the NW corner is in its correct position
  # up to orientation (ie. upside down or not)
  def _correctBoard(self):
    nw = self.p(0,0)
    ne = self.p(0, self.width-1)
    se = self.p(self.height-1, self.width-1)
    sw = self.p(self.height-1, 0)

    c = (nw+ne+se+sw)/4.0

    cnw = nw-c
    cne = ne-c
    # x-axis direction
    xvec = np.array([1.0, 0.0])

    # convert to direction vectors
    cnw /= np.linalg.norm(cnw)
    cne /= np.linalg.norm(cne)

    anw = np.sign(np.dot(cnw, xvec))
    ane = np.sign(np.dot(cne, xvec))

    # if no anomalies, return
    if anw == -1.0 and ane == 1.0:
      return 

    
    # check every case and fix accordingly
    if anw == 1.0 and ane == 1.0:
      self.rotateLeft()
    elif anw == 1.0 and ane == -1.0:
      self.flip()
    elif anw == -1.0 and ane == -1.0:
      self.rotateRight()
    

  def __getitem__(self, idx):
    if isinstance(idx, slice):
      return self.p(idx, slice(None, None, None))

    y, x = idx
    return self.p(y, x)


  def coords(self):
    h, w = self.chessboard.shape
    return np.reshape(self.corners["p"][self.chessboard.flatten()], (h, w, 2))

  def coordsToArray(self):
    c = self.coords()
    h, w = self.chessboard.shape
    return np.reshape(c, (h*w,2))

  def p(self, y, x):
    return self.corners["p"][self.chessboard[y,x]]
