import numpy as np
import matplotlib.pyplot as plt
from .helpers import normpdf
from .findModesMeanShift import findModesMeanShift
import time
from functools import partial

def edgeOrientations(img_angle, img_weight):
  """
  Calculates the two dominant edge orientations in the patch if they exist.
  
  Args:
    img_angle (np.array): Image patch containing gradient directions
    img_weight (np.array): Image patch containing gradient magnitudes
  
  Returns:
    None|np.array: Returns none if two dominant edge orientations don't exist and otherwise a 2x2 vector containing the two dominant edge directions
  """
  v1 = np.zeros(2)
  v2 = np.zeros(2)

  # number of bins for the histogram
  bin_num = 32

  # convert image patches to vectors
  vec_angle = np.reshape(img_angle, (img_angle.shape[0]*img_angle.shape[1]))
  vec_weight = np.reshape(img_weight, (img_weight.shape[0]*img_weight.shape[1]))

  # convert angles from normals to directions (90 deg rotation)
  vec_angle = vec_angle + np.pi/2.0
  vec_angle[vec_angle > np.pi] = vec_angle[vec_angle > np.pi] - np.pi

  # create a histogram of how much of an edge there is at a given angle in the patch.
  angle_hist = np.zeros(bin_num)
  # pi to bin_num ratio
  pi_bin_ratio = np.pi/bin_num

  # vectorized bin calculation
  binNos = vec_angle/pi_bin_ratio
  binNos = np.maximum(np.minimum(np.floor(binNos), bin_num-1),0).astype("int")

  for i in range(bin_num):
    idx = np.where(binNos == i)
    angle_hist[i] = np.sum(vec_weight[idx])
  

  (modes, angle_hist_smoothed) = findModesMeanShift(angle_hist, 1.0)
  # less than 2 maxima (ie. dominant edge directions) - not a corner!
  if modes.shape[0] <= 1:
    return None
  
  # compute orientations at each mode
  modes[:, 2] = modes[:, 0]*np.pi/bin_num

  # extract 2 strongest modes and sort by angle
  modes = modes[0:2,:]
  idx = np.argsort(modes[:,2])
  modes = modes[idx, :]

  
  # compute the positive angle between them
  # CHECK: why is min necessary when we just sorted?
  delta_angle = np.minimum(modes[1,2]-modes[0,2], modes[0,2]+np.pi-modes[1,2])

  # too small angle (<45 degrees), probably not a corner
  # old threshold was 17 degrees, but we dont have much distortion in our images, so 45 degrees should be fine
  if delta_angle <= 0.7:
    return None 

  # calculate orientations
  v1 = np.array([np.cos(modes[0,2]), np.sin(modes[0,2])])
  v2 = np.array([np.cos(modes[1,2]), np.sin(modes[1,2])])

  return v1, v2


def refineCorners(img_dx, img_dy, img_angle, img_weight, corners, r):
  """
  Refines the corners for subpixel accuracy

  Args:
    img_dx (numpy.array): Partial image derivative in x
    img_dy (numpy.array): Partial image derivative in y
    img_angle (numpy.array): Image gradient orientations between [0, pi]
    img_weight (numpy.array): Gradient magnitude of image
    corners (numpy.array): List of corner coordinates [[x1,y1],...,[xn, yn]]
    r (int): Window size for estimating the edge orientations
    pool (multiprocessing.Pool): Pool for multithreading

  Returns:
    None

  """
  height, width = img_dx.shape
  # vectorize norms calculation
  norms = np.linalg.norm(np.array([img_dx, img_dy]), axis=0)
  idx0 = np.where(norms == 0)
  norms[idx0] = 1.0 # prevent division by 0

  # vectorize gradients
  grads = np.zeros((img_dx.shape[0], img_dx.shape[1], 2))
  grads[:,:,0] = img_dx
  grads[:,:,1] = img_dy
  # normalize gradients
  gradsNormalized = np.copy(grads)
  gradsNormalized[:,:,0] /= norms 
  gradsNormalized[:,:,1] /= norms 

  # restore values
  norms[idx0] = 0.0

  for i in range(corners["N"]):
    cx, cy = corners["p"][i,:]
    cx = int(cx)
    cy = int(cy)

    # grid of (x, y coordinates) in radius r around (cx, cy) respecting the image boundaries
    grid = np.mgrid[np.maximum(cx-r,0):np.minimum(cx+r, width-1), np.maximum(cy-r,0):np.minimum(cy+r, height-1)]

    # estimate edge orientations by looking at a patch of the angles
    # and gradient magnitudes (edges) around the corner point
    img_angle_patch = img_angle[np.maximum(cy-r,0):np.minimum(cy+r, height-1),np.maximum(cx-r,0):np.minimum(cx+r, width-1)]
    img_weight_patch = img_weight[np.maximum(cy-r,0):np.minimum(cy+r, height-1),np.maximum(cx-r,0):np.minimum(cx+r, width-1)]

    orientations = edgeOrientations(img_angle_patch, img_weight_patch)
    # if the corner does not have two dominant edge orientations that are corner like
    # then don't include this corner.
    if orientations is None:
      continue
    
    # dominant edge orientations in the corner
    v1, v2 = orientations

    ### refine corner orientation
    A1 = np.zeros((2,2))
    A2 = np.zeros((2,2))

    coords = np.zeros((grid.shape[1]*grid.shape[2], 2), dtype="int")
    coords[:,0] = grid[0].reshape(grid.shape[1]*grid.shape[2])
    coords[:,1] = grid[1].reshape(grid.shape[1]*grid.shape[2])

    validIdx = np.where(norms[(coords[:,1], coords[:,0]) ] >= 0.1)
    coords = coords[validIdx]

    c2 = np.abs(np.dot(gradsNormalized[coords[:, 1], coords[:, 0]], v1.T)) < 0.25
    c4 = np.abs(np.dot(gradsNormalized[coords[:, 1], coords[:, 0]], v2.T)) < 0.25

    ## first loop vectorized
    validA1 = np.where(c2)[0]
    validA2 = np.where(c4)[0]

    # calculate structure tensors from valid points
    A1 = np.dot(grads[coords[validA1, 1], coords[validA1, 0]].T, grads[coords[validA1, 1], coords[validA1, 0]])
    A2 = np.dot(grads[coords[validA2, 1], coords[validA2, 0]].T, grads[coords[validA2, 1], coords[validA2, 0]])

    # update corner orientation to be the first eigenvector of A1, A2 (direction with smallest variation)
    _, eigV1 = np.linalg.eigh(A1)
    _, eigV2 = np.linalg.eigh(A2)
    
    v1 = eigV1[:,0]
    v2 = eigV2[:,0]

    corners["v1"][i,:] = v1
    corners["v2"][i,:] = v2

    ### corner location refinement
    outer1 = np.outer(v1.T, v1)
    outer2 = np.outer(v2.T, v2)

    ## second loop vectorized
    ws = (coords - [cx, cy]).astype("float")
    d1s = np.linalg.norm(ws-np.dot(ws, outer1), axis=1)
    d2s = np.linalg.norm(ws-np.dot(ws, outer2), axis=1)

    c1 = d1s < 3.0 
    c3 = d2s < 3.0 

    validIdx2 = np.where( ((c1 & c2) | (c3 & c4)) & ( (coords[:,0] != cx) | (coords[:,1] != cy)  ) )[0]

    # calculate structure tensors for valid points
    A = np.zeros((validIdx2.shape[0], 2, 2))
    A[:, 0, 0] = grads[coords[validIdx2, 1], coords[validIdx2, 0],0]**2.0 #gx*gx
    A[:, 0, 1] = grads[coords[validIdx2, 1], coords[validIdx2, 0],0]*grads[coords[validIdx2, 1], coords[validIdx2, 0],1] #gx*gy
    A[:, 1, 0] = A[:,0, 1] #gx*gy
    A[:,1,1] = grads[coords[validIdx2, 1], coords[validIdx2, 0],1]**2.0 #gy*gy
    G = np.sum(A, axis=0)

    pvec = coords[validIdx2]

    # this einsum multiplies rows of each A[i] with columns of each pvec
    b = np.einsum("aij,ai", A, pvec)

    # if G has full rank, ie. is invertible, set the new corner position
    if np.linalg.matrix_rank(G) == 2:
      newCorner = np.linalg.solve(G, b)
      corners["p"][i,:] = newCorner

      # if the distance between the corners is too long, then discard it
      # (probably happened due to some noise/instability)
      if np.linalg.norm(newCorner-[cx, cy]) >= 4:
        corners["v1"][i,:] = np.zeros(2)
        corners["v2"][i,:] = np.zeros(2)
        continue
    else:
      # corner is invalid, discard it
      corners["v1"][i,:] = np.zeros(2)
      corners["v2"][i,:] = np.zeros(2)    