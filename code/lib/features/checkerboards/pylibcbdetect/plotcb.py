import numpy as np 
import matplotlib.pyplot as plt 

def plotBoard(chessboard, corners, img):
  idx = chessboard.flatten()
  plt.imshow(img, cmap="gray")
  plt.scatter(corners["p"][idx,0], corners["p"][idx,1], facecolors="none", edgecolors="r")
  plt.show()

def plotChessboardCorners(chessboard, img):
  h,w = chessboard.shape 

  nw = chessboard[0,0]
  ne = chessboard[0, w-1]
  se = chessboard[h-1, w-1]
  sw = chessboard[h-1, 0]

  centroid = (nw+ne+se+sw)/4.0

  plt.imshow(img, cmap="gray")
  plt.scatter(nw[0], nw[1], label="nw", color="blue")
  plt.scatter(ne[0], ne[1], label="ne", color="yellow")
  plt.scatter(se[0], se[1], label="se", color="green")
  plt.scatter(sw[0], sw[1], label="sw", color="red")
  plt.scatter(centroid[0], centroid[1], label="center")
  plt.legend()
  plt.show()


def plotCorners(corners, img):
  plt.imshow(img, cmap="gray")
  plt.scatter(corners["p"][:,0], corners["p"][:,1], facecolors="none", edgecolors="r")
  plt.show()


def plotBoards(chessboards, img, corners=True):


  plt.imshow(img, cmap="gray")

  for i in range(len(chessboards)):
    cb = chessboards[i]
    #plt.scatter(corners["p"][idx,0], corners["p"][idx,1], facecolors="none", edgecolors="r")

    coords = cb.coords()
    h,w = cb.shape

    # plot lines in black
    for j in range(h):
      p = coords[j, :]
      plt.plot(p[:,0], p[:,1], color="r", linewidth="3")

    for j in range(w):
      p = coords[:,j]
      plt.plot(p[:,0], p[:,1], color="r", linewidth="3")

    # plot lines in white
    for j in range(h):
      p = coords[j, :]
      plt.plot(p[:,0], p[:,1], color="w")

    for j in range(w):
      p = coords[:,j]
      plt.plot(p[:,0], p[:,1], color="w")

    if corners:
      nw = cb[0,0]
      ne = cb[0, w-1]
      se = cb[h-1, w-1]
      sw = cb[h-1, 0]

      plt.scatter(nw[0], nw[1], label="nw", color="blue")
      plt.scatter(ne[0], ne[1], label="ne", color="yellow")
      plt.scatter(se[0], se[1], label="se", color="green")
      plt.scatter(sw[0], sw[1], label="sw", color="red")
  
  plt.legend()      

  plt.show()