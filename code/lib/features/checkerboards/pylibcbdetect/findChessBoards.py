from .chessboardsFromCorners import chessboardsFromCorners
from .findCorners import findCorners
from .chessboard import Chessboard

def findChessBoards(img, verbose=False):
  corners = findCorners(img, 0.01, verbose)
  if corners is None:
    return None

  cbs = chessboardsFromCorners(corners, img, False)
  if cbs is None:
    return None
  
  out = []
  for i in range(len(cbs)):
    out.append(Chessboard(cbs[i], corners))
  
  return out, corners