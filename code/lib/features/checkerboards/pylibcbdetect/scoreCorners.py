import numpy as np 
from .createCorrelationPatch import createCorrelationPatch

def cornerCorrelationScore(img, img_weight, v1, v2):
  """

  Args:
    img (np.array): Image patch around the corner
    img_weight (np.array): Image gradient magnitude around the corner
    v1 (np.array): First dominant edge orientation vector for the corner
    v2 (np.array): Second dominant edge orientation vector for the corner

  Returns:
    np.array: An array of correlation scores for a given corner point
  """
  # center
  c = np.ones(2)*img_weight.shape[0]/2

  # compute gradient filter kernel
  img_filter = -1.0*np.ones(img_weight.shape)

  ## vectorize loop 1
  # grid of x,y coordinates
  grid = np.mgrid[0:img_filter.shape[1], 0:img_filter.shape[0]]
  coords = np.zeros((grid.shape[1]*grid.shape[2], 2), dtype="int")
  coords[:,0] = grid[0].reshape(grid.shape[1]*grid.shape[2])
  coords[:,1] = grid[1].reshape(grid.shape[1]*grid.shape[2])

  p1s = coords - c
  p2s = np.dot(p1s, np.outer(v1, v1))
  p3s = np.dot(p1s, np.outer(v2, v2))


  c1 = np.linalg.norm(p1s-p2s, axis=1) <= 1.5 
  c2 = np.linalg.norm(p1s-p3s, axis=1) <= 1.5 
  idxs = np.where(c1 | c2)[0]

  img_filter[coords[idxs,1], coords[idxs,0]] = 1

  # convert into vectors
  vec_weight = np.reshape(img_weight, img_weight.shape[0]*img_weight.shape[1])
  vec_filter = np.reshape(img_filter, img_filter.shape[0]*img_filter.shape[1])
  vec_img = np.reshape(img, img.shape[0]*img.shape[1])

  std_weight = np.std(vec_weight)
  std_filter = np.std(vec_filter)

  if std_weight == 0.0: std_weight = 1.0
  if std_filter == 0.0: std_filter = 1.0

  # normalize vectors
  vec_weight = (vec_weight-np.mean(vec_weight))/std_weight
  vec_filter = (vec_filter-np.mean(vec_filter))/std_filter

  # calculate score of the gradient magnitude
  score_gradient = np.maximum(np.sum(vec_weight*vec_filter)/vec_weight.shape[0],0)

  # create intensity filter kernels whose corner angles correspond to the dominant edge orientations v1 v2
  # with radius corresponding to the patch size
  template = createCorrelationPatch(np.arctan2(v1[1], v1[0]), np.arctan2(v2[1], v2[0]), c[0])

  # checkerboard responses
  a1 = np.sum((template["a1"].ravel())*vec_img)
  a2 = np.sum((template["a2"].ravel())*vec_img)
  b1 = np.sum((template["b1"].ravel())*vec_img)
  b2 = np.sum((template["b2"].ravel())*vec_img)

  mu = (a1+a2+b1+b2)/4.0

  # case 1: a=white, b=black
  score_a = np.minimum(a1-mu, a2-mu)
  score_b = np.minimum(mu-b1, mu-b2)
  score_1 = np.minimum(score_a, score_b)

  # case 2: a=black, b=white
  score_a = np.minimum(mu-a1, mu-a2)
  score_b = np.minimum(b1-mu, b2-mu)
  score_2 = np.minimum(score_a, score_b)

  score_intensity = np.maximum(np.maximum(score_1, score_2), 0)

  return score_gradient*score_intensity



def scoreCorners(img, img_weight, corners, radii):
  """
  Calculates a score for each calculated corner point, at each scale (the radi)

  Args:
    img (numpy.array): The input image
    img_weight (numpy.array): The gradient magnitude for the image
    corners (dict): The corners dictionary structure set up in findCorners.py
    radii (list): List of radii given as floats
  
  Returns:
    None
  """
  height, width = img.shape 

  for i in range(corners["N"]):
    
    # corner points may be floats after corner location refinement, cast to nearest int
    cx = int(np.round(corners["p"][i, 0]))
    cy = int(np.round(corners["p"][i, 1]))

    # calculate a score for each radius, the corner score is then the best one
    scores = np.zeros(radii.shape[0])
    for j in range(radii.shape[0]):
      radius = int(radii[j])
      # bounds check
      if cx >= radius and cx < width-radius and cy >= radius and cy < height-radius:
        img_sub = img[cy-radius:cy+radius+1, cx-radius:cx+radius+1]
        img_weight_sub = img_weight[cy-radius:cy+radius+1, cx-radius:cx+radius+1]
        scores[j] = cornerCorrelationScore(img_sub, img_weight_sub, corners["v1"][i], corners["v2"][i])

    corners["score"][i] = np.max(scores)
