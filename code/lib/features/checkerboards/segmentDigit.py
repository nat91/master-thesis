import numpy as np
from skimage.filters.thresholding import threshold_otsu
import skimage.morphology
import matplotlib.pyplot as plt

def segmentDigit(img):
  # remove boundaries set by the homography when extracting the patch
  validIdx = np.where(img != 0.0)
  h, w = img.shape

  # the digit is centered in the patch, find out what the background color roughly is
  # by searching size*size pixels from the top left corner
  # and set the removed boundaries to that color
  y0, x0 = np.min(validIdx[0]), np.min(validIdx[1])
  size = 10
  med = np.median(img[y0:y0+size, x0:x0+size])
  img[img == 0.0] = med


  # remove border resulting from other cells
  marginH = int(np.round(h*0.1))
  marginW = int(np.round(w*0.1))
  img[0:marginH+1,:] = med
  img[h-marginH:, :] = med
  img[:,0:marginW+1] = med
  img[:, w-marginW:] = med
  borderPixels = 2.0*marginW*h + 2.0*marginH*w


  t = threshold_otsu(img)

  segment = img < t

  pixels = h*w 
  nonZero = np.count_nonzero(segment)
  if nonZero > pixels-nonZero:
    # invert it
    segment = np.invert(segment)

  isNumber = segment[segment == 1].shape[0]
  isNan = pixels - isNumber
   
  # remove squares not containing numbers
  # TODO: Test threshold with 8
  if pixels-borderPixels > 0 and isNumber/(pixels-borderPixels) > 0.9:
    print("Segmentation dropped: ", isNumber/(pixels-borderPixels))
    ##plt.subplot(1, 2, 1)
    #plt.imshow(img, cmap="gray")
    #plt.subplot(1, 2, 2)
    #plt.imshow(segment, cmap="gray")
    #plt.show()    
    segment[:,:] = 0.0


  # remove holes
  elem = skimage.morphology.square(1)
  segment = skimage.morphology.binary_closing(segment, elem)

  return segment