import numpy as np
import copy

def fitPlane(X):
  assert(X.shape[1] == 3)
  X = copy.deepcopy(X)
  mu = np.mean(X, axis=0)
  X -= mu
  # covariance matrix
  A = np.dot(X.T, X)
  # smallest eigenvector of A is the best surface normal
  eig, eigvec = np.linalg.eigh(A)

  n = eigvec[:,0]

  return n

def fitPlaneSVD(X):
  X = copy.deepcopy(X)
  mu = np.mean(X, axis=0)
  X -= mu
  coeff = np.ones(X.shape)
  coeff[:, 0:2] = X[:, 0:2]
  zs = X[:,2]

  U, S, V = np.linalg.svd(X.T)
  idx = S.shape[0]-1

  n = U[:, idx]
  MSE = np.sum((np.dot(n, coeff.T) - zs)**2.0)
  MSE /= X.shape[0]

  print("MSE: ", MSE)

  return n