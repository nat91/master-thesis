from .planeFitting import fitPlane
import numpy as np
import util


def findCubeCentroids(boards, depth, size, K):
  centroidsDict = dict()

  if boards:
    for _, board in boards:
      cubeId = board.cubeId
      key = 'cube_' + str(cubeId)
      centroidsDict[key]= findCubeCentroid(board, depth, size, K)

  return centroidsDict

def findCubeCentroid(board, depth, size, K):
  h, w = board.shape
  vlen = size/100.0 # 5cm

  nw = np.ceil(board[0,0]).astype("int")
  ne = np.floor(board[0, w-1]).astype("int")
  se = np.floor(board[h-1, w-1]).astype("int")
  sw = np.ceil(board[h-1,0]).astype("int")

  corners = np.array([nw, ne, se, sw])

  minX = np.min(corners[:,0])
  minY = np.min(corners[:, 1])
  maxX = np.max(corners[:, 0])
  maxY = np.max(corners[:, 1])

  grid = np.mgrid[minX:maxX, minY:maxY]
  xs = grid[0].ravel()
  ys = grid[1].ravel()

  z = depth[ys, xs]
  validIdx = np.where(z > 0)[0]
  X = np.array([xs[validIdx], ys[validIdx], z[validIdx]])

  X = util.deprojectPixelToPoint(K, X, z[validIdx]).T
  m = np.mean(X, axis=0)


  n = fitPlane(X)
  if n[2] < 0:
    n = -n

  c = board.coordsToArray()
  z = util.zInterp(c, depth)

  c = np.hstack((c,z.reshape((z.shape[0],1))))
  c = util.deprojectPixelToPoint(K, c.T, z).T

  nw = c[0]
  ne = c[w-1]
  se = c[h*w-1]
  sw = c[((h-1)*w)]
  centroid = np.mean(np.array([nw, ne, se, sw]), axis=0)

  centroid += n*vlen/2.0

  return centroid