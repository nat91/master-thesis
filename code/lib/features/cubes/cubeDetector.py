import sys
sys.path.append('../../')
from .findCubeCentroids import findCubeCentroids
from util import Registry

class CubeDetector:
  """
  Find cube centroids in an image containing checkerboard cubes.
  Must be run after checkerboardDetector.
  """

  def getFeatures(self, img, depth, _K):
    boards = Registry.getAll()
    features = findCubeCentroids(boards, depth, 5, _K)
    return features
