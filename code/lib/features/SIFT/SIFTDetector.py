import cv2
from .SIFTMatcher import SIFTMatcher
import skimage

class SIFTDetector:

  def __init__(self, config, matcher=None):
    self.sift = cv2.xfeatures2d.SIFT_create()
    if matcher is None:
      matcher = SIFTMatcher(config.get("SIFT.dist_thresh", 0.7))
    
    self.matcher = matcher

  def getFeatures(self, img, _depth, _K):
    gray = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
    gray = skimage.img_as_ubyte(gray)

    features, descriptors = self.sift.detectAndCompute(gray, None)

    retD = dict()
    retD["SIFT"] = {
      "features": features,
      "descriptors": descriptors,
      "matcher": self.matcher
    }

    return retD