import cv2
import numpy as np
import re

class SIFTMatcher:

  def __init__(self, distThresh=0.7):
    """
    Args:
      distThresh (float): When matching between two images, the second best match must be at least distTresh percent further away in distance, than the best match. This ensures that ambiguous matches are not considered.
    
    """
    self.distThresh = distThresh
    self.matchCounter = 0

  def calculateCorrespondenceOverlap(self, featureSets):
    N = len(featureSets)
    featuresSeen = set() 
    numFeatures = 0 # total number of unique keypoints  

    # symmetric overlap matrix, O_ij is camera i's overlap with camera j
    overlapMatrix = np.zeros((N,N))
    for i in range(N):
      fi = featureSets[i]["features"]
      di = featureSets[i]["descriptors"]
      matcher = featureSets[i]["matcher"]

      for j in range(i+1, N):
        if featureSets[i]["matcher"] != featureSets[j]["matcher"]:
          # incompatible matchers
          continue

        fj = featureSets[j]["features"]
        dj = featureSets[j]["descriptors"]
        _, labels = matcher.match(fi, di, fj, dj)
        overlapMatrix[i,j] = len(labels)
        overlapMatrix[j,i] = overlapMatrix[j,i]

        print("SIFT: cam1 {} cam2 {} |fi|={} |fj|={} |fi&fj|={}".format(i, j, len(fi), len(fj), len(labels)))

        for l in labels:
          m = re.search("^SIFT_\d+_(\d+)<->(\d+)$", l)
          if m is not None:
            kp1 = "{}_{}".format(i, m.group(1))
            kp2 = "{}_{}".format(j, m.group(2))

            if kp1 not in featuresSeen and kp2 not in featuresSeen:
              numFeatures +=1
            
            featuresSeen.add(kp1)
            featuresSeen.add(kp2)

  
    return overlapMatrix, numFeatures



  def match(self, f1, d1, f2, d2):
    cor2d = []
    labels2d = []

    if not len(d1) or not len(d2):
      return np.array(cor2d), labels2d

    bf = cv2.BFMatcher()
    matches = bf.knnMatch(d1, d2, k=2)
    # sort so matches with lower distance come first.
    matches = sorted(matches, key = lambda x: x[0].distance)

    added = set()

    for m,n in matches:
      if (m.queryIdx in added) or (m.trainIdx in added):
        continue 

      if m.distance < self.distThresh*n.distance:
        
        pt1 = f1[m.queryIdx].pt
        pt2 = f2[m.trainIdx].pt

        added.add(m.queryIdx)
        added.add(m.trainIdx)

        cor2d.append([pt1[0], pt1[1], pt2[0], pt2[1]])
        labels2d.append("SIFT_{}_{}<->{}".format(self.matchCounter, m.queryIdx, m.trainIdx))
    
    self.matchCounter += 1
    return np.array(cor2d), labels2d
