from . import nist
import sys
sys.path.append("../")
from reader.reader import FileReader
from tools.viewer import util
import cv2
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from scipy.spatial.transform import Rotation as R
import numpy as np
import numpy.ma as ma
import math
from skimage import feature, img_as_ubyte
from halo import Halo
from util import alignDepthToColor


debug = False
showClassification = False
# Ping Pong ball radius = 20mm
ballRadius = 0.02
red = 'red'
green = 'green'
blue = 'blue'
yellow = 'yellow'
white = 'white'

houghParams = {
  'red': (40, 15),
  'blue': (40, 20),
  'green': (40, 15),
  'yellow': (40, 15),
  'white': (40, 15),
}

colorDictBGR = {
  'red': (0,0,255),
  'blue': (255,0,0),
  'green': (0,255,0),
  'yellow': (0,255,255),
  'white': (255,255,255)
}

colorDictRGB = {
  'red': (255,0,0),
  'blue': (0,0,255),
  'green': (0,255,0),
  'yellow': (255,255,0),
  'white': (255,255,255)
}

def detectColoredSphere(hsv, depth, K, color):
  if debug:
    print('color: ', color)

  decideByFit = False


  rows = hsv.shape[0]
  # Find pixels of color in question
  idx = colorScore(hsv, color)[1]

  if debug:
    plt.imshow(hsv)
    plt.show()
    plt.imshow(idx, cmap="gray")
    plt.show()

  # Find edges
  idx = cv2.Canny(idx,100,200)

  # Find candidate circles
  candidates = cv2.HoughCircles(idx, cv2.HOUGH_GRADIENT, 1, rows/8,
                                param1=houghParams[color][0],param2=houghParams[color][1],minRadius=20,maxRadius=150)

  best = None
  lastScore = 0.0
  lastRes = np.inf

  if candidates is not None:
    for circle in candidates[0, :]:
      x_center, y_center, radius = (circle[0], circle[1], circle[2])

      # Mask of circle
      mask = np.zeros((hsv.shape[0],hsv.shape[1]), dtype=np.uint8)
      cv2.circle(mask,(x_center,y_center),int(radius),1,-1)
      # Smaller mask for sphere test, to avoid edge effect
      maskSmall = np.zeros((hsv.shape[0],hsv.shape[1]), dtype=np.uint8)
      cv2.circle(maskSmall,(x_center,y_center),int(radius*0.8),1,-1)

      # Get depth points corresponding to circle area
      innerCircle = np.logical_and(maskSmall, depth > 0).nonzero()

      # Split into individual arrays
      spX = innerCircle[1]
      spY = innerCircle[0]
      spZ = depth[spY, spX]
      # Convert coordinates to world coordinates
      spX_world = ((spX - K[0,2]) / K[0,0]) * spZ
      spY_world = ((spY - K[1,2]) / K[1,1]) * spZ
      spZ_world = spZ

      # Solve sphere equation
      sphereData = np.column_stack((spX_world, spY_world, spZ_world))
      # Protect fitting from failing
      if sphereData.shape[0] < 3:
        continue

      z_center = depth[int(y_center), int(x_center)]
      startCenter = (((x_center - K[0,2]) / K[0,0]) * z_center, ((y_center - K[1,2]) / K[1,1]) * z_center, z_center + ballRadius)
      fitResult = nist.coneCylAlgoE57(sphereData, startCenter, ballRadius,coneAngle=120)
      if fitResult is None:
        continue

      dataFinal, dataIgnored, foundCenter, res =  fitResult

      # Calculate color score as number of pixels of color divided by area pixel size
      area = np.count_nonzero(mask)
      patch = cv2.bitwise_and(hsv, hsv, mask=mask)
      score = colorScore(patch, color)[0] / area

      if debug: print('(score, res): ', (score, res))

      # Keep best candidate
      if score > 0.75 and res < 1e-2:

        if decideByFit:
          newIsBetter = res < lastRes
        else:
          newIsBetter = score > lastScore

        if newIsBetter:
          best = (foundCenter, (color, circle)) # Second element just for debug print
          lastRes = res
          lastScore = score


          ##### DEBUG #####
          if debug:
            cv2.imshow('better', cv2.cvtColor(patch, cv2.COLOR_HSV2BGR))
            cv2.waitKey()
            fig = plt.figure()
            ax = fig.gca(projection='3d')
            ax.scatter(dataFinal[0::4,0], dataFinal[0::4,1], dataFinal[0::4,2], c='r', marker='o', s=0.5, label='Inliers')
            ax.scatter(dataIgnored[0::4,0], dataIgnored[0::4,1], dataIgnored[0::4,2], c='b', marker='o', s=0.5, label='Outliers')
            ax.scatter(foundCenter[0], foundCenter[1], foundCenter[2], c='g', marker='*', label='Center')
            (xs,ys,zs) = drawSphere(foundCenter[0],foundCenter[1],foundCenter[2],ballRadius)
            ax.plot_wireframe(xs, ys, zs, color="r")
            max_range = np.array([xs.max()-xs.min(), ys.max()-ys.min(), zs.max()-zs.min()]).max() / 2.0
            mid_x = (xs.max()+xs.min()) * 0.5
            mid_y = (ys.max()+ys.min()) * 0.5
            mid_z = (zs.max()+zs.min()) * 0.5
            ax.set_xlabel('x')
            ax.set_ylabel('y')
            ax.set_zlabel('z')
            ax.set_xlim(mid_x - max_range, mid_x + max_range)
            ax.set_ylim(mid_y - max_range, mid_y + max_range)
            ax.set_zlim(mid_z - max_range, mid_z + max_range)
            ax.legend()
            plt.show()


  return best

#@Halo(text='Finding Spheres', spinner='dots')
def detectColorSpheres(src_rgb, depth, K):
  src_rgb = img_as_ubyte(src_rgb)

  src_bgr = cv2.cvtColor(src_rgb, cv2.COLOR_RGB2BGR)

  blur = cv2.GaussianBlur(src_bgr, (9,9), 2, 2)
  src_hsv = cv2.cvtColor(blur, cv2.COLOR_BGR2HSV)

  if debug:
    plt.imshow(src_hsv)
    plt.show()

  circles = []
  circlesDict = dict()

  for color in [red, green, blue, yellow]:
    detectedSphere = detectColoredSphere(src_hsv, depth, K, color)
    if detectedSphere != None:
      circlesDict[color] = detectedSphere[0]
      circles.append(detectedSphere[1]) # For print debug

  if len(circles) == 0:
    return circlesDict

  # For printing
  if debug or showClassification:
    colors, foundCircles  = map(list,zip(*circles))
    if foundCircles is not None:
      foundCircles = np.uint16(np.around(foundCircles))
      for i, foundCircle in enumerate(foundCircles):
        center = (foundCircle[0],foundCircle[1])
        # foundCircle center
        #cv2.circle(src_bgr, center, 1, (0, 100, 100), 3)
        # foundCircle outline
        radius = foundCircle[2]
        cv2.circle(src_bgr, center, radius, colorDictBGR[colors[i]], 3)


    cv2.imshow('Found', src_bgr)
    cv2.waitKey()

    cv2.destroyAllWindows()

  return circlesDict


def colorScore(patch, color):
  if color == red:
    return redScore(patch)
  elif color == green:
    return greenScore(patch)
  elif color == blue:
    return blueScore(patch)
  elif color == yellow:
    return yellowScore(patch)
  elif color == white:
    return whiteScore(patch)
  else:
    return None


def redScore(img):
  h,s,v = cv2.split(img)[:3]
  idx = (((h >= 170) & (h <= 180)) | ((h >= 0) & (h <= 5))) & ((s >= 100) & (s <= 255))
  totalRed = np.count_nonzero(idx)
  # Just for visual debugging
  # cv2.imshow('image', cv2.cvtColor(img, cv2.COLOR_HSV2BGR))
  # cv2.imshow('red_mask', idx.astype(np.uint8) * 255)
  # cv2.waitKey()
  return totalRed, idx.astype(np.uint8) * 255


def greenScore(img):
  h,s,v = cv2.split(img)[:3]
  idx = (((h >= 36) & (h <= 100)) & ((s >= 80) & (s <= 255)))
  totalGreen = np.count_nonzero(idx)
  # Just for visual debugging
  # cv2.imshow('image', cv2.cvtColor(img, cv2.COLOR_HSV2BGR))
  # cv2.imshow('green_mask', idx.astype(np.uint8) * 255)
  # cv2.waitKey()
  return totalGreen, idx.astype(np.uint8) * 255

def blueScore(img):
  h,s,v = cv2.split(img)[:3]
  idx = (((h >= 100) & (h <= 130)) & ((s >= 100) & (s <= 255)))
  totalBlue = np.count_nonzero(idx)
  # Just for visual debugging
  # cv2.imshow('image', cv2.cvtColor(img, cv2.COLOR_HSV2BGR))
  # cv2.imshow('blue_mask', idx.astype(np.uint8) * 255)
  # cv2.waitKey()
  return totalBlue, idx.astype(np.uint8) * 255

def yellowScore(img):
  h,s,v = cv2.split(img)[:3]
  idx = (((h >= 10) & (h <= 35)) & ((s >= 75) & (s <= 255)))
  totalYellow = np.count_nonzero(idx)
  # Just for visual debugging
  # cv2.imshow('image', cv2.cvtColor(img, cv2.COLOR_HSV2BGR))
  # cv2.imshow('yellow_mask', idx.astype(np.uint8) * 255)
  # cv2.waitKey()
  return totalYellow, idx.astype(np.uint8) * 255

def whiteScore(img):
  h,s,v = cv2.split(img)[:3]
  idx = (s <= 50)
  totalWhite = np.count_nonzero(idx)
  # Just for visual debugging
  # cv2.imshow('image', cv2.cvtColor(img, cv2.COLOR_HSV2BGR))
  # cv2.imshow('white mask', idx.astype(np.uint8) * 255)
  # cv2.waitKey()
  return totalWhite, idx.astype(np.uint8) * 255

def drawSphere(xCenter, yCenter, zCenter, r):
  #draw sphere
  u, v = np.mgrid[0:2*np.pi:20j, 0:np.pi:10j]
  x=np.cos(u)*np.sin(v)
  y=np.sin(u)*np.sin(v)
  z=np.cos(v)
  # shift and scale sphere
  x = r*x + xCenter
  y = r*y + yCenter
  z = r*z + zCenter
  return (x,y,z)


def draw3D(data1, name):
  xs = data1[:,0]
  ys = data1[:,1]
  zs = data1[:,2]
  fig = plt.figure()
  ax = fig.gca(projection='3d')
  ax.scatter(xs, ys, zs, c='r', marker='o')
  max_range = np.array([xs.max()-xs.min(), ys.max()-ys.min(), zs.max()-zs.min()]).max() / 2.0
  ax.set_title(name)
  mid_x = (xs.max()+xs.min()) * 0.5
  mid_y = (ys.max()+ys.min()) * 0.5
  mid_z = (zs.max()+zs.min()) * 0.5
  ax.set_xlim(mid_x - max_range, mid_x + max_range)
  ax.set_ylim(mid_y - max_range, mid_y + max_range)
  ax.set_zlim(mid_z - max_range, mid_z + max_range)
  ax.set_xlabel('x')
  ax.set_ylabel('y')
  ax.set_zlabel('z')
  plt.show()

if __name__ == "__main__":
  path = '/home/jakob/master-thesis/code/Sphere/4balls/recordingInfo.recdat'
  recdata = util.get_camera_files_from_file(path)
  filepaths = [elem[1] for elem in recdata]
  firstFile = filepaths[0]
  secondFile = filepaths[1]
  f1 = open(firstFile, 'rb')
  f2 = open(secondFile, 'rb')
  reader1 = FileReader(25, f1)
  reader1.skipFrames(20)
  reader2 = FileReader(25, f2)
  reader2.skipFrames(20)


  c1 = reader1.currentFrame.colorFrame

  d1 = reader1.currentFrame.depthFrame
  dIntrinsics = reader1.fileMetaData.depthIntrinsics.getIntrinsics()
  rIntrinsics = reader1.fileMetaData.rgbIntrinsics.getIntrinsics()
  depthToColor = reader1.fileMetaData.depthToColorExtrinsics.getTransform()
  d1Scale = reader1.fileMetaData.dScale
  d1 = d1Scale * d1
  d1 = alignDepthToColor(dIntrinsics, rIntrinsics, (c1.shape[0], c1.shape[1]), depthToColor, d1)
  d1 = (d1 < 2) * d1
  d1 = (d1 > 0.3) * d1
  d2 = reader2.currentFrame.depthFrame
  detectColorSpheres(c1, d1, reader1.fileMetaData)