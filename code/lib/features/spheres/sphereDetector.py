import sys
from .detection import detectColorSpheres
sys.path.append("../")
from .. import LabeledFeatureMatcher

class SphereDetector:
  """
  Detects red, green, blue and yellow spheres in an image
  """

  def getFeatures(self, img, depth, K):
    res = detectColorSpheres(img, depth, K)

    features = []
    descriptors = dict()
    i = 0

    for (k,v) in res.items():
      features.append(v)
      descriptors[k] = i
      i += 1

    resD = dict()
    resD["spheres"] = {
      "features": features,
      "descriptors": descriptors,
      "matcher": LabeledFeatureMatcher()
    }

    return resD
