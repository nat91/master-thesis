import numpy as np
from . import detection
from scipy.optimize import least_squares #leastsq is about 8% faster than least_squares in this trial

# This is a reduced version of the NIST sphere fitting algorithm: https://github.com/usnistgov/DMG_SphereFitting
# NIST-developed software is provided by NIST as a public service. You may use, copy and distribute copies of the software in any medium, provided that you keep intact this entire notice. You may improve, modify and create derivative works of the software or any portion of the software, and you may copy and distribute such modifications or works. Modified works should carry a notice stating that you changed the software and should note the date and nature of any such change. Please explicitly acknowledge the National Institute of Standards and Technology as the source of the software.
# NIST-developed software is expressly provided "AS IS." NIST MAKES NO WARRANTY OF ANY KIND, EXPRESS, IMPLIED, IN FACT OR ARISING BY OPERATION OF LAW, INCLUDING, WITHOUT LIMITATION, THE IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, NON-INFRINGEMENT AND DATA ACCURACY. NIST NEITHER REPRESENTS NOR WARRANTS THAT THE OPERATION OF THE SOFTWARE WILL BE UNINTERRUPTED OR ERROR-FREE, OR THAT ANY DEFECTS WILL BE CORRECTED. NIST DOES NOT WARRANT OR MAKE ANY REPRESENTATIONS REGARDING THE USE OF THE SOFTWARE OR THE RESULTS THEREOF, INCLUDING BUT NOT LIMITED TO THE CORRECTNESS, ACCURACY, RELIABILITY, OR USEFULNESS OF THE SOFTWARE.
# You are solely responsible for determining the appropriateness of using and distributing the software and you assume all risks associated with its use, including but not limited to the risks and costs of program errors, compliance with applicable laws, damage to or loss of data, programs or equipment, and the unavailability or interruption of operation. This software is not intended to be used in any situation where a failure could cause risk of injury or damage to property. The software developed by NIST employees is not subject to copyright protection within the United States.

noFit = [np.zeros((0,0)),np.zeros((0,0)),np.zeros((0,0)),np.infty]

def coneCylAlgoE57(data1, startCenter, trueRadius, coneAngle=120):
    # detection.draw3D(data1, 'data1')
    ITER = 10
    halfConeAngle = coneAngle/2
    centerFinal = np.zeros((ITER,3))
    for kk in  range(0,ITER):
        if kk == 0:
            if startCenter is None:
                res = loc_closestPointMethod(data1,trueRadius)
                if res is None:
                    return None
                
                [dataFinal,dataIgnored,centerInit,_] = res
                centerFinal[kk,:] = centerInit
            else:
                centerFinal[kk,:] = startCenter
        else:
            newCenter = centerFinal[kk-1,:]
            try:
                [dataFinal,dataIgnored,centerInit,cfTemp, residuals] = loc_coneCylAlgo(data1,trueRadius,halfConeAngle,newCenter)
                centerFinal[kk,:] = cfTemp
            except:
                # print('Fitting failed.\n')
                return noFit

    finalCenter = centerFinal[kk,:]
    results = [dataFinal, dataIgnored, finalCenter, np.average(np.square(residuals))]

    if dataIgnored.shape[0] == 0 or (dataFinal.shape[0] / (dataIgnored.shape[0] + dataFinal.shape[0])) > 0.7:
        return results
    else:
        # print('Too many outliers\n')
        return noFit

def loc_closestPointMethod(data1,trueRadius):

    POINTS  = 500
    PERCENT = 0.05

    # First sort the ranging data from the closest to furthest
    rng1 = rssq3(data1,1)
    rng2 = np.sort(rng1)


    # Then take the closest N points at the start of the surface and grab
    # data that is within the (d + 0.5*radius) distance
    len1 = int(np.floor(min(POINTS, PERCENT*len(data1))))
    if (len1 < 4): #Just a check, and to ensure a min. of 4 points
        len1 = int(max(4, np.ceil(0.1*len(data1))))

    surfaceStart = np.median(rng2[0:len1-1]) #Select the first len1 points

    # Extract points that are from the closest point to (d + 0.5*radius)
    # and find the center
    idx2          = rng1<(surfaceStart+0.5*trueRadius)
    dataFinal     = data1[idx2,:]
    # detection.draw3D(dataFinal, 'dataFinal')
    res = sphereFitLSQ1_conR(dataFinal,trueRadius)
    if res is None:
        return
    
    [cx,cy,cz,rr]  = res

    centerInit    = np.asarray([cx,cy,cz])

    dataIgnored   = data1[~idx2,:]
    centerFinal   = centerInit
    results       = [dataFinal,dataIgnored,centerInit,centerFinal]
    return results

def loc_coneCylAlgo(data1,trueRadius, halfConeAngle,centerInit):
#This is a cone-cylinder truncation algorithm, where data from the scan of
#a sphere from a laser scanner is processed to obtain a sphere center.
#
#Schematic: https://raw.githubusercontent.com/usnistgov/DMG_SphereFitting/master/Schematic_DistanceOfPointsFromAxis_CylinderRegion.png
#Calculate the radius of the cylinder corresponding to the cone angle at
#the sphere center
    cylRadius = trueRadius*np.sin(halfConeAngle*np.pi/180) #Cylinder radius (h)


    ## Now to:  finding data within a cone angle of 120 degrees
    #Let's say O is the origin, C is the center, and P is a point on the sphere
    #The angle between the vectors CO and CP should be within 60 degrees
    #here halfConeAngle = 60 degrees
    origin      = data1*0 #Easier way to get zeros instead of zeros()
    if centerInit.shape[0] == 3:
        centerInit = centerInit.T
    vectorCO    = origin - centerInit
    vectorCP    = data1 - centerInit
    allAngles1  = vectorAngle3(vectorCO, vectorCP)*180/np.pi #in degrees

    #Find the points in the original data set that are within the desired
    #cone angle for the segmentation
    idx4        = allAngles1 < halfConeAngle #Points within the cone
    vectorOP    = data1 - origin

    #The code below finds the distance of all the points from the axis joining
    #the initial center and the origin - to find points within a cylinder
    #O is the origin, C is the sphere center, and P is a point on the sphere
    #F is a point on the line CO such that PF is perpendicular to CO
    #Length of PF is the shortest distance between the point P and CO.
    #dOF = |OF| is the projection of OP on the unit vector along CO.
    dCO = rssq3(vectorCO[1,:],0)
    unitVectorCO = vectorCO/dCO #All rows of vectorCO are identical
    #dOF   = np.dot(vectorOP.T, unitVectorCO.T)dOF = dOF.T
    dOF   = vecDot(vectorOP,unitVectorCO)
    dOP   = rssq3(vectorOP,1)
    dPF   = np.sqrt(dOP*dOP - dOF*dOF) #Distance from each point to the line CO
    idx5  = dPF < cylRadius #Points within the cylinder

    #Now combine the points that are both within the cone and the cylinder
    idxA = idx4&idx5
    data2   = data1[idxA,:]
    # detection.draw3D(data2, 'data2')
    data2I = data1[~idxA,:]

    #Truncate points whose residuals exceed 3sigma
    [cx0,cy0,cz0,rr0]    = sphereFitLSQ1_conR(data2,trueRadius,centerInit)
    center2 = [cx0,cy0,cz0]
    resids2  = rssq3(data2-center2,1)-rr0
    idx8     = np.abs(resids2)<3*np.std(resids2,ddof=1)

    dataFinal    = data2[idx8,:]
    # detection.draw3D(dataFinal, 'dataFinal')
    data1I = data2[~idx8,:]
    dataIgnored  = np.concatenate([data2I,data1I])


    #Find the center of the final dataset
    [cxf,cyf,czf,_]= sphereFitLSQ1_conR(dataFinal,trueRadius,center2)
    centerFinal = [cxf,cyf,czf]

    results = [dataFinal, dataIgnored, centerInit, centerFinal, resids2]
    return results


def vectorAngle3(vec1,vec2): #calculates the angle between two vectors. Each vector can be a Nx3 matrix
    #import numpy as np
    cp = np.cross(vec1,vec2)
    dp = sum([i*j for (i, j) in zip(vec1.T, vec2.T)])
#    dp = [] #In MATLAB, this is a non-loop operation (i.e. matrix operation, here we have to do it in a loop)
#    for jj in xrange(len(vec1)):
#        dp = np.append(dp, np.dot(vec1[jj,:], vec2[jj,:]))
    sinValue = np.sqrt(np.sum(cp*cp,1))
    cosValue = dp
    vAngle = np.arctan2(sinValue,cosValue)
    return vAngle

def rssq3(data1,dim):    #Calculates the root-sum-square of a matrix
    sq1 = data1*data1
    vals = np.sqrt(np.sum(sq1,dim))
    return vals

def vecDot(vec1,vec2): #Calculates dot product of two Nx3 matrices
    dp = sum([i*j for (i, j) in zip(vec1.T, vec2.T)])
#   dp = []
#    for jj in xrange(len(vec1)):
#        dp = np.append(dp, np.dot(vec1[jj,:], vec2[jj,:]))
    return dp

def sphereFitLin2(data1, knownRadius):
    [xx,yy,zz] = data1.T
    r = knownRadius
    oo = xx*0+1 #simpler than using ones()

    AA = [-2*xx, -2*yy , -2*zz, oo]
    BB = [-xx**2-yy**2-zz**2 + oo*(r**2)]
    YY = np.linalg.lstsq(np.transpose(AA),np.transpose(BB), rcond=None)

    [a,b,c,D] = YY[0][0:4]


    result = np.hstack([a,b,c,r])
    return result

# This is a function that takes the point cloud data
# and a known radius as input arguments and returns the sphere center.
# Does not segment into inliers and outliers.
def sphereFitLSQ1_conR(data1,knownRadius, initGuess = None):
    if initGuess is None:
        initGuess = sphereFitLin2(data1, knownRadius)
    initGuess = initGuess[0:3]

    def calcResids2(initGuess2, data1,knownRadius):
        [x0, y0, z0] = initGuess2
        R = knownRadius
        [x, y, z] = data1.T
        resids1 = np.sqrt((x-x0)**2 + (y-y0)**2 + (z-z0)**2) - R
        return resids1


    try:
        #result, flag = leastsq(calcResids2, initGuess, args=(data1,knownRadius),maxfev=1500,ftol=1E-15, xtol=1E-15) #Same arguments as MATLAB's lsqnonlin() for consistent results
        result = least_squares(calcResids2, initGuess, args=(data1,knownRadius),method='lm',max_nfev=1500,ftol=1E-15, xtol=1E-15) #Same arguments as MATLAB's lsqnonlin() for consistent results
        result = result.x
    except ValueError:
        # catches when there is not enough data (ie. no sphere) to solve the least squares problem
        return None

    result = np.append(result,knownRadius) # Sending back a 4 element array
    return result