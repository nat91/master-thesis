import sys
sys.path.append('../../')
from .twoSidedBoard import findTwoSidedBoards
from ..labeledFeatureMatcher import LabeledFeatureMatcher
from util import Registry

class TwoSidedDetector:
  """
  Detects multiple two-sided checkerboards in an image
  """

  def __init__(self, config):
    """
    Initializes a twosided checkerboard detector

    Args:
      config (util.Config): Config file used to define the naming scheme of twosided boards.
    """

    self.boardsInImage = None
    if config is not None:
      if "twosided" in config:
        self.boardsInImage = config["twosided"]


  def getFeatures(self, img, depth, _K):
    boards = Registry.getAll()
    features = findTwoSidedBoards(boards, depth, self.boardsInImage, _K)

    descriptors = dict()
    fVals = []
    keys = list(features.keys())
    for i in range(len(keys)):
      descriptors[keys[i]] = i
      fVals.append(features[keys[i]])      

    retD = dict()
    retD["checkerboard"] = {
      "features": fVals,
      "descriptors": descriptors,
      "matcher": LabeledFeatureMatcher()
    }
    
    return retD
