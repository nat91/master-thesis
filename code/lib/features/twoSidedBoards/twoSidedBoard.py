import numpy as np
import util
import sys
# sys.path.append('../../')
# from lib.features.chessboard import Chessboard


def findTwoSidedBoards(boards, depth, boardsInImage, K):
  boardsDict = dict()

  if boards and boardsInImage:
    for x, board in boards:
      board = board.deepCopy()
      cubeId = board.cubeId
      boardId = board.boardId

      fullId = str(cubeId) + str(boardId)

      if fullId in boardsInImage:
        if cubeId < boardId:
          key = 'two_' + str(fullId)
          boardsDict[key] = board
        elif cubeId > boardId:
          key = 'two_' + str(fullId[::-1])
          board.chessboard = board.flip1()
          boardsDict[key] = board
        else:
          raise Exception("Twosided boards should not be a repdigit")

  print('boardsDict: ', boardsDict)
  return boardsDict