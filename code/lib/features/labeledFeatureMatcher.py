import numpy as np

class LabeledFeatureMatcher:

  def __eq__(self, other):
    # this class is semi static, so two objects are equal if they are an instance of this class.
    return isinstance(other, LabeledFeatureMatcher)

  def calculateCorrespondenceOverlap(self, featureSets):
    N = len(featureSets)
    # symmetric overlap matrix, O_ij is camera i's overlap with camera j
    overlapMatrix = np.zeros((N,N))
    features = set()

    for i in range(N):
      xi = set(featureSets[i]["descriptors"].keys())
      features |= xi
      for j in range(i+1, N):
        xj = set(featureSets[j]["descriptors"].keys())
        features |= xj
        overlapMatrix[i,j] = len(xj & xi) 
        overlapMatrix[j,i] = overlapMatrix[j,i]
    
    return overlapMatrix, len(features)

  @staticmethod
  def match(f1, d1, f2, d2):
    """
    Takes two feature dictionaries and returns an array of correspondences

    Args:
      f1 (np.array): Features points from image 1 of the shape Nx2 or Nx3 
      d1 (dict): Dict of labels: label => idx, where label is a descriptor label for f1[idx]
      f2 (np.array): Features points from image 2 of the shape Nx2 or Nx3 
      d2 (dict): Dict of labels: label => idx, where label is a descriptor label for f2[idx]

    Returns:
      (np.array, list): Tuple of (correspondences, labels)
    """
    
    keys = list(d1.keys())
    cor = []
    labels = []
    
    
    for k in keys:
      if k in d2:
        x1 = f1[d1[k]]
        x2 = f2[d2[k]]
        if x1.shape != x2.shape:
          raise RuntimeError("Cannot match correspondences, shapes are not equal! {} != {}".format(x1.shape, x2.shape))
        
        cor.append(np.hstack((x1, x2)))
        labels.append(k)

    return np.array(cor), labels