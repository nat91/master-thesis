from . import LabeledFeatureMatcher
import numpy as np

def partition(featureMap, leaveOut):


  outTrue = dict()
  outFalse = dict()


  for k in featureMap.keys():
    if not isinstance(featureMap[k]["matcher"], LabeledFeatureMatcher):
      continue
    
    outTrue[k] = {
      "features": [],
      "descriptors": dict(),
      "matcher": featureMap[k]["matcher"]
    }

    outFalse[k] = {
      "features": [],
      "descriptors": dict(),
      "matcher": featureMap[k]["matcher"]
    }

    for d,w in featureMap[k]["descriptors"].items():
      if np.any(list(map(d.startswith, leaveOut))):
        outFalse[k]["features"].append(featureMap[k]["features"][w])
        outFalse[k]["descriptors"][d] = len(outFalse[k]["features"])-1
      else:
        outTrue[k]["features"].append(featureMap[k]["features"][w])
        outTrue[k]["descriptors"][d] = len(outTrue[k]["features"])-1        
          
  return outTrue, outFalse

