import numpy as np
from util import toHomogeneous

def umeyama_ransac(Cs, t=1e-3, c=0.999, order=0):
  """
   Optimal least-squares estimation of transformation between points,
   that handles the case where a reflection is found instead of a rotation.
   This is implemented as described by Shinji Umeyama in his paper
   "Least-Squares Estimation of Transformation Parameters Between Two Point Patterns"

   Args:
    Cs (np.array): A Nx6 array of 3D correspondences, where each row is of the form [x0, y0, z0, x1, y1, z1]
    t (float): The RANSAC outlier threshold
    c (float): The RANSAC confidence, must be between 0 and 1
    order (int): If order is 0, a transformation from (x0, y0, z0) -> (x1, y1, z1) is found, if its 1 then the converse.

   Returns:
    ((np.array, np.array, float, float, np.array): Returns a pair where one is a 5-tuple of (rotation, translation, scale, MSE, 4x4 homogeneous transform matrix). None is returned on failure.
  """  

  if Cs.shape[0] < 4:
    return None
  
  eps = 0.5 # Initial outlier %

  x = Cs[:, 0:3]
  y = Cs[:, 3:]
  if order == 1:
    y = Cs[:, 0:3]
    x = Cs[:, 3:]

  xh = toHomogeneous(x)
  yh = toHomogeneous(y)


  numPoints = 4

  # number of iterations
  N = np.log(1.0-c)/np.log(1.0-(1.0-eps)**numPoints)
  # number of data points
  nd = x.shape[0]

  bestInlierSet = []
  maxInliers = 0
  bestModel = None

  i = 0
  while i < N:
    # make a random model from a subset of the data
    d = np.random.randint(0, nd, numPoints)
    xd = x[d]
    yd = y[d]

    res = umeyama(np.hstack((xd, yd)))

    if res is None:
      # bad rank
      continue

    _,_ ,_, _, Rt = res

    err = np.linalg.norm(np.dot(Rt, xh.T).T-yh, axis=1)**2.0

    inlierSet = np.where(err < t)[0].tolist()

    if len(inlierSet) > maxInliers:
      bestInlierSet = inlierSet
      maxInliers = len(inlierSet)
      bestModel = Rt

      #print("Best inlier set is now {}, N: {}".format(maxInliers, N))

      # reestimate number of iterations needed
      eps = 1.0 - maxInliers/nd
      if eps == 0.0:
        break

      N = np.log(1.0-c)/np.log(1.0-(1.0-eps)**numPoints)

    i += 1

  
  if len(bestInlierSet) == 0:
    return None

  # Make best model with inlier set
  return umeyama(np.hstack((x[bestInlierSet], y[bestInlierSet]))), bestInlierSet

def umeyama(Cs, order=0):
  """
   Optimal least-squares estimation of transformation between points,
   that handles the case where a reflection is found instead of a rotation.
   This is implemented as described by Shinji Umeyama in his paper
   "Least-Squares Estimation of Transformation Parameters Between Two Point Patterns"

   Args:
    Cs (np.array): A Nx6 array of 3D correspondences, where each row is of the form [x0, y0, z0, x1, y1, z1]
    order (int): If order is 0, a transformation from (x0, y0, z0) -> (x1, y1, z1) is found, if its 1 then the converse.

   Returns:
    (np.array, np.array, float, float, np.array): Returns a 5-tuple of (rotation, translation, scale, MSE, 4x4 homogeneous transform matrix)
  """
  if not order in (1,0):
    raise RuntimeError("Parameter order must be 0 or 1")

  x = Cs[:, 0:3]
  y = Cs[:, 3:]

  if order == 1:
    x = Cs[:, 3:]
    y = Cs[:, 0:3]


  N = Cs.shape[0]
  C = Cs.shape[1]//2

  mux = np.mean(x, axis=0)
  muy = np.mean(y, axis=0)
  varx = 0

  covar = 0
  for i in range(N):
    varx += np.dot(x[i]-mux, (x[i]-mux).T)
    covar += np.outer(y[i]-muy, x[i]-mux)

  covar /= N
  varx /= N
  S = np.eye(covar.shape[0])

  U, D, V = np.linalg.svd(covar)

  rank = np.linalg.matrix_rank(covar)
  #print("Rank: ", rank)
  #print("Determinant: ", np.linalg.det(covar))
  if rank == C-1:

    if np.sign(np.linalg.det(U)*np.linalg.det(V)) == -1:
      # rotation is in fact a reflection, change S
      S[C-1, C-1] = -1.0

  elif rank > C-1:
    if np.sign(np.linalg.det(covar)) == -1:
      # rotation is in fact a reflection, change S
      S[C-1, C-1] = -1.0
  else:
    # bad rank
    return None

  c = np.trace(np.dot(np.diag(D), S))/varx
  R = np.dot(U, np.dot(S, V))
  t = muy-c*np.dot(R, mux)

  xT = np.dot(c*R, x.T).T + t.reshape((1,C))
  #T = np.dot(c*R, x.T)+t
  #err = np.sum(np.dot(y-xT, y-xT ))/N
  err = 0.0
  for i in range(N):
    err += np.dot(y[i] - xT[i], y[i]-xT[i])

  err /= N
  #print("Error:", err)
  Rt = np.zeros((4,4))
  Rt[0:3,0:3] = R*c
  Rt[0:3,3] = t
  Rt[3,3] = 1.0

  return (R, t, c, err, Rt)


