import numpy as np
from util import Camera, Frame
from util.pointCloud import transformPointCloud
from correspondences import LooCorrespondences
from photogrammetry.absoluteOrientation import umeyama_ransac

def leaveOneOutCalibration(det, leaveOut, config, cam1, frame1, cam2, frame2):
  """
  Performs leave one out calibration using umeyama's method. The error is measured on the points that are left out.

  Args:
    det (features.FeatureDetectorColection): A collection of the feature detectors to use
    leaveOut (list): List of feature descriptors (text) to leave out of the calibration.
    config (Util.Config): Configuration object to use.
    cam1 (Util.Camera): First camera
    frame1 (Util.Frame): Frame from the first camera
    cam2 (Util.Camera): Second camera
    frame2 (Util.Frame): Frame from the Second camera   

  Returns:
    (float, np.array)|None: The MSE of the calibration calculated with the points that were left out of the calibration and the transformation.
  """

  cor = LooCorrespondences(det, leaveOut)
  c3da, c3db, f1b, f2b = cor.find(frame1.colorFrame,
    frame1.depthFrame, 
    cam1.intrinsics.getMatrix(),
    frame2.colorFrame,
    frame2.depthFrame,
    cam2.intrinsics.getMatrix()
    )
  

  if c3db is None or c3db.shape[0] == 0:
    return None

  res = umeyama_ransac(c3da)
  if res is None:
    return None
  
  (Ro, t, c, _, Rt), inliers = res
  print('inliers: ', inliers)

  Rti = np.linalg.inv(Rt)
  # transform from camera 2 to camera 1's coordinate system
  tx = transformPointCloud(c3db[:,3:], Rti)
  x = c3db[:,0:3]
  N = c3db.shape[0]

  errs = []
  errsDist = []
  for i in range(N):
    errs.append(np.dot(x[i]-tx[i], x[i]-tx[i]))
    errsDist.append(np.linalg.norm(x[i]-tx[i]))

  errorStruct = {}
  errorStruct["var"] = np.var(errs)
  errorStruct["mse"] = np.mean(errs)
  errorStruct["rmse"] = np.sqrt(errorStruct["mse"])  
  errorStruct["min"] = np.min(errs)
  errorStruct["max"] = np.max(errs)
  errorStruct["dist_var"] = np.var(errsDist)
  errorStruct["dist_mean"] = np.mean(errsDist)
  errorStruct["dist_min"] = np.min(errsDist)
  errorStruct["dist_max"] = np.max(errsDist)

  return errorStruct, Rti, len(inliers), c3db.shape[0]
  
