import numpy as np
import features

def autoROI(img):
  """
  Automatically finds a region of interest (a checkerboard) that can be used
  to adjust the auto exposure of a real sense camera.

  Args:
    img (np.array): The color image to find the ROI in.
  
  Returns:
    (int, int, int, int): (minX, maxX, minY, maxY) - the region of interest
  """
  det = features.checkerboards.CheckerboardDetector("cb")
  boards, corners = det.detect(img)
  if len(boards) < 1:
    return None

  bb = features.checkerboards.libcbprocess.findBoundingBox(boards[0])
  minX = int(np.min(bb[:,0]))
  maxX = int(np.max(bb[:,0]))
  minY = int(np.min(bb[:,1]))
  maxY = int(np.max(bb[:,1]))

  return minX, maxX, minY, maxY