import sys
sys.path.append("../")
from rswrapper import *
import numpy as np
import skimage
import pickle
import os
from util import alignDepthToColor, Camera, Intrinsics, Frame
import matplotlib.pyplot as plt
from . import autoROI

def framesetFromConnectedCameras(cacheFile, verbose=False, autoexposureFrames=60, backClippingPlane=3.0, showCapture=True, setROI=False):
  """
  Captures a frameset from connected cameras or from a cachefile. If only one camera is connected, you will be prompted to take two images with the same camera.

  Args:
    cacheFile (str|None): If the result should be cached, cachefile is a the path to the file it should be cached in. This function will look for this file and return the cached result as well.

    verbose (bool): Whether to print to the screen or not

    autoexposureFrames (int): The number of frames to discard in order to wait for      autoexposure to settle. Set this to a negative number or 0 to not wait for auto exposure.

    backClippingPlane (float): Depth values further away than this value (in meters) will be set to 0.

    showCapture (bool): Whether to show the captured images or not. Images are only shown if there's a cache miss and new images are captured. 

    setROI (bool): Whether to automatically set the region of interest for the cameras. This requires a checkerboard to be visible in the image. It can help with adjusting the auto exposure properly such that the checkerboards can be detected.

  Returns:
    (list, list): The first list is a list of util.Frame objects containing each captured frame. The second list is a list of util.Camera objects describing each camera the corresponding frame was taken with.


  """
  out = []
  camerasOut = []

  if cacheFile is not None and os.path.exists(cacheFile):
    with open(cacheFile, "rb") as f:
      out, camerasOut = pickle.load(f)
      return out, camerasOut
  
  # No cache, find connected cameras and capture
  cameras = py_ku_ccal.CameraDiscoverer.find()
  if not cameras.count():
    if verbose: print("No cameras connected!")
    
    return None

  if verbose: print("{} cameras connected".format(cameras.count()))

  cameras.start()
  
  # settle auto exposure
  if autoexposureFrames > 0:
    cam0 = cameras[0]
    
    if verbose: print("Settling auto exposure")

    for i in range(autoexposureFrames):
      cam0.capture()

    if verbose: print("Auto exposure settled")  
  
  if setROI:
    i = 0
    for cam in cameras.getCameras():
      frame = cam.capture()
      cFrame = np.array(frame)
      roi = autoROI(cFrame)
      if roi is None:
        print("No ROI for camera {} (no checkerboards found)".format(i))
        continue
      
      minX, maxX, minY, maxY = roi
      minX = max(0, min(minX, cFrame.shape[1]))
      maxX = max(0, min(maxX, cFrame.shape[1]))
      minY = max(0, min(minY, cFrame.shape[0]))
      maxY = max(0, min(maxY, cFrame.shape[0]))
      cam.setROI(minX, minY, maxX, maxY)
      i += 1

  # capture
  if cameras.count() > 1:
    for cam in cameras.getCameras():

      K = cam.getRGBIntrinsics().getMatrix()
      frame = cam.capture()
      frameCopy = skimage.img_as_float32(np.array(frame))
      frame.enableDepthBuffer()

      depthCopy = np.array(frame)*cam.getDepthScale()

      depthCopy = alignDepthToColor(cam.getDepthIntrinsics().getMatrix(), cam.getRGBIntrinsics().getMatrix(), (1080, 1920), cam.getDepthToColorExtrinsics().getTransform(), depthCopy)
      # threshold points
      depthCopy[depthCopy >= backClippingPlane] = 0

      frame = Frame(None, frameCopy, depthCopy)
      out.append(frame)
      
      K = cam.getRGBIntrinsics().getMatrix()
      
      # Resolutions
      camOut = Camera((frameCopy.shape[0], frameCopy.shape[1]), (frameCopy.shape[0], frameCopy.shape[1]))
      camOut.serialNo = cam.getSerialNumber()
      I = Intrinsics(K[0,0], K[1,1], K[0,2], K[1,2], [])
      camOut.intrinsics = I

      camerasOut.append(camOut)

  else:
    # capture multiple views from one camera
    cam = cam0
    K = cam.getRGBIntrinsics().getMatrix()


    for i in range(2):
      frame = cam.capture()
      frameCopy = skimage.img_as_float32(np.array(frame))
      frame.enableDepthBuffer()

      depthCopy = np.array(frame)*cam.getDepthScale()

      depthCopy = alignDepthToColor(cam.getDepthIntrinsics().getMatrix(), cam.getRGBIntrinsics().getMatrix(), (1080, 1920), cam.getDepthToColorExtrinsics().getTransform(), depthCopy)
      # threshold points
      depthCopy[depthCopy >= 3.0] = 0

      frame = Frame(None, frameCopy, depthCopy)
      out.append(frame)
      
      K = cam.getRGBIntrinsics().getMatrix()
      
      camOut = Camera((1080, 1920), (1080, 1920))
      camOut.serialNo = cam.getSerialNumber()
      I = Intrinsics(K[0,0], K[1,1], K[0,2], K[1,2], [])
      camOut.intrinsics = I

      camerasOut.append(camOut)

      if i < 1:
        input("Press enter for next image")


  if showCapture:
    N = len(out)
    for i in range(0, N):
      plt.subplot(N, 2, 2*i+1)
      plt.title("Cam {} color".format(i))
      plt.imshow(out[i].colorFrame)
      plt.subplot(N, 2, 2*i+2)
      plt.title("Cam {} depth".format(i))
      plt.imshow(out[i].depthFrame, cmap="gray")
    
    plt.show()

  # stop cameras and deallocate resources
  cameras.stop()

  # cache
  if cacheFile is not None:
    with open(cacheFile, "wb") as f:
      pickle.dump((out, camerasOut), f)


  return out, camerasOut