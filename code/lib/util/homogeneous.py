import numpy as np 


def toHomogeneous(X):
  """
  Converts a numpy array to homogeneous coordinates

  Args:
    X (numpy.array): Nxd numpy array to convert to homogeneous coordinates
  
  Returns:
    (numpy.array): Nx(d+1) numpy array in homogeneous coordinates
  """
  N = X.shape[0]

  return np.hstack((X, np.ones((N, 1))))

def fromHomogeneous(X):
  """
  Converts a numpy array from homogeneous coordinates

  Args:
    X (numpy.array): Nx(d+1) numpy array in homogeneous coordinates
  
  Returns:
    (numpy.array): Nxd numpy array normalized with the homogeneous component.
  """
  N = X.shape[0]
  C = X.shape[1]

  if C < 2:
    raise RuntimeError("Input X must have at least dimension 2")

  output = np.zeros((N, C-1))
  output[:, :C-1] = X[:,:C-1]

  for i in range(C-2):
    output[:,i] /= X[:, C-1]

  return output 