import json
import os
import copy

class Config(object):

  def __init__(self, fname):
    self.file = fname
    self._load()

  def get(self, key, defaultValue=None):
    if key in self.__dict__:
      return self[key]

    if key.find(".") != -1:
      # split and recursive lookup
      splits = key.split(".")
      cur = self.__dict__
      for split in splits:
        if split in cur:
          cur = cur[split]
        else:
          return defaultValue
      
      return cur

    return defaultValue

  def __setitem__(self, key, value):
    self.__dict__[key] = value
  
  def __getitem__(self,key):
    return self.__dict__[key]

  def __contains__(self, key):
    return key in self.__dict__
  
  def __delitem__(self, key):
    del self.__dict__[key]

  def _load(self):
    if not os.path.exists(self.file):
      return
    
    with open(self.file, "r") as f:
      data = f.read()
      js = json.loads(data)

      fBack = self.file
      self.__dict__ = js
      self.file = fBack

  
  def save(self):
    with open(self.file, "w") as f:
      conf = copy.deepcopy(self.__dict__)
      del conf["file"]

      f.write(json.dumps(conf, sort_keys=True, indent=2))



    
    

