import numpy as np

class Extrinsics:
  """
  Class representing extrinsics between two cameras.
  Note that the rotation matrix is stored in column-major order,
  this is an artifact from the RealSense API.
  """

  def __init__(self, rotation, translation):
    self.rotation = rotation
    self.translation = translation

  def getTransform(self):
    r = self.rotation
    t = self.translation
    matrix = np.array([[r[0,0], r[1,0], r[2,0], t[0]],
                      [r[0,1], r[1,1], r[2,1], t[1]],
                      [r[0,2], r[1,2], r[2,2], t[2]],
                      [0, 0, 0, 1]])
    return matrix