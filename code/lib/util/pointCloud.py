import numpy as np
from util import deprojectPixelToPoint, toHomogeneous, fromHomogeneous
import open3d as o3d
import skimage

def transformPointCloud(pc, T):
  """
  Transforms a point cloud with the transformation T.
  Note that the point cloud is modified in place, you must pass a copy to maintain the original point cloud

  Args:
    pc (np.array): The input point cloud, must be Nx3 or Nx6 (colored)
    T (np.array): Transformation matrix, must be 4x4
  
  Returns:
    np.array: The transformed point cloud
  """
  pch = toHomogeneous(pc[:,0:3])
  transformed = np.dot(T, pch.T).T
  pc[:,0:3] = fromHomogeneous(transformed)

  return pc



def toPointCloud(depth, K, frontClippingPlane=0.3, backClippingPlane=3.0):
  """
  Create a point cloud from a depth frame.

  Args:
    depth (np.array): An image of the depth frame
    K (np.array): The camera intrinsics matrix
    frontClippingPlane (float): Clips values away that are closer to the camera than this in meters.
    backClippingPlane (float): Clips values away that are further away from the camera than this in meters.
  
  Returns:
    np.array: An array of dimension Nx3 of world coordinates in the camera coordinate system
  """
  idx = np.where( (depth <= backClippingPlane) & (depth >= frontClippingPlane))
  xs = idx[1]
  ys = idx[0]
  zs = depth[idx]

  return deprojectPixelToPoint(K, np.array([xs, ys, zs]), zs).T


def toColoredPointCloud(img, depth, K, frontClippingPlane=0.3, backClippingPlane=3.0):
  """
  Create a colored point cloud from a depth frame and image frame.

  Args:
    depth (np.array): An image of the depth frame
    img (np.array): The RGB image
    K (np.array): The camera intrinsics matrix
    frontClippingPlane (float): Clips values away that are closer to the camera than this in meters.
    backClippingPlane (float): Clips values away that are further away from the camera than this in meters.
  
  Returns:
    np.array: An array of dimension Nx6 of world coordinates in the camera coordinate system and colors.
    The first 3 columns are world coordinates, the last 3 columns is the RGB color for the point.
  """  

  img = skimage.img_as_float(img)

  idx = np.where( (depth <= backClippingPlane) & (depth >= frontClippingPlane))
  xs = idx[1]
  ys = idx[0]
  zs = depth[idx]

  out = np.zeros((xs.shape[0], 6))
  out[:, 0:3] = deprojectPixelToPoint(K, np.array([xs, ys, zs]), zs).T
  out[:,3:] = img[ys, xs]

  return out

def downsamplePointCloud(pointcloud, voxelSize):
  """
  Downsamples a point cloud to a specified voxelSize in meters.
  The resulting voxels have dimensions voxelSize*voxelSize*voxelSize

  Args:
    pointcloud (np.array): A Nx3 or Nx6 (colored) array representing the point cloud 
    voxelSize (float): voxel size in meters
  
  Returns:
    np.array: The downsampled point cloud
  """
  pc = toO3DPointCloud(pointcloud)
  pc.voxel_down_sample(voxel_size=voxelSize)


  return fromO3DPointCloud(pc)


def fromO3DPointCloud(pointcloud):
  """
  Converts a point cloud represented as a open3d point cloud to a numpy point cloud. 

  Args:
    pointcloud (o3d.geometry.PointCloud): A open3d point cloud
  
  Returns:
    np.array: A Nx3 or Nx6 (colored) point cloud
  """
  pc = np.asarray(pointcloud.points)
  colors = np.asarray(pointcloud.colors)

  if colors.shape[0] > 0:
    return np.hstack((pc, colors))

  return pc

def toO3DPointCloud(pointcloud):
  """
  Converts a point cloud represented as a numpy array to a open3d point cloud.

  Args:
    pc (np.array): A Nx3 or Nx6 (colored) point cloud as a numpy array
  
  Returns:
    o3d.geometry.PointCloud: A o3d point cloud
  """  
  pcd = o3d.geometry.PointCloud()
  pcd.points = o3d.utility.Vector3dVector(pointcloud[:,0:3])
  if pointcloud.shape[1] == 6:
    pcd.colors = o3d.utility.Vector3dVector(pointcloud[:, 3:])
  
  return pcd
