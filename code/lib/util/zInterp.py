import numpy as np
 
def zInterp(X, depth):
  # nearest neighbour z interpolation
  ys = X[:,1].round().astype("int")
  xs = X[:,0].round().astype("int")

  return depth[ys, xs]