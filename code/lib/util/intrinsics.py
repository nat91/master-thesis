import numpy as np

class Intrinsics:
  """
  Class representing camera intrinsics
  """

  def __init__(self, fx, fy, px, py, distortionCoeffs):
    self.fx = fx
    self.fy = fy 
    self.px = px
    self.py = py 
    self.distortionCoeffs = distortionCoeffs

  def getMatrix(self):
    return self.getIntrinsics()

  def getIntrinsics(self):
    matrix = np.array([[self.fx, 0, self.px],
                      [0, self.fy, self.py],
                      [0,0,1]])
    return matrix