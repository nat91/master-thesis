import csv
import os

# given a path to a camera output file eg. "test.recdat"
# it returns a list of cameras and their log files
def readRecdatFile(file):
  if not os.path.exists(file):
    return False
  
  path = os.path.dirname(file)
  cameras = []
  with open(file, "r") as f:
    reader = csv.reader(f, delimiter=",")
    for row in reader:
      cameras.append((row[0], path + '/' + row[1]))
  
  return cameras

