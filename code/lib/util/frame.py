class FrameMetaData:
  def __init__(self,
              rgbtimestamp,
              rgbFrameNo,
              depthtimestamp,
              depthFrameNo):
    self.rgbtimestamp = rgbtimestamp
    self.rgbFrameNo = rgbFrameNo
    self.depthtimestamp = depthtimestamp
    self.depthFrameNo = depthFrameNo


class Frame:

  def __init__(self, frameMetaData, colorFrame, depthFrame):
    self.frameMetaData = frameMetaData
    self.colorFrame = colorFrame
    self.depthFrame = depthFrame
  
  def frameData(self):
    return self.frameMetaData, self.colorFrame, self.depthFrame
