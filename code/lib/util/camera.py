
class Camera:
  """
  A realsense software camera whose properties can be set and retrieved.
  """

  def __init__(self, rgbResolution, depthResolution, **kwargs):
    self.rgbResolution = rgbResolution
    self.depthResolution = depthResolution

    if "depthScale" in kwargs:
      self.depthScale = kwargs["depthScale"]

    if "intrinsics" in kwargs:
      self.intrinsics = kwargs["intrinsics"] 

    if "depthIntrinsics" in kwargs:
      self.depthIntrinsics = kwargs["depthIntrinsics"] 

    if "extrinsics" in kwargs:
      self.extrinsics = kwargs["extrinsics"]
    
    if "serialNo" in kwargs:
      self.serialNo = kwargs["serialNo"]