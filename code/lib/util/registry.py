# Dict of dicts
class Registry:
  currentCam = None
  storage = dict()

  @staticmethod
  def get(key, defaultValue=None):
    # print('Registry get: ', Registry.storage)
    return Registry.storage[Registry.currentCam][key] if key in Registry.storage[Registry.currentCam] else defaultValue

  @staticmethod
  def put(key, value):
    # print('Registry put: ', Registry.storage)
    Registry.storage[Registry.currentCam][key] = value

  @staticmethod
  def getAll():
    # print('Registry getAll: ', Registry.storage)
    return Registry.storage[Registry.currentCam].items()

  @staticmethod
  def clear():
    # print('Registry clear: ', Registry.storage)
    Registry.storage[Registry.currentCam] = dict()

  @staticmethod
  def setCurrentCam(identifier):
    # print('Registry.currentCam: ', Registry.currentCam)
    Registry.currentCam = identifier
    # print('Registry.currentCam: ', Registry.currentCam)

  @staticmethod
  def newCamDict(identifier):
    Registry.currentCam = identifier
    Registry.storage[Registry.currentCam] = dict()