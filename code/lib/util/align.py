import numpy as np 
import copy
from .homogeneous import toHomogeneous, fromHomogeneous


def deprojectPixelToPoint(intrinsics, depthPixel, depth):
  # (x-px)/fx
  # (y-py)/fy
  
  depthPixel[0] = ((depthPixel[0]-intrinsics[0,2]) / intrinsics[0,0])*depth
  depthPixel[1] = ((depthPixel[1]-intrinsics[1,2]) / intrinsics[1,1])*depth
  depthPixel[2] = depth 

  return depthPixel

def transformPoint(depthToColorExtrinsics, point):
  return np.dot(depthToColorExtrinsics, point)

def projectPointToPixel(intrinsics, point):
  point /= point[2]
  #r = np.dot(intrinsics, point)
  point[0] = point[0]*intrinsics[0,0]+intrinsics[0,2]
  point[1] = point[1]*intrinsics[1,1]+intrinsics[1,2]

  return point

def alignDepthToColor(depthIntrinsics, colorIntrinsics, colorDim, depthToColorExtrinsics, depthFrame):
  """
  Aligns a depth frame to a color frame and returns the aligned depth frame. If they do not have the same dimensionality, the depth frame is upsampled with nearest neighbour interpolation.

  This is a direct port of the method "align_images" in: https://github.com/IntelRealSense/librealsense/blob/9f99fa9a509555f85bffc15ce27531aaa6db6f7e/src/proc/align.cpp


  Args:
    depthIntrinsics (np.array): The 3x3 intrinsics matrix of the depth camera
    colorIntrinsics (np.array): The 3x3 intrinsics matrix of the color camera
    colorDim (tuple): Integer pair of (height, width)
    depthToColorExtrinsics (np.array): The 4x4 extrinsics matrix that transforms from the depth camera coordinate system to the color camera coordinate system
    depthFrame (np.array): The depth frame to align
  
  Returns:
    (np.array): The aligned depth frame upsampled to the same dimension as the color image.
  """

  if type(colorDim) != tuple or len(colorDim) != 2:
    raise RuntimeError("colorDim must be a tuple of two integers")

  # allocate output
  out = np.zeros((colorDim))

  # iterate over pixels of the depth image and map each of them to the aligned image
  ys, xs = np.where(depthFrame > 1e-3)
  xs = xs.astype("float64")
  ys = ys.astype("float64")

  depths = depthFrame[depthFrame > 1e-3].flatten()

  ## map top left corner of the depth pixel onto the other image
  depthPixels = np.array([xs-0.5, ys-0.5, np.ones(xs.shape[0])])

  # 1. Deproject pixel to points
  depthPoints = deprojectPixelToPoint(depthIntrinsics, depthPixels, depths)
  # 2. convert to homogeneous coordinates
  depthPoints = toHomogeneous(depthPoints.T).T
  # 3. transform the points from the depth camera coordinate system to the RGB camera coordinate system
  depthPoints = transformPoint(depthToColorExtrinsics, depthPoints)
  # 4. convert back from homogeneous
  depthPoints = fromHomogeneous(depthPoints.T).T
  # 5. project back onto the RGB image plane
  rgbPixel = projectPointToPixel(colorIntrinsics, depthPoints).T

  rgbPixel[:,0] += 0.5
  rgbPixel[:,1] += 0.5

  x0s = rgbPixel[:,0].astype("int")
  y0s = rgbPixel[:,1].astype("int")

  ## map bottom-.right corner of the depth pixel onto the other image
  depthPixels2 = np.array([xs+0.5, ys+0.5, np.ones(xs.shape[0])])
  # 1. Deproject pixel to points
  depthPoints2 = deprojectPixelToPoint(depthIntrinsics, depthPixels2, depths)
  # 2. convert to homogeneous coordinates
  depthPoints2 = toHomogeneous(depthPoints2.T).T
  # 3. transform the points from the depth camera coordinate system to the RGB camera coordinate system
  depthPoints2 = transformPoint(depthToColorExtrinsics, depthPoints2)
  # 4. convert back from homogeneous
  depthPoints2 = fromHomogeneous(depthPoints2.T).T
  # 5. project back onto the RGB image plane
  rgbPixel2 = projectPointToPixel(colorIntrinsics, depthPoints2).T

  rgbPixel2[:,0] += 0.5
  rgbPixel2[:,1] += 0.5

  x1s = rgbPixel2[:,0].astype("int")
  y1s = rgbPixel2[:,1].astype("int")

  valid = np.where( (x0s >= 0) &  (y0s >= 0) & (x1s < colorDim[1]) &  (y1s  < colorDim[0]) )

  x0s = x0s[valid]
  x1s = x1s[valid]

  y0s = y0s[valid]
  y1s = y1s[valid]
  
  ys = ys[valid].astype("int")
  xs = xs[valid].astype("int")


  for i in range(x1s.shape[0]):
    #print(i, y0s[i], y1s[i]+1, x0s[i], x0s[i]+1, "len:", y1s[i]+1-y0s[i], x1s[i]+1-x0s[i])
    for y in range(y0s[i], y1s[i]+1):
     # out[y0s[i]:y1s[i]+1, x0s[i]:x1s[i]+1] = depthFrame[ys[i], xs[i]]
      for x in range(x0s[i], x1s[i]+1):
        if out[y,x] < 1e-3:
          out[y,x] = depthFrame[ys[i], xs[i]]
        else:
          out[y,x] = np.minimum(out[y,x], depthFrame[ys[i], xs[i]])
  #exit()
  return out


