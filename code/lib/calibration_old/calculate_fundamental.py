import numpy as np
import cv2

def calculate_sampson_error(Cs, F):
  cost = 0.0 

  if F.shape[0] == 9:
    F = F.reshape((3,3))

  Xl = Cs[:, 0:2]
  Xr = Cs[:, 2:]

  Xl = np.hstack((Xl, np.ones((Xl.shape[0],1))))
  Xr = np.hstack((Xr, np.ones((Xr.shape[0],1))))

  numerators = np.diag(np.dot(np.dot(Xr, F), Xl.T)**2.0).flatten()
  ps = (np.dot(F, Xl.T)**2.0 + np.dot(F.T, Xr.T)**2.0).T

  costs = numerators/(ps[:,0]+ps[:,1])
  
  return np.sum(costs)


# Fundamental matrix computation from OpenCV
def calculateFundamentalMatrix(C):
  pts1 = C[:, :2]
  pts2 = C[:, 2:]

  F, mask = cv2.findFundamentalMat(pts1, pts2, cv2.FM_LMEDS)
  idx = np.where(mask)

  return F, idx[0]
