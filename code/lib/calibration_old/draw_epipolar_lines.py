import matplotlib.pyplot as plt 
import numpy as np


def getLines(F, Cs, pos="left"):
  N = Cs.shape[0]
  Xleft = Cs[:, 0:2]
  Xright = Cs[:, 2:]

  a = []
  b = []

  for i in range(N):
    xr = Xright[i]
    xl = Xleft[i]
    v = np.reshape(np.array([xr[0], xr[1], 1.0]), (1,3))
    if pos == "left":

      # get coefficients
      c = np.dot(v, F)
      # line params
      a.append(-c[0][0]/c[0][1])
      b.append(-c[0][2]/c[0][1])

    else:
      v = np.reshape(np.array([xl[0], xl[1], 1.0]), (3,1))
      # get coefficients
      c = np.dot(F, v)
      # line params
      a.append(-c[0]/c[1])
      b.append(-c[2]/c[1])

  return zip(a,b)      


def drawEpipolarLines(img1, img2, F, Cs):
  
  Xleft = Cs[:, 0:2]
  Xright = Cs[:, 2:]
  
  plt.subplot(1, 2, 1)
  plt.title("Left image")
  plt.imshow(img1)
  plt.scatter(Xleft[:,0], Xleft[:,1], color="blue")

  coeffs = getLines(F, Cs)
  for (a,b) in coeffs:
    xs = np.linspace(0, img1.shape[1], num=img1.shape[1])
    ys = a*xs+b
    yIdx = np.where((ys < img1.shape[0]) & (ys >= 0))    
    plt.plot(xs[yIdx], ys[yIdx], color="red")
  
  plt.subplot(1, 2, 2)
  plt.title("Right image")
  plt.imshow(img2)
  plt.scatter(Xright[:,0], Xright[:,1], color="blue")

  coeffs = getLines(F, Cs, pos="right")
  for (a,b) in coeffs:
    xs = np.linspace(0, img2.shape[1], num=img2.shape[1])
    ys = a*xs+b
    yIdx = np.where((ys < img2.shape[0]) & (ys >= 0))
    plt.plot(xs[yIdx], ys[yIdx], color="red")
  
  plt.show()

  