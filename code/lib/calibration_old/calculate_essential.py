import os
import pickle
import sys
import numpy as np
import cv2
sys.path.append('../')


def triangulatePoints(P1, P2, x1s, x2s):
  N = x1s.shape[0]
  A = np.zeros((4, 4))

  outPtrs = np.zeros((N, 4))

  for i in range(N):
    A[0,:] = x1s[i,0]*P1[2]-P1[0]
    A[1,:] = x1s[i,1]*P1[2]-P1[1]
    A[2,:] = x2s[i,0]*P2[2]-P2[0]
    A[3,:] = x2s[i,1]*P2[2]-P2[1]
    U, S, V = np.linalg.svd(A)
    outPtrs[i] = V[-1]/V[-1,3]

  return outPtrs



def computePFromEssential(E, x1, x2):
  """
   Calculates the normalized camera matrix for the right camera, given the essential matrix. This assumes that the left camera center is the center of the world coordinate system.

   Args:
    E (np.array): The Essential matrix of the two cameras
    x1 (np.array): A normalized coordinate calculated as K^(-1)x'
    where x' is an image coordinate
    x2 (np.array): A normalized coordinate calculated as K^(-1)x' where x is an image coordinate, must be a corresponding point to x1
   
   Returns:
    (np.array): The normalized camera matrix where the point x is in front of both cameras.
  """
  U, _, V = np.linalg.svd(E)

  # create matrices ("Hartley and zisserman p. 258")
  # W is an orthogonal matrix
  W = np.array([[0, -1, 0], [1, 0, 0], [0, 0, 1]])

  # all 4 possible solutions to P, only one of them has the point in front of both cameras.
  P2 = [np.vstack((np.dot(U, np.dot(W, V)).T,  U[:,2])).T,
        np.vstack((np.dot(U, np.dot(W, V)).T, -U[:,2])).T,
        np.vstack((np.dot(U, np.dot(W.T, V)).T,  U[:,2])).T,
        np.vstack((np.dot(U, np.dot(W.T, V)).T, -U[:,2])).T]

  P1 = np.zeros((3,4))
  P1[0:3, 0:3] = np.eye(3)

  #print("P1:", P1)

  maxRes = 0
  bestI = np.inf
  Xs = None
  # find the valid solution in P2
  for i in range(4):
    res = triangulatePoints(P1, P2[i], x1, x2)
    d1 = np.dot(P1, res.T)[2]
    d2 = np.dot(P2[i], res.T)[2]
    numValid = np.sum(d1 > 0) + np.sum(d2 > 0)
    if numValid > maxRes:
      maxRes = numValid
      bestI = i
      #print("Best: ", maxRes, bestI)
      Xs = res

  return P2[bestI], Xs.T

def calculateEssentialMatrix(KLeft, F, KRight):
  """
  Computes an Essential matrix of rank 2

  Args:
    KLeft (np.array): The intrinsic matrix of the left camera
    F (np.array): The Fundamental matrix of the two cameras
    KRight (np.array): The intrinsic matrix of the right camera

  Returns:
    (np.array): The 3x3 Essential matrix of rank 2
  """
  E = KRight.T @ F @ KLeft

  # make sure E is rank 2
  U, S, V = np.linalg.svd(E)

  # remove sign ambiguity
  if np.linalg.det(np.dot(U, V)) < 0:
    V = -V  
  
  # ensure 5 DoF
  E = np.dot(np.dot(U, np.diag([1,1,0])), V)

  return E
