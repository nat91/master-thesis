void setup() {   
  Serial.begin(9600);          
  pinMode(12, OUTPUT);
  digitalWrite(12, LOW);
  Serial.println(F("Ready"));
}

void loop() {
  // Establish connection
  while (Serial.available() == 0) {}
  int numOfFlash = Serial.read();
  Serial.println(F("numOfFlash received!"));

  while (Serial.available() == 0) {}
  int interval = Serial.read();
  interval = interval * 1000;
  Serial.println(F("Interval received!"));

  int flashCount = 0;
  while (flashCount < numOfFlash) {
      digitalWrite(12, HIGH);   // Trigger the flash
      delay(10);                // Wait for a 1/100 second
      digitalWrite(12, LOW);    // Turn off the flash
      if(flashCount + 1 < numOfFlash)
        delay(interval);          // Wait X seconds for flash to recharge
      flashCount = flashCount + 1;
  }

  Serial.println(F("Stopped"));
}
