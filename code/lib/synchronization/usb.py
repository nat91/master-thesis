import serial, time
import os

def startFlash(baudRate, numOfFlash, interval, async=False):

  # Convert to bytes
  numOfFlash = bytes([numOfFlash])
  interval = bytes([interval])

  arduino = None
  proc = os.popen("dmesg | egrep ttyACM | cut -f3 -d: | tail -n1")
  procRes = proc.read()
  arduinoPort = "/dev/"+procRes.strip()

  arduino = serial.Serial(arduinoPort, baudRate)
  msg = arduino.readline().decode("utf-8").rstrip("\r\n")
  if msg != "Ready":
    raise RuntimeError("Ready message not received from Arduino, received: {}".format(msg))

  # Send number of flashes
  # and confirm that Serial comunication is working
  res = arduino.write(numOfFlash)
  arduino.flush()

  msg = arduino.readline().decode('utf-8')

  if msg == 'numOfFlash received!\r\n':
    print('ok: numOfFlash received')
  else:
    print('msg: ', msg)
    raise ConnectionError('numOfFlash msg failed.')

  # Send interval
  arduino.write(interval)
  while arduino.in_waiting == 0:
    pass

  msg = arduino.readline().decode('utf-8')
  if msg == 'Interval received!\r\n':
    print('ok: interval received and flash started!')
  else:
    print('msg: ', msg)
    raise ConnectionError('interval msg failed.')
  
  if not async:
    msg = arduino.readline().decode("utf-8").rstrip("\r\n")
    if msg != "Stopped":
      raise RuntimeError("Ready message not received from Arduino, received: {}".format(msg))


if __name__ == "__main__":
  startFlash(9600, 5, 5)