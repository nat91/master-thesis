import os
import struct
import sys
import copy
import multiprocessing as mp
from halo import Halo

import numpy as np
import matplotlib.pyplot as plt

sys.path.append('../')
from reader.reader import FileReader
from viewer.util import get_camera_files_from_file


showGraphs = False



# Set the pixel intensity of events
eventThreshold = 50
rampThreshold = 50

fps = 30
frametime = 1/fps
rowtime = frametime / 1080

def rgb2gray(rgb):

  r, g, b = rgb[:,:,0], rgb[:,:,1], rgb[:,:,2]
  gray = 0.2989 * r + 0.5870 * g + 0.1140 * b

  return gray


# ---------------------------------------------------------------------------- #
#                                    Phase 1                                   #
# ---------------------------------------------------------------------------- #
@Halo(text='Finding Events', spinner='dots')
def findEvents(fileAndFreq):
  inputFile, eventFrequency = fileAndFreq
  handler = open(inputFile, 'rb')

  fileReader = FileReader(50, handler)
  fileMetadata = fileReader.header()
  # Skip auto exposure
  for _ in range(0,30):
    fileReader.nextFrame()

  (frameMetadata, colorFrame, _) = fileReader.currentFrame.frameData()
  frameNo = frameMetadata.rgbFrameNo
  timestamp = frameMetadata.rgbtimestamp

  frame_row_intensities = []
  framenumbers = []
  timestamps = []

  #### For debug
  prevFrameNo = frameNo
  prevTimestamp = timestamp
  ####

  for i in range(30, fileMetadata.numberOfFrames-1):
    frame = rgb2gray(colorFrame)
    frame_row_intensities.append(np.median(frame, axis=1))
    frameNo = frameMetadata.rgbFrameNo
    timestamp = frameMetadata.rgbtimestamp

    #### For debug
    if frameNo == prevFrameNo + 1 and timestamp - prevTimestamp > 40000:
      print('FPS ERROR!')
      print('prevFrameNo: ', prevFrameNo)
      print('frameNo: ', frameNo)
    prevFrameNo = frameNo
    prevTimestamp = timestamp

    framenumbers.append(frameNo)
    timestamps.append(timestamp)
    (frameMetadata, colorFrame, depthFrame) = fileReader.nextFrame().frameData()

  differences = []

  for i in range(1, len(frame_row_intensities)):
    differences.append(frame_row_intensities[i] - frame_row_intensities[i-1])


  if showGraphs:
    medians = [np.max(x) for x in differences]
    plt.scatter(range(len(medians)), medians, c= "r", marker="_")
    plt.title("Maximum per Frame Changes in Median Row Intensity")
    plt.xlabel("Frame number")
    plt.ylabel("Intensity")
    plt.ylim(-10, 150)
    plt.show()

  # Collect event framenumber, timestamp, and ramp row index
  events = []
  for i in range(len(frame_row_intensities)-1):
    if np.max(differences[i]) > eventThreshold:
      # Find first row that saw flash event
      eventRow = np.argmax(differences[i] > rampThreshold)
      events.append((framenumbers[i+1], timestamps[i+1], eventRow))

  # Need to join events that span more than 1 frame.
  prunedEvents = pruneEvents(events, eventFrequency)
  
  handler.close()

  return prunedEvents

# ---------------------------------------------------------------------------- #
#                                    Phase 2                                   #
# ---------------------------------------------------------------------------- #

@Halo(text='Solving Alignment', spinner='dots')
def solveEventAlignment(c1_e, c2_e):
  if len(c1_e) != len(c2_e):
    # The cameras have not seen the same number of events
    print('Error: One camera has seen more flashes than the other.\n')
  elif len(c1_e) == 0 and len(c2_e) == 0:
    print('No flash events detected.\n')
  else:
    lh_side = np.empty([len(c1_e), 4])
    rh_side = np.empty(len(c1_e))
    for i in range(len(c1_e)):
      tc = c2_e[i][1]
      rc = c2_e[i][2]
      # Tc_row = Tframe(ms) / Total number of sensor rows 
      # Tc_row = rowtime

      tcref = c1_e[i][1]
      rcref = c1_e[i][2]

      # a * tc + B + rc * tc_row - rcref * Tcref_row = tcref
      lh_side[i] = np.array([tc, 1, rc, -rcref])
      rh_side[i] = tcref

    solution = np.linalg.lstsq(lh_side, rh_side, rcond=None)[0]
    # print('solution: ', solution)
    return solution


# ---------------------------------------------------------------------------- #
#                                    Wrapper                                   #
# ---------------------------------------------------------------------------- #
def ajustTimestamp(alpha, beta, Tc_row, t, r):
  return int(alpha * t + beta + Tc_row * r)


def writeFileHeader(fp, header):
  fp.write(struct.pack('H', header.resolutionRGBW))
  fp.write(struct.pack('H', header.resolutionRGBH))
  fp.write(struct.pack('H', header.resolutionDepthW))
  fp.write(struct.pack('H', header.resolutionDepthH))
  fp.write(struct.pack('Q', header.fps))
  fp.write(struct.pack('Q', header.numberOfFrames))
  fp.write(struct.pack('f', header.dScale))
  fp.write(struct.pack('f', 0))
  fp.write(struct.pack('f', header.rgbIntrinsics.fx))
  fp.write(struct.pack('f', header.rgbIntrinsics.fy))
  fp.write(struct.pack('f', header.rgbIntrinsics.px))
  fp.write(struct.pack('f', header.rgbIntrinsics.py))
  fp.write(struct.pack('f', header.rgbIntrinsics.distortionCoeffs[0]))
  fp.write(struct.pack('f', header.rgbIntrinsics.distortionCoeffs[1]))
  fp.write(struct.pack('f', header.rgbIntrinsics.distortionCoeffs[2]))
  fp.write(struct.pack('f', header.rgbIntrinsics.distortionCoeffs[3]))
  fp.write(struct.pack('f', header.rgbIntrinsics.distortionCoeffs[4]))
  fp.write(struct.pack('f', 0))
  fp.write(struct.pack('f', header.depthIntrinsics.fx))
  fp.write(struct.pack('f', header.depthIntrinsics.fy))
  fp.write(struct.pack('f', header.depthIntrinsics.px))
  fp.write(struct.pack('f', header.depthIntrinsics.py))
  fp.write(struct.pack('f', header.depthIntrinsics.distortionCoeffs[0]))
  fp.write(struct.pack('f', header.depthIntrinsics.distortionCoeffs[1]))
  fp.write(struct.pack('f', header.depthIntrinsics.distortionCoeffs[2]))
  fp.write(struct.pack('f', header.depthIntrinsics.distortionCoeffs[3]))
  fp.write(struct.pack('f', header.depthIntrinsics.distortionCoeffs[4]))
  fp.write(struct.pack('f', 0))
  fp.write(struct.pack('f', header.depthToColorExtrinsics.rotation[0,0]))
  fp.write(struct.pack('f', header.depthToColorExtrinsics.rotation[0,1]))
  fp.write(struct.pack('f', header.depthToColorExtrinsics.rotation[0,2]))
  fp.write(struct.pack('f', header.depthToColorExtrinsics.rotation[1,0]))
  fp.write(struct.pack('f', header.depthToColorExtrinsics.rotation[1,1]))
  fp.write(struct.pack('f', header.depthToColorExtrinsics.rotation[1,2]))
  fp.write(struct.pack('f', header.depthToColorExtrinsics.rotation[2,0]))
  fp.write(struct.pack('f', header.depthToColorExtrinsics.rotation[2,1]))
  fp.write(struct.pack('f', header.depthToColorExtrinsics.rotation[2,2]))
  fp.write(struct.pack('f', header.depthToColorExtrinsics.translation[0]))
  fp.write(struct.pack('f', header.depthToColorExtrinsics.translation[1]))
  fp.write(struct.pack('f', header.depthToColorExtrinsics.translation[2]))
  fp.write(bytearray(1024)) # reserved space

def writeFrame(fp, frame):
  fp.write(struct.pack('q', frame.frameMetaData.rgbtimestamp))
  fp.write(struct.pack('q', frame.frameMetaData.rgbFrameNo))
  fp.write(struct.pack('q', frame.frameMetaData.depthtimestamp))
  fp.write(struct.pack('q', frame.frameMetaData.depthFrameNo))
  fp.write(frame.colorFrame.astype('B').tostring())
  fp.write(frame.depthFrame.astype('H').tostring())

def alignTimestamps(refRecordingFile, OtherRecordingFiles, flashFrequency, outputFolder, expRange=None):
  """
  Takes a path argument to a reference camera recording and a recording to be aligned.
  Returns aligned timestamps of the second recording.

  Args:
  refRecordingFile (filepath): Path to reference recording.
  OtherRecordingFile (filepath): Path to file that needs alignment.

  Returns:
  Does not return anything. Aligned timestamps written to file.
  """

  pool = mp.Pool(mp.cpu_count())

  handlers = [refRecordingFile]
  handlers.extend([(OtherRecordingFile, flashFrequency) for OtherRecordingFiles in OtherRecordingFiles])

  # Find flash events for each file (multiprocessing)
  flashEvents = pool.map(findEvents, handlers)
  pool.close()

  # Fist camera as reference camera
  spinner = Halo(text='Correcting reference file', spinner='dots')
  spinner.start()

  if expRange is None:
    referenceEvents = flashEvents[0]
    otherEvents = flashEvents[1:]
  else:
    referenceEvents = [flashEvents[0][index] for index in expRange]
    otherEvents = list(map(lambda x: [x[index] for index in expRange], flashEvents[1:]))

  refLastEventFrameNo = referenceEvents[-1][0]

  ref = open(refRecordingFile, 'rb')
  referenceFileReader = FileReader(50, ref)

  # Clean reference file of time synchronisation phase
  refFramesDeleted = 0
  while referenceFileReader.currentFrame.frameMetaData.rgbFrameNo <= refLastEventFrameNo:
    referenceFileReader.nextFrame()
    refFramesDeleted += 1 # How many frames are removed

  # Create updated header for new files
  refTotalFrames = referenceFileReader.fileMetaData.numberOfFrames

  refMetaData = copy.deepcopy(referenceFileReader.fileMetaData)
  refMetaData.numberOfFrames = refTotalFrames - refFramesDeleted - 1

  newRefFileName = os.path.join(outputFolder, os.path.split(refRecordingFile)[-1])
  print('newRefFileName: ', newRefFileName)
  newRefFile = open(newRefFileName,"wb")
  writeFileHeader(newRefFile, refMetaData)

  offset = referenceFileReader.currentFrame.frameMetaData.rgbFrameNo
  dOffset = referenceFileReader.currentFrame.frameMetaData.depthFrameNo
  while not referenceFileReader.atEOF:
    referenceFileReader.currentFrame.frameMetaData.rgbFrameNo = referenceFileReader.currentFrame.frameMetaData.rgbFrameNo - offset
    referenceFileReader.currentFrame.frameMetaData.depthFrameNo = referenceFileReader.currentFrame.frameMetaData.depthFrameNo - dOffset
    writeFrame(newRefFile, referenceFileReader.currentFrame)  
    referenceFileReader.nextFrame()  

  ref.close()
  newRefFile.close()
  spinner.stop()



  # spinner = Halo(text='Correcting remaining files', spinner='dots')
  # spinner.start()
  for i, recordingPath in enumerate(OtherRecordingFiles):

    ref, other = matchEvents(referenceEvents, otherEvents[i])


    # Get last frame of time synchronization phase    
    otherLastEventFrameNo = otherEvents[i][-1][0]    

    # Solve linear system
    alpha, beta, Tc_row, Tcref_row = solveEventAlignment(ref, other)
  
    # Reopen file, to ensure it is at beginning (rewind instead at some point
    other = open(recordingPath, 'rb')
    otherFileReader = FileReader(50, other)
    # Clean other file of time synchronisation phase
    otherFramesDeleted = 0
    while otherFileReader.currentFrame.frameMetaData.rgbFrameNo <= otherLastEventFrameNo:
      otherFileReader.nextFrame()
      otherFramesDeleted += 1 # How many frames are removed

    otherTotalFrames = otherFileReader.fileMetaData.numberOfFrames

    # Copy metadata object, so we can rewrite without destroying old
    otherMetaData = copy.deepcopy(otherFileReader.fileMetaData)
    otherMetaData.numberOfFrames = otherTotalFrames - otherFramesDeleted - 1

    newOtherFileName = os.path.join(outputFolder, os.path.split(recordingPath)[-1])
    newOtherFile = open(newOtherFileName,"wb")
    writeFileHeader(newOtherFile, otherMetaData)

    offset = otherFileReader.currentFrame.frameMetaData.rgbFrameNo
    dOffset = otherFileReader.currentFrame.frameMetaData.depthFrameNo
    while not otherFileReader.atEOF:
      otherFileReader.currentFrame.frameMetaData.rgbFrameNo = otherFileReader.currentFrame.frameMetaData.rgbFrameNo - offset
      otherFileReader.currentFrame.frameMetaData.depthFrameNo = otherFileReader.currentFrame.frameMetaData.depthFrameNo - dOffset
      # Align second recordings timestamps with reference
      # Should we remove "+ Tc_row - x2" since we are at first row always?
      otherFileReader.currentFrame.frameMetaData.rgbtimestamp = ajustTimestamp(alpha, beta, Tc_row, otherFileReader.currentFrame.frameMetaData.rgbtimestamp, 0)
      otherFileReader.currentFrame.frameMetaData.depthtimestamp = ajustTimestamp(alpha, beta, Tc_row, otherFileReader.currentFrame.frameMetaData.depthtimestamp, 0)
      writeFrame(newOtherFile, otherFileReader.currentFrame)
      
      otherFileReader.nextFrame()

    newOtherFile.close()
    other.close()
    spinner.stop()


    ########################### EXPERIMENTS ###########################
    referenceExpEvents = flashEvents[0]
    otherExpEvents = flashEvents[1:]
    # print('flashEvents: ', flashEvents)
    # print('otherExpEvents: ', otherExpEvents)

    adjustedEvents = []
    for event in otherExpEvents[i]:
      t, r = (event[1], event[2])
      adjustedEvents.append(ajustTimestamp(alpha, beta, Tc_row, t, r))
    
    adjustedRefEvents = []
    for event in referenceExpEvents:
      t, r = (event[1], event[2])
      adjustedRefEvents.append(ajustTimestamp(1, 0, Tcref_row, t, r))

    # print('adjustedRefEvents: ', adjustedRefEvents)
    # print('adjustedEvents: ', adjustedEvents)

    def getErrors(list1, list2):
      errors = []
      for i, elem in enumerate(list1):
        errors.append(list2[i] - elem)
      return errors
    
    errors = getErrors(adjustedRefEvents, adjustedEvents)
    print('errors: ', errors)
    ########################### EXPERIMEMNT STOP ######################


def matchEvents(refEvents, otherEvents):
  def getBadEvents(l):
    listOfEventRowNo = [x[2] for x in l]
    badEventIndices = [i for i, x in enumerate(listOfEventRowNo) if x == 0]
    return badEventIndices

  refBad = getBadEvents(refEvents)
  otherBad = getBadEvents(otherEvents)

  toExclude = list(set(refBad + otherBad))
  print('toExclude: ', toExclude)

  ref = [element for i, element in enumerate(refEvents) if i not in toExclude]
  other = [element for i, element in enumerate(otherEvents) if i not in toExclude]

  return ref, other

def pruneEvents(eventList, flashFrequency):
  def getNextIndex(eventList, i):
    # print('\n(eventList, i): ', (eventList, i))
    currentFrame = eventList[i][0]
    j = i
    while eventList[j][0] - currentFrame < flashFrequency:
      # print('\nj: ', j)
      # print('\neventList[j][0]: ', eventList[j][0])
      if j == len(eventList) - 1:
        return j + 1
      j = j + 1
    return j

  # print('eventList: ', eventList)
  prunedEvents = []
  i = 0
  while i <= len(eventList)-1:
    # Special case where second to last frame sees event
      prunedEvents.append(eventList[i])
      i = getNextIndex(eventList, i)
      continue
  return prunedEvents
