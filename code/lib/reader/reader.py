import gc
import struct
import time as tm

import matplotlib.pyplot as plt
import numpy as np
from util import Intrinsics, Extrinsics, Frame, FrameMetaData


class FileMetaData:
  def __init__(self,
         resolutionRGBW,
         resolutionRGBH,
         resolutionDepthW,
         resolutionDepthH,
         fps,
         numberOfFrames,
         dScale,
         rgbIntrinsics,
         depthIntrinsics,
         depthToColorExtrinsics
         ):

    self.resolutionRGBW = resolutionRGBW
    self.resolutionRGBH = resolutionRGBH
    self.resolutionDepthW = resolutionDepthW
    self.resolutionDepthH = resolutionDepthH
    self.fps = fps
    self.numberOfFrames = numberOfFrames
    self.dScale = dScale
    self.rgbIntrinsics = rgbIntrinsics
    self.depthIntrinsics = depthIntrinsics
    self.depthToColorExtrinsics = depthToColorExtrinsics


class Unpacker:

  def unpackUInt8(self, f):
    fst = struct.unpack('B', f.read(1))
    return fst[0]

  def unpackUInt16(self, f):
    fst = struct.unpack('H', f.read(2))
    return fst[0]

  def unpackUInt64(self, f):
    fst = struct.unpack('Q', f.read(8))
    return fst[0]

  def unpackLong(self, f):
    fst = struct.unpack('L', f.read(8))
    return fst[0]

  def unpackFloat(self, f):
    fst = struct.unpack('f', f.read(4))
    return fst[0]

  def unpackIntrinsics(self, f):
    res = Intrinsics(
      self.unpackFloat(f), # fx
      self.unpackFloat(f), # fy
      self.unpackFloat(f), # px
      self.unpackFloat(f), # py
      [self.unpackFloat(f) for i in range(5)] # distortion coefficients
    )
    # zero padding
    self.unpackFloat(f)

    return res

  def unpackExtrinsics(self, f):
    return Extrinsics(
      np.array([self.unpackFloat(f) for i in range(9)]).reshape((3,3)),
      np.array([self.unpackFloat(f) for i in range(3)])
    )

class FileReader(Unpacker):

  def __init__(self, framesInBuffer, f):
    """
    Takes number of frames to buffer and an open filepointer and return a filereader object.

    Args:
    framesInBuffer (int): How many frames to buffer.
    f (filepointer): Open filepointer of the image file.

    Returns:
    Filereader.
    """
    self._framesInBuffer = framesInBuffer
    self._f = f

    # Unpack file metadata
    resolutionRGBW = self.unpackUInt16(f)
    resolutionRGBH = self.unpackUInt16(f)
    resolutionDepthW = self.unpackUInt16(f)
    resolutionDepthH = self.unpackUInt16(f)
    fps = self.unpackUInt64(f)
    numberOfFrames = self.unpackUInt64(f)
    dScale = self.unpackFloat(f)
    # zero padding
    self.unpackFloat(f)

    rgbIntrinsics = self.unpackIntrinsics(f)
    depthIntrinsics = self.unpackIntrinsics(f)
    depthToColorExtrinsics = self.unpackExtrinsics(f)

    self.fileMetaData = FileMetaData(
      resolutionRGBW,
      resolutionRGBH,
      resolutionDepthW,
      resolutionDepthH,
      fps,
      numberOfFrames,
      dScale,
      rgbIntrinsics,
      depthIntrinsics,
      depthToColorExtrinsics
    )

    # move buffer ahead of the reserved padding, relative to the current position
    reservedSize = 1024
    f.seek(reservedSize, 1)

    # Float + int
    self.frameMetaDataSize = 32
    # Size of cFrame + dFrame + metadata
    self.cframeSize = self.fileMetaData.resolutionRGBW*self.fileMetaData.resolutionRGBH*3
    self.dframeSize =  self.fileMetaData.resolutionDepthW*self.fileMetaData.resolutionDepthH*2 
    self.frameSize = self.cframeSize + self.dframeSize + self.frameMetaDataSize
    # Buffer for frames plus metadata
    self.buf = f.read(framesInBuffer*self.frameSize)
    # Track the index of start and end frames in buffer
    self.buf_start = 0
    self.buf_end = framesInBuffer - 1
    self.buf_index = 0

    self.currentFrame = self.readFrame()

    self.atEOF = False

  def setEOF(self):
    # print('buf_index', (self.buf_index, self.fileMetaData.numberOfFrames))
    if self.buf_index == self.fileMetaData.numberOfFrames - 1:
      self.atEOF = True
    else:
      self.atEOF = False

  def header(self):
    return self.fileMetaData

  def readFrame(self):

    frameStart = (self.buf_index - self.buf_start) * self.frameSize

    rgbTimeStamp = struct.unpack('Q', self.buf[slice(frameStart, frameStart+8)])[0]
    rgbFrameNo = struct.unpack('Q', self.buf[slice(frameStart+8, frameStart+16)])[0]
    depthTimeStamp = struct.unpack('Q', self.buf[slice(frameStart+16, frameStart+24)])[0]
    depthFrameNo = struct.unpack('Q', self.buf[slice(frameStart+24, frameStart+32)])[0]

    self.frameMetaData = FrameMetaData(rgbTimeStamp, rgbFrameNo, depthTimeStamp, depthFrameNo)

    byteSlice = slice(frameStart + self.frameMetaDataSize, frameStart + self.frameMetaDataSize + self.cframeSize)
    readFrame = self.buf[byteSlice]
    colorFrame = np.ndarray((self.fileMetaData.resolutionRGBH,self.fileMetaData.resolutionRGBW,3), '<B', readFrame)

    byteSlice = slice(frameStart + self.frameMetaDataSize + self.cframeSize, frameStart + self.frameSize)
    readFrame = self.buf[byteSlice]
    depthFrame = np.ndarray((self.fileMetaData.resolutionDepthH,self.fileMetaData.resolutionDepthW), '<H', readFrame)

    self.currentFrame = Frame(self.frameMetaData, colorFrame, depthFrame)
    
    return self.currentFrame

  def nextFrame(self):
    if self.atEOF:
      pass
    elif self.buf_index == self.buf_end:
      self.buf = self._f.read(self._framesInBuffer * self.frameSize)
      self.buf_start = self.buf_end + 1
      self.buf_end += self._framesInBuffer
      self.buf_index += 1
      self.currentFrame = self.readFrame()
    else:
      self.buf_index += 1
      self.currentFrame = self.readFrame()

    self.setEOF()
    return self.currentFrame

  def prevFrame(self):
    if self.buf_index == 0:
      pass
    elif self.buf_index == self.buf_start:
      self._f.seek(- self.frameSize * (self._framesInBuffer + 1), 1)
      self.buf = self._f.read(self._framesInBuffer*self.frameSize)
      self.buf_start = self.buf_start -1
      self.buf_end = self.buf_end - 1
      self.buf_index = self.buf_index - 1
      self.currentFrame = self.readFrame()
    else:
      self.buf_index = self.buf_index - 1
      self.currentFrame = self.readFrame()
      
    self.setEOF()
    return self.currentFrame

  def skipFrames(self, n):
    if self.buf_index + n > self.fileMetaData.numberOfFrames:
      toEnd = self.fileMetaData.numberOfFrames - self.buf_index
      self.skipFrames(toEnd)
    elif self.buf_index + n < 0:
      self.skipFrames(-self.buf_index)
    elif self.buf_index + n > self.buf_end:
      self._f.seek(((self.buf_index + n) - self.buf_end - 1) * self.frameSize, 1)
      self.buf = self._f.read(self._framesInBuffer*self.frameSize)
    # Track the index of start and end frames in buffer
      self.buf_start = self.buf_index + n
      self.buf_end = self.buf_start + self._framesInBuffer - 1
      self.buf_index = self.buf_start
      self.currentFrame = self.readFrame()
    elif self.buf_index + n < self.buf_start:
      self._f.seek(- self.frameSize * (self._framesInBuffer + (-n)), 1)
      self.buf = self._f.read(self._framesInBuffer*self.frameSize)
    # Track the index of start and end frames in buffer
      self.buf_start = self.buf_start + n
      self.buf_end = self.buf_start + self._framesInBuffer - 1
      self.buf_index = self.buf_start
      self.currentFrame = self.readFrame()
    else:
      self.buf_index += n
      self.currentFrame = self.readFrame()
    
    self.setEOF()
    return self.currentFrame

  def frameMetadata(self):
    return (self.currentFrame.metaData())

  def getRGBTimestamps(self):
    curPos = self._f.tell()
    self._f.seek(64,0)
    timestamps = []
    for _ in range(0,self.header().numberOfFrames):
      timestamps.append(self.unpackUInt64(self._f))
      self._f.seek(24+self.cframeSize+self.dframeSize, 1)
    self._f.seek(curPos,0)
    return timestamps
