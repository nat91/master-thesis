import networkx as nx
import numpy as np

def getBestTransformPath(G, target, source, minWeight, k=np.inf, visited=set()):
  if source == target:
    return (0, [target])
  
  if k == 0:
    return None

  nb = G.adj[source]
  vs = list(nb.keys())
  if len(nb) == 0:
    return None
  
  weights = [nb[v]["weight"] for v in vs]
  argmax = np.flip(np.argsort(weights))

  visited.add(source)

  for j in argmax:
    if weights[j] < minWeight:
      continue 

    node = vs[j]
    if node in visited:
      continue # infinite loop prevention
    visited.add(node)

    res = getBestTransformPath(G, target, node, minWeight, k-1, visited)
    if res is None:
      # target not reachable in k steps
      visited.remove(node) # make sure node can still be visited by other edges
    else:
      cost, path = res
      visited.remove(source)
      return (cost+weights[j], [source]+path)
    
  visited.remove(source)
  return None


def getBestTransformPaths(G, target, tryKs=3, minEdgeWeight=20, weightReduction=5):
  """
  Greedy algorithm that attempts to greedily find the longest path (this is a NP hard problem) from every source vertex to the given target. It will also attempt to establish a shortest, but longest, path under the constraint that all edges have a minimum weight.

  Args:
    G (networkx.Graph): The graph
    
    target: Can be any type, its the vertex name of the target.
    
    tryKs (int): Try to find shortest highest cost paths up to length k, under the constraint that all edges must have minEdgeWeight weight

    minEdgeWeight (int): See above.

    weightReduction (int): The minEdgeWeight is reduced by this number (for the k=np.inf cases) until a path is found or the algorithm terminates because of no reachability

  Returns:
    dict: Dictionary of source vertex id => [path vertex list]
  """
  res = dict()
  for v in G.nodes:
    if target == v:
      continue
    
    visited = set()
    for i in range(tryKs+1):
      ret = getBestTransformPath(G, target, v, i, minEdgeWeight, visited=visited)
      visited.clear()
      if ret is None:
        continue
      cost, path = ret
      res[v] = path
      break

    if v not in res:
      # try finding a path with minEdgeWeight by reducing with weightReduction
      # till a path is succesfully found
      while minEdgeWeight > weightReduction:
        ret = getBestTransformPath(G, target, v, minEdgeWeight, visited=visited)
        if ret is None:
          continue

        minEdgeWeight -=weightReduction

        _, path = ret
        res[v] = path

  
  return res
