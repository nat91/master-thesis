

def loadCameraMap(fIn):
  root = fIn.root
  camMap = dict()
  for row in root["camera_map"]:
    camMap[row["serialNo"].decode("ascii")] = row["camIdx"]
  
  return camMap