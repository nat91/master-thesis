import sys
sys.path.append('../')
import numpy as np
import os
import tables
import struct
from alive_progress import alive_bar
import traceback


from .cameraCalibration import cameraCalibration
from .loadCameras import loadCameras
from features import FeatureDetectorCollection
from features.checkerboards import CheckerboardDetector
from features.spheres import SphereDetector
from features.SIFT import SIFTDetector
from util import Camera, Intrinsics, Frame, Config, deprojectPixelToPoint, alignDepthToColor

class BufferedArray(tables.IsDescription):
  frameNo = tables.UInt64Col(dflt=0, pos=1)
  # buffer start/end in pointcloud_buffer table
  bufferStart = tables.UInt64Col(dflt=0, pos=2)
  bufferEnd = tables.UInt64Col(dflt=0, pos=3)

# map of serialNo => camera index
class CameraMap(tables.IsDescription):
  serialNo = tables.StringCol(100)
  camIdx = tables.UInt16Col()

# keeps track of all transformations from camera x to camera 0 for a given frameNo
class TransformTable(tables.IsDescription):
  frameNo = tables.UInt64Col()
  # camera transformation matrix (rotation+translation)
  transformCam = tables.Float64Col(shape=(4,4), dflt=np.zeros((4,4)))
  # point cloud transformation matrix (rotation+translation+scale)
  transformPC = tables.Float64Col(shape=(4,4), dflt=np.zeros((4,4)))
  fromCamera = tables.UInt64Col()

# keeps track of the transform path from camera x to camera 0
class TransformationLogTable(tables.IsDescription):
  frameNo = tables.UInt64Col(dflt=0, pos=1)
  transformCam = tables.Float64Col(shape=(4,4), dflt=np.zeros((4,4)))
  transformPC = tables.Float64Col(shape=(4,4), dflt=np.zeros((4,4)))

  # buffer start/end in correspondences table
  bufferStart = tables.UInt64Col(dflt=0, pos=2)
  bufferEnd = tables.UInt64Col(dflt=0, pos=3)
  fromCamera = tables.UInt64Col()
  toCamera = tables.UInt64Col()  

def writeRecoveryLog(iter, fRec):
  fRec.seek(0)
  fRec.write(struct.pack('i', iter))
  fRec.flush() # force log to disk

def setupDatabase(fOut, cameras):
  ## setup database
  filters = tables.Filters(complib='zlib', complevel=5, shuffle=False)

  # camera map table
  fOut.create_table(fOut.root, "camera_map", CameraMap)

  # main transform table
  fOut.create_table(fOut.root, "transforms", TransformTable)

  for i in range(len(cameras)):
    cam = cameras[i]
    camRow = fOut.root["camera_map"].row
    camRow["serialNo"] = cam.serialNo
    camRow["camIdx"] = i
    camRow.append()

    grpName = "cam_{}".format(cam.serialNo)
    # create camera group in database
    fOut.create_group("/", grpName, "Data for camera with index {}".format(i))
    # pointcloud buffer for each camera
    fOut.create_table("/"+grpName, "pointcloud_index", BufferedArray)
    pcAtom = tables.Float32Atom()
    fOut.create_earray("/"+grpName, "pointcloud_buffer", pcAtom, shape=(0, 6), filters=filters)

    # only create transformation log table for cameras that are not the world coordinate system.
    if i > 0:
      # transformation log table for each camera
      fOut.create_table("/"+grpName, "transformation_log", TransformationLogTable)
      # correspondences buffer for each camera
      fOut.create_earray("/"+grpName, "correspondences_buffer", pcAtom, shape=(0,6), filters=filters)

def getFrameset(i, cameras, fIn):
  """
  Reads the frameset for a given frame number (i) for all cameras that there is a capture for, it stops if a missing frame is detected.

  Args:
    i (int): The frame number to retrieve the frameset for
    cameras (list): List of Camera objects
    fIn (tables.File): A pytables file handle to a HDF5 file generated by convert/convert.py
  
  Returns:
    (list, bool): A list of Frame objects and a bool that is true if any frames were missing. Ie. return is (frameset, framesMissing)

  """
  frames = []
  noFramesMissing = True
  for c in cameras:

    if not noFramesMissing:
      break
    
    tbl = fIn.root["/recordings/cam_"+c.serialNo]
    if i < len(tbl.timestamps):
      noFramesMissing = noFramesMissing and (not tbl.timestamps[i]["isMissing"])
      if not tbl.timestamps[i]["isMissing"]:
        dFrame = tbl.depth[i]*c.depthScale
        cFrame = tbl.rgb[i]
        # align depth to color
        dFrame = alignDepthToColor(c.depthIntrinsics.getMatrix(), c.intrinsics.getMatrix(), c.rgbResolution, c.depthToColorExtrinsics, dFrame)
        frame = Frame(None, cFrame, dFrame)
        frames.append(frame)
    else:
      noFramesMissing = False
  
  return frames, not noFramesMissing

def savePointClouds(frameNo, cameras, frameset, fOut, frontClip, backClip):
  """
  Saves the colored pointclouds for a given frameset (for a given frame number) to a HDF5 table.

  Args:
    frameNo (int): Frame number of the point clouds that are being saved
    cameras (list): List of Camera objects
    frameset (list): List of Frame objects for each camera in the same order as the cameras.
    fOut (tables.File): Pytables file handle to the output HDF5 file.
    frontClip (float): Front clipping z-plane
    backClip (float): Back clipping z-plane
  
  Returns:
    None
  """
  for i in range(len(cameras)):
    cam = cameras[i]
    frame = frameset[i]
    cFrame = frame.colorFrame
    dFrame = frame.depthFrame

    validIdx = np.where( (dFrame > frontClip) & (dFrame < backClip))
    ys = validIdx[0]
    xs = validIdx[1]
    zs = dFrame[validIdx]

    color = cFrame[validIdx]

    X = deprojectPixelToPoint(cam.intrinsics.getMatrix(), np.array([xs, ys, zs]), zs).T
    Xc = np.hstack((X, color))

    pcBufferTbl = fOut.root["/cam_{}/pointcloud_buffer".format(cam.serialNo)]
    pcIndexTbl = fOut.root["/cam_{}/pointcloud_index".format(cam.serialNo)]

    offset = len(pcBufferTbl)

    indexRow = pcIndexTbl.row
    indexRow["frameNo"] = frameNo
    indexRow["bufferStart"] = offset
    indexRow["bufferEnd"] = offset + Xc.shape[0]
    indexRow.append()

    pcBufferTbl.append(Xc)


def saveCameraCalibration(frameNo, calRes, fOut, cameras):
  """
  Saves the calculated camera calibration to the output calibration.h5 file.

  Args:
    frameNo (int): The frame number the calibration was done for
    calRes (dict): The result of the calibration as returned by analysis.cameraCalibration
    fOut (tables.File): A pytables file handle to the calibration.h5 output file
  
  Returns:
    None
  """
  transformsTbl = fOut.root["/transforms"]

  for camIdx in calRes.keys():
    res = calRes[camIdx]
    cam = cameras[camIdx]
    # save main transform from camIdx to camera 0 in transforms table
    row = transformsTbl.row
    row["fromCamera"] = int(camIdx)
    row["frameNo"] = frameNo 
    row["transformCam"] = res["transformCam"]
    row["transformPC"] = res["transformPC"]
  
    row.append()

    # for each transformation step from camIdx to camera 0, save the transformation
    # and the correspondences it was calculated from
    transformLogTbl = fOut.root["/cam_{}/transformation_log".format(cam.serialNo)]
    corBufferTbl = fOut.root["/cam_{}/correspondences_buffer".format(cam.serialNo)]

    for i in range(len(res["edge"])):
      (fromCamera, toCamera) = res["edge"][i]
      camTransform = res["camTransforms"][i]
      pcTransform = res["pcTransforms"][i]
      correspondences = res["correspondences"][i]

      offset = len(corBufferTbl)

      corBufferTbl.append(correspondences)

      row = transformLogTbl.row
      row["frameNo"] = frameNo
      row["transformCam"] = camTransform
      row["transformPC"] = pcTransform
      row["fromCamera"] = fromCamera
      row["toCamera"] = toCamera
      row["bufferStart"] = offset
      row["bufferEnd"] = correspondences.shape[0]
      row.append()


def recoveryUndo(fOut, startOffset, cameras):
  print("Recovery: Undoing inserts for frames with number > {}".format(startOffset))
  root = fOut.root
  # undo transforms, can at most be one
  transformsTbl = root["transforms"]
  if len(transformsTbl) > 0 and transformsTbl[len(transformsTbl)-1]["frameNo"] > startOffset:
    transformsTbl.remove_row(len(transformsTbl)-1)
  
  for i in range(len(cameras)):
    # retrieve tables we need to undo something in
    prefix = "cam_{}".format(cameras[i].serialNo)
    pciTbl = root[prefix+"/pointcloud_index"]
    pcbufTbl = root[prefix+"/pointcloud_buffer"]

    # undo point cloud index and point cloud buffer
    if len(pciTbl) > 0 and pciTbl[len(pciTbl)-1]["frameNo"] > startOffset:
      lastRow = pciTbl[len(pciTbl)-1]
      pciTbl.remove_row(len(pciTbl)-1)

    # undo transformation log and correspondences buffer for all cameras
    # but the world coordinate camera system
    if i > 0:
      translogTbl = root[prefix+"/transformation_log"]
      corbufTbl = root[prefix+"/correspondences_buffer"]

      if len(translogTbl) > 0 and translogTbl[len(translogTbl)-1]["frameNo"] > startOffset:
        lastRow = translogTbl[len(translogTbl)-1]
        translogTbl.remove_row(len(translogTbl)-1)

  print("Recovery: Undo done.")

def run(convertHdf5Path, config):
  """
  Runs the analysis phase given a directory that has a converted.h5 file generated by convert/convert.py and a Util.config object.

  Args:
    convertHdf5Path (string): A path to a directory that contains a converted.h5 file.
    config (Util.config): Configuration object.
  
  Returns:
    None: Nothing is returned, a output file called calibration.h5 is output into the convertHdf5Path given. If the calibration fails, run it again and it will try to recover its progress.
  """

  convertHdf5Path = convertHdf5Path.rstrip("/")
  
  if not os.path.exists(convertHdf5Path + "/converted.h5"):
    raise RuntimeError("hd5 file {} does not exist!".format(convertHdf5Path + "/converted.h5"))
  
  if not os.path.isdir(convertHdf5Path):
    raise RuntimeError("Given path {} is not a directory".format(convertHdf5Path))

  
  outFile = convertHdf5Path + "/calibration.h5"
  recoveryLog = convertHdf5Path + "/calibration.reclog"
  startOffset = 0 # what frame to start analysis at 

  fOut = None
  fRecovery = None
  fTbl = tables.open_file(convertHdf5Path + "/converted.h5", "r")
  ## read camera metadata and instantiate cameras
  cameras = loadCameras(fTbl)

  if os.path.exists(outFile) and os.path.exists(recoveryLog):
    fRecovery = open(recoveryLog, "rb")
    # recovery mode
    fOut = tables.open_file(outFile, "a")
    startOffset = struct.unpack('i', fRecovery.read(4))
    if isinstance(startOffset, tuple):
      startOffset = startOffset[0]

    print("Recovery from frame: {}".format(startOffset))
    # roll back by removing everything that is later than startOffset
    recoveryUndo(fOut, startOffset, cameras)
    print("Recovery complete")
    
    fRecovery.close()

  else:
    fOut = tables.open_file(outFile, "w")
    setupDatabase(fOut, cameras)

  # setup feature detectors
  fDet = FeatureDetectorCollection()
  if config.get("calibration.detect_checkerboards", True):
    cDet = CheckerboardDetector("cb", config)
    fDet.add(cDet)

  if config.get("calibration.detect_spheres", True):
    sDet = SphereDetector()
    fDet.add(sDet)

  if config.get("calibration.detect_SIFT", True):
    SIFTDet = SIFTDetector(config)
    fDet.add(SIFTDet)
  
  # setup recovery log
  fRecovery = open(recoveryLog, "wb")
  writeRecoveryLog(startOffset, fRecovery)


  ## read config
  
  # perform analysis on every step frame.
  step = config.get("calibration.step", 10)
  # z-clipping planes, front default 0.3 meters and back default 3 meters
  frontClip = config.get("calibration.front_clipping_plane", 0.3)
  backClip = config.get("calibration.back_clipping_plane", 3.0)

  ## analysis loop
  minNumFrames = int(np.min([len(fTbl.root["/recordings/cam_"+c.serialNo+"/rgb"]) for c in cameras]))
  i = startOffset

  # when this is 0, do calibration for all frames.
  nextCalibrationStep = 0

  with alive_bar(minNumFrames,  bar="blocks") as bar:
    bar(incr=startOffset)
    while i < minNumFrames:
      ## make frameset and find out if any frame is missing
      frameset, framesMissing = getFrameset(i, cameras, fTbl)

      if framesMissing:
        writeRecoveryLog(i, fRecovery)
        i += 1
        bar()
        continue
      
      savePointClouds(i, cameras, frameset, fOut, frontClip, backClip)

      nextCalibrationStep -= 1
      # store calibration for every nth frame
      if nextCalibrationStep <= 0:
        nextCalibrationStep = step
        try:
          # calculcate camera calibration
          res = cameraCalibration(cameras, frameset, fDet, config)
          if len(res) == 0:
            nextCalibrationStep = 0
            print("No calibration possible for this iter (not enough correspondences), trying next instead.") 
          else:
            print("Calibration found for {}/{} cameras".format(len(res)+1, len(cameras)))
            saveCameraCalibration(i, res, fOut, cameras)

        except Exception as e:
          # run calibration in next step in the event of a crash
          nextCalibrationStep = 0
          print("Exception during calibration, trying next iter instead. Exception was:")
          traceback.print_exc()

      # force hdf5 file changes to disk
      fOut.flush()

      writeRecoveryLog(i, fRecovery)
      i += 1
      bar()

  # clean up
  fTbl.close()
  fOut.close()
  fRecovery.close()
  
  os.unlink(recoveryLog)



if __name__ == "__main__":
  
  if len(sys.argv) != 3:
    print("Usage: python3 run.py hdf5FilePath configFile")
    exit()
  

  run(sys.argv[1], Config(sys.argv[2]))


