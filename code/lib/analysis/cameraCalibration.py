import numpy as np
import networkx as nx
from  photogrammetry.absoluteOrientation import umeyama_ransac
from correspondences import Finder as CF
from . import getBestTransformPaths

def calculateCorrespondenceOverlap(featureSets):
  N = len(featureSets)
  # symmetric overlap matrix, O_ij is camera i's overlap with camera j
  overlapMatrix = np.zeros((N,N))
  numFeatures = 0

  
  # find feature types and run for each
  for t in featureSets[0].keys():
    typeFeatureSets = []
    for i in range(N):
      typeFeatureSets.append(featureSets[i][t])
    
    matcher = featureSets[0][t]["matcher"]
    O, numF = matcher.calculateCorrespondenceOverlap(typeFeatureSets)

    overlapMatrix += O
    numFeatures += numF

  
  return overlapMatrix, numFeatures

def calculateTransformationGraph(cameras, frames, det, config, verbose=False, minCors=8):
  """
  Calculates a transformation graph between several cameras.
  The transformation graph has an edge (i,j) if there is a transformation
  from camera i to camera j.

  Args:
    cameras (list): List of util.Camera objects.
    frames (list): List of util.Frame objects
    det (features.FeatureDetectorCollection): A collection of feature detectors

  Returns:
   A calibration graph where edge (i,j) means there is a transformation from camera i to camera j. 
   The edges are weighed such that views with high overlap have a low weight.
   Each edge has the properties from umeyama's algorithm as well:

    edge.transformPC - the point cloud transformation matrix from camera i to j
    edge.transformCam - the camera transform matrix from camera i to j
    edge.translation - translation vector of the transformation
    edge.rotation - the rotation matrix of the transformation
    edge.cor - The correspondences used to calculate the transformation
    scale - the scale component of the transformation
  """

  featureSets = []
  N = len(cameras)
  # calculate features
  if verbose:
    print("Calculating features for {} frames.".format(N))


  for i in range(N):
    featureSets.append(det.getFeatures(frames[i].colorFrame, frames[i].depthFrame, cameras[i].intrinsics.getMatrix()))
  
  O, numFeatures = calculateCorrespondenceOverlap(featureSets) # overlap matrix
  G = nx.DiGraph()
  cors = CF(det) # correspondence finder

  for i in range(N):
    validIdx = np.where(O[i] >= minCors)
    if validIdx[0].shape[0] < 1:
      # no connections from this node to others
      continue

    K1 = cameras[i].intrinsics.getMatrix()

    # all cameras with more than minCors correspondences in common
    # with camera i
    for j in range(validIdx[0].shape[0]):
      k = validIdx[0][j]
      if G.has_edge(i, k):
        continue

      K2 = cameras[k].intrinsics.getMatrix()
      # find correspondences
      c3d, _ =  cors.c3DF(frames[i].colorFrame, frames[i].depthFrame, K1, featureSets[i], frames[k].colorFrame, frames[k].depthFrame, K2, featureSets[k])


      #idx = np.where((c3d[:,2] < 1.5) & (c3d[:,5] < 1.5))
      #c3d = c3d[idx]

      res = umeyama_ransac(c3d, t=config.get("umeyama_ransac_thresh", 1e-4))
      if res is None:
        continue 
      
      (R, t, c, err, Rt), inliers = res
      weight = numFeatures - O[i,k]


      #print(i,k, err)

      camTransform = np.zeros((4,4))
      camTransform[0:3, 0:3] = R
      camTransform[0:3,3] = t
      camTransform[3,3] = 1.0

      G.add_edge(i,k, weight=weight, transformPC=Rt, transformCam=camTransform, translation=t, rotation=R, scale=c, cor=c3d)

      Rtinv = np.linalg.inv(Rt)
      Cinv = np.linalg.inv(camTransform)

      c3dFlip = np.zeros(c3d.shape)
      c3dFlip[:,0:3] = c3d[:,3:]
      c3dFlip[:,3:] = c3d[:,0:3]

      G.add_edge(k, i, weight=weight, transformPC=Rtinv, transformCam=Cinv, translation=-t, rotation=R.T, scale=1.0/c, cor=c3dFlip)
  

  return G

def getTransformations(G):
  """
  Finds the smallest weight transformations from all cameras to camera 0 (the world coordinate system)

  Args:
    G (networkx.DiGraph): Transformation graph returned by calculateTransformationGraph

  Returns:
    dict: Dictionary with cameraId => 
      dict(
        x => Camera transformation from camera x to camera 0
        transformPC => Point Cloud transformation from camera x to camera 0
        translation => List of translations, one for each edge that was followed to make the final transformation
        camTransforms => List of camera transformation matrices
        pcTransforms => List of point cloud transformation matrices
        rotation => Same as above, but rotation  
        scale => Same as above, but scale
        correspondences => Same as above, but correspondences used to create each transformation
        edge => List of edges for the above, each entry is a pair (i,j) where i and j are camera IDs
        )

  """

  if 0 not in G:
    # camera 0 is not reachable, return empty result
    return dict()
  
  bestTransformPaths = nx.algorithms.shortest_paths.generic.shortest_path(G, target=0)  
  #bestTransformPaths = getBestTransformPaths(G, 0)
  #print(bestTransformPaths)
  transforms = dict()

  for k in bestTransformPaths.keys():
    if k == 0: 
      continue

    if len(bestTransformPaths[k]) < 2:
      continue

    camTransforms = []
    pcTransforms = []

    res = {
      "transformCam": None,
      "transformPC": None,
      "camTransforms": [],
      "pcTransforms": [],
      "translation": [],
      "rotation": [],
      "scale": [],
      "correspondences": [],
      "edge": []
    }

    for i in range(len(bestTransformPaths[k])-1):
      u = bestTransformPaths[k][i]
      v = bestTransformPaths[k][i+1]

      edge = G.edges[u,v]

      camTransforms.append(edge['transformCam'])
      pcTransforms.append(edge['transformPC'])

      res["camTransforms"].append(edge["transformCam"])
      res["pcTransforms"].append(edge["transformPC"])
      res["translation"].append(edge["translation"])
      res["rotation"].append(edge["rotation"])
      res["scale"].append(edge["scale"])
      res["correspondences"].append(edge["cor"])
      res["edge"].append((u,v))
    
    # calculate transformations
    camTransforms.reverse()
    pcTransforms.reverse() 

    camT = np.eye(4)
    pcT = np.eye(4)

    for i in range(len(camTransforms)):
      camT = np.dot(camTransforms[i], camT)
      pcT = np.dot(pcTransforms[i], pcT)
    
    res["transformCam"] = camT
    res["transformPC"] = pcT

    transforms[k] = res

  return transforms


def cameraCalibration(cameras, frames, det, config, verbose=True):
  """
  Performs multi-camera extrinsic calibration w.r.t. a reference camera (always camera 0) for a single timestep.

  Args:
    cameras (list): List of util.Camera objects.
    frames (list): List of util.Frame objects
    det (features.FeatureDetectorCollection): A collection of feature detectors
    config (util.Config): Configuration object

  Returns:
    dict: Dictionary with cameraId => 
      dict(
        transformCam => Camera transformation from camera x to camera 0
        transformPC => Point Cloud transformation from camera x to camera 0
        translation => List of translations, one for each edge that was followed to make the final transformation
        rotation => Same as above, but rotation  
        scale => Same as above, but scale
        correspondences => Same as above, but correspondences used to create each transformation
        edge => List of edges for the above, each entry is a pair (i,j) where i and j are camera IDs
        )
  """

  minCors = config.get("calibration.minimum_correspondences", 4)
  G = calculateTransformationGraph(cameras, frames, det, config, minCors=minCors)
  return getTransformations(G)


       
