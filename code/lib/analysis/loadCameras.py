from util import Camera, Intrinsics

def loadCameras(fIn):
  """
  Loads the cameras used in a recording and returns them as a list of Camera objects

  Args:
    fIn (tables.File): The pytables filehandle to the converted.h5 input file
  
  Returns:
    (list): List of Camera instances. It is sorted such that camera[0] is the camera the world coordinate system is located at.
  """
  cameras = []
  cameraMetadata = fIn.root.cameraMetadata
  isPrimary = 0

  for i in range(len(cameraMetadata)):
    cmeta = cameraMetadata[i]

    K = cmeta["colorIntrinsics"]
    I = Intrinsics(K[0,0], K[1,1], K[0,2], K[1,2], [])

    Kd = cmeta["depthIntrinsics"]
    Id = Intrinsics(Kd[0,0], Kd[1,1], Kd[0,2], Kd[1,2], [])


    c = Camera((cmeta["colorHeight"], cmeta["colorWidth"]), (cmeta["depthHeight"], cmeta["depthWidth"]))

    c.intrinsics = I
    c.depthIntrinsics = Id
    c.depthToColorExtrinsics = cmeta["depthToColorExtrinsics"]
    c.serialNo = cmeta["serialNumber"].decode("ascii")
    c.depthScale = cmeta["depthScale"]
    
    if cmeta["isPrimary"]:
      isPrimary = i
    
    cameras.append(c)

  # make sure the primary camera is first in the camera order
  if isPrimary > 0:
    tmp = cameras[0]
    cameras[0] = cameras[isPrimary]
    cameras[isPrimary] = tmp
  
  return cameras