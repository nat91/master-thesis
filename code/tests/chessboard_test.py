import os
import sys
import unittest
from skimage import io
import pickle
import numpy as np
from skimage import img_as_float

sys.path.append('../')
from reader.reader import FileReader
from pylibcbdetect import findChessBoards


class TestChessboard(unittest.TestCase):

    def test_findChessboard(self):
        path = os.path.dirname(os.path.realpath(__file__))
        # img0 = img_as_float(io.imread(path + '/data/images/00.png')) 
        img1 = img_as_float(io.imread(path + '/data/images/01.png')) 
        img2 = img_as_float(io.imread(path + '/data/images/02.png'))
        # out0, _ = findChessBoards(img0, False)
        out1, _ = findChessBoards(img1, False)
        out2, _ = findChessBoards(img2, False)
        # self.assertEqual(len(out0), 12)
        self.assertEqual(len(out1), 3)
        self.assertEqual(len(out2), 1)

        savefile = path + '/data/reference_solutions/chessboards.data'
        if os.path.exists(savefile):
            with open(savefile, "rb") as f:
                coords1_ref = pickle.load(f)
        else:
            coords1_ref = out1[0].coords()
            with open(savefile, "wb") as f:
                pickle.dump(coords1_ref, f)

        self.assertTrue(np.array_equal(out1[0].coords(), coords1_ref))

if __name__ == '__main__':
    unittest.main()

