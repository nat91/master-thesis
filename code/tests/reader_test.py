import os
import sys
import unittest

sys.path.append('../')
from reader.reader import FileReader
from viewer import util


class TestFileReader(unittest.TestCase):
    def setUp(self):
        path = os.path.dirname(os.path.realpath(__file__))
        recdata = util.get_camera_files_from_file(path + '/data/recordings/test.recdat')
        filepaths = [elem[1] for elem in recdata]
        firstFile = filepaths[0]
        self.f = open(firstFile, 'rb')
        self.reader = FileReader(5, self.f)

    def tearDown(self):
        self.f.close()

    def test_init(self):
        resolutionRGBW = self.reader.fileMetaData.resolutionRGBW
        resolutionRGBH = self.reader.fileMetaData.resolutionRGBH
        resolutionDepthW = self.reader.fileMetaData.resolutionDepthW
        resolutionDepthH = self.reader.fileMetaData.resolutionDepthH
        fps = self.reader.fileMetaData.fps
        numberOfFrames = self.reader.fileMetaData.numberOfFrames
        dScale = self.reader.fileMetaData.dScale
        focalLengthX = self.reader.fileMetaData.focalLengthX
        focalLengthY = self.reader.fileMetaData.focalLengthY
        principalPointX = self.reader.fileMetaData.principalPointX
        principalPointY = self.reader.fileMetaData.principalPointY
        
        self.assertTrue(resolutionRGBW > 0 and resolutionRGBW < 10000)
        self.assertTrue(resolutionRGBH > 0 and resolutionRGBH < 10000)
        self.assertTrue(resolutionDepthW > 0 and resolutionDepthW < 10000)
        self.assertTrue(resolutionDepthH > 0 and resolutionDepthH < 10000)
        self.assertTrue(fps > 0 and fps < 100)
        self.assertTrue(numberOfFrames > 0 and fps < 10000)
        self.assertTrue(dScale > 0 and dScale < 1)
        self.assertTrue(focalLengthX > 0 and focalLengthX < 5000)
        self.assertTrue(focalLengthY > 0 and focalLengthY < 5000)
        self.assertTrue(principalPointX > 0 and principalPointX < 1000)
        self.assertTrue(principalPointY > 0 and principalPointY < 1000)

        rgbFrameNo = self.reader.currentFrame.frameMetaData.rgbFrameNo
        depthFrameNo = self.reader.currentFrame.frameMetaData.depthFrameNo
        rgbtimestamp = self.reader.currentFrame.frameMetaData.rgbtimestamp
        depthtimestamp = self.reader.currentFrame.frameMetaData.depthtimestamp

        self.assertIn(rgbFrameNo, range(0,10))
        self.assertIn(depthFrameNo, range(0,10))
        self.assertEqual(rgbFrameNo, depthFrameNo)
        self.assertTrue(rgbtimestamp > 0 and rgbtimestamp < 100000000)
        self.assertTrue(depthtimestamp > 0 and depthtimestamp < 100000000)

    def test_next(self):
        rgbFrameNo = self.reader.currentFrame.frameMetaData.rgbFrameNo
        depthFrameNo = self.reader.currentFrame.frameMetaData.depthFrameNo
        rgbtimestamp = self.reader.currentFrame.frameMetaData.rgbtimestamp
        depthtimestamp = self.reader.currentFrame.frameMetaData.depthtimestamp
        self.reader.nextFrame()
        rgbFrameNext = self.reader.currentFrame.frameMetaData.rgbFrameNo
        depthFrameNext = self.reader.currentFrame.frameMetaData.depthFrameNo
        rgbtimestampNext = self.reader.currentFrame.frameMetaData.rgbtimestamp
        depthtimestampNext = self.reader.currentFrame.frameMetaData.depthtimestamp

        self.assertTrue(rgbFrameNo < rgbFrameNext)
        self.assertTrue(depthFrameNo < depthFrameNext)
        self.assertTrue(rgbtimestamp < rgbtimestampNext)
        self.assertTrue(depthtimestamp < depthtimestampNext)

    def test_prev(self):
        self.reader.nextFrame()
        rgbFrameNo = self.reader.currentFrame.frameMetaData.rgbFrameNo
        depthFrameNo = self.reader.currentFrame.frameMetaData.depthFrameNo
        rgbtimestamp = self.reader.currentFrame.frameMetaData.rgbtimestamp
        depthtimestamp = self.reader.currentFrame.frameMetaData.depthtimestamp
        self.reader.prevFrame()
        rgbFramePrev = self.reader.currentFrame.frameMetaData.rgbFrameNo
        depthFramePrev = self.reader.currentFrame.frameMetaData.depthFrameNo
        rgbtimestampPrev = self.reader.currentFrame.frameMetaData.rgbtimestamp
        depthtimestampPrev = self.reader.currentFrame.frameMetaData.depthtimestamp

        self.assertTrue(rgbFrameNo > rgbFramePrev)
        self.assertTrue(depthFrameNo > depthFramePrev)
        self.assertTrue(rgbtimestamp > rgbtimestampPrev)
        self.assertTrue(depthtimestamp > depthtimestampPrev)

    def test_skip(self):
        rgbFrameNo = self.reader.currentFrame.frameMetaData.rgbFrameNo
        depthFrameNo = self.reader.currentFrame.frameMetaData.depthFrameNo
        rgbtimestamp = self.reader.currentFrame.frameMetaData.rgbtimestamp
        depthtimestamp = self.reader.currentFrame.frameMetaData.depthtimestamp
        self.reader.skipFrames(3)
        rgbFramePrev = self.reader.currentFrame.frameMetaData.rgbFrameNo
        depthFramePrev = self.reader.currentFrame.frameMetaData.depthFrameNo
        rgbtimestampPrev = self.reader.currentFrame.frameMetaData.rgbtimestamp
        depthtimestampPrev = self.reader.currentFrame.frameMetaData.depthtimestamp

        self.assertTrue(rgbFrameNo < rgbFramePrev)
        self.assertTrue(depthFrameNo < depthFramePrev)
        self.assertTrue(rgbtimestamp < rgbtimestampPrev)
        self.assertTrue(depthtimestamp < depthtimestampPrev)

    def test_skipNeg(self):
        self.reader.skipFrames(5) # Need to spool forward, so we can skip back
        rgbFrameNo = self.reader.currentFrame.frameMetaData.rgbFrameNo
        depthFrameNo = self.reader.currentFrame.frameMetaData.depthFrameNo
        rgbtimestamp = self.reader.currentFrame.frameMetaData.rgbtimestamp
        depthtimestamp = self.reader.currentFrame.frameMetaData.depthtimestamp
        self.reader.skipFrames(-3)
        rgbFramePrev = self.reader.currentFrame.frameMetaData.rgbFrameNo
        depthFramePrev = self.reader.currentFrame.frameMetaData.depthFrameNo
        rgbtimestampPrev = self.reader.currentFrame.frameMetaData.rgbtimestamp
        depthtimestampPrev = self.reader.currentFrame.frameMetaData.depthtimestamp

        self.assertTrue(rgbFrameNo > rgbFramePrev)
        self.assertTrue(depthFrameNo > depthFramePrev)
        self.assertTrue(rgbtimestamp > rgbtimestampPrev)
        self.assertTrue(depthtimestamp > depthtimestampPrev)


    def test_skipLogic(self):
        rgbFrameNo = self.reader.currentFrame.frameMetaData.rgbFrameNo
        depthFrameNo = self.reader.currentFrame.frameMetaData.depthFrameNo
        rgbtimestamp = self.reader.currentFrame.frameMetaData.rgbtimestamp
        depthtimestamp = self.reader.currentFrame.frameMetaData.depthtimestamp
        self.reader.skipFrames(3)
        self.reader.skipFrames(-3)
        rgbFrameNew = self.reader.currentFrame.frameMetaData.rgbFrameNo
        depthFrameNew = self.reader.currentFrame.frameMetaData.depthFrameNo
        rgbtimestampNew = self.reader.currentFrame.frameMetaData.rgbtimestamp
        depthtimestampNew = self.reader.currentFrame.frameMetaData.depthtimestamp

        self.assertEqual(rgbFrameNo, rgbFrameNew)
        self.assertEqual(depthFrameNo, depthFrameNew)
        self.assertEqual(rgbtimestamp, rgbtimestampNew)
        self.assertEqual(depthtimestamp, depthtimestampNew)

        rgbFrameNo = self.reader.currentFrame.frameMetaData.rgbFrameNo
        depthFrameNo = self.reader.currentFrame.frameMetaData.depthFrameNo
        rgbtimestamp = self.reader.currentFrame.frameMetaData.rgbtimestamp
        depthtimestamp = self.reader.currentFrame.frameMetaData.depthtimestamp
        self.reader.skipFrames(8)
        self.reader.skipFrames(-8)
        rgbFrameNew = self.reader.currentFrame.frameMetaData.rgbFrameNo
        depthFrameNew = self.reader.currentFrame.frameMetaData.depthFrameNo
        rgbtimestampNew = self.reader.currentFrame.frameMetaData.rgbtimestamp
        depthtimestampNew = self.reader.currentFrame.frameMetaData.depthtimestamp

        self.assertEqual(rgbFrameNo, rgbFrameNew)
        self.assertEqual(depthFrameNo, depthFrameNew)
        self.assertEqual(rgbtimestamp, rgbtimestampNew)
        self.assertEqual(depthtimestamp, depthtimestampNew)

if __name__ == '__main__':
    unittest.main()
