import sys 
sys.path.append("../lib")
import matplotlib.pyplot as plt
import numpy as np
import skimage

from analysis import cameraCalibration
import features
import pyglet_test
import o3d_test
from util import Config, deprojectPixelToPoint, toHomogeneous, fromHomogeneous
from helpers import framesetFromConnectedCameras

cacheFile = "test_analysis.cache"
if len(sys.argv) > 1:
  cacheFile = sys.argv[1]

config = Config("config.json")

frames, cameras = framesetFromConnectedCameras(cacheFile, verbose=True, autoexposureFrames=30, setROI=True)

det1 = features.checkerboards.CheckerboardDetector("cb", config)
det2 = features.spheres.SphereDetector()
det3 = features.SIFT.SIFTDetector(config=config)
detCol = features.FeatureDetectorCollection()
detCol.add(det1)
detCol.add(det2)
detCol.add(det3)


# do camera calibration
T = cameraCalibration(cameras, frames, detCol, config)
N = len(cameras)
wnd = o3d_test.CalibrationWindow()

PCs = None

for i in range(N):
  if i not in T and i != 0:
    print("Camera calibration for camera {} failed".format(i))
    continue

  # camera i frustrum
  (h,w) = (1080, 1920) #cameras[i].rgbResolution
  cam = np.array([
    [0.0, 0.0, 0.0],
    [w, 0.0, 0.0],
    [w, h, 0.0],
    [0.0, h, 0.0],
    [0.0, 0.0, 0.0]
  ])  

  K = cameras[i].intrinsics.getMatrix()

  coords = deprojectPixelToPoint(K, cam.T, np.array([0.1, 0.1, 0.1, 0.1, 0.0]).T).T
  # transform the camera to cam 0's coordinate system if it is not cam 0
  if i > 0:
    coords = toHomogeneous(coords)
    coords = fromHomogeneous(np.dot(T[i]["transformCam"], coords.T).T)

  wnd.addCamera(coords, "cam{}".format(i))

  # draw camera i's point cloud
  dThresh_h = 2.0
  dThresh_l = 0.3

  depth = frames[i].depthFrame

  idx = np.where( (depth <= dThresh_h) & (depth >= dThresh_l))
  xs = idx[1]
  ys = idx[0]
  zs = depth[idx]

  X = deprojectPixelToPoint(K, np.array([xs, ys, zs]), zs).T
  # transform point cloud to cam 0's coordinate system if it is not cam 0
  if i > 0:
    X = fromHomogeneous(np.dot(T[i]["transformPC"], toHomogeneous(X).T).T)

  
  colorFrame = skimage.img_as_float(frames[i].colorFrame)
  color = colorFrame[ys, xs]
  #print(color.shape)

  X = np.hstack((X, color))

  if PCs is not None:
    PCs = np.vstack((PCs, X))
  else:
    PCs = X

wnd.addPointCloud(PCs, "PC")

wnd.run()    