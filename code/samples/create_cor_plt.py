# Tool for making correspondence plots from two images containing checkerboards
import numpy as np 
import matplotlib.pyplot as plt 
import correspondences as corlib
from features.checkerboards import CheckerboardDetector
from features import FeatureDetectorCollection
import skimage.io as io
from skimage import img_as_float
import sys
import os 


if len(sys.argv) < 3:
  print("Usage: python3 create_cor_plt.py img1 img2")
  exit()

pathPrefix = "features/checkerboards/test_images/"
img1 = pathPrefix + sys.argv[1] + ".png"
img2 = pathPrefix + sys.argv[2] + ".png"

if not os.path.exists(img1):
  print("Path {} does not exist".format(img1))
  exit()

if not os.path.exists(img2):
  print("Path {} does not exist".format(img2))
  exit()

im1 = img_as_float(io.imread(img1))
im2 = img_as_float(io.imread(img2))

cutLeft = 0
cutRight = 0
cutTop = 0
cutBottom = 0


im1c = im1[cutTop:im1.shape[0]-cutBottom, cutLeft:im1.shape[1]-cutRight]
im2c = im2[cutTop:im2.shape[0]-cutBottom, cutLeft:im2.shape[1]-cutRight]



'''
plt.imshow(im2)
plt.show()
exit()
'''

boards = dict()
boards["14"] = (4,4)
boards["11"] = (4,4)

K = np.array([
  [1380.14, 0, 965.5],
  [0, 1377.14, 529.21],
  [0, 0, 1]
])

ft = FeatureDetectorCollection()
cb = CheckerboardDetector("ns", boards)
ft.add(cb)

finder = corlib.Finder(ft, K, K)
c2d, _ = finder.c2D(im1c, im2c)
corlib.plot2D(im1c, im2c, c2d)