import open3d as o3d
import numpy as np
import pickle
import sys
sys.path.append("../lib")
import os
from util.pointCloud import *
import o3d_test


def preprocess_point_cloud(pcd, voxel_size):
  """
  From: http://www.open3d.org/docs/release/tutorial/Advanced/global_registration.html
  """
  #print(":: Downsample with a voxel size %.3f." % voxel_size)
  pcd_down = pcd.voxel_down_sample(voxel_size)

  radius_normal = voxel_size * 2
  #print(":: Estimate normal with search radius %.3f." % radius_normal)
  pcd_down.estimate_normals(
    o3d.geometry.KDTreeSearchParamHybrid(radius=radius_normal, max_nn=30))

  radius_feature = voxel_size * 5
  #print(":: Compute FPFH feature with search radius %.3f." % radius_feature)
  pcd_fpfh = o3d.registration.compute_fpfh_feature(
    pcd_down,
    o3d.geometry.KDTreeSearchParamHybrid(radius=radius_feature, max_nn=100))
  return pcd_down, pcd_fpfh




def execute_fast_global_registration(source_down, target_down, source_fpfh,
                                     target_fpfh, voxel_size):
  """
  From: http://www.open3d.org/docs/release/tutorial/Advanced/global_registration.html
  """                                     
  distance_threshold = voxel_size * 0.5
  #print(":: Apply fast global registration with distance threshold %.3f" \
          #% distance_threshold)
  result = o3d.registration.registration_fast_based_on_feature_matching(
    source_down, target_down, source_fpfh, target_fpfh,
    o3d.registration.FastGlobalRegistrationOption(
        maximum_correspondence_distance=distance_threshold))
  return result



def refine_registration(source, target, distThresh, T0):
  """
  Based on: http://www.open3d.org/docs/release/tutorial/Advanced/global_registration.html
  """                 
  #print(":: Point-to-plane ICP registration is applied on original point")
  #print("   clouds to refine the alignment. This time we use a strict")
  #print("   distance threshold %.3f." % distance_threshold)
  result = o3d.registration.registration_icp(
      source, target, distThresh, T0,
      o3d.registration.TransformationEstimationPointToPlane())
  return result


if len(sys.argv) < 2:
  print("Usage: python3 test_global_reg.py cacheFile")
  exit()

cacheFile = sys.argv[1]
if not os.path.exists(cacheFile):
  print("Cache file {} does not exist or is not readable.".format(cacheFile))
  exit()

voxelSize = 0.01

cameras = None 
frames = None
with open(cacheFile, "rb") as f:
  frames, cameras = pickle.load(f)


c0 = cameras[0]
c1 = cameras[1]
f0 = frames[0]
f1 = frames[1]

distThresh = 0.01 # 1cm

K1 = c0.intrinsics.getMatrix()
K2 = c1.intrinsics.getMatrix()

zClip = 3.0

pc1 = toColoredPointCloud(f0.colorFrame, f0.depthFrame, K1, backClippingPlane=zClip)
pc2 = toColoredPointCloud(f1.colorFrame, f1.depthFrame, K2, backClippingPlane=zClip)

pc1_o3d = toO3DPointCloud(pc1)
pc2_o3d = toO3DPointCloud(pc2)

pc1_o3d.estimate_normals()
pc2_o3d.estimate_normals()


pc1_d, pc1_fpfh = preprocess_point_cloud(pc1_o3d, voxelSize)
pc2_d, pc2_fpfh = preprocess_point_cloud(pc2_o3d, voxelSize)

res = execute_fast_global_registration(pc2_d, pc1_d, pc2_fpfh, pc1_fpfh, voxelSize)
print("Result fast global registration:", res)

T0 = res.transformation
print("ICP refinement")
res2 = refine_registration(pc2_o3d, pc1_o3d, distThresh, T0)
T = res2.transformation
print("Result point to plane ICP:", res2) 

#print(T)


cam1 = np.array([
  [0.0, 0.0, 0.0],
  [1920.0, 0.0, 0.0],
  [1920.0, 1080.0, 0.0],
  [0.0, 1080.0, 0.0],
  [0.0, 0.0, 0.0]
])

cam2 = np.copy(cam1)

#Cti = np.linalg.inv(T)

coords = deprojectPixelToPoint(K2, cam1.T, np.array([0.1, 0.1, 0.1, 0.1, 0.0]).T).T 

coords2 = deprojectPixelToPoint(K1, cam2.T, np.array([0.1, 0.1, 0.1, 0.1, 0.0]).T).T 
coords2 = toHomogeneous(coords2)
coords2 = fromHomogeneous(np.dot(T, coords2.T).T)

wnd = o3d_test.CalibrationWindow()

pc2 = transformPointCloud(pc2, T)

wnd.addCamera(coords, "test")
wnd.addCamera(coords2, "test2")

wnd.addPointCloud(np.vstack((pc1, pc2)), "cam1")

wnd.run()