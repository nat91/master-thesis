import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import skimage
import open3d as o3d
import copy
import sys

sys.path.append("../lib")
sys.path.append("../tools")
import features
import correspondences as corlib
from util import alignDepthToColor, deprojectPixelToPoint, toHomogeneous, fromHomogeneous, Config
from util.pointCloud import *
from photogrammetry.absoluteOrientation import umeyama, umeyama_ransac
import pyglet_test
from helpers import framesetFromConnectedCameras
from scipy.spatial.transform import Rotation as R
import o3d_test

def draw_pointcloud_o3d(src):
  o3d.visualization.draw_geometries([src])


def draw_registration_result(srcIn, tarIn, transform):
  src = copy.deepcopy(srcIn)
  tar = copy.deepcopy(tarIn)

  src.paint_uniform_color([1, 0.706, 0])
  tar.paint_uniform_color([0, 0.651, 0.929])
  src.transform(transform)
  o3d.visualization.draw_geometries([src, tar])


cacheFile = "test_cor.cache"
if len(sys.argv) > 1:
  cacheFile = sys.argv[1]


print("Cachefile: {}".format(cacheFile))

frames, cameras = framesetFromConnectedCameras(cacheFile, verbose=True, setROI=False)

config = Config("config.json")

imgFeatures = []
boards = dict()

det1 = features.checkerboards.CheckerboardDetector("cb", config)
det2 = features.spheres.SphereDetector()
det3 = features.SIFT.SIFTDetector(config)
detCol = features.FeatureDetectorCollection()
detCol.add(det3)
detCol.add(det1)
detCol.add(det2)


# plot and calculate correspondences for all image pairs
for i in range(len(frames)-1):
  for j in range(i+1, len(frames)):
    if i == j: 
      continue 

    img1 = frames[i].colorFrame
    img2 = frames[j].colorFrame
    depth1 = frames[i].depthFrame
    depth2 = frames[j].depthFrame
    K1 = cameras[i].intrinsics.getMatrix()
    K2 = cameras[j].intrinsics.getMatrix()

    f1 = detCol.getFeatures(img1, depth1, K1)
    f2 = detCol.getFeatures(img2, depth2, K2)

    cFinder = corlib.Finder(detCol)
    c3d, labels = cFinder.c3DF(img1, depth1, K1, f1, img2, depth2, K2, f2)

    c1 = toHomogeneous(c3d[:,0:3])
    c2 = toHomogeneous(c3d[:, 3:])

    #c1 = Cf[:,0:3]
    #c2 = Cf[:,3:]
    #print(c1

    idx1 = np.where((c1[:,2] > 0.1) & (c2[:,2] > 0.1) )
    c1 = c1[idx1]
    c2 = c2[idx1]

    print("Points in: {}".format(c1.shape[0]))


    #Rt, inliers = optim_ransac(c1, c2, t=0.005, nPoints=4, c=0.999)
    #Rt = optimAll(c1, c2)
    #inliers = np.array(range(c1.shape[0]))
    
    
    (Ro, t, c, err, Rt), inliers = umeyama_ransac(c3d, t=config.get("umeyama_ransac_thresh", 1e-4))

    print("Points out: {}".format(len(inliers)))

    print("MSE: ", err)

    r = R.from_matrix((Ro.T))
    print("Rotations:", r.as_euler("xyz", degrees=True))
    print("Translation:", -t)
    print("Scale:", 1.0/c)

    c1t = np.dot(Rt, c1[inliers].T).T

    fig = plt.figure()
    ax = fig.gca(projection="3d")
    ax.plot(c2[inliers, 0], c2[inliers, 1], c2[inliers,2], "r.", label="c2")
    ax.plot(c1t[:, 0], c1t[:, 1], c1t[:,2], "b.", label="c1t")
    max_range = np.array([c2[inliers, 0].max()-c2[inliers, 0].min(), c2[inliers, 1].max()-c2[inliers, 1].min(), c2[inliers, 2].max()-c2[inliers, 2].min()]).max() / 2.0
    mid_x = (c2[inliers, 0].max()+c2[inliers, 0].min()) * 0.5
    mid_y = (c2[inliers, 1].max()+c2[inliers, 1].min()) * 0.5
    mid_z = (c2[inliers,2].max()+c2[inliers,2].min()) * 0.5
    ax.set_xlim(mid_x - max_range, mid_x + max_range)
    ax.set_ylim(mid_y - max_range, mid_y + max_range)
    ax.set_zlim(mid_z - max_range, mid_z + max_range)
    ax.set_xlabel('X axis')
    ax.set_ylabel('Y axis')
    ax.set_zlabel('Z axis')    
    ax.view_init(elev=0., azim=0)
    plt.legend()

    plt.show()
    
    dThresh1_h = 2.0
    dThresh1_l = 0.3
    
    
    x1 = toColoredPointCloud(img1, depth1, K1, frontClippingPlane=dThresh1_l, backClippingPlane=dThresh1_h)
    x2 = toColoredPointCloud(img2, depth2, K2, frontClippingPlane=dThresh1_l, backClippingPlane=dThresh1_h)


    cam1 = np.array([
      [0.0, 0.0, 0.0],
      [1920.0, 0.0, 0.0],
      [1920.0, 1080.0, 0.0],
      [0.0, 1080.0, 0.0],
      [0.0, 0.0, 0.0]
    ])

    cam2 = np.copy(cam1)
    cameraTransform = np.zeros((4,4))
    cameraTransform[0:3, 0:3] = Ro 
    cameraTransform[0:3,3] = t 
    cameraTransform[3,3] = 1.0



    Rti = np.linalg.inv(Rt)
    Cti = np.linalg.inv(cameraTransform)

    coords = deprojectPixelToPoint(K1, cam1.T, np.array([0.1, 0.1, 0.1, 0.1, 0.0]).T).T 
    
    coords2 = deprojectPixelToPoint(K2, cam2.T, np.array([0.1, 0.1, 0.1, 0.1, 0.0]).T).T 
    coords2 = toHomogeneous(coords2)
    coords2 = fromHomogeneous(np.dot(Cti, coords2.T).T)


    pc1 = x1
    pc2 = transformPointCloud(x2, Rti)
    
    #print("Down sampling...")
    #voxSize = 0.01/100.0 # 0.01 cm voxel size
    #downpcd = downsamplePointCloud(np.vstack((pc1, pc2)), voxSize)
    #print("Down sampling complete!")

    wnd = o3d_test.CalibrationWindow()

    wnd.addCamera(coords, "test")
    wnd.addCamera(coords2, "test2")

    wnd.addPointCloud(np.vstack((pc1, pc2)), "cam1")

    wnd.run()
    exit()


    wnd = pyglet_test.CalibrationWindow()

    wnd.addCamera(coords, "test")
    wnd.addCamera(coords2, "test2")

    wnd.addPointCloud(np.vstack((pc1, pc2)), "cam1")

    wnd.run()






