import numpy as np
import skimage
import open3d as o3d
import sys

sys.path.append("../lib")
import features
import correspondences as corlib
from util import alignDepthToColor, deprojectPixelToPoint, toHomogeneous, fromHomogeneous, Config
from util.pointCloud import *
from photogrammetry.absoluteOrientation import umeyama, umeyama_ransac
import pyglet_test
from helpers import framesetFromConnectedCameras
from scipy.spatial.transform import Rotation as R
import o3d_test
import time

def preprocess_point_cloud(pcd, voxel_size):
  """
  From: http://www.open3d.org/docs/release/tutorial/Advanced/global_registration.html
  """
  #print(":: Downsample with a voxel size %.3f." % voxel_size)
  pcd_down = pcd.voxel_down_sample(voxel_size)

  radius_normal = voxel_size * 2
  #print(":: Estimate normal with search radius %.3f." % radius_normal)
  pcd_down.estimate_normals(
    o3d.geometry.KDTreeSearchParamHybrid(radius=radius_normal, max_nn=30))

  radius_feature = voxel_size * 5
  #print(":: Compute FPFH feature with search radius %.3f." % radius_feature)
  pcd_fpfh = o3d.registration.compute_fpfh_feature(
    pcd_down,
    o3d.geometry.KDTreeSearchParamHybrid(radius=radius_feature, max_nn=100))
  return pcd_down, pcd_fpfh




def execute_fast_global_registration(source_down, target_down, source_fpfh,
                                     target_fpfh, voxel_size):
  """
  From: http://www.open3d.org/docs/release/tutorial/Advanced/global_registration.html
  """                                     
  distance_threshold = voxel_size * 0.5
  #print(":: Apply fast global registration with distance threshold %.3f" \
          #% distance_threshold)
  result = o3d.registration.registration_fast_based_on_feature_matching(
    source_down, target_down, source_fpfh, target_fpfh,
    o3d.registration.FastGlobalRegistrationOption(
        maximum_correspondence_distance=distance_threshold))
  return result



def refine_registration(source, target, distThresh, T0):
  """
  Based on: http://www.open3d.org/docs/release/tutorial/Advanced/global_registration.html
  """                 
  #print(":: Point-to-plane ICP registration is applied on original point")
  #print("   clouds to refine the alignment. This time we use a strict")
  #print("   distance threshold %.3f." % distance_threshold)
  result = o3d.registration.registration_icp(
      source, target, distThresh, T0,
      o3d.registration.TransformationEstimationPointToPlane())
  return result

def coloredIcp(source, target, initialTransform, distThresh, initialRadius=0.04):
  #based on
  #http://www.open3d.org/docs/release/tutorial/Advanced/colored_pointcloud_registration.html
  scale = initialRadius
  max_iter = 100
  current_transformation = initialTransform
  result_icp = None
  last_icp = None
  #print("3. Colored point cloud registration")
  while True:
      radius = scale
      #print([iter, radius, scale])

      #print("3-1. Downsample with a voxel size %.2f" % radius)
      source_down = source.voxel_down_sample(radius)
      target_down = target.voxel_down_sample(radius)

      #print("3-2. Estimate normal.")
      source_down.estimate_normals(
          o3d.geometry.KDTreeSearchParamHybrid(radius=radius * 2, max_nn=30))
      target_down.estimate_normals(
          o3d.geometry.KDTreeSearchParamHybrid(radius=radius * 2, max_nn=30))

      #print("3-3. Applying colored point cloud registration")
      result_icp = o3d.registration.registration_colored_icp(
          source_down, target_down, radius, current_transformation,
          o3d.registration.ICPConvergenceCriteria(relative_fitness=1e-6,
                                                  relative_rmse=1e-6,
                                                  max_iteration=max_iter))
      
      if len(result_icp.correspondence_set) == 0:
        result_icp = last_icp
        break 
    

      current_transformation = result_icp.transformation
      last_icp = result_icp
      scale /= 2.0

      #print(result_icp)

  #print(result_icp)
  return result_icp

if len(sys.argv) < 3:
  print("Usage: test_icp.py cacheFile [point|plane|color|none] umeyamaInit")
  exit()

cacheFile = sys.argv[1]
icpType = sys.argv[2]
umeyamaInit = len(sys.argv) == 4 and sys.argv[3] == "U"
FGRInit = len(sys.argv) == 4 and sys.argv[3] == "F"

if icpType not in ["point", "plane", "color", "none"]:
  print("Usage: test_icp.py cacheFile [point|plane|color|none]")
  print("Icp type must be one of point, plane or color")
  exit()

print("Cachefile: {}".format(cacheFile))

frames, cameras = framesetFromConnectedCameras(cacheFile, verbose=True, setROI=False)

config = Config("config.json")

imgFeatures = []
boards = dict()

det1 = features.checkerboards.CheckerboardDetector("cb", config)
#det2 = features.spheres.SphereDetector()
#det3 = features.SIFT.SIFTDetector()
detCol = features.FeatureDetectorCollection()
#detCol.add(det3)
detCol.add(det1)
#detCol.add(det2)


img1 = frames[0].colorFrame
img2 = frames[1].colorFrame
depth1 = frames[0].depthFrame
depth2 = frames[1].depthFrame
K1 = cameras[0].intrinsics.getMatrix()
K2 = cameras[1].intrinsics.getMatrix()

T0 = np.eye(4)

voxelSize = 0.01
dThresh1_h = 2.0
dThresh1_l = 0.3

# how far away can a correspondence at most be in ICP
distThresh = 0.01 # 1cm


x1 = toColoredPointCloud(img1, depth1, K1, frontClippingPlane=dThresh1_l, backClippingPlane=dThresh1_h)
x2 = toColoredPointCloud(img2, depth2, K2, frontClippingPlane=dThresh1_l, backClippingPlane=dThresh1_h)

x1 = toO3DPointCloud(x1)
x2 = toO3DPointCloud(x2)

x1.estimate_normals()
x2.estimate_normals()


maxIter = 100
convCriteria = o3d.registration.ICPConvergenceCriteria(
  relative_fitness=1e-6,
  relative_rmse=1e-6,
  max_iteration=maxIter)

if umeyamaInit:
  print("Umeyama init")
  f1 = detCol.getFeatures(img1, depth1, K1)
  f2 = detCol.getFeatures(img2, depth2, K2)

  cFinder = corlib.Finder(detCol)
  c3d, labels = cFinder.c3DF(img1, depth1, K1, f1, img2, depth2, K2, f2)

  (_, _, _, _, T0), _ = umeyama_ransac(c3d, t=config.get("umeyama_ransac_thresh", 1e-4))
  evalUmeyama = o3d.registration.evaluate_registration(x1, x2, distThresh, T0)
  print(evalUmeyama)

elif FGRInit:
  print("FGR init")
  t1 = time.time()
  pc1_d, pc1_fpfh = preprocess_point_cloud(x1, voxelSize)
  pc2_d, pc2_fpfh = preprocess_point_cloud(x2, voxelSize)

  res = execute_fast_global_registration(pc1_d, pc2_d, pc1_fpfh, pc2_fpfh, voxelSize)
  print(time.time()-t1)
  print("Result fast global registration:", res)
  T0 = res.transformation

T = None 
Ro = None
reg_icp = None
# max correspondence distance

print("T0", T0)


if icpType != "none":
  print("ICP start")
  if icpType == "point":
    reg_icp = o3d.registration.registration_icp(
      x1, x2, distThresh, T0,
      o3d.registration.TransformationEstimationPointToPoint(), criteria=convCriteria)
  elif icpType == "plane":
    reg_icp = o3d.registration.registration_icp(
        x1, x2, distThresh, T0,
        o3d.registration.TransformationEstimationPointToPlane(), criteria=convCriteria)
  else:
    reg_icp = coloredIcp(x1, x2, T0, distThresh)
  print("ICP done")
  Rt = reg_icp.transformation

  print(reg_icp)
else:
  Rt = T0



Ro = Rt[0:3, 0:3]  
t = Rt[0:3, 3]

cam1 = np.array([
  [0.0, 0.0, 0.0],
  [1920.0, 0.0, 0.0],
  [1920.0, 1080.0, 0.0],
  [0.0, 1080.0, 0.0],
  [0.0, 0.0, 0.0]
])

cam2 = np.copy(cam1)
cameraTransform = np.zeros((4,4))
cameraTransform[0:3, 0:3] = Ro 
cameraTransform[0:3,3] = t 
cameraTransform[3,3] = 1.0



Rti = np.linalg.inv(Rt)
Cti = np.linalg.inv(cameraTransform)

coords = deprojectPixelToPoint(K1, cam1.T, np.array([0.1, 0.1, 0.1, 0.1, 0.0]).T).T 

coords2 = deprojectPixelToPoint(K2, cam2.T, np.array([0.1, 0.1, 0.1, 0.1, 0.0]).T).T 
coords2 = toHomogeneous(coords2)
coords2 = fromHomogeneous(np.dot(Cti, coords2.T).T)

x1 = fromO3DPointCloud(x1)
x2 = fromO3DPointCloud(x2)

print(Rt)
print(Rti)

pc1 = x1
pc2 = transformPointCloud(x2, Rti)

#print("Down sampling...")
#voxSize = 0.01/100.0 # 0.01 cm voxel size
#downpcd = downsamplePointCloud(np.vstack((pc1, pc2)), voxSize)
#print("Down sampling complete!")

wnd = o3d_test.CalibrationWindow()

wnd.addCamera(coords, "test")
wnd.addCamera(coords2, "test2")

wnd.addPointCloud(np.vstack((pc1, pc2)), "cam1")

wnd.run()





