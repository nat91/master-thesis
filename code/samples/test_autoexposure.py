import sys
sys.path.append("../lib")
from rswrapper import *
import numpy as np
import features
from util import Config
import matplotlib.pyplot as plt
import time

cams = CameraDiscoverer.find()
if cams.count() < 0:
  print("No cameras connected!")
  exit()

cams.start()

c0 = cams[0]

c0.disableAutoExposure()
c0.setExposure(500)
time.sleep(1)

frame = c0.capture()

print("Auto exposure off")
plt.imshow(np.array(frame))
plt.show()

print("Gain 50")
c0.setGain(50)
time.sleep(1)

frame = c0.capture()
plt.imshow(np.array(frame))
plt.show()


print("Gain 15")
c0.setGain(15)
time.sleep(1)

frame = c0.capture()
plt.imshow(np.array(frame))
plt.show()

print("Exposure 5ms")
c0.setExposure(5000)
time.sleep(1)

frame = c0.capture()
plt.imshow(np.array(frame))
plt.show()

c0.enableAutoExposure()
time.sleep(1)
print("Auto exposure enabled")
frame = c0.capture()
frame = np.array(frame)
plt.imshow(frame)
plt.show()

frame = c0.capture()
frameC = np.array(frame)

config = Config("config.json")
det = features.checkerboards.CheckerboardDetector("cb", config)
boards, corners = det.detect(frameC)

bb = features.checkerboards.libcbprocess.findBoundingBox(boards[0])
minX = int(np.min(bb[:,0]))
maxX = int(np.max(bb[:,0]))
minY = int(np.min(bb[:,1]))
maxY = int(np.max(bb[:,1]))

print("setROI")
c0.enableAutoExposure()
roiRes = c0.setROI(minX, minY, maxX, maxY)
print("setROI done")
if roiRes:
  print("ROI:", minX, minY, maxX, maxY)
  time.sleep(1)
  frame = c0.capture()
  frameC = np.array(frame)
  plt.imshow(frameC)
  plt.show()
else:
  print("Setting ROI failed")

cams.stop()