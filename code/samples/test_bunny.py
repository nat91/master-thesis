from pyntcloud.io import read_ply
import numpy as np
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt

bunny = read_ply("FLATFOOT_StanfordBunny_jmil_HIGH_RES_Smoothed.ply")

bunny_xyz = bunny["points"][["x", "y", "z"]].values
print('bunny_xyz: ', bunny_xyz)


fig = plt.figure()
# Add an axes
ax = fig.add_subplot(111,projection='3d')
xs = bunny_xyz[:,0]
ys = bunny_xyz[:,1]
zs = bunny_xyz[:,2]
ax.scatter(xs, ys, zs, "g.")
max_range = np.array([xs.max()-xs.min(), ys.max()-ys.min(), zs.max()-zs.min()]).max() / 2.0
mid_x = (xs.max()+xs.min()) * 0.5
mid_y = (ys.max()+ys.min()) * 0.5
mid_z = (zs.max()+zs.min()) * 0.5
ax.set_xlabel('x')
ax.set_ylabel('y')
ax.set_zlabel('z')
ax.set_xlim(mid_x - max_range, mid_x + max_range)
ax.set_ylim(mid_y - max_range, mid_y + max_range)
ax.set_zlim(mid_z - max_range, mid_z + max_range)
ax.set_xlabel('X axis')
ax.set_ylabel('Y axis')
ax.set_zlabel('Z axis')
plt.show()