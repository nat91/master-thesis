import sys
sys.path.append("../lib")
import open3d as o3d 
import numpy as np
from util.pointCloud import *

# https://stackoverflow.com/a/6802723
def rotation_matrix(axis, theta):
    """
    Return the rotation matrix associated with counterclockwise rotation about
    the given axis by theta radians.
    """
    axis = np.asarray(axis)
    axis = axis / np.sqrt(np.dot(axis, axis))
    a = np.cos(theta / 2.0)
    b, c, d = -axis * np.sin(theta / 2.0)
    aa, bb, cc, dd = a * a, b * b, c * c, d * d
    bc, ad, ac, ab, bd, cd = b * c, a * d, a * c, a * b, b * d, c * d
    return np.array([[aa + bb - cc - dd, 2 * (bc + ad), 2 * (bd - ac)],
                     [2 * (bc - ad), aa + cc - bb - dd, 2 * (cd + ab)],
                     [2 * (bd + ac), 2 * (cd - ab), aa + dd - bb - cc]])

class CalibrationWindow:

  def __init__(self):
    self.vis = o3d.visualization.Visualizer()
    self.vis.create_window(window_name="Calibration VIsualizer")
    renderOpt = self.vis.get_render_option()

    renderOpt.point_size = 1.0
    renderOpt.show_coordinate_frame = False
    renderOpt.background_color = np.array([0,0,0])
  

    self.cameras = dict()
    self.pointClouds = dict()

    # set view to our normal, y-axis upside down
    ctr = self.vis.get_view_control()
    paramOut = ctr.convert_to_pinhole_camera_parameters()

    # rotate 180 deg
    extr = np.eye(4, dtype="float32")
    extr[:,3] = np.array(paramOut.extrinsic[:,3], copy=True)
    rot = rotation_matrix((1, 0, 0), np.radians(-90))
    extr[0:3, 0:3] = rot

    params = o3d.camera.PinholeCameraParameters()
    params.intrinsic = paramOut.intrinsic
    params.extrinsic = extr
    ctr.convert_from_pinhole_camera_parameters(params)

    self.vis.reset_view_point(False)
    # draw coordinate axes
    alen = 0.3
    axes = o3d.geometry.LineSet(o3d.utility.Vector3dVector([
      [0,0,0],
      [0,0,alen], #z
      [0,alen,0], # y
      [alen,0,0] # x
    ]), o3d.utility.Vector2iVector([
      [0,1],
      [0,2],
      [0,3]
    ]))

    axes.colors = o3d.utility.Vector3dVector([
      [0.0, 0.0, 1.0],
      [0.0, 1.0, 0.0],
      [1.0, 0.0, 0.0]
    ])

    self.vis.add_geometry(axes)

  def addPointCloud(self, cloud, idx, color=np.array([1.0, 0.0, 0.0])):

    #print(cloud.shape)

    if cloud.shape[1] != 6:
      cloudColored = np.zeros((cloud.shape[0], cloud.shape[1]+3))
      cloudColored[:, 0:cloud.shape[1]] = cloud 
      cloudColored[:, cloud.shape[1]:] = color
      cloud = cloudColored
    
    if idx in self.pointClouds:
      pc = self.pointClouds[idx]
      pc.points = o3d.utility.Vector3dVector(cloud[:,0:3])
      pc.colors = o3d.utility.Vector3dVector(cloud[:, 3:])      

      self.vis.update_geometry(pc)

    else:
      pc = toO3DPointCloud(cloud)
      self.vis.add_geometry(pc)
      self.pointClouds[idx] = pc

  def addCamera(self, pos, idx):
    """
    Adds a camera surface to be drawn where pos is an array of corner points and one center.

    Args:
      pos (np.array): A array of 3D coordinates [top left, top right, bottom right, bottom left, center]
      idx (str): The id of the camera, used for updating.
    
    Returns:
      None
    
    """
    self.cameras[idx] = pos
    cam = o3d.geometry.LineSet(o3d.utility.Vector3dVector(pos),o3d.utility.Vector2iVector([
      [0,1],
      [1,2],
      [2,3],
      [3,0],
      [4,1],
      [4,2],
      [4,3],
      [4,0]
    ]))

    cam.paint_uniform_color([1.0, 1.0, 1.0])
    
    self.vis.add_geometry(cam)


  def run(self):
    self.vis.run()
   