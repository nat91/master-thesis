import numpy as np
import sys
sys.path.append("../lib")
from helpers import framesetFromConnectedCameras, leaveOneOutCalibration
from util import Config 
from util.pointCloud import *
import features 
import open3d
import copy

def decompose(T):
  T = copy.deepcopy(T)
  #print(T)

  t = T[0:3, 3] # translation
  R = T[0:3, 0:3]  # rotation + scaling here
  #print(np.dot(R.T, R))
  s = np.linalg.norm(R, axis=0) # scale
  R /= s # now rot
  

def coloredIcp(source, target, initialTransform):
  #based on
  #http://www.open3d.org/docs/release/tutorial/Advanced/colored_pointcloud_registration.html
  voxel_radius = [0.04, 0.02, 0.01, 0.005, 0.0025]
  max_iter = [50, 50, 50, 50, 50]
  current_transformation = initialTransform
  print("3. Colored point cloud registration")
  for scale in range(len(max_iter)):
      iter = max_iter[scale]
      radius = voxel_radius[scale]
      print([iter, radius, scale])

      print("3-1. Downsample with a voxel size %.2f" % radius)
      source_down = source.voxel_down_sample(radius)
      target_down = target.voxel_down_sample(radius)

      print("3-2. Estimate normal.")
      source_down.estimate_normals(
          o3d.geometry.KDTreeSearchParamHybrid(radius=radius * 2, max_nn=30))
      target_down.estimate_normals(
          o3d.geometry.KDTreeSearchParamHybrid(radius=radius * 2, max_nn=30))

      print("3-3. Applying colored point cloud registration")
      result_icp = o3d.registration.registration_colored_icp(
          source_down, target_down, radius, current_transformation,
          o3d.registration.ICPConvergenceCriteria(relative_fitness=1e-6,
                                                  relative_rmse=1e-6,
                                                  max_iteration=iter))
      current_transformation = result_icp.transformation
      print(result_icp)
  
  print(current_transformation)
  return current_transformation


config = Config("config.json")
cacheFile = "test_loo.cache"
frames, cameras = framesetFromConnectedCameras(cacheFile, verbose=True)

det1 = features.checkerboards.CheckerboardDetector("cb", config)
det2 = features.spheres.SphereDetector()
detCol = features.FeatureDetectorCollection()
detCol.add(det1)
detCol.add(det2)

error, Rti = leaveOneOutCalibration(detCol, ["cb_01"], config, cameras[0], frames[0], cameras[1], frames[1])

print("Leave one out mean squared error: {}".format(error))

distThresh = 0.005 # 0.5 cm


pc1 = toColoredPointCloud(
  frames[0].colorFrame, 
  frames[0].depthFrame, 
  cameras[0].intrinsics.getMatrix())

pc2 = toColoredPointCloud(
  frames[1].colorFrame, 
  frames[1].depthFrame, 
  cameras[1].intrinsics.getMatrix())

pc1 = toO3DPointCloud(pc1)
pc2 = toO3DPointCloud(pc2)

pc1.estimate_normals()
pc2.estimate_normals()

print("Our eval")
evalUmeyama = o3d.registration.evaluate_registration(pc2, pc1, distThresh, Rti)

print(evalUmeyama)

print("-"*50)
print("Applying colored ICP")
Tcolor = coloredIcp(pc2, pc1, np.eye(4))
decompose(Tcolor)
print("-"*50)

print("Apply point-to-point ICP")
reg_p2p = o3d.registration.registration_icp(
    pc2, pc1, distThresh, np.eye(4),
    o3d.registration.TransformationEstimationPointToPoint())
	

# estimate normals
pc1.estimate_normals()
pc2.estimate_normals()

print("Apply point-to-plane ICP")
reg_p2l = o3d.registration.registration_icp(
    pc2, pc1, distThresh, np.eye(4),
    o3d.registration.TransformationEstimationPointToPlane())



print("-"*50)

print("ICP P2P eval")
print(reg_p2p)
print("-"*50)

print("ICP P2L eval")
print(reg_p2l)
