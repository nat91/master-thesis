import pyglet
import pyglet.gl as gl
import numpy as np
from util import deprojectPixelToPoint

# Based on
# https://github.com/IntelRealSense/librealsense/blob/development/wrappers/python/examples/pyglet_pointcloud_viewer.py

# https://stackoverflow.com/a/6802723
def rotation_matrix(axis, theta):
    """
    Return the rotation matrix associated with counterclockwise rotation about
    the given axis by theta radians.
    """
    axis = np.asarray(axis)
    axis = axis / np.sqrt(np.dot(axis, axis))
    a = np.cos(theta / 2.0)
    b, c, d = -axis * np.sin(theta / 2.0)
    aa, bb, cc, dd = a * a, b * b, c * c, d * d
    bc, ad, ac, ab, bd, cd = b * c, a * d, a * c, a * b, b * d, c * d
    return np.array([[aa + bb - cc - dd, 2 * (bc + ad), 2 * (bd - ac)],
                     [2 * (bc - ad), aa + cc - bb - dd, 2 * (cd + ab)],
                     [2 * (bd + ac), 2 * (cd - ab), aa + dd - bb - cc]])



class AppState:

  def __init__(self):
    self.translation = np.array([0,0,1], np.float32)
    self.distance = 2
    self.mouse_btns = [False, False, False]
    self.pitch, self.yaw = np.radians(-10), np.radians(-15)

  @property
  def rotation(self):
    Rx = rotation_matrix((1, 0, 0), np.radians(-self.pitch))
    Ry = rotation_matrix((0, 1, 0), np.radians(-self.yaw))
    return np.dot(Ry, Rx).astype(np.float32)


def axes(size=1, width=1):
  """draw 3d axes"""
  gl.glLineWidth(width)
  pyglet.graphics.draw(6, gl.GL_LINES,
                        ('v3f', (0, 0, 0, size, 0, 0,
                                0, 0, 0, 0, size, 0,
                                0, 0, 0, 0, 0, size)),
                        ('c3f', (1, 0, 0, 1, 0, 0,
                                0, 1, 0, 0, 1, 0,
                                0, 0, 1, 0, 0, 1,
                                ))
                        )


def grid(size=1, n=10, width=1):
  """draw a grid on xz plane"""
  gl.glLineWidth(width)
  s = size / float(n)
  s2 = 0.5 * size
  batch = pyglet.graphics.Batch()

  for i in range(0, n + 1):
    x = -s2 + i * s
    batch.add(2, gl.GL_LINES, None, ('v3f', (x, 0, -s2, x, 0, s2)))
  for i in range(0, n + 1):
    z = -s2 + i * s
    batch.add(2, gl.GL_LINES, None, ('v3f', (-s2, 0, z, s2, 0, z)))

  batch.draw()

def drawPointClouds(pointClouds):

  gl.glPointSize(4.0)
  for (k, pc) in pointClouds.items():    
    pc.draw(gl.GL_POINTS)


def drawCameras(cameras):

  batch = pyglet.graphics.Batch()
  for (k, v) in cameras.items():
    if v.shape[0] != 5:
      print("Camera {} is invalid, 5 points must be supplied.".format(k))
      continue
    
    v = v.tolist()
    
    # draw lines around camera image plane
    batch.add(2, gl.GL_LINES, None, ('v3f', v[0] + v[1]))
    batch.add(2, gl.GL_LINES, None, ('v3f', v[1] + v[2]))
    batch.add(2, gl.GL_LINES, None, ('v3f', v[2] + v[3]))
    batch.add(2, gl.GL_LINES, None, ('v3f', v[3] + v[0]))
    # draw lines from center to corners
    batch.add(2, gl.GL_LINES, None, ('v3f', v[4] + v[1]))
    batch.add(2, gl.GL_LINES, None, ('v3f', v[4] + v[2]))
    batch.add(2, gl.GL_LINES, None, ('v3f', v[4] + v[3]))
    batch.add(2, gl.GL_LINES, None, ('v3f', v[4] + v[0]))

  batch.draw()


class CalibrationWindow:

  def __init__(self):
    self.window = pyglet.window.Window(
      config=gl.Config(
        double_buffer=True,
        samples=8 #MSAA
      ),
      resizable=True,
      vsync=True,
    )

    self.window.set_caption("Camera Calibration Visualizer")

    keys = pyglet.window.key.KeyStateHandler()
    self.window.push_handlers(keys)
    self. window.on_mouse_press = self.window.on_mouse_release = self.handle_mouse_btns
    self.window.push_handlers(self.on_key_press)

    self.state = AppState()
    self.cameras = dict()
    self.pointClouds = dict()

    self.window.on_draw = self.on_draw
    self.window.on_mouse_drag = self.on_mouse_drag

    self.yaw_label = pyglet.text.Label("Yaw: {:.2f}".format(self.state.yaw),
                          font_size=12,
                          x=10, y=self.window.height-20)


    self.pitch_label = pyglet.text.Label("Pitch: {:.2f}".format(self.state.pitch),
                          font_size=12,
                          x=10, y=self.window.height-40)


    self.trans_label = pyglet.text.Label("",
                          font_size=12,
                          x=10, y=self.window.height-60)
    self.trans_label.text = "Translation: x: {:.2f} y: {:.2f} z: {:.2f} ".format(self.state.translation[0], self.state.translation[1], self.state.translation[2])


    self.ymax = 0.5


  def addPointCloud(self, cloud, idx, color=np.array([1.0, 0.0, 0.0])):

    #print(cloud.shape)

    if cloud.shape[1] != 6:
      cloudColored = np.zeros((cloud.shape[0], cloud.shape[1]+3))
      cloudColored[:, 0:cloud.shape[1]] = cloud 
      cloudColored[:, cloud.shape[1]:] = color
      cloud = cloudColored

    ymax = np.max(cloud[:,1])
    self.ymax = np.maximum(self.ymax, ymax)
    #print(cloud.shape)

    vList = pyglet.graphics.vertex_list(cloud.shape[0],
      ("v3f", cloud[:,0:3].ravel()),
      ("c3f", cloud[:,3:].ravel())
    )

    self.pointClouds[idx] = vList

  def addCamera(self, pos, idx):
    """
    Adds a camera surface to be drawn where pos is an array of corner points and one center.

    Args:
      pos (np.array): A array of 3D coordinates [top left, top right, bottom right, bottom left, center]
      idx (str): The id of the camera, used for updating.
    
    Returns:
      None
    
    """
    self.cameras[idx] = pos

  def on_draw(self):
    self.window.clear()

    gl.glEnable(gl.GL_DEPTH_TEST)
    gl.glEnable(gl.GL_LINE_SMOOTH)

    width, height = self.window.get_size()
    gl.glViewport(0, 0, width, height)

    gl.glMatrixMode(gl.GL_PROJECTION)
    gl.glLoadIdentity()
    gl.gluPerspective(60, width / float(height), 0.01, 20)

    gl.glMatrixMode(gl.GL_MODELVIEW)
    gl.glLoadIdentity()

    # eyex, eyey, eyez, centerx, centery, centerz, upX, upY, upZ
    # sets y axis to pointing downwards
    gl.gluLookAt(0, 0, 0, 0, 0, 1, 0, -1, 0)

    gl.glRotated(self.state.pitch, 1, 0, 0)
    gl.glRotated(self.state.yaw, 0, 1, 0)

    gl.glTranslatef(*self.state.translation)

    gl.glPushMatrix()
    gl.glTranslatef(0, self.ymax, 0) # move the plane down in the Y direction
    grid(3.0, 30) # 3x3 meters, one cell is 10x10cm
    gl.glPopMatrix()

    
    drawPointClouds(self.pointClouds)
    
    axes(0.5, 4)

    drawCameras(self.cameras)


    # reset transformations before drawing 2d UI
    gl.glMatrixMode(gl.GL_PROJECTION)
    gl.glLoadIdentity()
    gl.glOrtho(0, width, 0, height, -1, 1)  
    gl.glMatrixMode(gl.GL_MODELVIEW)
    gl.glLoadIdentity()
    # draw 2d text
    self.yaw_label.text = "Yaw: {:.2f}".format(self.state.yaw)
    self.yaw_label.y = self.window.height-20
    self.yaw_label.draw()

    self.pitch_label.text = "Pitch: {:.2f}".format(self.state.pitch)
    self.pitch_label.y = self.window.height-40
    self.pitch_label.draw()

    self.trans_label.text = "Translation: x: {:.2f} y: {:.2f} z: {:.2f}".format(self.state.translation[0], self.state.translation[1], self.state.translation[2])
    self.trans_label.y = self.window.height-60
    self.trans_label.draw()


  def on_mouse_drag(self, x, y, dx, dy, buttons, modifiers):
    w, h = map(float, self.window.get_size())
    
    if buttons & pyglet.window.mouse.LEFT:
      self.state.yaw += -dx * 0.5
      self.state.pitch += -dy * 0.5

    if buttons & pyglet.window.mouse.RIGHT:
      dp = np.array((dx / w, -dy / h, 0), np.float32)
      self.state.translation += np.dot(self.state.rotation, dp)

    if buttons & pyglet.window.mouse.MIDDLE:
      dz = dy * 0.01
      self.state.translation -= (0, 0, dz)
      self.state.distance -= dz

  def handle_mouse_btns(self, x, y, button, modifiers):
    self.state.mouse_btns[0] ^= (button & pyglet.window.mouse.LEFT)
    self.state.mouse_btns[1] ^= (button & pyglet.window.mouse.RIGHT)
    self.state.mouse_btns[2] ^= (button & pyglet.window.mouse.MIDDLE)


  def on_key_press(self, symbol, modifiers):

    if symbol == pyglet.window.key.W:
      tns = np.array([0.0, 0.0, -0.05])
      self.state.translation += np.dot(self.state.rotation, tns)

    if symbol == pyglet.window.key.S:
      tns = np.array([0.0, 0.0, 0.05])
      self.state.translation += np.dot(self.state.rotation, tns)

    if symbol == pyglet.window.key.A or symbol == pyglet.window.key.LEFT:
      tns = np.array([0.05, 0.0, 0.0])
      self.state.translation += np.dot(self.state.rotation, tns)

    if symbol == pyglet.window.key.D  or symbol == pyglet.window.key.RIGHT:
      tns = np.array([-0.05, 0.0, 0.0])
      self.state.translation += np.dot(self.state.rotation, tns)

    if symbol == pyglet.window.key.UP:
      tns = np.array([0.0, 0.05, 0.0])
      self.state.translation += np.dot(self.state.rotation, tns)

    if symbol == pyglet.window.key.DOWN:
      tns = np.array([0.0, -0.05, 0.0])
      self.state.translation += np.dot(self.state.rotation, tns)

  def run(self):
    pyglet.app.run()


if __name__ == "__main__":
  w = CalibrationWindow()

  c1 = np.array([
    [0.0, 0.0, 0.0],
    [1920.0, 0.0, 0.0],
    [1920.0, 1080.0, 0.0],
    [0.0, 1080.0, 0.0],
    [0.0, 0.0, 0.0]
  ])


  intrinsics = np.array([
    [1380.14, 0, 965.5],
    [0, 1377.14, 529.21],
    [0, 0, 1]
  ])

  coords = deprojectPixelToPoint(intrinsics, c1.T, np.array([0.1, 0.1, 0.1, 0.1, 0.0]).T).T 
  c2 = coords + np.array([1.0, 0.0, 0.0])



  w.addCamera(coords, "test")
  w.addCamera(c2, "test2")

  w.run()
