import sys
sys.path.append("../lib")
from util import deprojectPixelToPoint
from calview import serverStart
import numpy as np


serverStart("localhost", 8098, "../testRec")

exit()
'''
w = CalibrationWindow()

c1 = np.array([
  [0.0, 0.0, 0.0],
  [1920.0, 0.0, 0.0],
  [1920.0, 1080.0, 0.0],
  [0.0, 1080.0, 0.0],
  [0.0, 0.0, 0.0]
])


intrinsics = np.array([
  [1380.14, 0, 965.5],
  [0, 1377.14, 529.21],
  [0, 0, 1]
])

coords = deprojectPixelToPoint(intrinsics, c1.T, np.array([0.1, 0.1, 0.1, 0.1, 0.0]).T).T 
c2 = coords + np.array([1.0, 0.0, 0.0])



w.addCamera(coords, "test")
w.addCamera(c2, "test2")

w.setYaw(20)
w.setPitch(10)
w.setTranslation(np.array([1.0, 1.0, 1.0]))

w.run()
'''