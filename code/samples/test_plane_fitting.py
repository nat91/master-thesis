import numpy as np
import pickle
import sys
import os
sys.path.append("../lib")
from features.checkerboards import CheckerboardDetector
from features.cubes import planeFitting
import util
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D


showBoth = True

def rotation_matrix(axis, theta):
  """
  Return the rotation matrix associated with counterclockwise rotation about
  the given axis by theta radians.
  """
  axis = np.asarray(axis)
  axis = axis / np.sqrt(np.dot(axis, axis))
  a = np.cos(theta / 2.0)
  b, c, d = -axis * np.sin(theta / 2.0)
  aa, bb, cc, dd = a * a, b * b, c * c, d * d
  bc, ad, ac, ab, bd, cd = b * c, a * d, a * c, a * b, b * d, c * d
  return np.array([[aa + bb - cc - dd, 2 * (bc + ad), 2 * (bd - ac)],
                    [2 * (bc - ad), aa + cc - bb - dd, 2 * (cd + ab)],
                    [2 * (bd + ac), 2 * (cd - ab), aa + dd - bb - cc]])

def cube(board):
  h, w = board.shape

  nw = np.ceil(board[0,0]).astype("int")
  ne = np.floor(board[0, w-1]).astype("int")
  se = np.floor(board[h-1, w-1]).astype("int")
  sw = np.ceil(board[h-1,0]).astype("int")

  corners = np.array([nw, ne, se, sw])

  minX = np.min(corners[:,0])
  minY = np.min(corners[:, 1])
  maxX = np.max(corners[:, 0])
  maxY = np.max(corners[:, 1])

  grid = np.mgrid[minX:maxX, minY:maxY]
  xs = grid[0].ravel()
  ys = grid[1].ravel()

  z = depth[ys, xs]
  validIdx = np.where(z > 0)[0]
  X = np.array([xs[validIdx], ys[validIdx], z[validIdx]])

  X = util.deprojectPixelToPoint(K, X, z[validIdx]).T
  m = np.mean(X, axis=0)
  
  c = board.coordsToArray()
  z = util.zInterp(c, depth)


  c = np.hstack((c,z.reshape((z.shape[0],1))))
  c = util.deprojectPixelToPoint(K, c.T, z).T
  

  print('X: ', X.shape)
  n = planeFitting.fitPlane(c)
  if n[2] < 0:
    n = -n
  print('n: ', n)
  print('np.linalg.norm(n): ', np.linalg.norm(n))

  #n = fitPlaneSVD(X)
  #print(n)
  #print(np.linalg.norm(n))

  fig = plt.figure()
  # Add an axes
  ax = fig.add_subplot(111,projection='3d')

  planex = np.linspace(np.min(X[:,0]), np.max(X[:,0]))
  planey = np.linspace(np.min(X[:,1]), np.max(X[:,1]))

  d = np.dot(n, m)
  print(d)
  xx, yy = np.meshgrid(planex, planey)
  zz = (d-n[0]*xx-n[1]*yy)/n[2]
  # plot the surface
  ax.plot_surface(xx, yy, zz, alpha=0.5)
  ax.quiver(m[0], m[1], m[2], n[0], n[1], n[2], length=0.2)

  #ax.plot(X[:,0], X[:,1], X[:,2], "g.")
  xs = X[:,0]
  ys = X[:,1]
  zs = X[:,2]
  max_range = np.array([xs.max()-xs.min(), ys.max()-ys.min(), zs.max()-zs.min()]).max() / 2.0
  mid_x = (xs.max()+xs.min()) * 0.5
  mid_y = (ys.max()+ys.min()) * 0.5
  mid_z = (zs.max()+zs.min()) * 0.5
  ax.set_xlim(mid_x - max_range, mid_x + max_range)
  ax.set_ylim(mid_y - max_range, mid_y + max_range)
  ax.set_zlim(mid_z - max_range, mid_z + max_range)
  ax.set_xlabel('X axis')
  ax.set_ylabel('Y axis')
  ax.set_zlabel('Z axis')
  plt.show()




  vlen = 7.0/100.0 # 5cm

  c2 = c+n*vlen

  nw = c[0]
  print('nw: ', nw)
  ne = c[w-1]
  print('ne: ', ne)
  se = c[h*w-1]
  print('se: ', se)
  sw = c[((h-1)*w)]
  print('sw: ', sw)



  checkerCenter = np.mean(np.array([nw, ne, se, sw]), axis=0)
  print('checkerCenter: ', checkerCenter)

  leftPoint = sw + ((nw - sw) / 2)
  print('leftPoint: ', leftPoint)
  rightPoint = se + ((ne - se) / 2)
  print('rightPoint: ', rightPoint)

  topPoint = (nw+ne)/2

  rotVec = checkerCenter-topPoint
  rotVec /= np.linalg.norm(rotVec)

  leftVector = leftPoint - checkerCenter
  print('leftVector: ', leftVector)
  # Normalize left vector and resize to edge
  leftVector = (leftVector / np.linalg.norm(leftVector)) * vlen/2.0
  print('corrected leftVector: ', leftVector)

  rightVector = rightPoint - checkerCenter
  print('rightVector: ', rightVector)
  # Normalize right vector and resize to edge
  rightVector = (rightVector / np.linalg.norm(rightVector)) * vlen/2.0
  print('corrected rightVector: ', leftVector)

  centroid = checkerCenter + n*vlen/2.0

  # 90 deg right
  rAng = -np.pi/2.0
  Rr = rotation_matrix(rotVec, rAng)

  # 90 deg left
  lAng =  np.pi/2.0
  Rl = rotation_matrix(rotVec, lAng)

  c3 = (np.dot(Rl, (c-checkerCenter).T ).T+checkerCenter) + n*vlen/2.0 + leftVector
  c4 = (np.dot(Rr, (c-checkerCenter).T ).T+checkerCenter) + n*vlen/2.0 + rightVector

  return (c, c2, c3, c4, centroid, n, X, m)


if len(sys.argv) < 2:
  print("Usage: python3 test_plane_fitting.py inputFile")
  exit()

inputFile = sys.argv[1] + ".data"
if not os.path.exists(inputFile):
  print("File {} does not exist".format(inputFile))
  exit()

with open(inputFile, "rb") as f:
  imgs, K = pickle.load(f)

  img, depth = imgs[0]
  print(K)

  boards = dict()
  boards["14"] = (4,4)
  boards["15"] = (4,4)
  det = CheckerboardDetector("cb", boards)
  boardsD, _ = det.detect(img)

  board = boardsD[0]
  board2 = boardsD[1]
  print('board: ', board)

  c, c2, c3, c4, centroid, n, X, m = cube(board)
  cc, cc2, cc3, cc4, centroid2, n2, X2, m2 = cube(board2)

  fig = plt.figure()
  # Add an axes
  ax = fig.add_subplot(111, projection='3d')

  ax.plot(c[:,0], c[:,1], c[:,2], "b.", label="Cube 1, side 1")
  ax.plot(c2[:,0], c2[:,1], c2[:,2], "b*", label="Cube 1, side 2")
  ax.plot(c3[:,0], c3[:,1], c3[:,2], "b1", label="Cube 1, side 3")
  ax.plot(c4[:,0], c4[:,1], c4[:,2], "bP", label="Cube 1, side 4")
  ax.plot(np.array([centroid[0]]), np.array([centroid[1]]), np.array([centroid[2]]), "b^", label="Cube 1 Center")

  planex = np.linspace(np.min(X[:,0]), np.max(X[:,0]))
  planey = np.linspace(np.min(X[:,1]), np.max(X[:,1]))

  d = np.dot(n, m)
  print(d)
  xx, yy = np.meshgrid(planex, planey)
  zz = (d-n[0]*xx-n[1]*yy)/n[2]
  # plot the surface
  ax.plot_surface(xx, yy, zz, alpha=0.5)
  ax.quiver(m[0], m[1], m[2], n[0], n[1], n[2], length=0.02)

  if showBoth:
    ax.plot(cc[:,0], cc[:,1], cc[:,2], "rP", label="Cube 2, side 1")
    ax.plot(cc2[:,0], cc2[:,1], cc2[:,2], "r1", label="Cube 2, side 2")
    ax.plot(cc3[:,0], cc3[:,1], cc3[:,2], "r.", label="Cube 2, side 3")
    ax.plot(cc4[:,0], cc4[:,1], cc4[:,2], "r*", label="Cube 2, side 4")
    ax.plot(np.array([centroid2[0]]), np.array([centroid2[1]]), np.array([centroid2[2]]), "r^", label="Cube 2 Center")

    planex = np.linspace(np.min(X2[:,0]), np.max(X2[:,0]))
    planey = np.linspace(np.min(X2[:,1]), np.max(X2[:,1]))

    d = np.dot(n2, m2)
    print(d)
    xx, yy = np.meshgrid(planex, planey)
    zz = (d-n2[0]*xx-n2[1]*yy)/n2[2]
    # plot the surface
    ax.plot_surface(xx, yy, zz, alpha=0.5)
    ax.quiver(m2[0], m2[1], m2[2], n2[0], n2[1], n2[2], length=0.02)




  # Fix axis range
  allPoints = np.concatenate((c, c2,c3,c4), axis=0)
  xs = allPoints[:,0]
  ys = allPoints[:,1]
  zs = allPoints[:,2]
  max_range = np.array([xs.max()-xs.min(), ys.max()-ys.min(), zs.max()-zs.min()]).max() / 2.0
  mid_x = (xs.max()+xs.min()) * 0.5
  mid_y = (ys.max()+ys.min()) * 0.5
  mid_z = (zs.max()+zs.min()) * 0.5
  ax.set_xlim(mid_x - max_range, mid_x + max_range)
  ax.set_ylim(mid_y - max_range, mid_y + max_range)
  ax.set_zlim(mid_z - max_range, mid_z + max_range)

  ax.set_xlabel('X axis')
  ax.set_ylabel('Y axis')
  ax.set_zlabel('Z axis')
  ax.legend()
  plt.show()

  squared_dist = np.sum((centroid-centroid2)**2, axis=0)
  dist = np.sqrt(squared_dist)
  print('dist: ', dist)