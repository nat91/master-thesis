import numpy as np
import skimage
import open3d as o3d
import sys
import os
sys.path.append("../lib")
import o3d_test
import pickle
from util.pointCloud import *


if len(sys.argv) < 2:
  print("Usage: python3 display_pointclouds.py pathToCacheFile")
  exit()

cacheFile = sys.argv[1]
if not os.path.exists(cacheFile):
  print("File {} does not exist.".format(cacheFile))
  exit()

f = open(cacheFile, "rb")
frames, cameras = pickle.load(f)
f.close()

T = np.eye(4)
T[0,3] = 0.18
T[2,3] = -0.485

wnd = o3d_test.CalibrationWindow()

for i in range(len(frames)):
  K = cameras[i].intrinsics.getMatrix()
  pc = toColoredPointCloud(frames[i].colorFrame, frames[i].depthFrame, K)
  if i > 0:
    pc = transformPointCloud(pc, T)
  wnd.addPointCloud(pc, "pc_{}".format(i))

wnd.run()
