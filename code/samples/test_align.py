import sys
sys.path.append("../lib")
from rswrapper import *
import matplotlib.pyplot as plt
import numpy as np
import os
from util import alignDepthToColor, deprojectPixelToPoint, toHomogeneous, fromHomogeneous, transformPoint, projectPointToPixel
import skimage
import skimage.color
import copy
import pyrealsense2 as rs

cameras = py_ku_ccal.CameraDiscoverer.find()
if not cameras.count():
  print("No cameras connected!")
  exit()

print("{} cameras connected".format(cameras.count()))

'''

pipe = rs.pipeline()

config = rs.config()
config.enable_stream(rs.stream.depth, 1280, 720, rs.format.z16, 30)
config.enable_stream(rs.stream.color, 1920, 1080, rs.format.rgb8, 30)
profile = pipe.start(config)

colorDev = profile.get_stream(rs.stream.color).as_video_stream_profile()
depthDev = profile.get_stream(rs.stream.depth).as_video_stream_profile()

extr = depthDev.get_extrinsics_to(colorDev)

Kd = depthDev.get_intrinsics()
Kr = colorDev.get_intrinsics()

Kd2 = np.zeros((3,3))
Kd2[0,0] = Kd.fx 
Kd2[1,1] = Kd.fy 
Kd2[0,2] = Kd.ppx
Kd2[1,2] = Kd.ppy

Kr2 = np.zeros((3,3))
Kr2[0,0] = Kr.fx 
Kr2[1,1] = Kr.fy 
Kr2[0,2] = Kr.ppx
Kr2[1,2] = Kr.ppy
Kr2[2,2] = 1.0

extr2 = np.eye(4)
extr2[0:3, 0:3] = np.array(extr.rotation).reshape((3,3)).T 
extr2[0:3, 3] = extr.translation


res = rs.rs2_deproject_pixel_to_point(Kd, [0.0,0.0], 3.0)
res2 = deprojectPixelToPoint(Kd2, [0.0,0.0,0.0], 3.0)
print(res)
print(res2)
print("-"*50)
res2.append(1.0)

resp = rs.rs2_transform_point_to_point(extr, res)
resp2 = transformPoint(extr2, res2)
print(resp)
print(resp2.tolist())
print("-"*50)

#rs.rs2_project_point_to_pixel
#rs.rs2_transform_point_to_point()
resd = rs.rs2_project_point_to_pixel(Kr, resp)
resd2 = projectPointToPixel(Kr2, resp2)

print(resd)
print(resd2.tolist())

exit()
'''

cameras.start()
cam = cameras[0]


print("Settling auto exposure")
for i in range(60):
  cam.capture()



K = cam.getRGBIntrinsics().getMatrix()
frame = cam.capture()

frameCopy = skimage.img_as_float(np.array(frame))
frame.enableDepthBuffer()

depthCopy = np.array(frame, dtype="float")
depthCopy *= cam.getDepthScale()

origDepth = copy.deepcopy(depthCopy)


depthCopy = alignDepthToColor(cam.getDepthIntrinsics().getMatrix(), cam.getRGBIntrinsics().getMatrix(), (1080, 1920), cam.getDepthToColorExtrinsics().getTransform(), depthCopy)

depthCopy[depthCopy >= 3.0] = 0
origDepth[origDepth >= 3.0] = 0

T = cam.getDepthToColorExtrinsics().getTransform()

cameras.stop()

# now with realsense


pipe = rs.pipeline()

config = rs.config()
config.enable_stream(rs.stream.depth, 1280, 720, rs.format.z16, 30)
config.enable_stream(rs.stream.color, 1920, 1080, rs.format.rgb8, 30)


profile = pipe.start(config)
depth_sensor = profile.get_device().first_depth_sensor()
depth_scale = depth_sensor.get_depth_scale()

depthDev = profile.get_stream(rs.stream.depth).as_video_stream_profile()
colorDev = profile.get_stream(rs.stream.color).as_video_stream_profile()


align_to = rs.stream.color
align = rs.align(align_to)

# settle auto exposure
print("Settling auto exposure again")
for i in range(60):
  frames = pipe.wait_for_frames()


extr = depthDev.get_extrinsics_to(colorDev)
print("-"*100)
print(T)
T2 = np.eye(4)
T2[0:3, 0:3] = np.array(extr.rotation).reshape((3,3)).T 
T2[0:3,3] = extr.translation
print(T2)
print(T-T2)
print("-"*100)


frames = pipe.wait_for_frames()
theirDepth1 = np.asanyarray(frames.get_depth_frame().get_data())*depth_scale
theirDepth = alignDepthToColor(cam.getDepthIntrinsics().getMatrix(), cam.getRGBIntrinsics().getMatrix(), (1080, 1920), cam.getDepthToColorExtrinsics().getTransform(), theirDepth1)
theirDepth[theirDepth >= 3.0] = 0
theirDepth1[theirDepth1 >= 3.0] = 0


aligned_frames = align.process(frames)
aligned_depth_frame = np.asanyarray(aligned_frames.get_depth_frame().get_data())*depth_scale
aligned_depth_frame[aligned_depth_frame >= 3.0] = 0

color_frame = np.asanyarray(aligned_frames.get_color_frame().get_data())


plt.subplot(2, 2, 1)
plt.title("Theirs")
plt.imshow(aligned_depth_frame, cmap="gray")
plt.subplot(2, 2, 2)
plt.title("Ours")
plt.imshow(depthCopy, cmap="gray")
plt.subplot(2, 2, 3)
plt.title("Ours with Theirs")
plt.imshow(theirDepth, cmap="gray")

plt.subplot(2, 2, 4)
plt.title("Original")
plt.imshow(theirDepth1, cmap="gray")
plt.show()

plt.imshow( np.abs(aligned_depth_frame-theirDepth), cmap="gray" )
plt.show()

plt.imshow(0.5*depthCopy+0.5*skimage.color.rgb2gray(frameCopy), cmap="gray")
plt.show()