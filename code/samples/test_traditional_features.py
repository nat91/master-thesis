"""
This file tests traditional features (SIFT) and holds them up against using calibration objects with designed features.
"""
import sys
import os
import cv2
import matplotlib.pyplot as plt
import skimage
import numpy as np
from scipy.spatial.transform import Rotation as R
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import time
import copy

from helpers import framesetFromConnectedCameras, leaveOneOutCalibration
from util import zInterp, deprojectPixelToPoint, Config
from util.pointCloud import *
import pyglet_test
from photogrammetry.absoluteOrientation import umeyama_ransac
import correspondences as corlib
import features

def getSIFTCorrespondences(frame1, cam1, frame2, cam2):
  # https://docs.opencv.org/master/dc/dc3/tutorial_py_matcher.html
  gray1 = cv2.cvtColor(frame1.colorFrame, cv2.COLOR_RGB2GRAY)
  gray1 = skimage.img_as_ubyte(gray1)
  gray2 = cv2.cvtColor(frame2.colorFrame, cv2.COLOR_RGB2GRAY)
  gray2 = skimage.img_as_ubyte(gray2)  

  sift = cv2.xfeatures2d.SIFT_create()
  features1, descriptors1 = sift.detectAndCompute(gray1, None)
  features2, descriptors2 = sift.detectAndCompute(gray2, None)

  bf = cv2.BFMatcher()
  matches = bf.knnMatch(descriptors1, descriptors2, k=2)
  # sort so matches with lower distance come first.
  matches = sorted(matches, key = lambda x: x[0].distance)

  # ratio test, only use something as a correspondence
  # if the best match is sufficiently different from the next best match.
  
  correspondences = []

  added = set()

  for m,n in matches:
    if (m.queryIdx in added) or (m.trainIdx in added):
      continue 

    if m.distance < 0.6*n.distance:
      
      pt1 = features1[m.queryIdx].pt
      pt2 = features2[m.trainIdx].pt

      added.add(m.queryIdx)
      added.add(m.trainIdx)

      correspondences.append([pt1[0], pt1[1], 0.0, pt2[0], pt2[1], 0.0])


  C = np.array(correspondences)
  d1 = frame1.depthFrame
  d2 = frame2.depthFrame

  #corlib.plot2D(frame1.colorFrame, frame2.colorFrame, np.hstack((C[:,0:2], C[:, 3:5])))


  z1 = zInterp(C[:,0:2], d1)
  z2 = zInterp(C[:,3:5], d2)

  validIdx = np.where((z1 > 0) & (z2 > 0))
  C = C[validIdx]
  C[:,2] = z1[validIdx]
  C[:,5] = z2[validIdx]
  
  C[:,0:3] = deprojectPixelToPoint(cam1.intrinsics.getMatrix(), C[:,0:3].T, C[:,2]).T
  C[:,3:] = deprojectPixelToPoint(cam2.intrinsics.getMatrix(), C[:,3:].T, C[:,5]).T
  
  return C


if len(sys.argv) < 2:
  print("Usage: python3 test_traditional_features.py cacheFile")
  exit()

cacheFile = sys.argv[1]
frames, cameras = framesetFromConnectedCameras(cacheFile, verbose=True)

config = Config("config.json")

tStart = time.time()

C = getSIFTCorrespondences(frames[0], cameras[0], frames[1], cameras[1])
(Rot, t, scale, err, Rt), inliers = umeyama_ransac(C, t=1e-4)

print("Calibration took: {}s".format(time.time()-tStart))

corlib.plot3D(C[inliers])
c1 = toHomogeneous(C[:,0:3])
c2 = toHomogeneous(C[:, 3:])

c1t = np.dot(Rt, c1[inliers].T).T

fig = plt.figure()
ax = fig.gca(projection="3d")
ax.plot(c2[inliers, 0], c2[inliers, 1], c2[inliers,2], "r.", label="c2")
ax.plot(c1[inliers, 0], c1[inliers, 1], c1[inliers,2], "g.", label="c1")

ax.plot(c1t[:, 0], c1t[:, 1], c1t[:,2], "b.", label="c1t")
max_range = np.array([c2[inliers, 0].max()-c2[inliers, 0].min(), c2[inliers, 1].max()-c2[inliers, 1].min(), c2[inliers, 2].max()-c2[inliers, 2].min()]).max() / 2.0
mid_x = (c2[inliers, 0].max()+c2[inliers, 0].min()) * 0.5
mid_y = (c2[inliers, 1].max()+c2[inliers, 1].min()) * 0.5
mid_z = (c2[inliers,2].max()+c2[inliers,2].min()) * 0.5
ax.set_xlim(mid_x - max_range, mid_x + max_range)
ax.set_ylim(mid_y - max_range, mid_y + max_range)
ax.set_zlim(mid_z - max_range, mid_z + max_range)
ax.set_xlabel('X axis')
ax.set_ylabel('Y axis')
ax.set_zlabel('Z axis')    
ax.view_init(elev=0., azim=0)
plt.legend()

plt.show()



print("MSE:", err)

r = R.from_matrix(Rot)
print("Rotations:", r.as_euler("xyz", degrees=True))
print("Translation:", t)
print("Scale:", scale)

print("inliers/totalPoints: {}/{}".format(len(inliers), C.shape[0]))
print(Rt)

## plot
dThresh1_h = 2.0
dThresh1_l = 0.3

K1 = cameras[0].intrinsics.getMatrix()
img1 = frames[0].colorFrame
depth1 = frames[0].depthFrame

K2 = cameras[1].intrinsics.getMatrix()
img2 = frames[1].colorFrame
depth2 = frames[1].depthFrame


x1 = toColoredPointCloud(img1, depth1, K1, frontClippingPlane=dThresh1_l, backClippingPlane=dThresh1_h)
x2 = toColoredPointCloud(img2, depth2, K2, frontClippingPlane=dThresh1_l, backClippingPlane=dThresh1_h)

cam1 = np.array([
  [0.0, 0.0, 0.0],
  [1920.0, 0.0, 0.0],
  [1920.0, 1080.0, 0.0],
  [0.0, 1080.0, 0.0],
  [0.0, 0.0, 0.0]
])

cam2 = np.copy(cam1)
cameraTransform = np.zeros((4,4))
cameraTransform[0:3, 0:3] = Rot
cameraTransform[0:3,3] = t 
cameraTransform[3,3] = 1.0

Rti = np.linalg.inv(Rt)
Cti = np.linalg.inv(cameraTransform)

testLeaveOneOut = True

if testLeaveOneOut:
  # detect checkerboards for an accuracy measure
  det1 = features.checkerboards.CheckerboardDetector("cb", config)
  detCol = features.FeatureDetectorCollection()
  detCol.add(det1)

  cFinder = corlib.LooCorrespondences(detCol, ["cb_21"])

  c3da, c3db, _,_ = cFinder.find(img1, depth1, K1, img2, depth2, K2)
  err = 0.0 
  x = c3db[:,0:3]
  N = c3db.shape[0]
  tx = transformPointCloud(copy.deepcopy(c3db[:,3:]), Rti)
  for i in range(N):
    err += np.dot(x[i]-tx[i], x[i]-tx[i])

  err /= N


  err2, _ = leaveOneOutCalibration(detCol, ["cb_21"], config, cameras[0], frames[0], cameras[1], frames[1])

  print("Calibration targets trans MSE: {}".format(err2))
  print("SIFT Transformation MSE: {}".format(err))

# draw calibration result
coords = deprojectPixelToPoint(K1, cam1.T, np.array([0.1, 0.1, 0.1, 0.1, 0.0]).T).T 

coords2 = deprojectPixelToPoint(K2, cam2.T, np.array([0.1, 0.1, 0.1, 0.1, 0.0]).T).T 
coords2 = toHomogeneous(coords2)
coords2 = fromHomogeneous(np.dot(Cti, coords2.T).T)  

pc1 = x1
pc2 = transformPointCloud(x2, Rti)

wnd = pyglet_test.CalibrationWindow()
wnd.addCamera(coords, "test")
wnd.addCamera(coords2, "test2")


wnd.addPointCloud(np.vstack((pc1, pc2)), "pc1")

wnd.run()