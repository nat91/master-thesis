import numpy as np
import matplotlib.pyplot as plt
from skimage.transform import resize

def segments(mask, onColor, offColor):
  seg = np.ones((9, 5, 3))*offColor
  segments = [
    [(1,4), (2,4), (3,4)], #0
    [(0,1), (0,2), (0,3)], #1
    [(1,0), (2,0), (3,0)], #2
    [(4,1), (4,2), (4,3)], #3
    [(5,4), (6,4), (7,4)], #4
    [(8,1), (8,2), (8,3)], #5
    [(5,0), (6,0), (7,0)], #6
    ]
  
  for i in range(len(segments)):
    color = onColor if (mask >> i) & 0x1 else offColor 
    for (y,x) in segments[i]:
      seg[y,x,:] = color
  
  return seg

def get_digit(digit, onColor, offColor):
  digits = [
    247, # 11110111 -> 0
    17,  #   10001 -> 1
    107, # 1101011 -> 2
    59,  #  111011 -> 3
    29,  #   11101 -> 4
    62,  #  111110 -> 5  
    254, # 11111110 ->6
    19, #  10011 -> 7
    127, # 1111111 -> 8
    63, #  111111 -> 9
  ]

  return segments(digits[digit], onColor, offColor)


def gen_checkerboard(size, width, digit1, digit2):
  img = np.zeros((size*width, size*width, 3))
  primaryColor = np.zeros(3)
  secondaryColor = np.ones(3)
  for i in range(size):
    for j in range(size):
      # white or black patch?
      isPrimary = (i + j) % 2 != 0

      color = primaryColor if isPrimary else secondaryColor
      numColor = secondaryColor if isPrimary else primaryColor

      img[i*width:i*width+width,j*width:j*width+width,:] = color

      if i == 0 or i == size-1 or j == 0 or j == size-1:
        # center digit
        digitToDraw = digit1 if not isPrimary else digit2
        img[i*width+2:i*width+width-2,j*width+4:j*width+width-4,:] = get_digit(digitToDraw, numColor, color)

  return img 


def gen_oriented_checkerboard(size, digit1, digit2, scale=1):
  """
  Generates a checkerboard pattern width size*size squares identified by two digits digit1 and digit2,
  the checkerboard includes a red, green and blue orientation pattern to determine which side is visible.

  Args:
    size (int): How many squares the checkerboard should have, size*size squares will be generated
    digit1 (int): Digit to show in even squares, must be in range 0-9
    digit2 (int): Digit to show in odd squares, must be in range 0-9
    scale (int): Scale of the output image
  
  Returns:
    (np.array): Returns the generated RGB checkerboard image

  """

  # width/height of one checker
  width = 13
  # orientation pattern size
  orientPSize = width//3
  borderSize = 0
  borderColor = np.ones(3)

  w = size*width + 2*borderSize+2*orientPSize
  h = w
  img = np.ones((h, w, 3))

  red = np.array([1, 0, 0])
  green = np.array([0, 1, 0])
  blue = np.array([0, 0, 1])
  purple = np.array([0.5, 0.0, 0.8])

  checkerboard = gen_checkerboard(size, width, digit1, digit2)
  # set checkerboard loc
  img[borderSize+orientPSize:borderSize+orientPSize+size*width, borderSize+orientPSize:borderSize+orientPSize+size*width] = checkerboard
  # set orientation patterns
  img[borderSize:borderSize+orientPSize, borderSize:w-borderSize] = red
  img[borderSize+orientPSize:h-borderSize-orientPSize, borderSize:borderSize+orientPSize] = blue
  img[borderSize+orientPSize:h-borderSize-orientPSize, w-borderSize-orientPSize:w-borderSize] = green
  img[h-borderSize-orientPSize:h-borderSize, borderSize:w-borderSize] = purple

  # set quiet zone
  img[0:borderSize, :] = borderColor
  img[h-borderSize:, :] = borderColor
  img[:, 0:borderSize] = borderColor
  img[:, w-borderSize:] = borderColor

  # set cut border
  img[0:h-1, 0] = np.zeros(3)
  img[0:h-1, w-1] = np.zeros(3)
  img[h-1, :] = np.zeros(3)
  img[0, :] = np.zeros(3)


  if scale != 1:
    h = int(np.round(h*scale))
    w = int(np.round(w*scale))
    img = resize(img, (h,w), order=0, anti_aliasing=True, anti_aliasing_sigma=None, preserve_range=True)

  return img 
