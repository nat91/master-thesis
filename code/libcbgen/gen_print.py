import matplotlib.pyplot as plt
import subprocess
import os
from .gen import gen_oriented_checkerboard

def gen_print(digits1, digits2, sizeInCm=5, dim=5):
  """
  Given a list of digits, generates a latex file containing images of all checkerboards
  with the given digits

  Args:
    digits1 (list): List of most significant digits
    digits2 (list): List of least significant digits
    sizeInCm (float): The printable width/height of each checkerboard in cm. Note the actual dimensions may vary depending on your printers accuracy.
    dim (int): The number of checkers to generate, must be odd

  Returns:
    None: Nothing is returned, the function outputs a latex file and automatically compiles it with pdflatex
  
  """

  if dim % 2 == 0:
    raise RuntimeError("Dim must be an odd integer")

  plt.tight_layout(pad=0)

  
  for digit1 in digits1:
    for digit2 in digits2:
      plt.imshow(gen_oriented_checkerboard(dim, digit1, digit2))
      plt.axis("off")
      plt.savefig("pat_{}_{}.eps".format(digit1, digit2), bbox_inches="tight", pad_inches=0)
      plt.savefig("pat_{}_{}.eps".format(digit1, digit2), bbox_inches="tight", pad_inches=0) 
      plt.clf()

  path = os.path.dirname(__file__)

  with open(path + "/header.tex", "r") as f:
    doc = f.read()
    for i in digits1:
      for j in digits2:
        doc += "\\includegraphics[height={}cm,width={}cm]{{pat_{}_{}.eps}}".format(sizeInCm, sizeInCm, i, j)
        doc += "\\\\\\\\\\vspace{1cm}"
    
    doc += "\\end{document}"
    out = open("output.tex", "w")
    out.write(doc)
    out.close()

  cwd = os.getcwd()

  subprocess.run(["pdflatex", "output.tex"], cwd=cwd)
  os.remove("output.log")
  os.remove("output.aux")

