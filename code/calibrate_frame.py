import sys 
sys.path.append("lib")
sys.path.append("samples")
import numpy as np
import pickle
import skimage


from analysis import cameraCalibration
import features
from util import Config
from helpers import framesetFromConnectedCameras
from util import Config, deprojectPixelToPoint, toHomogeneous, fromHomogeneous
import o3d_test

if len(sys.argv) < 2:
  print("Usage: python3 calibrate_frame.py cacheFilePath")
  exit()


## 1. Capture to from connected cameras or use cache
cacheFile = sys.argv[1]
config = Config("config.json")

autoExposureFrames = config.get("cameras.auto_exposure_frames", 30)
setROI = config.get("cameras.auto_ROI", False)
backClip = config.get("calibration.back_clipping_plane", 3.0)

frames, cameras = framesetFromConnectedCameras(cacheFile, verbose=True, autoexposureFrames=autoExposureFrames, setROI=setROI, backClippingPlane=backClip)

## 2. Setup feature detectors
fDet = features.FeatureDetectorCollection()
if config.get("calibration.detect_checkerboards", True):
  cDet = features.checkerboards.CheckerboardDetector("cb", config)
  fDet.add(cDet)

if config.get("calibration.detect_spheres", True):
  sDet = features.spheres.SphereDetector()
  fDet.add(sDet)

if config.get("calibration.detect_SIFT", True):
  SIFTDet = features.SIFT.SIFTDetector(config)
  fDet.add(SIFTDet)

## 3. Perform camera calibration
print("Now calibrating...")
T = cameraCalibration(cameras, frames, fDet, config)
print("Calibration done.")

## 4. Save calibration
outFile = cacheFile[:cacheFile.rfind(".")] + ".cal_dat"
with open(outFile, "wb") as f:
  print("Writing result to {}...".format(outFile))
  pickle.dump(T, f)
  print("Result written to {}".format(outFile))


## 5. Plot
N = len(cameras)
wnd = o3d_test.CalibrationWindow()
PCs = None

for i in range(N):
  if i not in T and i != 0:
    print("Camera calibration for camera {} failed".format(i))
    continue

  # camera i frustrum
  (h,w) = (1080, 1920) #cameras[i].rgbResolution
  cam = np.array([
    [0.0, 0.0, 0.0],
    [w, 0.0, 0.0],
    [w, h, 0.0],
    [0.0, h, 0.0],
    [0.0, 0.0, 0.0]
  ])  

  K = cameras[i].intrinsics.getMatrix()

  coords = deprojectPixelToPoint(K, cam.T, np.array([0.1, 0.1, 0.1, 0.1, 0.0]).T).T
  # transform the camera to cam 0's coordinate system if it is not cam 0
  if i > 0:
    coords = toHomogeneous(coords)
    coords = fromHomogeneous(np.dot(T[i]["transformCam"], coords.T).T)

  wnd.addCamera(coords, "cam{}".format(i))

  # draw camera i's point cloud
  dThresh_h = 2.0
  dThresh_l = 0.3

  depth = frames[i].depthFrame

  idx = np.where( (depth <= dThresh_h) & (depth >= dThresh_l))
  xs = idx[1]
  ys = idx[0]
  zs = depth[idx]

  X = deprojectPixelToPoint(K, np.array([xs, ys, zs]), zs).T
  # transform point cloud to cam 0's coordinate system if it is not cam 0
  if i > 0:
    X = fromHomogeneous(np.dot(T[i]["transformPC"], toHomogeneous(X).T).T)

  
  colorFrame = skimage.img_as_float(frames[i].colorFrame)
  color = colorFrame[ys, xs]
  #print(color.shape)

  X = np.hstack((X, color))

  if PCs is not None:
    PCs = np.vstack((PCs, X))
  else:
    PCs = X

wnd.addPointCloud(PCs, "PC")

wnd.run()