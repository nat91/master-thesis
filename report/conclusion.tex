\section{Discussion}
\subsection*{Camera Noise}
One thing that we have noticed throughout the project is that if the lighting conditions are not ideal, then the cameras have significant noise both in the depth and color images. The former manifests itself as waves in flat surfaces and the latter as electronic noise and poor exposure. Both affect the quality of the alignment and the point cloud. Furthermore, the cameras also have problems at discontinuities. There are filters provided by the Intel RealSense API that can be applied to reduce the depth noise, but they require the cameras to be static or they change the geometry of the scene (smoothing). We cannot assume the cameras to be static and the latter might change the geometry of the robot. It is also an option to apply the Kalman filter~\cite{kalman1960} to the point cloud, to utilize temporal information to correct for noise. This filter assumes Gaussian noise and has been used on point clouds obtained with the Kinect \cite{KinectKalman}, but without a noise model for the D415, we cannot predict the usefulness of the filter.

We have found that artificial and uniform lighting works well and that it is important to restrict the depth data to the working volume. The latter ensures that no correspondences are found in the part of the data that is the noisiest. If correspondences are found in the noisy part of the data, then it will try to align parts of the point cloud with significantly different noise levels, resulting in a compromise where the alignment is not tight.

\subsection*{Inferring World Positions from Known Geometry}
As discussed earlier, it is beneficial to have a calibration object that can be recognized from all angles and still provide many feature points. To achieve this, we explored using a cube of known size with known checkerboards on all its sides. Then, in theory, detecting one side and knowing the cube dimensions we would be able to infer the checkerboard points on all sides of the cube. We found that detecting one side of the cube is not enough, because of the noise in the data. However, detecting two sides may be sufficient because we can then enforce geometric constraints like perpendicularity, in order to improve the estimated cube geometry. The cube was of interest due to the many synthetic feature points it could provide. We did succeed with getting a synthetic feature point from the sphere, its centroid. The reason the sphere is easier to fit is that its entire surface is described by one formula, in contrast, the cube is discontinuous and is described by its dimensions, multiple planes, and perpendicularity constraints between them.

\subsection*{Our Calibration Algorithm}
Currently, at DIKU, camera calibration is limited to the two-camera scenario, and calibration is a preprocessing step. Fussballs (spheres) are placed in the empty learning cube and the calibration algorithm is run once. After that, the spheres are removed and the experiment with the robot, motors, and other externals can be set up. While this works, it has the inconvenience that if a camera is accidentally moved, the calibration has to be redone. This means emptying the cube and starting over. The old calibration also had a few degenerate setups, where symmetry in the sphere positions could give a wrong calibration. We have improved on these shortcomings in several ways. Our calibration objects can stay in the scene during the experiment. If a camera is accidentally moved the calibration can be rerun without resetting. Our algorithm works for an arbitrary amount of cameras. We provided a Python API that can be incorporated into existing scripts. We have made it possible to time synchronize multiple recordings automatically. Previously, there was no way of doing this, so when multiple cameras were used it was necessary to take still images. As long as more than one calibration object is used, and at least $4$ correspondences can be found, then to the best of our knowledge no degenerate solutions are possible and a valid calibration will be returned.

\citet{MathiasMA} noted in his thesis that segmenting the soft robot was a computationally heavy task.
We believe our algorithm and calibration objects can be used to ease this process. If the calibration objects have known positions relative to the soft robot,
their locations can be used to find the volume the soft robot is located in automatically.

While we have improved the situation in general, there are still some shortcomings that need to be investigated further and improved. For one, the calibration is relatively slow, which makes it unsuitable for real-time usage, but fine for calibrating every once in a while or as a post-processing step if it is an entire video sequence. Lastly, we have kept the world coordinate system in one of the cameras, which is a disadvantage because the world coordinate system will move if the camera does. We have considered using an object placed in the scene as a world coordinate system, however, we are not sure if this is robust enough when there is a risk of occlusion and lighting conditions making detection temporarily unavailable.

We demonstrated that our time calibration method bounds the time alignment error between the cameras to at most one frame. If the soft robots exhibit fast movement, this error is possibly too large. This can be alleviated by increasing the FPS, but this necessitates a lower image resolution, due to bandwidth limitations, which in turn makes it more difficult to find correspondences. The ideal solution would be to implement the hardware synchronization, however, this requires making an active interconnect between the cameras. Making such an interconnect is beyond our knowledge in electronics, but might be feasible for others.
% or be able to detect the movement and correct for it afterwards

% Calibration objects can stay in the scene, they dont need to be removed
% More robustness
% Calibration of an entire video stream as compared to stills
% Python API that can be used by other students
% Support for more than two cameras


%% Enables:
% -------------------------
% Segmenting the robot (manual) - segment within a calibration spanned volume - Mathias
% Resetting problem - Mathias + Max
% Always find markers => more than two cameras 

%% Made but not sure it was there
% ---------------------------
% Data acquisition tools
% Analyze entire video stream

%% Negatives:
% ---------------------------
% Slow 
% placement of calibration objects (SIFT or similar can help)
% World coordinate system in cameras - more work needed. Better to make a rig that can firmly keep a camera in place, we think it is ultimately hard to have it in an object because it will inevitably be occluded. (hard to avoid)

% Time calibration: Higher frame rate needed for fast movements (gives lower image resolution => harder feature detection) or hardware sync. Hard to see camera flash in outdoors environments (buy better flash?) - Should it have its own section?
\subsection*{Calibration Objects}
An important question is what calibration objects, if any, should be used. We found that the spheres, when placed correctly, were fairly robust and can be detected from all angles. However, the spheres only provide one feature point which is their biggest shortcoming. Furthermore, it is hard to tell more than a few colors apart robustly, which also effectively limits the number of spheres that can be placed. The checkerboards provide many feature points, but cannot be detected from all angles. Furthermore, the checkerboards are vulnerable to noise in the RGB data and low light conditions. SIFT can in theory find feature points as long as there is enough texture in the scene. However, we found that matching the SIFT features between scenes often produced false correspondences. A SIFT feature is discarded if the match is ambiguous, i.e. the second closest match must be far away from the best match. This is determined by a threshold, but even when set restrictively, false correspondences are still found that have to be filtered away by RANSAC. Based on that, we have found that it is the most robust to use a combination of SIFT (or similar) and calibration objects.

\section{Future Work}
\input{futureWork}

\section{Conclusion}
In this thesis, we have presented our algorithm for aligning the point clouds of multiple RGBD cameras automatically.
We based it on Umeyama's algorithm, and modified it with RANSAC, to make it robust to false and noisy correspondences.
We also presented a solution for automatically aligning the recordings from multiple cameras in time, using a camera flash triggered by an Arduino. We argued that the upper bound on the alignment error is $\frac{1}{\text{FPS}}$ seconds and noted that if there are fast movements in the scene, then an increase in the FPS or hardware synchronization will be necessary.
We also performed a brief literature review on both marker-based and markerless deformation tracking. It was our impression, that the marker-based methods are more mature, while the markerless ones are still an ongoing research topic. Of the markerless methods we reviewed, we found VolumeDeform by~\citet{InnmannMatthias2016VRVN} to be the most promising for future work at DIKU, due to its impressive results and use of commodity RGBD cameras.

In conclusion, our algorithm improves on the current tools available at DIKU, by being more robust and allowing for continuous calibration, as opposed to being a preprocessing step. This was made possible by designing calibration objects that can remain in the experiment environment during recording. We have experimentally validated the algorithm's robustness and accuracy in aligning the point clouds. Our results showed that as long as enough correspondences can be established, the alignment will in general succeed.