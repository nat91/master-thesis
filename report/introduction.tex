The purpose of this master thesis is to explore methods for automatically combining the point cloud data from multiple cameras, to assist with tracking the deformations of a soft robot. Soft robots are made of a compliant material that make them more suitable for interacting with humans and other tasks. They can be actuated by motors pulling on internal cables, pneumatics, and other methods. They interact with their environment by deforming.
The soft robots at the Department of Computer Science at the University of Copenhagen (DIKU) are typically made of silicone rubber or a similar elastic material.
They are then put through motions that deform them in various ways.
These deformations can be tracked either directly through markers on the surface of the robot, or by a markerless approach, where the deformed robot is matched to its mesh.
For both approaches, it is necessary to use multiple cameras to get a full capture of the soft robot.
Each camera produces point cloud data in its own coordinate system, making it is necessary to merge the data from each camera into a common reference frame.
We can merge the point cloud data between any two cameras by finding the transformation between their coordinate systems. This is called extrinsic camera calibration. The camera positions are not static, they may change due to being moved or because of small imperceptible vibrations.
Thus, for accuracy, it is necessary to redo the camera calibration every once in a while.
Furthermore, we also need to synchronize the timestamps of the different recordings, because multiple cameras may start recording at different times.

% For instance, a heavy vehicle passing by the building or someone moving through the room.

% Regardless of the method, model validation is done by tracking the deformation and comparing it to the simulated deformations of the model. The marker-based approach benefits from its simplicity but provides few data points. The markerless approach is essentially the converse.

Most methods for performing extrinsic camera calibration rely on finding corresponding points between multiple cameras. A calibration object is an object designed to make it easier to find correspondences. They can be placed in the scene to ensure correspondences can always be found.
In this thesis, we explore and design several different kinds of calibration objects. We will analyze what visual aids are necessary for camera calibration and experimentally validate the calculated calibration in the context of two settings that the tools will commonly be used in. The first setting is a portable cube whose sides are painted black. It is called the Learning Cube~\cite{FredrikMA}. The cameras are usually mounted on flexible arms attached to the frame. The black sides make it easier to segment the soft robot, calibration objects, and markers. In the second setting, the robots and the cameras can be placed anywhere in a potentially cluttered scene.
We call it the "free space setting".

Currently at DIKU, the \lstinline{PySoRo}~\cite{pysoro} library originally developed by~\citet{FredrikMA} is used for calibrating two cameras. The calibration is done as a preprocessing step. This approach works, but if the cameras move the calibration becomes invalid. Our work is ultimately intended as an improvement on the existing software and method.

\section{Document Structure}
In Chapter~\ref{section:cameras} we introduce our choice of depth cameras, the Intel RealSense D415. We explore the various properties of the camera, e.g. its shutter type, bandwidth limitations when recording with multiple cameras, and the noise in its depth data.

In Chapter~\ref{section:time_calibration} we present different methods of performing time synchronization of multiple cameras, including both hardware and software-based approaches. We have implemented a software-based approach, which performs time synchronization based on the detection of camera flashes. For this purpose, we have designed a simple flash-triggering Arduino board, the design of which is also presented in this chapter.

In Chapter~\ref{section:calibration_objects} we discuss different kinds of calibration objects and their advantages and disadvantages. We then introduce our three different calibration objects: checkerboards, colored spheres, and a 3D printed cube with checkerboards on its sides. Finally, we analyze the robustness of each calibration object to occlusion, different lighting conditions, and viewing angles.

In Chapter~\ref{sect:cam_cal_methods} we discuss and compare different algorithms for performing extrinsic camera calibration. We also analyze what visual aids are necessary for camera calibration and discuss the placement of the world coordinate system.

In Chapter~\ref{section:cam_calibration_algo} we present our camera calibration algorithm and experimentally validate it.


Chapter~\ref{section:software} describes parts of the software architecture, the directory structure, and two small tutorials on how to use the command-line programs.

In Chapter~\ref{section:marker_vs_markerless} we discuss the differences between the marker-based and markerless approaches for deformation tracking, as well as their pros and cons. Finally, we envision the next steps for deformation tracking at DIKU.

\textbf{Source Code Repository and Documentation:}\\
Gitlab link: \url{https://gitlab.com/nat91/master-thesis}\\
Documentation on Gitlab pages: \url{https://nat91.gitlab.io/master-thesis}

\section{Definitions}
In this section, we briefly define a few terms that might be unfamiliar to readers that do not have a background in image analysis or have not worked with cameras before.
\begin{itemize}
\item[]\textbf{Feature:} A feature is a point of interest. It can either be in an image or a point in 3D. In an image, it is typically a point with a lot of surrounding texture and structure. In 3D it is typically an area with distinctive geometry.
\item[]\textbf{Descriptor:} A descriptor describes a feature. It is typically a vector of numbers, but it can also be a text label or similar. Descriptors can be matched across images to determine if two feature points are the same point in the world.
\item[]\textbf{Correspondence:}  A correspondence consists of two feature points from different views, either in an image or in 3D, that are the same point in the world. We denote a correspondence between point $\mathbf{x}_i$ and point $\mathbf{x}_j$ with $\mathbf{x}_i \leftrightarrow \mathbf{x}_j$.
\item[]\textbf{Camera Intrinsics:} The camera intrinsics matrix can be used to project a world point onto the image and vice versa, it will be described in detail later in Chapter~\ref{sect:cam_cal_methods}.
\item[]\textbf{Stereo:} Stereo refers to the recovery of depth from two 2D images, using the perceived differences in the positions of objects between the two images.
\item[]\textbf{Baseline:} Each camera has a single point that is its center. The baseline is the line that connects two camera centers. Each camera has its own coordinate system that has its origin in the camera center.
\item[]\textbf{SIFT}: Scale Invariant Feature Transform~\cite{SIFT}. It is a feature detector and descriptor that is invariant to scale, rotation, and partially to illumination changes. It was the state of the art in its field for many years, but it has recently gotten competition from learning approaches~\cite{DBLP:journals/corr/YiTLF16}.
\item[]\textbf{RANSAC}: Random Sample Consensus~\cite{RANSAC} is a method for estimating a given model in the presence of outliers. It iteratively constructs the model from a subset of the data points and classifies the remaining points as inliers or outliers. The model with the largest inlier set is kept.
\end{itemize}

\section{Previous Work}
\subsection*{Time Calibration}
In \citet{proquest2187123788}, a simple method for the synchronization of video streams with a precision better than one millisecond is proposed.
The approach relies on registering matching abrupt changes in lighting intensities,
facilitated by photographic flashes, captured by each camera. Each camera's timestamps of the illumination
events can then be matched by solving an overdetermined system in a least-squares fashion.
The main advantage of the method is that it promises sub-millisecond accuracy and does not require overlapping fields of view from the cameras, as long as each camera can see the flash events.
In terms of disadvantages, abrupt changes in exposure could be interpreted as flash events,
thus producing false positives.
We do not expect this to be a problem in our setting since we are in a controlled environment.

\citet{10.1007/978-3-642-32717-9_27}
rely on a feature-based approach, in which general image features are extracted
from the motion in the recordings, without requiring explicitly tracking any specific object.
Optimal frame offset and framerate estimates are found by identifying and matching feature point trajectories between the recordings.
The main disadvantage of the method is that
it is dependent on sufficient motion being present and visible to all cameras.
Since the intended use of our system is to capture the motion of soft robots,
this is not an unrealistic expectation, but we are hesitant to introduce
requirements to the size or duration of movements of the soft robot.

A screen-based approach was implemented in \cite{dersch2016}.
A smartphone with a 60 frames-per-second OLED screen is used to
display alternating black-and-white images.
Since OLED cameras have a rapid pixel response time, and the pixels of the screen are
updated in a sequential scanning pattern, calibration of the cameras
can be achieved by registering the number of screen pixels that have been
updated according to each camera. The method achieves an accuracy of $\mp$0.5ms,
but is dependant on the screen being in clear view of all cameras, which is
unlikely to be achievable when the angle between cameras is large.

\subsection*{Obtaining Correspondences}
\citet{ieee_s6224570} introduces a checkerboard corner detector that is shown to outcompete traditional corner detectors like Harris detector~\cite{Harris88acombined}. A novel energy function is also introduced, that is used to detect multiple checkerboards in the image, by associating the correct corner points to each checkerboard. This energy function also discards corners that are not a part of any checkerboard.

\citet{FredrikMA} looks into using spheres (fussballs) as calibration objects in his thesis. The centroids are estimated using sphere fitting. Correspondences are established by considering all permutations of the spheres since all the spheres have the same color and are not otherwise distinguishable.

\citet{HuangLin2019Romc} proposes a three-dimensional cube calibration object. The cube is of known dimensions and has a checkerboard pattern on each side. It is defined to be the basis of the world coordinate system. Since the different sides of the cube are part of the object that defines the unified world coordinate system, the world coordinates of each side can be deduced by each camera. Using the checkerboards as correspondences, Zhang's method \cite{zhang2000} can be used to find the transformation to the world coordinate system for each camera.

\citet{SIFT} introduces the Scale Invariant Feature Transform (SIFT), which is both a feature detector and descriptor. The descriptor is shown to be invariant to scale, rotation, and lighting conditions. It consists of gradient orientation histograms appended to each other.

\citet{DBLP:journals/corr/YiTLF16} introduces the Learned Invariant Feature Transform (LIFT). Like SIFT, it is also a feature detector and descriptor, with the major difference that the feature detection and the descriptors are learned rather than engineered. The results in the paper show it to outperform SIFT.

% \citet{ZhouKun2020RoSM} reviews deep learning approaches for solving the stereo matching problem, considering the entire pipeline from finding correspondences to recovering depth.
\subsection*{Camera Calibration and Point Cloud Alignment}
% ICP, Fast Global Registration, Epipolar geometry, Umeyama's algorithm

Iterative Closest Point (ICP) is an algorithm that iteratively finds the transformation between two point clouds~\cite{ICP_P2Point}.
It does this by repeatedly finding geometric correspondences in the point clouds, and finding the transformation that minimizes an error function based on the distance between correspondences. Different versions exist: Point-to-point only considers point correspondences, while point-to-plane~\cite{ICP_P2Plane} and Colored ICP~\cite{ICP_colored} incorporate surface normals and color information respectively.

\citet{Hartley2000} reviews the different geometries that describe multi-camera setups. All of these methods work solely by finding 2D correspondences. From the correspondences, the geometry can then be estimated.

\citet{FredrikMA} does camera calibration using the centroids of fussballs as correspondences, and uses Arun's algorithm~\cite{Arun} to find the rigid transformation between two camera coordinate systems.

\citet{UmeyamaS1991Leot} introduces a closed-form solution for finding the 3D rigid transformation between two point clouds, given that corresponding points between them are known. Unlike previous methods, it is shown that a proper rotation is always returned.

\citet{FGR} introduces the Fast Global Registration method of aligning point clouds from different coordinate systems.
This method downsamples the point clouds and finds geometric correspondences in each point cloud. It then identifies the transformation between the point clouds that minimizes an objective function, based on the distance between correspondences.

% Neural networks for performing camera calibration, for camera calibration refer to the existing methods


