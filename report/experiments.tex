To validate the quality of our calibration objects and algorithm, we performed a series of quantitative and qualitative experiments. We have simulated both the learning cube environment and the "free space" setting in our experiments. Since we are also interested in the performance of the calibration objects, SIFT is not used unless otherwise stated.

\begin{itemize}
  \item In Section \ref{section:white}, we attempt to coarsely validate the accuracy of the estimated transformation between two cameras.

  %\item In Section \ref{section:leave} we repeatedly leave one calibration object out and calibrate a transformation with the remaining calibration objects. We then use the left out calibration object to measure the alignment error.

  \item In Section \ref{section:span}, we investigate the quality of the alignment outside of the volume spanned by the correspondences used to estimate the transformation.

  \item In Section \ref{section:distance} we investigate how the distance to the calibration objects affects the alignment quality.

  \item In Section \ref{section:human} we calculate an initial calibration under two different lighting conditions and then we move one of the cameras and see whether a new calibration can be obtained or not. The purpose is to verify the robustness of the algorithm to different lighting conditions and the cameras being moved.

  \item In Section \ref{section:multi}, we perform a full $360^\circ$ reconstruction of a 3D printed Stanford bunny using six camera angles. We evaluate the reconstruction by aligning the reconstructed point clouds to the point cloud of the mesh we 3D printed the bunny from.

  \item In Section \ref{section:videoStream} we calibrate an entire video stream where the scene is static, and one camera is moving, to see how the quality holds up over time and during movement.

  \item Finally, in Section~\ref{sect:exp_tracking_known_positions} we measure the accuracy with which we can track a known position in the world, when the camera is moving.

  %\item Finally, in Section~\ref{sect:dyn_scene} we calibrate an entire video stream where the scene is dynamic. This also serves as an integration test of our time calibration and per frame calibration.
\end{itemize}
One problem with validating the point cloud alignment is that without manual verification, a robust method does not seem to exist~\cite[p.~1]{AlmqvistHakan2018Ltdm}. There exist alignment benchmarks with a ground truth, e.g. the UWA benchmark~\cite{MianA.S2006TMOR}, but those benchmarks do not include calibration objects and we can therefore not use them directly. Therefore, we find it necessary
to perform a qualitative description of the quality of our point cloud alignment,
as a supplement to our quantitative experiments. An alignment is qualitatively good if there are no visible misalignments.

\subsection{Error measures}
For the quantitative experiments, we rely on the root mean squared error~\eqref{eq:exp_rmse}, which is the distance in meters from where the reference camera expects an object's feature point to be, and where the other cameras consider the feature point to be. The root mean squared error is the average correspondence error measured in meters, and thus serves as a general measure of alignment quality.
\begin{equation}\label{eq:exp_rmse}
RMSE = \sqrt{\frac{1}{N}\sum_{i=0}^{N-1}\lVert \mathbf{T}\mathbf{p}_i - \mathbf{q}_i} \rVert^2,\ \mathbf{p}_i \leftrightarrow \mathbf{q}_i
\end{equation}

% For the quantitative experiments, we predominantly rely on the root mean squared error, the variance of the error and the maximum distance between the reference and the aligned correspondences. The root mean squared error is the average correspondences error measured in meters, and thus serves as a general measure of alignment quality. The variance is included since we would prefer low variability, since a low variance means higher predictability in the measure. Last the maximum error distance is included, since we wish to estimate the worst case error as well. For all measures, lower is better. No failed alignments have been included, since they were not observed, but naturally the error measures would be significantly worse in the case of global misalignment.

\subsection{Coarse Quantitative Verification}\label{section:white}

\textbf{Introduction:}\\
To coarsely verify the camera transformations estimated by our algorithm,
we devised a simple sheet of paper with angles and distances drawn upon it,
as shown in Figure \ref{fig:whiteSheet_ry20}. High-resolution images of all the camera setups used for the experiments on the sheet can be seen in Appendix~\ref{app:coarse_ver}. We would expect the calculated transformations to closely match the real-world ones. They may not match perfectly, because of noise in the depth data.

\textbf{Procedure:}\\
For the translations we placed the left side of each camera on one of the 5cm marks using a ruler, thus making the placement of the cameras as accurate as possible. For the rotations, we placed the reference camera such that the center of its RGB camera was roughly aligned with the origin. That is because the center of the RGB camera is the origin of the camera world coordinate system, that the rotation is about. The other camera was placed along one of the lines with a known angle. To test the translation along another axis, we placed one camera on top of a 3D printed cylinder with a height of $7$cm. These placements should allow us to get a coarse estimate of the real-world transformation between the cameras, and compare them to the transformations output by the calibration algorithm.
\begin{figure}[H]
  \centering
  \includegraphics[width=\textwidth]{images/whitesheet_ry20.jpg}
  \caption{The second camera placed at an angle of $20$ degrees. Each translation mark is 5cm apart, the sheet contains lines for a $20$, $30$, and $45$ degree angle.}
  \label{fig:whiteSheet_ry20}
\end{figure}

\textbf{Results:}\\
Table \ref{tab:whiteSheet} shows the results of the experiment.
The results are encouraging, the maximum deviation in the translations is $1$cm and $1.5$ degrees in the angles. Especially considering that there are significant sources of inaccuracies.
Firstly, noise in the depth data is likely to cause the estimated transformation to deviate slightly from the real world one. Secondly, it is hard to place the cameras precisely without holding them down, as the tension in the USB cable tends to move them otherwise. It would be good to have a rig where rotation and translation can be controlled more precisely or access to an accurate inertial measurement unit.

\begin{table}[H]
  \centering
  \begin{tabular}{|l|l|l|l|l|l|l|l|}
    \hline
    Scenario & rx  & ry  & rz & tx & ty & tz & scale \\
    \Xhline{2\arrayrulewidth}
    tx -0.25m  & -3.52 & 0.09 & -0.67 & \underline{-0.26} & 0.02 & 0.01 & 0.99 \\ \hline
    tx -0.15m  & -5.65 & 1.04 & 0.03 & \underline{-0.15} & -0.01 & 0.00 & 1.00 \\ \hline
    ty 0.07m  & 0.43 & -1.43 & -0.69 & 0 & \underline{0.068} & 0.017 & 1.00 \\ \hline
    ry $20\deg$      & -8.91 & \underline{20.53} & -2.98 & -0.31 & 0.00 & 0.01 & 1.00 \\ \hline
    ry $30\deg$      & 8.89 & \underline{31.54} & -4.55 & -0.33 & -0.01 & -0.00 & 1.00 \\ \hline
    ry $45\deg$      & 1.60 & \underline{44.77} & -1.87 & -0.28 & 0.03 & -0.00 & 1.01 \\ \hline
  \end{tabular}
  \caption{Results for the coarse validation experiment. All distances are in meters and all angles are in degrees. Each row shows the resulting transformation components, between the two cameras, in the given scenario. The first three columns are the rotations about each axis, the next three are the translations and the last the relative scale between the two coordinate systems. The figure of interest in each scenario is underlined. The maximum deviation from the expected is $1$cm and $1.5$ degrees in the angles.}
  \label{tab:whiteSheet}
\end{table}

\subsection{Robustness to Volume Span}\label{section:span}
\textbf{Introduction:}\\
A concern when aligning the point clouds of different cameras,
is that the point cloud might only be well-aligned in the space spanned by the
correspondences used for finding the transformation.

\textbf{Procedure:}\\
We set up several checkerboards and spheres spanning a volume,
and placed a green sphere within the volume as seen in Figure \ref{fig:span}. We calibrated the cameras and noted the error as measured using the green reference sphere. We then calibrated the cameras two more times, each time moving a sphere to the opposite side, creating less volume span. We then checked if the alignment of the correspondence point provided by the green sphere got worse. The green sphere was not used for calculating the calibration. Figure~\ref{fig:nospan} shows the final setup, where both the yellow and red sphere have been moved such that the calibration objects do not span the volume the green sphere is located in.

\begin{figure}[H]
  \begin{subfigure}[t]{0.48\textwidth}
    \centering
    \includegraphics[width=0.85\textwidth]{images/span.jpg}
    \caption{Initial setup. The volume around the green sphere is spanned by the calibration objects.}
    \label{fig:span}
  \end{subfigure}
  \hspace{0.1cm}
  \begin{subfigure}[t]{0.48\textwidth}
    \centering
    \includegraphics[width=0.85\textwidth]{images/nospan.jpg}
    \caption{Final setup. The volume around the green sphere is not spanned by calibration objects.}
    \label{fig:nospan}
  \end{subfigure}
   \caption{Setups for volume span experiment. Green sphere used as reference to calculate the error.}
\end{figure}

\textbf{Results:}\\
Table \ref{tab:span} shows the root mean squared error for the reference case where the green sphere is in the volume spanned by the calibration objects and the cases where the green sphere is not in the spanned volume, due to the calibration spheres being moved.

\begin{table}[H]
  \centering
  \begin{tabular}{|l|l|}
    \hline
    Scenario  & error (cm)\\
    \Xhline{2\arrayrulewidth}
    Span & 0.25 \\
    \hline
    Red sphere moved      & 0.40 \\
    \hline
    Yellow sphere moved         & 0.19 \\
    \hline
    \end{tabular}
  \caption{Alignment error as measured on the green sphere, when the calibration objects span different areas of the volume. There is no pattern indicating that volume span matters for our method.}
  \label{tab:span}
\end{table}
The initial error is $0.25$cm and it then increases to $0.4$cm when the red sphere is moved and finally it decreases again to $0.19$cm when the yellow sphere is moved and the volume is not spanned by the calibration objects. The errors are small throughout the experiment, and might as well be caused by noise, and based on that we conclude that volume span does not seem to matter.

\subsection{The Effect of the Distance to Correspondences}\label{section:distance}
\textbf{Introduction:}\\
We wish to estimate how sensitive the calibration quality is to the distance to the correspondences.
We assume that the quality of the between-camera transform depends on the distance to the calibration objects, since areas further away from the camera exhibit more noise.

\textbf{Procedure:}\\
We perform the calibration process twice, once roughly 50cm from the calibration objects, and once roughly 1m away. We did $3$ trials for each case. We used a measuring tape to measure the distance to the camera.
Our setup was as shown in Figure \ref{fig:distance}. We vary the distance of the cameras to the scene, as measured from the cameras to the red sphere. The red sphere is used as a control object and is not used for calibration.
  \begin{figure}[H]
    \centering
    \includegraphics[width=0.65\textwidth]{images/distance.jpg}
    \caption{Setup for distance experiment. The cameras were moved to be approximately 50cm or 100cm to the red sphere.}
    \label{fig:distance}
  \end{figure}
\textbf{Results:}\\
As seen in Table \ref{tab:distance}, there appears to be
a significant tendency for the error to increase as the distance increases. When the distance is roughly 50cm, the error is around 1.5mm,
but when the distance is 1m, the error is around 4.5mm, corresponding to a threefold increase in error. This emphasizes the importance of having the volume of interest as close to the cameras as possible.
\begin{table}[H]
  \centering
  \begin{tabular}{|l|l|} \hline
    Scenario & error (cm) \\
    \Xhline{2\arrayrulewidth}
    Trial 1 (0.5m)    & 0.17 \\ \hline
    Trial 2 (0.5m)    & 0.18 \\ \hline
    Trial 3 (0.5m)    & 0.13 \\ \hline
    Trial 1 (1m)      & 0.41 \\ \hline
    Trial 2 (1m)      & 0.45 \\ \hline
    Trial 3 (1m)      & 0.48 \\ \hline
    \end{tabular}
  \caption{Distance to correspondences. The left column is the distance to the scene in meters and the right column is the resulting alignment error in centimeters of the red control sphere. The error increases with distance.}
  \label{tab:distance}
\end{table}

% \subsection{The Effect of the Number of Correspondences}\label{section:numOfCor}
% \textbf{Introduction:}\\
% We wish to estimate how sensitive the calibration quality is to the number of correspondences
% used to align the point clouds between cameras. Because of different noise levels, we expect that using more correspondences will help to fit the calibration better to the different noise, which in turn gives a calibration that on average produces a better alignment in the volume of interest. Furthermore, more correspondences makes it easier to identify and remove outliers, assuming the majority of the points are inliers.

% \textbf{Procedure:}\\
% We vary the number of correspondences used for calibration, by removing one calibration object a time
% and measuring the error using the red reference sphere as in the previous experiments.
% Our setup is shown in Figure \ref{fig:numOfCor}, and again we used the red sphere as a validation object.
%   \begin{figure}[H]
%     \centering
%     \includegraphics[width=0.65\textwidth]{images/numOfCor.jpg}
%     \caption{Setup for testing the effect of the number of correspondences. The numbered checkerboards indicate the ones used. One checkerboard was removed with each iteration, from highest numbered to lowest.}
%     \label{fig:numOfCor}
%   \end{figure}
% \textbf{Results:}\\
% As seen from Table \ref{tab:numCors}, there appears to be
% a tendency for the error to increase as the number of correspondences falls.
% The effect is not exhibited while the number of correspondences is above 48,
% but once the number falls to 32 and 16, we see an error roughly 10 and 20 times the previously observed error rate.
% \begin{table}[H]
%   \centering
%   \begin{tabular}{|l|l|}
%     \hline
%     $n$ & error (cm) \\
%     \Xhline{2\arrayrulewidth}
%     80  & 0.15 \\ \hline
%     64  & 0.17 \\ \hline
%     48  & 0.13 \\ \hline
%     32  & 1.03 \\ \hline
%     16  & 2.03 \\ \hline
%     \end{tabular}
%   \caption{Error as a function of the number of correspondences, measured as the alignment error of the red control sphere. We denote the number of correspondences by $n$. We remove a checkerboard between each row in the Table.}
%   \label{tab:numCors}
% \end{table}

% It should be noted however, that the distance to the correspondences is not accounted for, and as we saw in Section \ref{section:distance},
% the distance will also increase the error.

\subsection{Robustness to Human Intervention and Different Lighting Conditions}\label{section:human}
\textbf{Introduction:}\\
The purpose of this experiment is to determine our calibration algorithm's robustness to human intervention, in particular, the camera being moved. We perform the experiment using the same scene but under two different lighting conditions. The scene is static. This experiment is of interest because it can show the algorithms ability to recalibrate in the event the camera is moved, this can happen on purpose or by accident. E.g. at conventions an attendee may accidentally cause the cameras to move.

\textbf{Procedure:}\\
In the first experiment, the curtains in the room are open and sunlight enters the room. In the second the curtains are drawn. We calculated an initial alignment in each setup, then we moved one of the cameras without recalibrating. Finally, we recalibrated at the new position to determine if a new calibration could be obtained. No artificial lighting was used in either experiment. We used checkerboards, spheres, and SIFT in both experiments.

\textbf{Results:}\\
The initial setup with and without the curtains drawn can be seen in Figure~\ref{fig:dataset_nolight_initial} and~\ref{fig:dataset_light_initial}. The initial alignment looks good in both instances. Counterintuitively, the image where the curtains are drawn appears lighter than the one where they are not. This is due to the auto exposure.

After moving the cameras a new alignment is needed, the unaligned point clouds for both settings can be seen in Figure~\ref{fig:dataset_light_moved} and~\ref{fig:dataset_nolight_moved}. Finally, the scenes after recalibration can be seen in Figure~\ref{fig:dataset_light_final} and~\ref{fig:dataset_nolight_final}. The point clouds have been realigned.

\begin{figure}[H]
  \begin{subfigure}[t]{0.48\textwidth}
    \centering
    \includegraphics[width=\textwidth]{images/human_perturbation_light_1_1.png}
    \caption{The initial calibrated setting with the curtains open.}
    \label{fig:dataset_light_initial}
  \end{subfigure}
  \hspace{0.1cm}
  \begin{subfigure}[t]{0.48\textwidth}
    \centering
    \includegraphics[width=\textwidth]{images/human_perturbation_1_2.png}
    \caption{The initial calibrated setting with the curtains drawn.}
    \label{fig:dataset_nolight_initial}
  \end{subfigure}
  \caption{The initial settings and point cloud alignments under both lighting conditions. The estimated camera poses are shown. Due to the auto-exposure, it is hard to perceive a difference in the lighting conditions. Drawing the curtains caused a shadow to fall on the area.}
  \label{fig:dataset_static_initial}
\end{figure}

\begin{figure}[H]
  \begin{subfigure}[t]{0.48\textwidth}
    \centering
    \includegraphics[width=\textwidth]{images/human_perturbation_light_2_1.png}
    \caption{The setting with the curtains open after the camera was moved.}
    \label{fig:dataset_light_moved}
  \end{subfigure}
  \hspace{0.1cm}
  \begin{subfigure}[t]{0.48\textwidth}
    \centering
    \includegraphics[width=\textwidth]{images/human_perturbation_2_1.png}
    \caption{The setting with the curtains drawn after the camera was moved.}
    \label{fig:dataset_nolight_moved}
  \end{subfigure}
  \caption{After the cameras are moved, the point clouds are misaligned and recalibration is necessary.}
  \label{fig:dataset_static_moved}
\end{figure}

\begin{figure}[H]
  \begin{subfigure}[t]{0.48\textwidth}
    \centering
    \includegraphics[width=\textwidth]{images/human_perturbation_light_2_2.png}
    \caption{The setting with the curtains open after recalibration.}
    \label{fig:dataset_light_final}
  \end{subfigure}
  \hspace{0.1cm}
  \begin{subfigure}[t]{0.48\textwidth}
    \centering
    \includegraphics[width=\textwidth]{images/human_perturbation_2_3.png}
    \caption{The setting with the curtains drawn after recalibration.}
    \label{fig:dataset_nolight_final}
  \end{subfigure}
  \caption{After the recalibration is done, the alignment is recovered in both settings.}
  \label{fig:dataset_static_final}
\end{figure}

\subsection{Using Multiple Cameras: A Complete Reconstruction}\label{section:multi}
\textbf{Introduction:}\\
Up until now, we have focused on the two-camera case, but such setups are
unlikely to be sufficient if complete 360$^\circ$ reconstruction is desired.

\textbf{Procedure:}\\
To achieve full reconstruction, we relied on seven camera angles, placed at intervals of approximately
45$^\circ$.
Present in the scene was the Stanford bunny, the calibration spheres
and the checkerboards. We used one camera to capture all angles, simply because we do not have enough cameras to do a full reconstruction. After aligning all the point clouds, we manually extracted the area the bunny was located in. To measure the quality of the reconstruction, we aligned the extracted bunny (see Figure~\ref{fig:bunnyIso}) with the point cloud of the mesh, the bunny was 3D printed from.

\begin{figure}[H]
  \centering
  \begin{subfigure}[t]{.48\textwidth}
    \centering
    \includegraphics[width=\textwidth]{images/bunnyFront.png}
    \caption{Frontal view.}
    \label{fig:fullReconstruction}
  \end{subfigure}
  \hspace{0.1cm}
  \begin{subfigure}[t]{.48\textwidth}
    \centering
    \includegraphics[width=\textwidth]{images/BunnyBack.png}
    \caption{Seen from behind.}
    \label{fig:fullReconstructionBack}
  \end{subfigure}
  \caption{Full reconstruction using six cameras, as seen from different angles. The estimated locations of the camera frustums are plotted. Visually, the alignment looks good.}
\end{figure}

\begin{figure}[H]
  \centering
  \includegraphics[width=0.75\textwidth]{images/bunnyCloud.png}
  \caption{Isolated bunny point cloud. The shape of the bunny is clearly visible, but the point cloud is noisy.}
  \label{fig:bunnyIso}
\end{figure}

\textbf{Results:}\\
Figure~\ref{fig:fullReconstruction} and~\ref{fig:fullReconstructionBack} shows the aligned point clouds and the estimated camera poses. Calibration failed for one camera angle, bringing us down to six views.
The point cloud alignment looks good.

Figure ~\ref{fig:bunnyOver} and~\ref{fig:bunnyIsoOverlaid} shows the alignment of the extracted bunny point cloud with that of the point cloud of the mesh. We see that alignment is good since the reconstructed point cloud fits the original model well.
We do, however, see that noise in the depth data is an issue, particularly
around the ears.

The average distance between the closest points of each point cloud is 1.5mm,
which is also an indication that good alignment has been achieved, especially considering
the amount of noise. We calculated the Hausdorff distance between the two sets and obtained a value of 14mm. This says that the longest distance between a point in one set, and the closest point in the other set, is 14mm. This is likely the distance to one of the noisy data points around the ears.

\begin{figure}[H]
  \centering
  \begin{subfigure}[t]{.45\textwidth}
    \centering
    \includegraphics[width=\textwidth]{images/bunnyAlign.png}
    \caption{View from above the bunny. Good alignment appears to have been achieved.}
    \label{fig:bunnyOver}
  \end{subfigure}
  \hspace{0.1cm}
  \begin{subfigure}[t]{.48\textwidth}
    \centering
    \includegraphics[width=\textwidth]{images/bunnyAlignAlt.png}
    \caption{View from the front of the bunny. We see some noise between and around the ears. This is attributed to the discontinuities.}
    \label{fig:bunnyIsoOverlaid}
  \end{subfigure}
  \caption{The blue points are the bunny point cloud isolated from the camera point clouds. The red points are the target point cloud, extracted from the 3D mesh used to print the bunny. The overall alignment looks good, but some noise is visible.}
\end{figure}

\subsection{Analyzing an Entire Video Stream: Static Scene}\label{section:videoStream}
\textbf{Introduction:}\\
In this section, we try to calibrate an entire video stream, where one camera is moving and another is static. Its purpose is to test whether we can do dynamic per frame calibration. It also tests robustness towards human intervention, namely one camera being moved to a new position during a recording.

\textbf{Procedure:}\\
We perform the experiment on a static scene, two checkerboards are present and a book and the RealSense camera packaging have been added to add texture for the SIFT detector. The camera was lifted and moved to the right. Two separate recordings from the same camera were used for this static scene.

\textbf{Results:}\\
Frame 1 can be seen in Figure~\ref{fig:static_scene_1}, the cameras are stacked on top of each other. This is as expected as the second recording started from the position the first recording was taken from. The scene appears dark, this is because the camera auto-exposure has not had time to settle yet. In our experience, it typically happens within 10-30 frames. The image is too dark for colors to be recognized and as a result, it is only the SIFT detector that returned correspondences for this frame. This emphasizes the importance of using multiple detectors for robustness.

The calibration failed for one frame, it can be seen in Figure~\ref{fig:static_scene_222}, there was heavy motion blur in the right camera image. This likely caused false correspondences to be identified and hence the result. The calibration was regained in the following frame. Figure~\ref{fig:static228} shows the calibration of the final frame. The alignment looks good and the camera has moved up and to the right as expected.

It is hard to present the results of an entire video as images only, therefore we have made videos of the entire calibration with the above dataset, one where we calibrate every frame and another where we calibrate every 5 frames. The videos are played back at 5 FPS such that it is easier to see the movement of the cameras between frames.
\begin{itemize}
  \item Videos from three different angles with recalibration every 5 frames. Note that each frame is calibrated with the closest available calibration in time:
  \begin{itemize}
    \item \url{https://www.youtube.com/watch?v=PlnJwe600qc}
    \item \url{https://www.youtube.com/watch?v=OGcqOvYCt9E}
    \item \url{https://www.youtube.com/watch?v=zVCI7tyGe5o}
  \end{itemize}
  \item Videos from three different angles with recalibration done every frame:
  \begin{itemize}
    \item \url{https://www.youtube.com/watch?v=jTFhknCBAiE}
    \item \url{https://www.youtube.com/watch?v=ZQCggHne-IA}
    \item \url{https://www.youtube.com/watch?v=g3b_DrDIV7c}
  \end{itemize}
\end{itemize}
In the videos where we recalibrate every 5 frames, the point clouds start moving apart in between the calibrations and then they realign. This is as expected as the right camera is moving in between the recalibrations. In the videos where we recalibrate every frame, some jitter can be seen, especially in the beginning. However, the important takeaway is that alignment looks good at all times. Thus, the jitter is likely caused by it being moved by a human hand.

\begin{figure}[H]
  \centering
  \begin{subfigure}[t]{.48\textwidth}
    \centering
    \includegraphics[width=\textwidth]{images/static_scene_1.png}
    \caption{Frame 1 of the recording. The auto-exposure has not settled yet and the scene appears dark. The cameras are on top of each other.}
    \label{fig:static_scene_1}
  \end{subfigure}
  \hspace{0.1cm}
  \begin{subfigure}[t]{.48\textwidth}
    \centering
    \includegraphics[width=\textwidth]{images/static_scene_20.png}
    \caption{Frame 20 of the recording. The auto-exposure has begun to settle, the cameras are still roughly on top of each other.}
    \label{fig:static_scene_20}
  \end{subfigure}
  \caption{Frame 1 and 20 of the estimated camera poses and aligned point clouds. The transition between the point clouds can be seen from the color differences.}
  \label{fig:static_scene_res_1}
\end{figure}

\begin{figure}[H]
  \centering
  \begin{subfigure}[t]{.48\textwidth}
    \centering
    \includegraphics[width=\textwidth]{images/static_scene_222.png}
    \caption{Frame 222, the estimated camera pose, and the point cloud alignment are off.}
    \label{fig:static_scene_222}
  \end{subfigure}
  \hspace{0.1cm}
  \begin{subfigure}[t]{.48\textwidth}
    \centering
    \includegraphics[width=\textwidth]{images/static_scene_228.png}
    \caption{Frame 228, the final frame of the recording. The alignment looks good.}
    \label{fig:static228}
  \end{subfigure}
  \caption{Frame 222 and 228 of the estimated camera poses and aligned point clouds. The transition between the point clouds can be seen from the color differences. A misalignment in frame 222 can be seen, this was the only major misalignment in the video stream. It is difficult to see, but there is significant motion blur in one of the images, which has likely caused false correspondences to be established. Frame 228 seen on the right is the final common frame of the video streams.}
\end{figure}

\subsection{Tracking a Known Position in the World}\label{sect:exp_tracking_known_positions}
\textbf{Introduction:}\\
The purpose of this experiment is to estimate how well the algorithm can track a known position in the world coordinate system when the camera is moved. This known position could for instance be a marker on a soft robot.

\textbf{Procedure:}\\
We placed four spheres and two checkerboards in the scene. Subsequently, we captured the scene from five different camera positions. The aligned point clouds and the estimated camera positions can be seen in Figure~\ref{fig:known}.
The blue sphere centroid was used as the known position, while the remaining calibration objects were used for calibration.
Using the first camera position as the world coordinate system, we found the remaining camera positions with our calibration algorithm.

We then detected the blue sphere in each image and transformed it to the coordinate system of the first camera, using the calibration. We then calculated the distance between the estimated position of the blue centroid, relative to the reference position.
Since the blue sphere has not moved between images, it should have the same world coordinate for all camera positions.

\begin{figure}[H]
  \centering
  \begin{subfigure}[t]{.48\textwidth}
    \centering
    \includegraphics[width=\textwidth]{images/known.png}
    \caption{The scene seen from the front.}
    \label{fig:known1}
  \end{subfigure}
  \hspace{0.1cm}
  \begin{subfigure}[t]{.48\textwidth}
    \centering
    \includegraphics[width=\textwidth]{images/known_2.png}
    \caption{The scene seen from above.}
    \label{fig:known2}
  \end{subfigure}
  \caption{Setup for testing the stability of known positions. The centroid of the blue sphere was used as the known position and was not used for calibration.}
  \label{fig:known}
\end{figure}



\textbf{Results:}\\
Table \ref{tab:known} shows the position of the blue centroid as estimated from each camera position, as well as the error, measured as the distance to the reference position.
We observe a maximum error of $4.4$mm and a mean error of $2.7$mm.
Given that the data is noisy, we consider this an acceptable tracking error.

\begin{table}[H]
  \centering
  \begin{tabular}{|l|l|l|}
    \hline
    Camera postion & Centroid position & Error to reference (mm) \\
    \Xhline{2\arrayrulewidth}
    0 (reference)  & (0.053, -0.091, 0.556) & - \\ \hline
    1  & (0.057, -0.094, 0.555) & 4.4 \\ \hline
    2  & (0.057, -0.0938, 0.556) & 4.0 \\ \hline
    3  &  (0.053, -0.092, 0.556] & 0.7 \\ \hline
    4  & (0.052, -0.092, 0.556) & 1.6 \\ \hline
    \end{tabular}
  \caption{Results from the experiment testing the stability of known positions. Mean error is 2.7mm with a variance of 2.3mm.}
  \label{tab:known}
\end{table}

