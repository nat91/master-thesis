
In this chapter, we present our camera calibration algorithm and the experiments we have conducted to validate it. In section~\ref{sect:ccal_algo}, based on the discussions in Chapter~\ref{sect:cam_cal_methods} we design and implement a camera calibration algorithm that uses our RANSAC-based implementation of Umeyama's algorithm to find the transformation between each pair of cameras. We then use these to construct a transformation graph between the cameras. Finally, in section~\ref{sect:cal_experiments} we experimentally validate the algorithm.

\section{The Camera Calibration Algorithm}\label{sect:ccal_algo}
In this section we propose an implementation of a multi-camera calibration algorithm, that uses our calibration objects described in Chapter~\ref{section:calibration_objects} and a RANSAC implementation of Umeyama's algorithm. Umeyama's algorithm is attractive to use because of its relative simplicity, compared to the methods reviewed in the previous chapter, and because it provides a closed-form solution. It is also a solution unique to depth cameras as it requires depth data. The steps of the algorithm are as follows:
\begin{enumerate}
  \item Find features in each camera frame.
  \item Find the feature overlap between each camera.
  \item Calculate transformations between cameras with overlapping views.
  \item Find a series of transformations that lead from each camera coordinate system to the world coordinate system and compose them into one.
  \end{enumerate}
The complete algorithm can be seen in Algorithm~\ref{alg:cal_algo}. As previously mentioned, we have the world coordinate system in a camera, but we also propose how the cube world coordinate system, described in the previous chapter, might be incorporated. In the following sections, we briefly describe the details of our RANSAC implementation of Umeyama's algorithm and the construction of the transformation from each camera to the world coordinate system.

\begin{algorithm}[H]
  \caption{Multi RGBD Camera Calibration Algorithm}\label{alg:cal_algo}
  \hspace*{\algorithmicindent} \textbf{Input:} List of cameras and list of frames from each camera. \\
  \hspace*{\algorithmicindent} \textbf{Output:} List of transformations from each camera to the world coordinate system.
  \begin{enumerate}
  \item For each camera frame, detect features with the configured detectors. The individual feature detectors can be disabled via the configuration file, they are by default enabled.
  \item Calculate the overlap matrix $\mathbf{O}$, where $\mathbf{O}_{ij}$ is the number of features common to camera $i$ and $j$. It is calculated by matching descriptors.
  \item Calculate transformations between cameras with \emph{sufficient} overlap, using our RANSAC implementation of Umeyama's algorithm. Sufficient overlap means that $\mathbf{O}_{ij} \geq \tau \geq 4$, where $\tau$ is $4$ by default and configurable in the configuration file. It must be larger than $4$ for Umeyama's algorithm to work.
    \begin{itemize}
      \item For cameras $(i,j)$ if a transformation is already calculated for $(j,i)$, its inverse is used instead of running Umeyama's algorithm again.
    \end{itemize}
  \item Construct a transformation digraph $\mathbf{G}$ where an edge $(i,j)$ indicates there is a transformation from camera $i$ to camera $j$. If $(i,j)$ is in the graph then so is $(j,i)$. Each edge has a transformation attached to it. The weight of each edge is $\lvert D \rvert - \mathbf{O}_{ij}$, where $\lvert D \rvert$ is the total number of unique features across all views. This gives views with more overlap a lower weight, which makes shortest path algorithms favor them.
  \item For each camera, construct a transformation matrix that transforms from the camera coordinate system to the world coordinate system.
    \begin{itemize}
      \item If a camera is the world coordinate system: Run shortest paths on $\mathbf{G}$ to find the shortest path from each camera to the world camera. Use the transformations along the edges on the shortest path to make each transformation.
      \item If a cube is used as the world coordinate system: Identify the cameras that can see most of the world cube and calculate transformations from those to the world coordinate system using Umeyama's algorithm. Construct the transformations the same way as in the above.
    \end{itemize}
  \end{enumerate}
\end{algorithm}

\subsection{RANSAC Implementation of Umeyama's Algorithm}
We have implemented a RANSAC~\cite{RANSAC} version of Umeyama's algorithm. RANSAC stands for Random Sample Consensus and is a method for removing outliers from a dataset. Removing outliers from the set of correspondences is necessary due to the camera noise, as a few noisy correspondences can have an adverse effect on the resulting calibration. Furthermore, the SIFT detector may sometimes establish false correspondences that RANSAC can also remove.

In each iteration RANSAC randomly samples a set $\mathcal{S}$, where $\lvert \mathcal{S}\rvert \geq 4$, from a set of correspondences $\mathcal{C}$ and uses Umeyama's algorithm to calculate a transformation on those. The remainder of the correspondences are then classified as inliers if:
\begin{equation}
\lVert \mathbf{T}\mathbf{p}_i - \mathbf{q}_i \rVert^2 \leq \tau,\ (\mathbf{p}_i, \mathbf{q}_i) \in \left(\mathcal{C}\setminus \mathcal{S}\right),\ \tau \in \mathbb{R}_+
\end{equation}
If this inlier set is larger than a previously seen one, it is kept and the outlier fraction $\epsilon$ is re-estimated. The outlier fraction $\epsilon$ is the estimated fraction of the data points that are outliers. The initial outlier fraction $\epsilon_0$ is set to $0.5$ because that is, in theory, the maximum fraction of outliers RANSAC can handle and still terminate with a correct result. The number of iterations RANSAC has to run to gain confidence $c$ in the resulting inlier set is:
\begin{equation}
N = \frac{\log(1-c)}{\log(1-(1-\epsilon)^{\lvert S \rvert})}
\end{equation}
where $N$ is updated when $\epsilon$ changes. We use $\lvert S \rvert = 4$ as that is the minimum amount of correspondences Umeyama's algorithm needs and $c = 0.99$. The larger $\lvert S \rvert$ is and the closer to $1$ $c$ is, the larger the number of iterations needed. The inlier acceptance threshold $\tau$ is configurable, and we found a good default value to be $10^{-4}$. In practice, when our calibration objects are used RANSAC terminates in less than $3$ iterations and often instantly. When the SIFT detector is used, false correspondences can occur and RANSAC will sometimes use a few more iterations. Note that due to the noise increasing with the distance from the camera, if many correspondences are found in the noisy area these may dominate and cause RANSAC to choose those as inliers. Therefore, the far clipping plane should be adjusted such that there is only depth for the volume of interest, which should be placed close to the cameras. The far clipping plane can be set in the configuration file. After RANSAC terminates Umeyama's algorithm is run on the largest inlier set found and the resulting transformation is returned.

\subsection{Finding the Transformations to the World Coordinate System}
As described in Algorithm~\ref{alg:cal_algo} we construct the transformations from each camera to the world coordinate system by composing a series of transformations. This allows transformations to be found between cameras that do not share enough correspondences, by successively jumping between nearby camera coordinate systems until the world coordinate system is reached. There may be multiple ways to construct this transformation, so to reason about which one is "best", we construct a graph $G(V, E)$.
Each camera is represented in the graph with a vertex $V_i$, and there is an edge between two vertices $(V_i,V_j)$ if there is a transformation between camera $V_i$ and $V_j$. We have that $(V_i,V_j) \in E \Leftrightarrow (V_j,V_i) \in E$.
The weight of each edge is $\lvert D \rvert - \mathbf{O}_{ij}$, where $\lvert D \rvert$ is the total number of unique features across all views and $\mathbf{O}_{ij}$ is the number of features matched between camera $V_i$ and $V_j$. Now if a camera $V_k$ is the world coordinate system or it can see the world coordinate cube, we calculate the shortest path from camera $V_i$ to $V_k$ in $G$ and construct the transformation by composing the transformations on that path. This has the effect that if there are two or more possible transformation paths of the same length, the path whose transformations have been estimated using the most correspondences is chosen. The idea is that we can get a more accurate final transformation if more data was used to estimate its parts. This is just a heuristic we use and it is not necessarily optimal.

\subsection{Handling Errors}
In the event of false correspondences, RANSAC will filter them away as long as the majority of the points are correct. If this is not the case, poor alignment can occur. We have no way of detecting when this happens, since it would be equivalent to automatically determining whether two point clouds are well aligned, which is a hard problem~\cite[p.~1]{AlmqvistHakan2018Ltdm}. The calibration objects are fairly robust, in the sense that false correspondences are rare. However, we have observed that SIFT will sometimes find many false correspondences if the angle between the cameras is large (around $180^\circ$). SIFT can also find poor correspondences, where one of the points is too far away if the far clipping plane is too distant. This is because it will then find correspondences in an area with heavy noise. To mitigate this, we recommend setting the far clipping plane no further than $2$ meters from the camera.


% Alignment errors
% Not calculating a calibration because of lack of correspondences - discard, but might be able to find a path through another camera coordinate system.
% If the majority of the correspondences are false positives then, RANSAC will classify them as inliers and a transformation based on the false positives will be returned. This transformation will in general have a high MSE and can be discarded with a threshold.

\section{Experiments}\label{sect:cal_experiments}
\input{experiments}

\section{Summary}
In this chapter, we constructed an algorithm that can perform automatic camera calibration using our calibration objects, and SIFT, to find correspondences. Our algorithm uses a combination of RANSAC, Umeyama's algorithm and graph theory to find the transformations between the cameras.

We experimentally validated the robustness and correctness, by performing a series of experiments, testing different aspects. We did a coarse alignment test in order to estimate the calculated transformations deviation from the real one. The experiment showed a maximum deviation of $1.5^\circ$ and $1$cm in rotation and translation. We also tested how the alignment error was affected by the distance to the correspondences and the correspondences (calibration objects) spanning the volume of interest. We concluded that distance matters, but volume span does not. We performed a $360^\circ$ reconstruction of the Stanford Bunny aligning the point clouds from $6$ different camera positions. We then aligned the reconstruction to the point cloud of the bunny's mesh and calculated the error. The average error was $1.5$mm and seemed to be mostly attributed to noise rather than misalignment. We tested the calibration algorithm's ability to realign the point clouds if one of the cameras moved. We demonstrated its ability to do that on an entire video stream, where one of the cameras were constantly moving. Lastly, we tested the algorithm's ability to track a known position in the world, between different views. A mean error of $2.7$mm was observed.
Based on our experiments, we conclude that our approach can be used for reconstructing objects and tracking points in the world, based on the aligned point clouds.

% Moving the camera (can we recalibrate?)
% Calibrating an entire video sequence


%Experimentally validate the robustness and correctness of the calibration algorithm, by measuring the algorithms accuracy in tracking known positions in the world coordinate system, subject to controlled perturbations made by a human user. For instance, moving the camera or the visual aid during a capture sequence.