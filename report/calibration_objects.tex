Calibration objects aid in finding correspondences in different, overlapping, views of the same scene. Each calibration object provides feature points that can be matched between views to establish correspondences.
A feature point is a point of interest in the image or world.
A correspondence consists of two points $(\mathbf{x}_i,\ \mathbf{y}_i) \in (\mathbb{R}^{d},\ \mathbb{R}^{d}),\ d \in \{2,3\} $ from different views that represent the same point in the world. We denote a correspondence with the notation $\mathbf{x}_i \leftrightarrow \mathbf{y}_i$. Correspondences are central, for some algorithms, to perform camera calibration. This chapter is structured as follows:
\begin{itemize}
  \item In Section \ref{sect:kindsofcalib} we distinguish between two types of calibration objects and discuss what properties a good calibration object should possess. The two types differ based on whether their feature points are directly observed or inferred.
  \item In Section \ref{sect:cal-checkerboards} we design a new RGB calibration object by modifying a checkerboard to improve it for finding correspondences.
  \item In Section \ref{sect:sphereDetection} we introduce colored spheres as a 3D calibration object, expanding on the work previously done by \citet{FredrikMA} in his master thesis.
  \item In Section \ref{sect:cubeDetection}, we explore the design space further, by considering a cube calibration object that has both inferred and observable feature points.
\end{itemize}

\section{Types of Calibration Objects and Desirable Properties}\label{sect:kindsofcalib}
We distinguish between two types of calibration objects,
those whose feature points can be directly observed, and those that need to be inferred. Directly observed feature points are present on the surface of the calibration object, and as such, they can be located in 2D. Subsequently, their 3D coordinate can be retrieved from the aligned depth frame.
This is in contrast to inferred feature points, where the exact location has to be estimated from the 3D data.


% \section{Kinds of Calibration Objects and Desirable Properties}\label{sect:kindsofcalib}
% This section defines three different kinds of calibration objects and discusses what properties a good calibration object should have, in general and in the context of soft robotics.

% \subsection{RGB Calibration Objects}
% As the name implies RGB calibration objects provide 2D feature points that are determined from the image color data alone. However, we can get 3D correspondences from RGB calibration objects by aligning the RGB image with the depth frame produced by the cameras.
% RGB calibration objects are constrained by viewing angle and distance.
% If either increase, then detection gets harder. One ubiquitous RGB calibration object for camera calibration is the checkerboard. Its advantages and disadvantages are discussed in section~\ref{sect:cal-checkerboards}.
% \subsection{3D Calibration Objects}
% We define 3D calibration objects, as objects that are of known geometry and scale. For instance, it could be a cube or a sphere. The idea is to locate the 3D calibration objects in the RGB or depth data and then use the known geometry and scale to establish 3D correspondence points that are invariant to viewing angle, thus overcoming one limitation of the RGB calibration objects. For instance, given a number of points on the surface of a sphere, we can fit a sphere to the points to determine the sphere's center. The center can be used as a feature point. The disadvantage of this approach, is that it relies on the use of noisy depth data to estimate the feature points.

% \subsection{Hybrid Calibration Objects}
% A hybrid calibration object combines an RGB calibration object with a 3D calibration object. This could for instance be a cube with checkerboards on each side.

\subsection{Properties of Good Calibration Objects}\label{sect:good_cal_obj}
We can reason about some of the properties a good calibration object should have:
\begin{itemize}
\item[] \textbf{Distinguishable:} The calibration objects must be easy to distinguish from each other, otherwise, false correspondences may be established.
\item[] \textbf{Robust to different viewing angles:} To make it easy to position the cameras, it is beneficial if the calibration object can be identified even when the different viewing angles are large.
\item[] \textbf{Resistant to partial occlusion:} The calibration object can be occluded by the soft robot or if the camera is moved. In the ideal case, correspondences can still be recovered if a partial occlusion occurs.
\item[] \textbf{Robust to illumination changes:} If the color is used to identify the calibration object, the algorithm must be robust to illumination changes. Color can change quite drastically depending on the lighting conditions and inter-reflections from other objects. The detection algorithm has to be able to handle this.
\item[] \textbf{Robust to rotation:} If the calibration object or the camera is rotated, the feature points should still be extractable.
\item[] \textbf{Many feature points:} For robustness the calibration objects should provide many feature points, such that outlier detection can be applied, noise can be canceled and partial occlusion can be tolerated.
\end{itemize}

\section{Checkerboards}\label{sect:cal-checkerboards}
Checkerboards are often used as calibration objects for camera calibration because they contain straight lines and can provide a dense set of feature points: the corners of the checkers. Straight lines are beneficial because they are one of the few things that are preserved by perspective projections. However, checkerboards do not have many of the properties that good calibration objects should have, that we discussed in the previous section. In the following, "\textcolor{red}{$\div$}" denotes that the property is not satisfied, while "\textcolor{codegreen}{\CheckmarkBold}" denotes that the property is satisfied.
\begin{itemize}
\item[\textcolor{red}{$\div$}] \textbf{Distinguishable:} We cannot tell two checkerboards apart unless they have a different number of rows and columns. In the case of partial occlusion, that is not sufficient either.
\item[\textcolor{red}{$\div$}]\textbf{Robust to different viewing angles:} Since it is a plane, it cannot be seen when viewed from the side.
\item[\textcolor{red}{$\div$}]\textbf{Resistant to partial occlusion:} If a checkerboard is partially occluded, most detectors will still detect a part of the checkerboard, but they will not know what part of the checkerboard they are seeing.
\item[\textcolor{codegreen}{\CheckmarkBold}]\textbf{Robust to illumination changes:} The contrast between the white and black checkers is very high and easy to detect even in poor lighting conditions.
\item[\textcolor{red}{$\div$}]\textbf{Robust to rotation:} Checkerboards are inherently ambiguous with respect to rotation. If we e.g. rotate it $180^\circ$ it will look exactly the same as if there was no rotation.
\item[\textcolor{codegreen}{\CheckmarkBold}]\textbf{Many feature points:} If the checkerboard has $n$ rows and $m$ columns, we can obtain $(n-1)*(m-1)$ checkerboard corner points (feature points) assuming no occlusion.
\end{itemize}
In the following sections we alter the checkerboard to fulfil most of the above properties, making it more suitable for calibration and present our checkerboard detection algorithm. The initial detection is based on~\citet{ieee_s6224570}. The modifications to the checkerboard, that allow us to detect the orientation and distinguish them, are our own.

\subsection{The Modified Checkerboard}
The modified checkerboard can be seen in Figure~\ref{fig:checkerboard}. We have used dimensions between $5\times 5$cm and $8\times 8$cm.
Compared to a normal checkerboard it has been modified in two ways. Firstly, it has been equipped with four orientation bars of different colors. Each orientation bar identifies a side of the checkerboard, one visible bar is enough to find the side that is up. The orientation bars are also used to determine what part of the checkerboard is occluded. Secondly, the white and black checkers around the edges have numbers in them, with checkers of the same color displaying the same number. This acts as an identifier for the checkerboard. For instance, the board in Figure~\ref{fig:checkerboard} has id $14$. The number is repeated around the edges for redundancy. It is not in the interior of the checkerboard because it can interfere with the corner detection. It does not have to be a number, it can be replaced with anything else, as long as a classifier is provided for it. The following sections introduce our detection algorithm and show how each modification remedies the missing properties from the above.

\begin{figure}[H]
  \centering
  \includegraphics[height=5cm]{images/checkerboard.eps}
  \caption{The modified checkerboard consists of four orientation bars in separate colors and the checkerboard. Each checker around the edge has a 7-segment style number in it, the white and black checker numbers identify the checkerboard. E.g. this checkerboard has id $14$. The black border is only there to make it easier to cut it out when printed.}
  \label{fig:checkerboard}
\end{figure}

%We have ported it from Matlab to Python, vectorized its loops for increased performance, and modified it slightly to allow for partial occlusion.

\subsection{The Detection Algorithm}\label{sect:detect_algo}
An example of a checkerboard detection can be seen in Figure~\ref{fig:cb_detect}. Let $(h,w)$ be the number of rows and columns of the checkerboard. Each checkerboard then has its own coordinate system ranging from $(0,0)$ to $(h-1, w-1)$, see Figure~\ref{fig:checkerboard_coords}. We use compass coordinates to identify each corner. The corner at $(0,0)$ is labeled "nw" for northwest. Each side is labeled according to the corner labels it connects, e.g. the top side in Figure~\ref{fig:cb_detect} would be labeled "nw-ne". This section explains the initial detection and our modifications to it, followed by orientation detection and checkerboard identification.

\begin{figure}[ht]
  \centering
  \begin{subfigure}[t]{.45\textwidth}
    \centering
    \includegraphics[height=6cm]{images/cb_detect.png}
    \caption{A checkerboard detected by the detection algorithm. The corners are marked with compass coordinates, e.g. the upper left corner where the coordinate $(0,0)$ is located is labeled "nw" for northwest.}
    \label{fig:cb_detect}
  \end{subfigure}
  \hspace{1cm}
  \begin{subfigure}[t]{.45\textwidth}
    \centering
    \includegraphics[height=6cm]{images/checkerboard_coords.png}
    \caption{Illustration of the checkerboard coordinate system. Each corner point has an integral coordinate. The coordinate system is made up of the interior points, the checker corners along the edges are not a part of it.}
    \label{fig:checkerboard_coords}
  \end{subfigure}
  \caption{Illustrations of checkerboard corner labels and coordinate system.}
\end{figure}

\subsubsection{Initial detection}
For the initial checkerboard detection, we use the algorithm derived by~\citet{ieee_s6224570}.
The algorithm introduces a new corner detector, tailored to checkerboards, that is shown to outcompete conventional detectors like Harris' corner detector \cite{Harris88acombined}. It also introduces a way to detect multiple checkerboards in one image, by minimizing an energy function. We have ported the initial detection from Matlab to Python, vectorized its loops for increased performance, and modified the initial detection slightly to allow for partial occlusion.

The algorithm first finds corner points, but not all of them may belong to checkerboards. The checkerboards are then found by trying to grow a checkerboard from each detected corner point: The steps of the initial detection algorithm are briefly summarized below:
\begin{enumerate}
\item \textbf{Find corners:}
  \begin{enumerate}
    \item\label{itm:first} Identify corner points using a tailored feature detector at three different scales and two different orientations.
    \item Calculate the image $I$'s gradient orientations as $\alpha = \arctan(\frac{\partial I}{\partial dy},\frac{\partial I}{\partial dx})$. The output is an image of gradient orientations.
    \item Calculate the gradient magnitude $\lVert \nabla_I \rVert = \sqrt{(\frac{\partial I}{\partial dx})^2 + (\frac{\partial I}{\partial dy})^2}$. The output is an image where edge locations have high values and non-edge locations have low values.
    \item For each corner identified in \ref{itm:first}, the corner position is refined to sub-pixel accuracy and the edge orientations of the checker are found:
    \begin{enumerate}
      \item Make a histogram with $32$ bins of edge orientations in the neighbourhood of the corner, weighted by the gradient magnitude. The edge orientations are found with $\alpha + \pi/2$ because image gradients are perpendicular to edge directions.
      \item Use a mean shift approximation to find modes (peaks) in the edge orientation histogram. If two modes exist, use these as the edge orientations of the checkerboard corner. Otherwise, discard the corner. Two modes should be found because a checkerboard corner is where two edges meet.
      \item Discard corners where the angle between the two edges is too small or too large, since they should be close to perpendicular for a checkerboard corner.
      \item Refine corner locations to get sub-pixel accuracy. A sub-pixel corner location is found by optimizing~\eqref{eq:corner_loc}:
      \begin{align}\label{eq:corner_loc}
        \mathbf{c} = \arg \min_{\mathbf{c}'} \sum_{\mathbf{p}\in \mathcal{N}_I(\mathbf{c}')}(\mathbf{g}^T_\mathbf{p}(p-\mathbf{c}'))^2
      \end{align}
      where $\mathbf{c}$ is a corner candidate, $\mathbf{g}_\mathbf{p}$ is the image gradient at a neighboring pixel $\mathbf{p}$, and $\mathcal{N}_I$ is a neighborhood of $11\times 11$ pixels around the corner candidate. This places the corner in the position where the most neighbours are edge pixels, which is exactly in the corner of the checker.
      \item Refine edge orientations by optimizing \ref{eq:edge_loc}:
      \begin{align}\label{eq:edge_loc}
        \mathbf{e}_i = \arg \min_{\mathbf{e}_i'} \sum_{\mathbf{p}\in \mathcal{M}_i}(\mathbf{g}^T_\mathbf{p}\mathbf{e}_i')^2 \qquad \mathbf{e}_i^{\prime T}\mathbf{e}_i'=1
      \end{align}
      where $\mathcal{M}_i$ is the set of pixels in $\mathcal{N}_I$, which lie on the edge and $\mathbf{e}_i$ is the edge orientation vector. This attempts to align the edge orientations with the edges of the checkers.
      %Refine edge orientations by minimizing a function that
      %nd corner positions by minimizing two functions that use local pixel neighbourhood gradient information. This gives subpixel corner accuracy.
      \item Score the corner using the tailored feature detector from  \ref{itm:first} instantiated with the calculated edge orientations. Discard it if the score is low. A threshold of 0.01 is used.
    \end{enumerate}
  \end{enumerate}
\item[]
\item \textbf{Find checkerboards from corners}:
Find the checkerboards that minimize the energy function
\begin{equation}\label{eq:cb_energy}
E(\mathcal{X}, \mathcal{Y}) = E_{\text{corners}}(\mathcal{Y}) + E_{\text{struct}}(\mathcal{X}, \mathcal{Y})
\end{equation}
where $\mathcal{X} = \{\mathbf{c}_0, \hdots, \mathbf{c}_{n-1}\},\ \mathbf{x}_i \in \mathbb{R}_+^2$ are the corner points and $\mathcal{Y} = \{\mathbf{y}_0, \hdots, \mathbf{y}_{n-1} \},\ \mathbf{y}_i \in \mathbb{Z}_+^2$ are their corresponding labels, each label is a pair $(r,c)$ of a checkerboard row and column. The components of the energy function are given by:
\begin{align}
    E_{\text{corners}}(\mathcal{Y}) &= -\lvert \mathcal{Y} \rvert \label{eq:cb_e_corners} \\
    E_{\text{struct}}(\mathcal{X}, \mathcal{Y}) &= \lvert \mathcal{Y} \rvert\max_{(i,j,k)\in \mathcal{T}}\frac{\lVert \mathbf{c}_i + \mathbf{c}_k - 2\mathbf{c}_j\rVert}{\lVert \mathbf{c}_i - \mathbf{c}_k \rVert}\label{eq:cb_e_struct}
\end{align}
where $\mathcal{T}$ is a sliding window of three adjacent corners, that iterates through the columns and rows of the checkerboard as shown in Figure \ref{fig:sliding_window}. If a checkerboard candidate's energy is above a threshold it is discarded. If two checkerboard candidates share corner points, the checkerboard with the lowest energy is kept.

\begin{figure}[H]
  \centering
  \includegraphics[width=.45\textwidth]{images/sliding_window.png}
  \caption{The sliding window moving through a row. In this example it will first consider the triple $((0,0), (0,1), (0,2))$ and then the triple $((0,1),(0,2),(0,3))$, when evaluating $E_\text{struct}$.}
  \label{fig:sliding_window}
\end{figure}

$\lvert\mathcal{Y}\rvert$ is the number of corner points in the checkerboard, thus $E_{\text{corners}}$ ensures that larger checkerboards have lower energy. $E_{struct}$ ensures that the distance between any three adjacent checkerboard corners on a row or column must have roughly the same distance between them. The leading constant $\lvert\mathcal{Y}\rvert$ and the $\max$ ensures that it only takes one outlier to discard the checkerboard.

To minimize eq.~\eqref{eq:cb_energy} directly, all possible constructions of $\mathcal{Y}$ and $\mathcal{X}$ have to be considered. This has exponential complexity at best and many constructions will not represent a valid checkerboard. To get around this the edge orientations and corner points from step 1 are used as follows:
  \begin{enumerate}
    \item For each corner point $\mathbf{c}_i$ use it as a seed to make an initial $3 \times 3$ checkerboard, neighbors are found via the edge orientation vectors. It has to be at least $3 \times 3$ because otherwise eq.~\eqref{eq:cb_e_struct} cannot be evaluated in both directions.
    \item Iteratively grow the initial checkerboard. In each iteration, the checkerboard can be grown in either of the four directions. The one with the least energy is chosen in each iteration until the checkerboard can be grown no more.
    \item\label{item:energy} If the checkerboard energy is greater than $-10$, discard it.
    The magic number $-10$ is the threshold used by \citet{ieee_s6224570}, but this has the unfortunate consequence of prohibiting $3\times 3$ checkerboards.
    \item If the checkerboard contains any corner points that a previously found checkerboard also contains, keep the one with the smallest energy. This is usually the bigger checkerboard due to eq.~\eqref{eq:cb_energy}.
  \end{enumerate}
\end{enumerate}

\textbf{Our modifications:}\\
We have modified the initial detection in two ways, in order to allow for smaller checkerboards which in turn allows more occlusion. Firstly, we have modified the energy threshold in (\ref{item:energy}) such that it depends on the checkerboard size and allows for $3 \times 3$ checkerboards. In the above form it does not, because the minimum energy a $3 \times 3$ checkerboard can have is $-9$ and the threshold was set to $-10$. We modified it as follows:
\begin{equation}
E_{\text{thresh}} = -\lvert \mathcal{Y} \rvert + \text{max\_pixel\_error} \cdot \lvert \mathcal{Y} \rvert
\end{equation}
where $\text{max\_pixel\_error}$ is the largest allowed value of $E_{\text{struct}}$ (eq.~\eqref{eq:cb_e_struct}). Secondly, we have modified the detection to allow for $2 \times 3$ and $3 \times 2$ checkerboards. This allows for more occlusion. We had to modify the energy function, because $E_\text{struct}$ can only be evaluated if three points are present in a given direction. If the evaluation of $E_\text{struct}$ is omitted in one direction, false detections can happen, see Figure~\ref{fig:energy_neg_ex}. We modified the energy function by imposing the constraint that, for each checkerboard corner, the distance to the adjacent checkerboard corner in both directions, must be close to the same. The added constraint resolved the false detection in Figure~\ref{fig:energy_neg_ex}, see Figure~\ref{fig:energy_neg_ex_after}. The constraint is only active if the checkerboard is $2 \times 3$ or $3 \times 2$.


%because $E_{\text{struct}}$ can only be evaluated in one direction (along the y-axis).
\begin{figure}[H]
  \centering
  \begin{minipage}[t]{0.45\textwidth}
    \centering
    \includegraphics[height=5cm]{images/energy_neg_ex.png}
    \caption{An example of a falsely detected $4 \times 2$ checkerboard. The edge lengths in the $y$ and $x$ direction are far from equal.}
    \label{fig:energy_neg_ex}
  \end{minipage}
  \hspace{0.1cm}
  \begin{minipage}[t]{0.45\textwidth}
    \centering
    \includegraphics[height=5cm]{images/energy_neg_ex_after.png}
    \caption{Figure~\ref{fig:energy_neg_ex} using the modified energy function, this time no checkerboard is detected as expected. The red circles mark the potential checkerboard corner points.}
    \label{fig:energy_neg_ex_after}
  \end{minipage}
\end{figure}


\subsubsection{Finding the Orientation}
To allow us to find correspondences, we need to be able to determine which side of the checkerboard is up.
This is our own addition to the algorithm by \citet{ieee_s6224570}.
As previously mentioned, we label each checkerboard side according to the corner points it connects. Thus, we get the edge labels "nw-ne", "ne-se", "se-sw", "sw-nw". The checkerboard is initialized such that the point "nw" has the smallest $x$ and $y$ coordinate in the image. We want to determine the side the red bar is located at, such that we can rotate the checkerboard coordinate system to have $(0,0)$ at the same point regardless of the checkerboard or camera's orientation. The steps of the orientation detection algorithm can be described as follows:
\begin{enumerate}
  \item Find the bounding box of the checkerboard.
  \item Extract the four colored orientation bars from the edges of the checkerboard.
  \item Score each orientation bar with four different functions, one for each color. This gives the $4 \times 4$ score matrix $\mathbf{S}$, where $\mathbf{S}_{ij}$ is the score of color $i$ for side $j$. The rows (colors) of $\mathbf{S}$ are enumerated as $\{\text{red},\ \text{green},\ \text{purple},\ \text{blue}  \}$ and the columns (edge labels) as $\{\text{nw-ne},\ \text{ne-se},\ \text{se-sw},\ \text{sw-ne}\}$. E.g. $\mathbf{S}_{0,0}$ is the red color score for the orientation bar labeled with "nw-ne".
  \item Greedily assign an edge label and a color to each side of the checkerboard by assigning the highest scores first. If no score is sufficiently high for a given color, note that that side is missing.
  \item Verify that the permutation of the assigned edge labels is valid: It must be a cycle of $\text{nw-ne} \rightarrow \text{ne-se} \rightarrow \text{se-sw} \rightarrow \text{sw-nw} \rightarrow \text{nw-ne} \rightarrow \hdots$, possibly with holes in it due to a side missing, e.g. due to occlusion.
  \item If the permutation is invalid, try to swap the blue and purple side and check again. This is necessary because blue and purple are close together in hue, and can be mistaken for one another.
  \item We can now determine what edge label the red bar is at, or would be at, depending on what other sides have been labeled. We then rotate the checkerboard coordinate system such that $(0,0)$ is at the correct corner.
\end{enumerate}
The orientation bars are extracted at four different scales, the scale that gives the highest score is chosen. The scales are 2px, 4px, 8px, and 12px. We need the scales because we do not know in advance how large the checkerboard is in the image.
The score functions have the signature $f: I \rightarrow [0,1]$, where $I$ is the image patch of the orientation bar and the resulting score is the percentage of pixels in the orientation bar that are classified as having the given color. The scores are calculated by converting the image to the HSV color space, as colors are more stable in HSV compared to RGB. A threshold on the hue channel is then done to determine the color class of each pixel. Red, green, and blue are easy to separate as they are far away from each other in hue. Purple and blue can easily be mistaken for each other depending on the lighting conditions, this is why step $6$ is sometimes necessary. If it is only the blue or purple bar that is visible, a false checkerboard orientation may be detected. False positives should be avoided because they give false correspondences, which in turn can make the calibration fail. However, if the detected orientation is wrong, the identification step will fail as described in the next section, at which point the checkerboard is discarded.

\subsubsection{Distinguishing the Checkerboards}
To distinguish each checkerboard, we label the checkerboard in accordance with the digits around the checkerboard edge. We need to be able to recognize the digits, and since they are printed as 7-segment numbers, we simply need to determine which segments are "turned on".
Our approach is loosely based on \cite{7segment}.
The steps are as follows:
\begin{enumerate}
  \item Extract the checkers containing numbers around the border of the checkerboard.
  \item For each extracted checker:
  \begin{enumerate}
    \item Use the orientation to map the four corners of the checker to the four corners of a square, such that the correct side is pointing upwards.
    \item Unwarp the checker using a plane homography $\mathbf{H}$. The plane homography is a $3 \times 3$ transformation matrix that relates the warped $\mathbf{x}$ and unwarped $\mathbf{x'}$ image coordinates. We utilize that we know that the digit is contained in a proper square, to express the homography. We use scikit-image's implementation to estimate it~\cite{skimage-homography}.
    \item Segment the number from the background.
    \item Classify the number. Discard if no digit detected.
  \end{enumerate}
  \item Create two bins of votes, one for black checkers and another for white checkers. Each checker votes with its classified number and checker color. The majority vote determines the digit in the white and black checker respectively.
\end{enumerate}
Given the bounding box of the checkerboard calculated by the orientation detection step, we can extract the images of the checkers containing the numbers along the edges of the checkerboard. However, the extracted checkers may have the wrong orientation and be heavily warped by the perspective projection. An example of this can be seen in the left of Figure~\ref{fig:number_pipeline}. The checkerboards are planar and we can thus correct for the rotation and perspective warping by finding a plane homography $\mathbf{H}$:
\begin{equation}
\mathbf{x'} = \mathbf{H}\mathbf{x} = \begin{bmatrix}
r_{11} & r_{12} & t_x \\
r_{21} & r_{22} & t_y \\
d_x & d_y & 1
\end{bmatrix}
\mathbf{x}
\end{equation}
It consists of 2D rotation, translation and distortion correction ($d_x$ and $d_y$) coefficients.
The homography is only determinable up to a scale ambiguity, and therefore has $8$ degrees of freedom and can be determined with $4$ corresponding points. We know that the checker was originally square, hence we find the four corner points of the distorted checker and have them correspond to four corner points of a square when finding $\mathbf{H}$. We use the detected orientation to map the corners to each other, such that the transformed checkers are pointing upwards. The image of the checker unwarped via $\mathbf{H}$ can be seen in the middle of Figure~\ref{fig:number_pipeline}. Finally, we segment the number and the checker-background from each other using Otsu's method~\cite{Otsu}, see the segmented digit in Figure~\ref{fig:number_pipeline}. Otsu's method gives a threshold that maximizes the intra-class variance. After segmentation, we check if the background is white, and invert if so. This gives the classification step white digits on a black background.
\begin{figure}[H]
  \centering
  \includegraphics[height=6cm]{images/number_pipeline.png}
  \caption{The number pipeline. From left to right: The extracted, warped, checker containing the number $4$, the checker unwarped with the plane homography, the number segmented from the checker using Otsu's method and the original digit.}
  \label{fig:number_pipeline}
\end{figure}
To classify the digit, we crop the image to the digit and determines what segments of the 7-segment number are on. A segment is on if the majority of its pixels are white. If the height/width ratio of the digit is sufficiently large, it is classified as a one. If the segments that are on correspond to a number, it is classified as such. If the segments do not match any number, classification fails. Thus, if the detected orientation of the checkerboard is false, most of the digits will fail classification and the checkerboard will be discarded.

\subsubsection{Handling Occlusion}
Usually, correspondences are established by describing the vicinity of each feature point, in each view, with a traditional descriptor like SIFT~\cite{SIFT}. These descriptors are typically vectors. We then have a correspondence $\mathbf{x}_i \leftrightarrow \mathbf{y}_i$ if the descriptors for any two points are close by some distance measure. These descriptors only work if there is sufficient and diverse texture in the vicinity of the feature points to uniquely match them between views. Checkerboard corners appear identical, so such methods cannot be used for them. Instead we create text descriptors of the form: "cb\_\{id\}\_\{row\}\_\{column\}" and match them between views by equality. E.g. a descriptor for the point $(0,0)$ in a checkerboard with id "14" as in Figure~\ref{fig:checkerboard_offset} would be "cb\_14\_0\_0". If a checkerboard is partially occluded in one view, we may get one or more partial detections of the same checkerboard. Thus, to generate the correct descriptors for each feature point in the partial checkerboards, we need to know the offset from $(0,0)$ for each partial detection, see Figure~\ref{fig:checkerboard_offset}. If wrong descriptors are generated, false correspondences will be made and the camera calibration may fail. If we know the dimensions of each checkerboard, some of the offsets can be recovered depending on what sides of the partial checkerboard are visible:
\begin{itemize}
  \item If two or three adjacent orientation bars are visible, some corner of the checkerboard is visible in the partial checkerboard (assuming the checkerboard is not large). Knowing the checkerboard dimensions and the size of the partial checkerboard, we can then calculate the offset. See Figure~\ref{fig:checkerboard_offset}.
  \item If two non-adjacent or just one orientation bar is visible, we can only determine the offset if the entire bar is visible. Otherwise, there is some offset from two sides of the checkerboard, but we cannot determine how much from each side, see Figure~\ref{fig:checkerboard_offset_m}.
\end{itemize}
\begin{figure}[H]
  \centering
  \includegraphics[height=6cm]{images/checkerboard_offset.png}
  \caption{The checkerboard has dimensions $4 \times 4$, the left side is occluded and the initial detection has the wrong coordinates. Because we know the dimensions, the size of the new detection ($4 \times 2$), and what side we are seeing, we can infer the correct offset and adjust the coordinates.}
  \label{fig:checkerboard_offset}
\end{figure}

\begin{figure}[H]
  \centering
  \includegraphics[width=0.45\textwidth]{images/offset_middle.png}
  \caption{The checkerboard is occluded such that only two non-adjacent orientation bars are visible. We cannot in general determine the offset in this instance even if we know the dimensions.}
  \label{fig:checkerboard_offset_m}
\end{figure}


\subsection{Validation}
In this section we show that our modifications have remedied some of the identified issues the checkerboard has, such that correspondences can still be correctly determined with multiple checkerboards in the image, and when rotation or occlusion occurs. We show correspondences by putting two images side by side and drawing lines between the corresponding points. Figure~\ref{fig:exp_cb_occluded} shows that correspondences are established correctly under rotation and partial occlusion. Figure~\ref{fig:exp_cb_rotated} shows that correspondences are established correctly under rotation. Finally, Figure~\ref{fig:exp_cb_multiple} shows that the algorithm can correctly establish correspondences when there are multiple checkerboards in the image.

\begin{figure}[H]
  \centering
  \includegraphics{images/cor1.png}
  \caption{The checkerboard on the right has been rotated by $90^\circ$ and it has been occluded s.t. it is only its lower right part that is visible. This shows the algorithm's ability to handle rotation and occlusion simultaneously.}
  \label{fig:exp_cb_occluded}
\end{figure}

\begin{figure}[H]
  \centering
  \includegraphics{images/cor2.png}
  \caption{The checkerboard on the right has been rotated $180^\circ$. This shows the algorithm's ability to handle rotation.}
  \label{fig:exp_cb_rotated}
\end{figure}

\begin{figure}[H]
  \centering
  \includegraphics[width=0.8\textwidth]{images/cor4.png}
  \caption{Correspondences between two images with two checkerboards in them. This shows that the algorithm can handle multiple checkerboards and rotation.}
  \label{fig:exp_cb_multiple}
\end{figure}

\subsection{Observations}
If the lighting conditions are not ideal, then the camera automatically tries to compensate by increasing or decreasing exposure time, gain, and other parameters. This can cause problems with detecting orientation and identifying the checkerboards. Figure~\ref{fig:cb_bad_exp} shows an example of this, as blur and electronic noise are making the colors blend and the digits hard to detect. The orientation markers are noisy as well. Figure~\ref{fig:cb_better_exp} shows an example where the exposure is better, making the checkerboard detectable. Finally, Figure~\ref{fig:cb_good_exp} shows an example where the exposure is good and digits are clearly recognizable and the checkerboard is easily detectable. Figure~\ref{fig:cb_good_exp} is closer to the camera, which is also a factor, as the lack of resolution may otherwise cause blur. Figure~\ref{fig:cb_bad_exp} and~\ref{fig:cb_better_exp} were taken from two different angles with roughly the same distance.
To remedy the problem of poor exposure, we provide the option to set a checkerboard as the camera's auto-exposure target, such that contrast is maximized on the checkerboard. In conclusion, the checkerboards should be correspondingly larger if they are placed further from the camera and good lighting conditions are important as well.

\begin{figure}[H]
  \centering
  \begin{subfigure}[t]{.45\textwidth}
    \centering
    \includegraphics[height=6cm]{images/cb_bad_exp.png}
    \caption{Poor exposure and distance, the colors in the checkers have started blending and electronic noise is visible.}
    \label{fig:cb_bad_exp}
  \end{subfigure}
  \hspace{1cm}
  \begin{subfigure}[t]{.45\textwidth}
    \centering
    \includegraphics[height=6cm]{images/cb_better_exposure.png}
    \caption{Better exposure and distance, the digits are still blurry but legible.}
    \label{fig:cb_better_exp}
  \end{subfigure}
  \begin{subfigure}[t]{.45\textwidth}
    \centering
    \includegraphics[height=6cm]{images/cb_good_exposure.png}
    \caption{Good exposure and distance, the digits are clearly visible.}
    \label{fig:cb_good_exp}
  \end{subfigure}
  \caption{Image quality variation due to distance and blur.}
\end{figure}


