We use the Intel\textsuperscript{\textregistered} RealSense\textsuperscript{TM} D415 Depth camera, which can capture RGB images as well as depth data. The D415 cameras have been used for previous soft robot projects at DIKU, and are also the cameras that are available to us. Compared to other depth cameras they come in a small form factor and provide a lot of functionality at an affordable price. In this section, we explain how the cameras work, their limitations, and properties.
\section{The D415 RGBD Sensor}
\begin{figure}[ht]
  \centering
  \includegraphics[width=0.8\textwidth]{images/cameraAnno.png}
  \caption{The D415 RGBD sensor. It consists of three cameras and one laser projector. From left to right: Infrared camera 1, Laser projector, Infrared camera 2 and RGB camera.}
  \label{fig:d415}
\end{figure}
An image of the D415 sensor can be seen in Figure~\ref{fig:d415}. It consists of two infrared (IR) cameras, a laser projector, and one RGB camera. It recovers depth using stereo. Correspondences are found between the left and right IR cameras, and because the cameras are mounted on a rigid frame with a fixed distance, the length of the baseline is known. This information along with the camera intrinsics is enough to recover depth. The D415 is both an active and passive stereo device. Active stereo is achieved by using the laser projector, which projects a textured pattern onto the scene that is only visible in the IR spectrum, see Figure~\ref{fig:active_IR}. Features are then found in the projected texture and correspondences can be established. The laser projector can be turned off and the camera will revert to passive stereo if no external laser projector is used. In passive stereo, the camera analyzes the RGB input from the IR cameras to find correspondences. This requires that there is sufficient texture in the scene, meaning there must be sufficient variation in color and pattern. It is common that multiple active stereo sensors can interfere with each other, but the RealSense sensors do not~\cite{inteld400bestknown} and it is thus safe to use multiple RealSense sensors with their lasers turned on. This is important for our use-case since we will be using multiple cameras.

\begin{figure}[ht]
  \centering
  \begin{minipage}[t]{.48\textwidth}
    \centering
    \includegraphics[width=\textwidth]{images/active_IR_pattern.png}
    \caption{An image from one of the IR cameras of two pieces of cardboard with checkerboards on them. The active stereo pattern projected by the laser is visible.}
    \label{fig:active_IR}
  \end{minipage}
  \hspace{0.1cm}
  \begin{minipage}[t]{.48\textwidth}
    \centering
    \includegraphics[width=\textwidth]{images/deadzone.png}
    \caption{A depth image aligned with the RGB camera, a resulting band of deadzone appears in the left of the depth image because the two cameras are displaced on the x-axis. Black indicates no depth value, brighter values are further away.}
    \label{fig:deadzone}
  \end{minipage}
\end{figure}
Since the IR cameras and the RGB camera are displaced on the sensor, we need to align the depth and RGB data, which can be done through the RealSense API. This allows us to find the depth of a pixel in the RGB image directly. There is a missing band of data on the left side of the depth frames, because the two IR cameras are displaced, see Figure~\ref{fig:deadzone}. Aligning the depth frame with the RGB camera increases the size of this band a little. This increase can be avoided by using the IR camera as the color camera, but as can be seen in Figure~\ref{fig:active_IR} the colors in the IR image look muted and the laser pattern is also visible. The muted colors are detrimental to color-sensitive work, while the
laser-pattern makes it difficult to locate markers etc.
The laser pattern can be removed on the device by providing a color correction matrix as described in~\cite{inteld400bestknown}.

The cameras also have requirements for the distance to the scene.
The minimum depth is 450mm when a depth resolution of $1280\times 720$ is used~\cite[p.~60]{inteld400datasheet}, and can be calculated using the following expression:
\begin{equation}
  \text{minZ} = \frac{\text{focal length(pixels)} * \text{baseline in mm}}{126}
\end{equation}
where
\begin{equation}
\text{focal length(pixels)} = \frac{1}{2}\frac{\text{depth x resolution in pixels}}{\tan (\frac{\text{HFOV}}{2})}
\end{equation}
and the baseline is the line connecting the two IR camera coordinate centers and HFOV is the horizontal field of view of the camera, all of which are available through the RealSense API. Any object closer than 450mm will not have any depth data associated with it, and any object of interest should, therefore, be placed at least 450mm from the camera.
This minimum distance can be reduced in two ways: Firstly, by choosing a lower resolution of the depth camera, giving rise to a tradeoff between the effective range of the sensor, depth error, and the resolution of the depth data.
Secondly, the cameras have an advanced setting called "disparity shift"
which can reduce the minimum distance, at the cost of lowering the maximum distance substantially.
The disparity shift and the maximum distance are theoretically related as follows~\cite[p.7]{CarfagniMonica2018MaCC}:
\begin{equation}
\text{maxZ} = \frac{\text{focal length(pixels)} * \text{baseline in mm}}{\text{disparity shift}}
\end{equation}
 The above relationship can be plotted to determine what the disparity shift can be set to, depending on the distances to the region of interest, to improve the minimum range of the camera. The default disparity shift is $0$ giving a theoretical max $Z$ of infinity.

The camera captures the image frames one row at a time, which is known as a rolling shutter. This is contrary to a global shutter where all rows are captured simultaneously. The rolling shutter makes the camera's images susceptible to tearing when it is moved, because the image may then be captured in more than one position.

% camera structure, laser projector, 2 IR sensors and one RGB sensor
% Shutter type, what does it entail?
% Auto exposure, ROI - can all be controlled via the API

\section{Bandwidth Limitations}
When connecting multiple cameras to a single USB 3.0 controller, bandwidth quickly becomes an issue, because the theoretical bandwidth of a USB 3.0 controller is 5Gbps and the maximum throughput of a single camera is 3.2 Gbps~\cite{bandwidthwhitepaper}.

For the best depth performance, Intel recommends using the maximum depth resolution of $1280\times 720$~\cite{inteld400bestknown}. Furthermore, for maximum accuracy of the algorithms using the RGB data to find feature points, we use the maximum RGB resolution of $1920 \times 1080$. We stream both at $30$ FPS which is the maximum for the RGB stream at this resolution. Each RGB frame takes up $1920*1080*3\text{B}$ and each depth frame $1280*720*2\text{B}$, since depth is stored as two-byte integers. This gives a total bandwidth usage of $(1920*1080*3\text{B}+1280*720*2\text{B})*30\text{FPS}*8\text{b} \approx 1.93\text{Gbps}$. Hence, no more than two cameras can be connected per USB 3.0 controller. If a frame is not retrieved when it arrives, it will be dropped. Thus high CPU activity can also cause frame drops. We have found that to ensure no frame drops, each camera should run on a separate USB controller and little to no processing should be done while recording. When many cameras are used it can be difficult to get enough USB ports from a single machine, as adjacent USB ports often share the same controller. To get around this limitation our implementation supports merging multiple recordings, that have been created on separate machines.
Intel has published some results on what configurations are possible on single and multiple USB controllers~\cite{bandwidthwhitepaper}, to summarize it: recording at high resolution with multiple cameras quickly necessitates multiple USB controllers.

%As seen in Figure \ref{fig:bandwidth-hub}, once the amount of data streamed
%surpasses 1200MBps, there is a risk of dropped frames occurring,
%when connecting the cameras on a single USB bus. The green cells indicate that streaming has been verified successfully, while the yellow means
%slightly reduced framerate (i.e. some dropped frames), the red means less than half-framerate or
%none, and grey is “not tested”.
%\begin{figure}[ht]
%  \caption{Streaming Results for 4-port USB3 hub (Should we produce our own data?)}
%  \label{fig:bandwidth-hub}
%  \centering
%  \includegraphics[width=0.85\textwidth]{images/bandwidth-hub}
%\end{figure}
%As revealed by Figure \ref{fig:bandwidth-individual}, performance is improved by connecting each camera to its own dedicated
%USB port on the PC, thus limiting congestion. This brings the effective
%available bandwidth to 1700MBps, before instances of dropped frames are encountered.
%\begin{figure}[ht]
%  \caption{Streaming results for connecting directly to 4 ports of a PC (Should we produce our own data?%)}
%  \label{fig:bandwidth-individual}
%  \centering
%  \includegraphics[width=0.85\textwidth]{images/bandwidth-individual}
%\end{figure}
%Interestingly, it is revealed that the occurrence of dropped frames
%is not purely a question of the total amount of data being received by the PC,
%but also depends on the number of cameras and their configuration.
%This is evident from the fact that with a two-camera setup, 2345MBps
%can be handled without dropped frames, while in a four-camera setup
%1769MBps result in some dropped frames, even when using dedicated USB ports.
%This suggests that USB bandwidth is not the only factor affecting
%dropped frames, but that CPU availability is also likely to be an issues,
%when working with multiple cameras and large image resolutions.

\section{Depth Error and Noise Modeling}
Depth errors propagate directly to all aspects of the deformation tracking, for example, noise could create false deformations. Additionally, errors in the depth data result in inaccurate correspondences, which affect the quality of the point cloud alignment. Therefore, it is important to ensure good depth quality.
As shown in the Intel whitepaper on depth quality, poor exposure is the main reason for poor and noisy depth data~\cite{inteld400bestknown}. The best way to ensure good exposure is to ensure that the lighting conditions are good. Good lighting conditions also reduce exposure time, which in turn reduces blurring effects when the camera is moving. Alternatively, the gain and exposure settings can be adjusted to the environment through the RealSense API for maximum accuracy. The camera has an auto-exposure function as well, but it can have a detrimental effect in the learning cube setting, because it is mostly black, which the camera interprets as a lack of light. This can be alleviated by setting the region of interest (ROI) for the auto-exposure to e.g. a calibration object through the RealSense API. We have implemented this functionality in our API. In low-light and learning cube settings, we find it beneficial to set the ROI to a calibration object, to ensure good contrast, while in the free-space setting, we allow the camera to set the ROI automatically.

Between the minimum depth and up to two meters while within $80\%$ of the field of view, the cameras promise less than $2\%$ error in the depth data. An object one meter from the camera will thus be estimated to be between 98cm and 102cm from the camera in the worst case. Under ideal circumstances, the error is expected to be much less, roughly around 1mm\cite[p.~4]{inteld400bestknown} according to Intel. This claim has also been independently verified under ideal circumstances and short distances, see~\cite{CarfagniMonica2018MaCC}. The theoretical lower limit on the error states that the depth error increases quadratically with the distance to the object and decreases linearly with the width of the depth resolution, which is why higher depth resolution is better~\cite{inteld400bestknown}. An example of how the depth error manifests itself visually in the point cloud can be seen in Figure~\ref{fig:cam_noise}. The waves in the point cloud that can be seen in Figure~\ref{fig:cam_noise} can also manifest themselves at shorter ranges if the exposure is poor.

\begin{figure}[H]
  \centering
  \includegraphics[height=8cm]{images/noise.png}
  \caption{The point cloud of a flat wall that was roughly 1.5 meters away from the camera. The noise manifests itself like waves in the flat surface.}
  \label{fig:cam_noise}
\end{figure}
A noise model fits a distribution to the axial and lateral noise at different distances and angles of incidence. Axial noise is depth noise along the z-axis of the data and lateral noise is noise in directions that are perpendicular to the z-axis. Noise models exist for the Kinect~\cite{NguyenCV2012MKSN} and for the RealSense D435 camera~\cite{AhnMinSung2019AaNM}, but to the best of our knowledge no noise model exists for the D415 yet, and the model for the D435 is likely not applicable, as the cameras differ significantly. For instance, the D435 has a global shutter and a larger field of view. In~\cite{NguyenCV2012MKSN} they use the noise model to smooth the depth data and improve the reconstruction of small features on objects. They also use the axial noise model as weights in ICP (Iterative Closest Point) such that more noisy points that are further away, weigh less in the solution. The smoothing could be useful because of how much it smooths depends on the noise at a given distance. However, smoothing can change the shape of the robot and we, therefore, think it is best if it is avoided. Rather than weighing noisy points less, we prefer to estimate our transformation using correspondences that are close to the camera instead (45-150cm). A noise model might be interesting for future projects however, for instance, for reconstruction of small features on the robot.
A noise model for the D415 camera could be created by replicating the experiment used for the D435.
The experiment involves gathering depth data of a flat surface at different distances and angles, using accurate distance-measuring equipment, and then modeling the observed deviations from the flat surface and the straight edges around the surface boundary.

% Smoothing could be useful because it is dynamic, but may also change the shape of the robot
% The improvements in reconstruction might be interesting for the markerless approaches
% For weighing points that are further away, it is not that necessary for us because we need high accuracy, and therefore have to consider points that are close to the camera and less noisy

% It would be beneficial to have a noise model for the D415 camera, since this could ease restrictions of the distance between cameras and objects of interest. To avoid the acquisition of new equipment, we have opted to instead restrict the distance to 30-150cm,
% since the error is small within this range.

% Noise modelling, what they can be used for and why they should be made for the D415 - perhaps by another student? Exists for the Kinect (ref) and the D430 (ref), but we haven't found any for the D415. The D430 noise model does not apply because it is a significantly different camera.
% Verification done of the D415 in one paper

\section{Summary}
In this chapter, we discussed the strengths and weaknesses of the D415 camera. We have found that USB-bandwidth quickly becomes an issue, and suggested that multiple machines could be used to solve it.
The noise in the depth is primarily dependent on the lighting conditions and distance to the camera, and we proposed that the volume of interest should be kept as close to the cameras as possible.