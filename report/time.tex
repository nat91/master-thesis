The timestamps of the different cameras are reported as each camera's system time
at the moment of frame capture. Since these system times can vary greatly between
cameras, and we wish to unify the point clouds both in space and time, it is necessary to align the timestamps
of the different recordings.

There are in general two different approaches to time synchronization, using either hardware or software. The hardware approach requires connecting the cameras with synchronization cables. The D415 cameras have a sync port for this purpose, one camera is then configured to be the master and the others as slaves. The master sends a sync signal on the wire right when it starts its frame and the slaves start their frame when they receive it. The software approaches we have considered rely on detecting events in the images, e.g. motion or changes in lighting. They then use these to construct a model that can relabel and align the timestamps between different cameras. At its simplest, this model is an offset in the timestamps. It can also include compensation for clock drift and other elements. After relabeling the timestamps, the frames can be aligned in time. Regardless of what software method is used, the maximum alignment error can never go below $\frac{1}{\text{FPS}}s$, because they cannot control when each individual camera starts. Thus, if the framerate is too low, then fast movements may appear misaligned in time.

Section \ref{section:timeAlgo}
introduces our approach to time synchronization, based on the work done by \citet{proquest2187123788}. Section \ref{section:hardwareTime}
discusses the option of using hardware synchronization, and the pros and cons of such an approach,
as compared to our solution.

\section{The Algorithm}\label{section:timeAlgo}
We have implemented the algorithm described in \citet{proquest2187123788}. We use a flash-based synchronization method,
in which the abrupt changes in pixel intensities caused by the camera flash,
are used as \textit{synchronization events}. We have also extended it to handle a few special cases that occur in general and with the D415 sensor. In the following, we will explain how the algorithm works.

\subsection{Detecting Synchronization Events}
With a rolling shutter camera, each frame of the recording is captured in a
sequential manner, where the frame is constructed one row at a time, from top
to bottom. This means that every row captures the imaged scene at slightly different moments.
Due to the brevity of a camera flash, only some rows in the image will capture the flash event, see Figure~\ref{fig:flashEvent},
thus enabling us to determine when the flash occurred, at sub-framerate granularity.
\begin{figure}[H]
  \centering
  \includegraphics[width=0.9\textwidth]{images/flashEvent.png}
  \caption{Flash event detected, it starts at row 378.}
  \label{fig:flashEvent}
\end{figure}

We require that the majority of the scene receives light from the flash.
Otherwise the events may not be detected by the cameras.

\begin{figure}[H]
  \centering
  \includegraphics[width=0.85\textwidth]{images/rowIntensities.png}
  \caption{Median row intensity differences across frames, 10 flashes were fired and detected. The flashes are clearly visible in the median row intensity difference between frames. Two of the events span two frames (4 and 8), however, we are only interested in the starting time of the event, so the "b" events are discarded. Event $0$ is the auto exposure settling and is ignored.}
  \label{fig:medianIntensity}
\end{figure}
The detection of the changes in lighting can be achieved by
computing a median row intensity profile for every frame and detecting large changes in intensity profiles between frames. The median row intensity profile
for a frame is the median intensity value for each row in the frame. Using the median helps to make the method more robust against noise when compared to using the mean. This is because the median is more outlier resistant than the mean. Subtracting the median row intensity profile from two consecutive frames
will reveal if a sudden change in intensity has occurred and in which row the change starts.
Figure \ref{fig:medianIntensity} shows the maximum difference in median line row intensity between subsequent frames,
with the synchronization events visible as clear spikes in the intensity differences.
By thresholding the intensity differences we identify the synchronization events and corresponding
frames and rows.
We also observe a couple of anomalies in Figure~\ref{fig:medianIntensity}. Event 0 is not caused by a flash event, but rather by the initial settling of the auto exposure in the recording. The change in intensity is small enough, that a suitable choice of the threshold will solve the issue. We have utilized 50 as our threshold. Additionally, we see from events 4a and 4b that two subsequent frames can exhibit large enough intensity changes to both be considered as events. This is the case when the flash event crosses two frames. Since we are only interested in the onset of the flash event, we simply discard the trailing event. It can also happen that a flash event starts between frames, this would be the case if the first row is lit up by the flash but the last row in the previous frame is not. In this case, we have to discard the flash event because we need to know when it starts.
\begin{algorithm}[H]
  \renewcommand{\algorithmicforall}{\textbf{for each}}
  \renewcommand{\And}{\textbf{and }}
  \caption{findEvents}\label{phase1}
  \hspace*{\algorithmicindent} \textbf{Input:} image sequences, eventFrequency \\
  \hspace*{\algorithmicindent} \textbf{Output:}  synchronization events
  \begin{algorithmic}[1]
    \FORALL{camera}
    \FORALL{frame} \STATE{$m_f$ := median row intensity profile for frame} \ENDFOR
    \ENDFOR
    \FORALL{frame} \STATE{compute difference profiles \\ $d_f := m_f - m_{f-1}$} \ENDFOR
    \FOR{frames with difference profiles above threshold} \STATE {$r$ := find raising edge row in $d_f$
    \IF{r > 0 \And event occurs when expected from eventFrequency}
    \STATE{event := $(f,r)$}\ENDIF} \ENDFOR
  \end{algorithmic}
\end{algorithm}
The algorithm is described in Algorithm \ref{phase1}.
We have made three changes to the algorithm as it was formulated by \citet{proquest2187123788}: firstly, we discard the event that is caused by the auto exposure settling. Secondly, we discard events that start between frames. Finally, we know the frequency of the flash and can use this to remove false positives.

\subsection{The Model}
Using the events detected by Algorithm \ref{phase1}, we can then construct a linear model that can relabel the timestamps from other cameras to a reference camera's time. With basis in a reference camera $c_{\text{ref}}$ we wish to map the timestamps
of each additional camera $c$ into the reference time of $c_{\text{ref}}$,
i.e. we wish to find the transformation $s^{c \rightarrow c_{\text{ref}}}(f,r)$, where $(f ,r)$
is a frame/row pair. Letting $t_0$ be the timestamp for the beginning of the recording, and $\text{FPS}$ be the number of frames per second, the timestamp of the first
row exposure of a frame $f$ is then
\begin{align}
  t_f(f)=f\cdot \frac{1}{\text{FPS}}+t_0
\end{align}
Additionally, we have to take into account the hidden pixel rows around the active pixel area.
Cameras typically use hidden pixel rows to reduce noise and fix color interpretation at the sensor edges.
If we name the bottom hidden pixel rows $R_0$, and the top hidden pixel rows $R_1$,
then there is a delay between each frame proportional to $R_1 + R_0$,
since these hidden rows need to be exposed before the next frame can begin.
If we dub the effective number of rows as $R_h$ (height of the image), then we can calculate the
timestamp of a specific row in a frame as:
\begin{align}\label{eq:ts_row}
  t(f,r) = t_f(f) + \frac{R_0 + r}{R_0 + R_h + R_1}\cdot \frac{1}{\text{FPS}}
\end{align}
where $t_f(f)$ is the frame timestamp and $1/\text{FPS}$ is the nominal frame duration. Having found synchronization events in each image stream, we transform
the timestamps of each non-reference camera, such that the event timestamps correspond
between cameras. The proposed transformation is:
\begin{align}\label{eq:time1}
  s^{c \rightarrow c_{\text{ref}}}(f,r) = \alpha t_f(f) +\beta+r\cdot \frac{1}{R\cdot\text{FPS}}
\end{align}
where $\alpha$ is the camera clock drift compensation, $\beta$ is the time-offset, $f$ is the frame number, $r$ is the row number, $t_f$ is the
frame acquisition timestamp, and $R= R_0 + R_h + R_1$ is the total number of sensor rows.
We wish to estimate $\alpha$ and $\beta$. The frame and row numbers of the events are treated as input. Since we do not know the number of hidden rows in the D415 sensor, the time per row $\frac{1}{R\cdot\text{FPS}}$ is not known. We instead estimate the time per row as well, and let $T^{c_{\text{ref}}}_{row} = 1/(R^{c_{\text{ref}}} \cdot \text{FPS}^{c_{\text{ref}}})$ be the time per row of the reference camera, while
$T^c_{\text{row}} = 1/(R^c\cdot \text{FPS}^c)$ is the time per row of the other camera.

For any given event observed in camera $c$ and $c_{\text{ref}}$ at time $(f^c,r^c)$ for camera $c$ and at time $(f^{cref},r^{cref})$ for the reference camera, the timestamps should be equal after synchronization:
\begin{align}\label{eq:time2}
  s^{c \rightarrow c_{\text{ref}}}(f^c,r^c) &= t^{c_{\text{ref}}}(f^{c_{\text{ref}}},r^{c_{\text{ref}}})
\end{align}
Given $k$ synchronization events, we can solve the system
resulting from the above equation. Combining \eqref{eq:time1} and \eqref{eq:time2} then yields the equation we have to solve for the four unknowns $\alpha$, $\beta$, $T^c_{\text{row}}$ and $T^{c^{ref}}_{row}$:
\begin{align}
\alpha t_f^{c}(f^{c}) +\beta +r^c \cdot T^c_{\text{row}} = t_f^{c_{\text{ref}}}(f^{c_{\text{ref}}}) +r^{c_{\text{ref}}}\cdot T^{c^{ref}}_{row}
\end{align}
where the left and the right side is the time the event started in each camera.

The full algorithm is then to find events for each recording as described in Algorithm \ref{phase1} and then using the found events to align the timestamps of all recordings as described in Algorithm \ref{phase2}.
\begin{algorithm}[H]
  \renewcommand{\algorithmicforall}{\textbf{for each}}
  \caption{alignTimestamps}\label{phase2}
  \hspace*{\algorithmicindent} \textbf{Input:} frame timestamps, detected synchronization
  events, reference camera $c_{\text{ref}}$ \\
  \hspace*{\algorithmicindent} \textbf{Output:} synchronization parameters
  \begin{algorithmic}[1]
    \FORALL{camera except reference camera} \STATE{$E^{c,c_{\text{ref}}}$ := match events between $c$ and $c_{\text{ref}}$}
    \FORALL{matched event in $E^{c,c_{\text{ref}}}$} \STATE {\{$(f^c,r^c), (f^{c_{\text{ref}}}, r^{c_{\text{ref}}}$\}:=event\\
    $t_f^c(f^c)$ is the timestamp for frame $f^c$\\
    $t_f^{c_{\text{ref}}}(f^{c_{\text{ref}}})$ is the timestamp for frame $f^{c_{\text{ref}}}$\\
    add equation: \\
    $\alpha^c t_f^{c}(f^{c}) +\beta^c +r^c \cdot T^c_{\text{row}} = t_f^{c_{\text{ref}}}(f^{c_{\text{ref}}}) +r^{c_{\text{ref}}}\cdot T^{c^{ref}}_{row}$\\
    to the system of equations
    } \ENDFOR
    \ENDFOR \\
    solve the system in a least squares sense for $\alpha^c, \beta^c, T^c_{\text{row}}$ and $T^{c^{ref}}_{row}$ for each camera.
  \end{algorithmic}
  \hspace*{\algorithmicindent} \textbf{Return:} $\alpha^c, \beta^c, T^c_{\text{row}}, T^{c^{ref}}_{row}$ for each camera.
\end{algorithm}

\subsection{Our Time Synchronization Circuit}
To make our flash controllable from the PC, we built a simple Arduino circuit.
As shown in Figure \ref{fig:circuit} and~\ref{fig:circuitReal}, our circuit consists of:
\begin{itemize}
  \item 1x Arduino
  \item 1x hotshoe camera flash with input jack
  \item 1x Breadboard
  \item 1x MOC3021 optocoupler
  \item 1x 220 ohm resistor
  \item 4x solid core wires
\end{itemize}
The Arduino provides power to the circuit, while the flash has its own power source, namely two AA batteries.
Using the optocoupler, the flash unit is electrically isolated from the Arduino, which is important because
once the flash unit is turned on, it will begin charging, resulting in a high voltage, which is potentially dangerous if discharged to a human or the Arduino.
The optocoupler allows us to maintain two isolated circuits, while still being able
to communicate between the circuits, via the optocoupler.

A small Python interface was written, allowing communication between a computer and the Arduino/flash via USB. It is available as part of the command-line interface, but can also be used standalone. The script allows the user to set the number and frequency of flashes.
\begin{figure}[H]
  \centering
  \includegraphics[width=0.65\textwidth]{images/circuit.png}
  \caption{Our flash circuit design}
  \label{fig:circuit}
\end{figure}
\begin{figure}[H]
  \centering
  \includegraphics[width=0.65\textwidth]{images/flash.jpg}
  \caption{Our physical flash circuit design}
  \label{fig:circuitReal}
\end{figure}

\subsection{Experiments}
The algorithm relabels the timestamps to match that of the reference camera. It does this by calculating the time the synchronization events occur. Thus, if the model is perfect, the synchronization events should happen at the same timestamp in every camera. However, because it is a least squared fit, it may not be. Our error measure is the difference in the calculated timestamps the events happen at.

The purpose of our synchronization experiments are twofold:
\begin{enumerate}
  \item Estimate the model quality, i.e. does the linear model produce small residuals.
  \item Estimate how much synchronization suffers, if the synchronization events do not span the entirety of the recording.
\end{enumerate}
The second point is of interest since we would prefer the synchronization process to either precede or follow the
actual recording, such that the recordings are not polluted by the synchronization events.

All experiments were performed at a framerate of 30 and an RGB resolution of $1920 \times 1080$,
but we have verified that our system also works at other framerates and resolutions the D415 camera is capable of.

\subsubsection{Synchronization Model Quality}
\textbf{Introduction:}\\
We wish to validate that the linear model fits the data well. To do this we measure the fitting error on all events, and then we randomly sample a subset of the events and run the calibration using them, to test if the model still fits the remaining data points well, even though they have not been used for model estimation.

% Error between events (caused by least square fit)
% Error on frames that are in between the events in time

\textbf{Procedure:}\\
To estimate the overall quality of the time synchronization algorithm,
we created five recordings of 60 seconds, in which 12 flash events occur, one every 5 seconds. For each recording, we run the algorithm and compare the event timestamps of the reference
recording and the aligned event timestamps of the other camera, in order to get an estimate of
the model quality.

To determine if the fit is as good outside of the synchronization events and if the number of events used matter, we also subsample 6 of the 12 events randomly
and run the algorithm on these subsets. Afterward, we check the fit quality
of the events not used for the synchronization.

\textbf{Results:}\\
As seen in Table \ref{tab:syncExp1}, we have achieved an average
error of 8.8 microseconds, which is minuscule, while the largest error
observed was 27 microseconds, also minuscule.

We see from Table \ref{tab:syncExp2} that the average error has slightly increased to 14.4 microseconds compared to 8.8 microseconds when we only use half of the events for synchronization. Furthermore, we see small errors for the events held out
of the synchronization model estimation, although slightly larger than in the full case. We conclude that the linear model fits the data well and that the model does not suffer greatly when fewer data points are used.

\begin{table}[H]
  \resizebox{\textwidth}{!}{%
  \begin{tabular}{l|llllllllllll|l}
    \diagbox[width=5em]{Trial}{Event} & E1  & E2  & E3  & E4  & E5  & E6  & E7  & E8  & E9  & E10 & E11 & E12 & Avg. \\
    \hline
    Trial 1     & -4 & -9 & -8 & -12 & 27 & -9 & 16 & 18 & 13 & -8 & -14 & -11 & -0.1  \\
    Trial 2     & -10 & 10 & 0 & 0 & - & 20 & -20 & 0 & 0 & 0 & -10 & 10 & 0.0  \\
    Trial 3     & 24 & -12 & -19 & - & -2 & -9 & 14 & 9 & 1 & -6 & -13 & 11 & -0.2  \\
    Trial 4     & -11 & -11 & 19 & -7 & 4 & 3 & 3 & 2 & 7 & 17 & -13 & -13 & 1.8  \\
    Trial 5     & 9 & 1 & -8 & - & 4 & - & -13 & 8 & -2 & -10 & 11 & 3 & 0.3  \\
    \hline
    Max abs.    & 24 & 12 & 19 & 12 & 27 & 20 & 20 & 18 & 13 & 17 & 14 & 13 & 17.6\\
    \hline
    Avg. abs.& 11.6 & 8.6 & 10.8 & 3.8 & 7.4 & 8.2 & 13.3 & 7.4 & 4.6 & 8.2 & 12.2 & 10.2 & 8.8
    \end{tabular}}
  \caption{Errors for different trial runs (microseconds), all 12 events used. If "-", the event was discarded because the event started between frames for one or more cameras. The Avg. column indicates that the timestamps do not drift over time.}
  \label{tab:syncExp1}
\end{table}

\begin{table}[H]
  \resizebox{\textwidth}{!}{%
  \begin{tabular}{l|llllllllllll|l}
    \diagbox[width=5em]{Trial}{Event} & E1  & E2  & E3  & E4  & E5  & E6  & E7  & E8  & E9  & E10 & E11 & E12 & Avg. \\
    \hline
    Trial 1     & -15 & -15 & -15 & \underline{-15} & \underline{14} & \underline{-15} & \underline{15} & \underline{15} & 15 & \underline{-15} & -15 & -15 & -2.6  \\
    Trial 2     & -23 & -20 & \underline{-5} & -3 & \underline{11} & \underline{-16} & \underline{16} & 31 & 34 & -13 & \underline{-10} & \underline{5} & 5.3 \\
    Trial 3     & \underline{-2} & -15 & -17 & -30 & 39 & \underline{-5} & 13 & \underline{11} & \underline{-3} & \underline{6} & -6 & \underline{-8} & 2.0  \\
    Trial 4     & \underline{2} & -11 & \underline{7} & -7 & \underline{39} & \underline{-4} & 13 & \underline{30} & 18 & \underline{2} & -11 & 7 & 10.2  \\
    Trial 5     & \underline{0} & 15 & \underline{-15} & 0 & \underline{14} & \underline{0} & 45 & \underline{15} & 30 & \underline{-15} & 0 & -30 & 8.6  \\
    \hline
    Max abs.    & 23 & 20 & 17 & 30 & 39 & 16 & 45 & 31 & 34 & 15 & 15 & 30 & 28.1 \\
    \hline
    Avg. abs.   & 8.4 & 15.2 & 11.8 & 11.0 & 23.4 & 8.0 & 20.4 & 20.4 & 20.0 & 10.2 & 8.4 & 15.8 & 14.4
    \end{tabular}}
  \caption{Errors for different trial runs (microseconds), underlined held out}
  \label{tab:syncExp2}
\end{table}


\subsubsection{Quality when Synchronization Does Not Span Recording}
\textbf{Introduction:}\\
We wish to determine if it is necessary to keep the flash running throughout the entire recording, in order for the frames to remain synchronized. It would be preferable if this was not the case because then we only have to run the time synchronization at the beginning or end of the recording, leaving the rest of the recording undisturbed. The reason why the time model is not exact throughout an entire recording is likely due to drift in the camera clocks and small variations in the framerate.

\textbf{Procedure:}\\
To validate that synchronization is not greatly affected by the synchronization events
failing to span the entire recording, we run the algorithm on the first 6 events
and verify the alignment of all 12 events as seen in Table \ref{tab:syncExp3}. We have reused the recording from the previous experiment, such that the errors are directly comparable.
\begin{table}[H]
  \resizebox{\textwidth}{!}{%
  \begin{tabular}{l|llllllllllll|l}
    \diagbox[width=5em]{Trial}{Event} & E1  & E2  & E3  & E4  & E5  & E6  & E7  & E8  & E9  & E10 & E11 & E12 & Avg. \\
    \hline
    Trial 1     & -5 & 5 & -5 & 5 & 10 & -10 & 30 & 20 & 30 & -24 & -15 & -25 & 1.3  \\
    Trial 2     & -5 & 10 & -5 & 0 & - & 0 & -45 & -31 & -25 & -40 & -55 & -40 & -21.5  \\
    Trial 3     & 5 & -10 & 5 & - & 0 & 0 & 45 & 60 & 10 & 10 & 25 & 70 & 20.0  \\
    Trial 4     & 2 & -10 & 8 & 0 & 6 & -6 & -18 & -30 & -8 & -2 & -44 & -56 & -13.2  \\
    Trial 5     & 0 & 0 & 0 & - & 0 & - & 0 & 31 & 0 & 0 & 31 & 31 & 9.3  \\
    \hline
    Max abs.    & 5 & 10 & 8 & 5 & 10 & 10 & 45 & 60 & 30 & 40 & 55 & 70 & 34.4 \\
    \hline
    Avg. abs.& 3.4 & 7 & 4.6 & 1.7 & 4 & 6.5 & 27.6 & 34.4 & 14.6 & 15.2 & 34 & 48.7 & 16.4
    \end{tabular}}
  \caption{Errors for different trial runs (microseconds), E6-E12 held out.  If "-", the event was discarded because the event started between frames for one or more cameras.}
  \label{tab:syncExp3}
\end{table}
\textbf{Results:}\\
We note that the synchronization error remains acceptably small across all events,
although the error is smaller for the 6 events used for synchronization than the remaining 6, and the error appears to grow the further away from the synchronization events we go. This is inferred from the 12th
event showing the largest average absolute error, that of roughly 50 microseconds. The synchronization will be correct as long as the error is not so large that the synchronization can misalign two frames. Thus, at 30 FPS, the error must be below $30,000$ microseconds. Based on our results, assuming the clock drift is roughly constant, a misalignment would not happen for a very long time. We think that the assumption of constant, or at least similar, clock drift is reasonable given that the cameras are of the same make. This suggests that synchronization quality does not suffer significantly when the synchronization events
do not span the entire recording.

\subsubsection{Observations}
During the experiments, we observed that occasionally poor synchronization quality was achieved.
Further investigation revealed the cause to be a sudden drop in framerate
from one or more cameras. The reduced framerate was not due to dropped frames,
but rather due to the cameras dropping their framerates to 15 frames per second. We have found out that this happens when another USB device is plugged into the same USB controller. In our case, it was our Arduino board. The drop in framerate causes poor synchronization because it violates a central assumption
of the algorithm, i.e. that the exposure time of each pixel row is constant for all frames. Our software warns the user if a change in the framerate is detected.

\section{Hardware Synchronization}\label{section:hardwareTime}
The option exists to use hardware synchronization for synchronizing the shutters of the different cameras~\cite{bandwidthwhitepaper}.
This entails connecting each camera to a master camera using special cables.
Hardware synchronization provides the best synchronization between cameras, but it has some limitations.
For starters, since each camera needs to be connected to the master camera, in addition to being connected to the central PC, the number of cables in the system is effectively doubled. This adds clutter to the scene, while also making it more time consuming to move the cameras between recordings. Also, the distance between each camera is limited by the length of the synchronization cable, adding further restrictions to the potential setup. Worse still, the effective range of the synchronization cable is only three meters, when a passive interconnect is utilized, thus putting a hard limit on the distance between the cameras, unless a more complex active interconnect is pursued.
If the length of the wires exceed three meters and a passive interconnect is used, static electricity and other electrical noise sources may cause random frame counter resets to occur, which would then need to be reconstructed in software~\cite[p.~4]{bandwidthwhitepaper}. Another obstacle is that Intel does not provide any cables or interconnects for this off the shelf, and one would, therefore, have to make it or order it elsewhere.

We think that our software-based synchronization model is good enough, as long as the framerate is set high enough. How high it needs to be depends on how quickly the soft robot moves. We have not opted to pursue a hardware solution, but we recognize that hardware synchronization could have been used to verify our software approach.
We chose not to do so, due to time constraints and the risk of damaging the cameras, when creating custom wiring.

\section{Summary}
In this chapter we have discussed different methods of achieving time-synchronization, using either software or hardware. Hardware synchronization is the most accurate, but also more unwieldy when working with many cameras that need to be moved frequently or far.
We have chosen a software approach where we detect events produced by a camera flash in each camera and construct a model that can align the frames in time. We have validated that the model is a good fit for our cameras and has a low error. The accuracy of the software can be increased, by increasing the framerate, as the \emph{maximum} alignment error is limited by $\frac{1}{\text{FPS}}$. However, there is a limited bandwidth budget and thus increasing the framerate, also means decreasing the resolution.