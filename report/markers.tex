In this section, we will discuss the two main approaches of tracking the deformation of a soft robot: Marker-based and markerless. The deformation poses a unique challenge in tracking since the appearance of the object is not static. Additionally, the non-rigid surface changes of the subject, make it non-trivial to adhere tracking-markers. We will also examine previous work done at DIKU on both approaches. Finally, based on the previous work at DIKU and the literature review we try to analyze what is next.

\section{Markerless Tracking}
In the absence of markers, the soft robot needs to be reconstructed
solely from the RGB and depth data. If we assume an initial segmentation,
such that the robot has been isolated from the background, we have a point cloud corresponding to the soft robot. The methods in the literature we have surveyed differ in a few ways:
\begin{itemize}
\item Some require an initial template of the undeformed mesh, others generate it as a part of their pipeline.
\item All of the methods minimize an energy function to find the "best" deformation. They consist of a number of terms and regularizers (e.g. ARAP - As Rigid As Possible). These differ between the methods.
\item Most of the methods we have surveyed use RGBD cameras, but one uses only RGB data and another uses a complicated setup with cameras and a laser scanner.
\item When RGBD cameras are used, the current point cloud data must be mapped to the deformed mesh from the previous iteration. The methods differ in how they find these correspondences between the mesh and the point cloud data.
\end{itemize}

\citet{324257890efa446fae379435bb773933} proposed a markerless method in which a template mesh in correspondence with the pose of the object in the first frame is taken as input, along with the sequence of textured 3D point cloud frames. A setup of a light projector, two grey-scale cameras, and a color camera is used. The setup can be seen as a precursor to the modern commodity depth cameras.
The algorithm outputs vertex locations of the template mesh for each subsequent frame, thus mapping each point on the surface of the deformed object to its undeformed self. The solution is found by minimizing an energy function, essentially finding the least costly and most likely deformation, given the observed data.
The energy function is comprised of the squared distance from the template mesh to the scanned surface, the color of the vertex, the acceleration of the vertex, and the amount of bending or stretching exhibited by the vertex. The last two terms are shape-preserving because they penalize fast and drastic deformations.

In \citet{InnmannMatthias2016VRVN}, a markerless approach called VolumeDeform is suggested.
A notable innovation is that the initial undeformed point cloud is used to construct an initial template mesh, using the Marching Cubes algorithm, thus providing the undeformed template mesh automatically. The algorithm proceeds as follows for each frame:
\begin{enumerate}
  \item Convert the current deformation field to a mesh using Marching Cubes and render it in the coordinate frame to get a depth map.
  \item Find correspondences between the depth map and the RGBD frame, using SIFT and geometric features.
  \item Optimize an energy function and update the deformation field in a data-parallel way, using the one-ring neighborhood of vertices.
  \item Iterate.
  \item When converged, integrate the RGBD frame into the deformation field.
\end{enumerate}
The energy function consists of three terms, the point-to-plane distance between the current mesh and its correspondences in the RGBD data, the squared distances between the global sparse SIFT correspondences from the initial mesh and the corresponding points in the RGBD data, and lastly the ARAP regularizer, which serves to restrict how much the mesh is deformed.

In a recent paper, \citet{YangLi2020LtON} uses CNNs (Convolutional Neural Networks) to improve convergence time and the minima the optimization of the energy function converges to. Specifically, they train a network to extract good features for non-rigid registration and they train a network to predict a good preconditioner that improves the convergence of the optimization. Finally, the energy function is also modified to include an alignment energy term using the learned features. The complete energy function consists of the learned term, the ARAP regularizer, and a geometric term which is an approximation of the point-to-plane distance measure.

Tracking is also possible when only monocular RGB data is available. In this case, \citet{NRST_GCPR2018} proposes a method that relies on having a textured
mesh template of the deforming object’s surface. Since soft robots are likely to be constructed from a 3D printable file format, getting such a mesh is easily achievable, but texturing the mesh may prove more difficult.
The authors note that that the texture can be as small as the yarn patterns of fabrics, thus enabling them to track even uniformly colored materials, as long a the exhibit sufficient microstructure. The method assumes the camera is static and that the camera intrinsics are known. The energy function consists of some photometric terms and some geometric regularizers and motion regularizers (velocity/acceleration). The photometric terms in the energy function are evaluated by using the camera intrinsics to project the textured mesh onto the image. The energy function is then minimized to find the rotation and translation of each vertex in the original mesh, for every frame.

In \citet{SenguptaAgniva2019ToNO},
a method is proposed which only requires a rough estimate of Young’s modulus and the Poisson ratio of
the tracked object. The method relies on two object models, one of the surface, similar to that of a triangle mesh, and an internal volumetric tetrahedral model, which serves as a mechanical model, capturing the physical constraints of the object. Combining the two models into a base model, then any deformation of the object can be solved as
a second-order differential equation, mapping points on the deformed object to points on the reference mesh of the undeformed object.

\section{Marker-based Tracking}
As noted in \cite{KolahiA2007Doam}, marker-based tracking systems usually use small physical markers attached to the tracked object and a set of two or more cameras capture its motions.
An image processing program detects the markers captured by the cameras, and by combining 2D data with 3D depth data (if available), calculates the three-dimensional positions of each marker. Assuming a typical video frame rate, each marker is likely to be close to its position in the previous frame, making labeling of the markers a relatively simple task across frames, once an initial labeling has been achieved. Each camera needs to have their observed markers labeled and merged with the markers of the other cameras. Given that their global 3D location is known, the registered markers can often be merged by the simple heuristic of merging markers closest to one another~\cite[p.~20]{FredrikMA}.

\section{Pros and Cons of Marker/Markerless Tracking}
The main disadvantage of marker-based tracking is the possibility of missing markers due to occlusion or the markers falling off. Marker-based tracking presents a very substantial advantage, however, namely the relative ease of implementation, given that the markers can be designed such that they are easily segmented and classified. This enables the collection of deformation data, even with no knowledge of the underlying physical properties of the deformable object in question. This is in contrast to the markerless approach, in which the physical properties are often used to define an energy function or constraint when solving the deformation. The main challenges for the marker-based approach, are missing markers due to occlusion and in the case of soft robots, the simple act of attaching the markers to the robot can be an issue, since the markers are likely to either fall off during deformation or restrict the movement of the robot, giving unreliable results.

In contrast to the marker-based approach, the markerless approaches are
comparatively difficult to implement, and preferred methods have yet to solidify. In addition, as demonstrated by \citet{CeseracciuElena2014CoMa}, even when tracking rigid objects, the accuracy of markerless methods is less than when markers are utilized. An advantage of markerless methods is that they provide more dense deformation tracking and occlusion is less of an issue.

\section{Previous Work at DIKU}
\citet{FredrikMA} worked with marker-based tracking in his thesis, in which he developed the \lstinline{PySoRo} library. He segmented the markers observed by two cameras using the HSV color space and he then unified them into the same coordinate system using a camera calibration. Two markers were classified as being the same if they were closest to each other and if the distance was lower than a set threshold. Markers were tracked between frames by doing "greedy linking", i.e. linking markers that are closest in distance between two consecutive frames. Markers disappearing between frames were dealt with by using interpolation. To remove false positives, if the robot was known to contain $n$ markers, and $m > n$ markers were detected, then the markers that were interpolated the most were removed. The markers were dark pearls glued onto the robot, they were segmented in HSV color space by using the value channel.

\citet{maxThesis} used \lstinline{PySoRo} in his thesis to perform marker-based tracking. He extended it by adding colored markers (red) to increase the ease of segmentation and detection, as he noted the dark markers were hard to detect when the geometry was very fine. He also noted that it would be ideal to be able to capture all markers for better accuracy.


\citet{MathiasMA} looked into a markerless approach in his thesis, where a mesh of the robot in its resting position was used as a prior. A spring force system together with a finite element method simulation was used to iteratively drive the model mesh towards the point cloud, using point-to-point correspondences in each iteration, thus giving the algorithm an ICP flavor. The camera calibration from \lstinline{PySoRo} was also used in this project, it was noted that a more robust frame by frame calibration method should be developed.

% Frederik:
% - Extract markers collected from multiple cameras
% - Pearls glued on, value segmentation
% - Points observed by only one camera or by both, must be merged.
% - Merge common points by their mean value
% - Markers merged by distance (threshold)
% - Tracking markers between frames, interpolation and linking
% - Matching between differences by smallest distance (greedy linking)
% - Removing false positives: How often is a marker interpolated + number of markers on robot

% Max uses PySoro implemented by Frederik.
% - Marker interpolation
% - Segment robot away using the value channel (threshold)
% - Find the markers by finding "holes" in the robot.
% - Extraction is done frame by frame
% - Segmentation does not always exist
% - Overcome missing markers by interpolation
% - Need to capture all markers for better accuracy
% - Increasing accuracy: Color thresholding (HSV) threshold, because the dark markers can be hard to detect when the geometry is very fine

% Mathias:
% - Registration: Simulated ICP using a spring force model. P2P error.
% - Requires scan of the soft robot in a resting position with no forces applied.
% - Register point cloud to deformed virtual model of the soft robot, initial model in resting position is moved towards the point cloud
% -   Point clouds driven towards the model using a spring force model in an iterative way
% - Problem: Bringing robot and point cloud to the same scale
\section{What is Next?}
Based on the above and our literature review, we think that both methods have merit. The marker-based approach is attractive because of its simplicity, but it provides less data than the markerless and is not suited for all types of soft robots. For instance, there are robots in the DIKU lab that are made of pockets that are expanded and contracted with air. On these robots, it can be hard to attach markers that will stay on due to the potentially large expansions. It remains to be seen if using more cameras together with our calibration software will solve the occlusion and detection issues that the markers suffer from. As remarked in the thesis of~\citet{maxThesis} it would be ideal if the markers can stay tracked at all times. This would necessitate that each marker is seen by at least one camera. Our software should make this possible since multiple cameras can be used.

For the markerless approaches, as described in the above we have seen one instance where a CNN is used to improve the matching of the point cloud data and the mesh, as well as the optimization that solves for the deformations~\cite{YangLi2020LtON}. Learning approaches have proved to be superior to traditional ones in many fields in computer vision, and may also end up as the winning technology in this field. However, based on the number of available papers, we feel that they are currently also less mature than other methods and they likely also require additional training data to work in settings like the learning cube. For the more mature methods, we feel that VolumeDeform as described in~\cite{InnmannMatthias2016VRVN} seems promising for a number of reasons. Firstly, the results look impressive and seem to outcompete similar methods. Secondly, they use commodity RGBD sensors which are also the ones available to us. Finally, the method is online and works in real-time. The latter is achieved by using low resolution (640x480) and data-parallel parts that can be run on a GPU. For maximum accuracy and data, the algorithm could be adapted to use the maximum depth resolution of the RealSense cameras (1280x720). This would be at the cost of the real-time processing speed, but we do not consider this a major obstacle as the camera calibration does not run in real-time either. The method only uses a single depth camera, but it does not seem to be restricted to it. The method is also convenient in the sense that it does not require a prior mesh of the scene.
%We have provided dynamic camera calibration algorithm and software for data acquisition from an entire video stream. The camera calibration can be used to align the point clouds, both temporarily and spatially, when multiple cameras are used. This can be used to aid both markerless and marker-based approaches.  