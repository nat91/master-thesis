Besides the theoretical and hardware aspects of this thesis, it is also a fairly large software project. The software and libraries have two target audiences: The researchers who may use the tools and the students who may extend our work in the future. For the former, we provide command-line programs that can be used with little to no knowledge of the inner workings. For the latter, we have tried to maintain online documentation on how to use the various parts of the API. In addition to the documentation, we have provided the function signatures with so-called "doc comments", which modern editors can utilize to show IntelliSense suggestions for the libraries. This section explains the directory structure, an example of the architecture, and how the software can be used in the two use scenarios described in Chapter~\ref{section:introduction}. Finally, the chapter has two small tutorials on how to use the command-line programs.

Gitlab link: \url{https://gitlab.com/nat91/master-thesis}\\
Documentation on Gitlab pages: \url{https://nat91.gitlab.io/master-thesis}

\section{The Directory Structure}
The main \emph{code} directory contains the following subdirectories and programs:
\begin{itemize}
\item \emph{experiments} - Contains the scripts we have used to run most of our experiments.
\item \emph{lib} - Contains the different libraries we have developed for the project.
\item \emph{libcbgen} - Contains a tool that can generate PDF files with checkerboards in a specified size for printing.
\item \emph{samples} - Small code snippets that we have used for testing, they can be used by others to get an idea of how to use the code as well.
\item \emph{tools} - Contains various tools for e.g. converting a directory of images to a movie and for making an artificial dataset using one camera.
\item \emph{main.py} - The main program, all the steps of the calibration can be started from it. It is designed to make an entire recording of a soft robot as it goes through its motions and then performs the calibration and time calibration afterward. Doing the processing afterward was motivated by avoiding dropped frames as discussed in Chapter~\ref{section:cameras}.
\item \emph{calibrate\_frame.py} - Calibrates a single frame from the currently connected cameras.
\end{itemize}

\section{An Architecture Example: The FeatureDetectorCollection}
Future students may want to experiment with using a different calibration method or different calibration objects. As an example, adding a different calibration object usually entails adding a new feature detector. This can be done by implementing a feature detector object implementing the method: \lstinline{getFeatures(img, depth, K) -> detected_features}.
It takes the image, the aligned depth frame, and the intrinsic calibration matrix $\mathbf{K}$. It returns a dictionary \lstinline{detected_features} stating its kind (e.g. SIFT, checkerboard, sphere), an array of feature points, a map of associated descriptors and a matcher function. The matcher function is necessary because different kinds of features are matched differently, e.g. our checkerboards are matched with text labels and the SIFT features are matched via the smallest euclidean distance. Once the new feature detector has been added, it can be instantiated and added to the \lstinline{FeatureDetectorCollection} via its \lstinline{add} method and the camera calibration will then use it to establish correspondences. This is an application of the open-closed principle, as the software is open to extension, but closed to modification \cite[p.~164]{agilePrinciples}.

\section{Using the Software in Different Scenarios}
This section briefly describes how the calibration software can be used in the Learning Cube and "free space" settings introduced in Chapter \ref{section:introduction}.

\subsection{The Learning Cube}
There are at least two use cases for the learning cube. The first is where it is used in the lab to do a full recording of a soft robot in motion. In this case \emph{main.py} can be used for the entire recording and calibration process. It can be configured how often, i.e. every $i$th frame, the calibration should be done. For the second case, the learning cube is brought to conventions where some vision algorithm needs calibrated cameras to function. The cameras are mostly static in this setting and are precalibrated when the learning cube is set up. However, as the learning cube and its contents are examined by the convention attendees the cameras may be disturbed and need to be recalibrated. For this our API can be invoked to get a new calibration for each camera, which can then be re-input to the part of the setup relying on it. It is also conceivable that an accelerometer could be attached to the cameras, such that we can detect when they move, and then recalibrate.

For either case, because the learning cube is equipped with black sides, it is necessary to place the calibration objects (see Chapter~\ref{section:calibration_objects}) in the cube to ensure the calibration algorithm can establish correspondences between the views. Alternatively, the SIFT~\cite{SIFT} detector can be used if some sufficiently textured pattern is mounted on the sides of the cube or on the robot itself.
The calibration objects should remain in the cube throughout the recording, such that calibration can be done periodically if need be.

The current learning cube at DIKU is around $80 \times 80$cm. Given that no depth data is available between $0$ to $40$cm from the camera, this can make it hard to get a large enough working volume. As discussed in Chapter~\ref{section:cameras}, one can adjust the disparity shift to get depth values closer to the camera.

\subsection{The "Free Space" Setting}
In the "free space" setting the cameras can be placed on tripods or similar. We do not recommend placing the camera on something that moves often due to the rolling shutter. The Intel RealSense D435 is better suited for that purpose due to its global shutter. If the scene has a sufficient amount of texture, the SIFT~\cite{SIFT} detector can be enabled and no calibration objects are necessary. The calibration objects we have implemented (see Chapter~\ref{section:calibration_objects}) can also be used in the "free space" setting for more redundancy, they do not assume anything about their environment.

% \subsection{Real Time Usage}
% In some future use scenarios it could be of interest to run the calibration in near real time. Our current implementation is not optimized for real time calibration ...

% \subsubsection{Exploiting Temporal Coherence}
% One of the main bottlenecks with respect to the running time is that we use traditional image analysis techniques to find features in the images, this entails examining and filtering all the image and depth data available per frame. However, assuming the cameras are relatively static, this only has to be done periodically. We can avoid examining all the data by exploiting temporal coherence, if a position of a calibration object is known at time $t$ then at time $t+1$ we can search for it in the vicinity of where it was at time $t$ and update its position after detection. However, because calibration objects can enter and leave the view over time, all the data still has to be examined periodically.

% \subsubsection{Using a Lower Level Language or More Parallelization}
% The SIFT detector is the most efficient, we use an implementation of it provided by some versions of OpenCV. This implementation is written in C++ and highly optimized. Our other feature detectors are implemented in Python and uses numpy to vectorize calculations where applicable. However, Python, being an interpreted language, is inherently slow and speedups could be gained by porting them to C++ or similar.

% Another option is to utilize more parallelization. For instance, for each image, the feature detectors can run in parallel if multiple kinds of calibration objects are used. The feature detectors themselves can also be parallelized further.

\section{Using main.py for a Full Session}
This section is a step-to-step guide on how to use \emph{main.py} for a full recording session. We will assume you have a directory called \emph{recordings} in the directory \emph{main.py} is located in and that you name the recording "newRecording".
\begin{enumerate}
\item \textbf{Setup:} Plug the cameras and the time synchronization Arduino board into the computer. If necessary, use multiple machines such that each camera is connected to its own USB controller. Place the calibration objects in the scene if necessary, possibly while using the Intel RealSense Viewer application to check visibility and depth quality before starting the recording.
\item \textbf{Recording:} \lstinline{python3 main.py --outputFolder=recordings --name=newRecording record}
\begin{itemize}
\item This will create a directory called "newRecording" in the "recordings" directory. \item Unless the \lstinline{-disablesync} flag is provided, the program will search for a connected time calibration Arduino board and start the camera flash. By default, $10$ flashes with a frequency of $5$ seconds will be done, but you can adjust this in the config file if necessary (see the online documentation).
\item \lstinline{-disablesync} must be provided on the machines the time calibration board is not connected to if multiple machines are used.
\item The recording for each camera will be saved in a binary file named \emph{cameraSerialNumber.dat} where \emph{cameraSerialNumber} is replaced by the serial number of each camera. In addition to that a csv file with extension ".recdat" will be created, it keeps track of the cameras used to make the recording.
\item Press "e" at any time to stop the recording or "p" to pause it.
\item Note: The part of the recording containing the flashes from the time calibration will be removed afterward, wait for time calibration to finish before moving the soft robot.
\end{itemize}
\item \textbf{Merging (Optional):} If you recorded on multiple machines, copy the resulting \texttt{.dat} files to the machine you want to do analysis on and append the ".recdat" files on that machine as well.
\item \textbf{Time Synchronization:} \lstinline{python3 main.py --recPath=recordings/newRecording sync}

This performs time synchronization as described in Chapter~\ref{section:time_calibration}. It will rename the current \texttt{.dat} files and output aligned \texttt{.dat} files such that if frame $i$ is retrieved from each file, they are aligned in time. Missing frames are removed.
\item \textbf{Convert:} \lstinline{python3 main.py --recPath=recordings/newRecording convert}

This converts the binary recording data to an HDF5\footnote{\url{https://www.hdfgroup.org/solutions/hdf5/}} database, which is a file database similar to SQLite, but more lightweight. This makes it more convenient to work with the recording data and also makes it easy to do so from any programming language. By default, the data will be compressed with zlib as well, but if you want to save time it can be disabled with the flag \lstinline{--compress=false}. Optionally, \emph{convert} will also output a lossless video of the recording for each camera if the flag \lstinline{--outputVideo=true} is supplied. The calibration reads the resulting HDF5 file, if you want to use it directly you can find the structure of it in the online documentation.
\item \textbf{Calibrate:} \lstinline{python3 main.py --recPath=recordings/newRecording calibrate}

Uses the HDF5 database generated in the previous step and produces a new one that contains the calculated calibrations, point cloud data for each camera, and other information. By default, a calibration will be calculated every $10$ frames (every $1/3$ second) unless something else is specified in the config file. Since this step takes a long time we have focused on making it robust. Firstly, if any part of the calibration throws an exception, it will automatically retry the next frame. Secondly, after each iteration, the current progress is saved to a file that is forced to disk. In the event of the entire process crashing or being interrupted, the calibration will use the recovery file to resume from where it stopped. An image of the running calibration program can be seen in Figure~\ref{fig:cal_process}\begin{figure}[H]
  \centering
  \includegraphics[width=0.8\textwidth]{images/cal_process.png}
  \caption{The calibration program has a progress bar and it continuously outputs the status from the feature detectors. For instance, in the above, it found checkerboard $21$ and $22$ in both images (it is printed twice) and it matched $57$ SIFT features between the frames.}
  \label{fig:cal_process}
\end{figure}

The structure of the output HDF5 file and all configuration settings that affect the calibration can be found in the online documentation.
\item \textbf{Use the data:} The resulting calibration and point clouds can now be accessed via the produced HDF5 database, using any HDF5 reader e.g. \texttt{pytables} for Python. You can also explore the tools we have developed that read it, e.g. "tools/calibration\_viewer\_server.py" which starts a server that allows you to see the calculated calibrations and visualize the point clouds. It can be controlled via \texttt{telnet} or from a client script, see e.g. "tools/calibration\_to\_images.py".
\end{enumerate}

\section{Using calibrate\_frame.py}
In this section, we show a step-to-step guide to use \emph{calibrate\_frame.py}. It calculates a calibration from a single set of frames. This can e.g. be used at conventions to recalibrate when the cameras are moved.
\begin{enumerate}
\item \textbf{Setup:}
  \begin{enumerate}
    \item Plug the cameras in. You may need to use multiple machines to get sufficient bandwidth.
    \item Set any necessary configuration in "config.json". You can e.g. set the auto-exposure time, what feature detectors to be used, the back clipping plane, etc. Sensible default values are provided if you do not configure anything.
  \end{enumerate}
\item \textbf{Capture:}
  \begin{enumerate}
    \item Run the script on all machines.
    \begin{lstlisting}[numbers=none]
      python3 calibrate_frame.py cacheFile.data
    \end{lstlisting}
    \item The recorded frames will be saved to "cacheFile.data". If the file already exists the frames from that file will be used instead. Replace "cacheFile.data" with whichever filename you like.
    \item The resulting calibration will be output to the file "cacheFile.cal\_dat" by pickle. It stores a dictionary of transformations and auxiliary information. If a key $x$ is in the dictionary then there exists a transformation from camera $x$ to the world coordinate system.
    \item A visualization of the calibration is shown at the end.
  \end{enumerate}
\item \textbf{Merge (optional):} If you recorded from multiple machines, move the cache files to one machine. Then use the tool "tools/merge\_cache\_files.py" to merge them into one cache file and repeat step $2$. If you have a machine with just one camera connected to it, you can use "tools/artificial\_dataset.py" to get a cache file.
\item \textbf{Use the data:} You can access the calibration by loading it with \emph{pickle} as exemplified in the following:
\begin{lstlisting}
import pickle

with open("cacheFile.cal_dat", "rb") as f:
  calibration = pickle.load(f)
\end{lstlisting}
\end{enumerate}
