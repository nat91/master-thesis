An advantage of using spheres as calibration objects is that they appear the same from all directions and can thus be identified from all angles and their centroids can be used as a correspondence. This has the disadvantage that each sphere only provides a single correspondence. The 3D coordinate of the correspondence is also not observed directly since the centroid is inside the sphere, rather than on its surface.
The centroid coordinate must be estimated from the location of the sphere in the RGBD data, and thus becomes a source of error if estimated inaccurately. We, therefore, need a robust method to estimate the sphere centroid in the presence of noisy depth data.

Prior work by \citet{FredrikMA}, for the \lstinline{PySoRo} project at DIKU, also used sphere fitting for the problem of finding correspondences between cameras~\cite[p.~17]{FredrikMA}.
In the \lstinline{PySoRo} approach, the centroid and location of the sphere are estimated, followed by an adjustment based on the known true radius of the sphere.
Additionally, all the spheres are the same color, thus making it necessary to try all permutations of sphere labels to match them between different views, which has a complexity of $O(n!)$.
We have endeavored to improve on this approach by using radius-constrained sphere fitting using the true radius and by removing noisy outliers. We also implement a color-based sphere classification as suggested in~\citet{FredrikMA}, such that we avoid the $O(n!)$ complexity and the degenerate setups caused by symmetry as described in~\citet{FredrikMA}.

In the following, we will refer to sphere fitting where the radius is part of the estimated variables as the \textbf{unconstrained} case,
and refer to sphere fitting where the radius is constrained to the true radius as the \textbf{constrained} case. The unconstrained case is a linear problem with a closed-form solution, while the constrained problem is non-linear with no closed-form solution.

\subsection{The Algorithm}
The algorithm finds centroids of colored spheres located in the scene, using RGBD data.
The algorithm is described in Algorithm \ref{algo:sphereDetection} and utilizes the \textit{coneCylAlgoE57} non-linear sphere-fitting algorithm
developed by \citet{RachakondaPrem2017Mact}, and proposed by \citet{FranaszekM2009FStR}, and which we will refer to as the NIST sphere-fitting algorithm.
\begin{algorithm}[ht]
  \renewcommand{\algorithmicforall}{\textbf{for each}}
  \caption{Finding Calibration Spheres}\label{algo:sphereDetection}
  \hspace*{\algorithmicindent} \textbf{Input:} HSV and depth image pair\\
  \hspace*{\algorithmicindent} \textbf{Output:}  List of centroids
  \begin{algorithmic}[1]
    \STATE{$trueRadius$ := 0.02}
    \STATE{$centroids$ = []}
    \FORALL{color in [red,green,blue,yellow]}
    \STATE{Color segment HSV image based on hue and saturation}
    \STATE{Use Hough circle detection to get candidate circles}
    \FORALL{candidate}
    \STATE{$score$ := percentage of pixels in color range}
    \STATE{($centroid$, $residual$) := fit of sphere of true radius to the corresponding depth data}
    \ENDFOR
    \FOR{candidate with highest $score$ and acceptable $fit$} \STATE{append $(color, centroid)$ to $solutions$} \ENDFOR
    \ENDFOR
  \end{algorithmic}
  \hspace*{\algorithmicindent} \textbf{Return:} $solutions$
\end{algorithm}
As a preprocessing step, we convert the image to the HSV color space and segment the image according to each possible sphere color. This gives a binary image for each color, highlighting possible sphere locations. The result before and after preprocessing can be seen in Figure~\ref{fig:spherePreProcessing}.
\begin{figure}[H]
  \centering
  \begin{subfigure}[t]{.5\textwidth}
    \centering
    \includegraphics[width=1\linewidth]{images/sphere_orig.png}
    \caption{Original image}
    \label{fig:spherePreProcessingSub1}
  \end{subfigure}%
  \begin{subfigure}[t]{.5\textwidth}
    \centering
    \includegraphics[width=1\linewidth]{images/sphere_canny.png}
    \caption{Red-segmented image - the shape of the bunny and the red sphere are visible.}
    \label{fig:spherePreProcessingSub2}
  \end{subfigure}
  \caption{Before and after preprocessing step for red spheres}
  \label{fig:spherePreProcessing}
\end{figure}

Following the preprocessing step we apply the Hough circle detector \cite{openCVHough} to locate sphere candidates. The Hough circle detector first detects edges using the Canny edge detector \cite{CannyJohn1986ACAt}\cite{openCVCanny} on the binary image to find the edges. It then traverses the edges, and using the function of a circle, votes on potential circle centers. Once all edge pixels have been treated, certain areas of the image have received more votes than others, and these are returned as circle center candidates, consisting of a center and radius for each circle, measured in pixels.

We keep the parameters of the Hough circle detector fairly loose, to limit the risk of the true sphere being rejected by the Hough detector.
To reduce the number of candidates, we compare the percentage of pixels within the correct color range, for each circle, using the center and radius reported by the Hough circle detector. At least $75\%$ of the pixels should be in the color range.
It is then necessary to robustly find the actual
sphere among the remaining candidates and to do so, we rely on a trio of criteria about the sphere:
\begin{enumerate}
  \item The sphere is uniformly colored, and so most pixels within the circle should be in the correct color range. Due to self-shadowing, the sphere will not be perceived as uniformly colored, but in the HSV color model, the hue component is not greatly affected by shadows, and so we can treat the sphere as uniformly colored.
  \item The sphere should produce small residuals when fitted by the constrained NIST sphere-fitting algorithm.
  \item The sphere has a radius of $2$cm.
\end{enumerate}
We use the constrained rather than the unconstrained fitting algorithm since the constrained fit results in a lower overall variation in the location of the centroid of the
sphere~\cite[p.~16]{RachakondaPrem2017Mact}. The NIST sphere-fitting algorithm also removes outliers, which ensures that the calculated centroid is more accurate.
We compare the quality of fit between different candidates by comparing the size of the residuals.
In case multiple candidates have very small residuals, we pick the one that has the best color score, once again calculated as the percentage of pixels in the correct color range, using the center and radius proposed by the Hough detector.

Finally, when the best candidates for all spheres have been obtained, we return
their centroids. These centroids are located in the camera's coordinate system. Figure \ref{fig:sphereDepth} shows a reconstructed sphere, and we note the tight fit between the depth data and the reconstruction.
\begin{figure}[ht]
  \centering
    \centering
    \includegraphics[width=.8\linewidth]{images/sphere_fit.png}
    \caption{Fitted sphere for the red sphere seen in Figure~\ref{fig:spherePreProcessingSub1}. The wireframe is the reconstructed sphere. Red points are inliers and blue are outliers. The green star is the estimated centroid. The sphere appears flat, but this is because of the axes' spacing being different.}
    \label{fig:sphereDepth}
\end{figure}

\subsection{The NIST Sphere Fitting Algorithm}
Since the quality of the produced centroids relies heavily on the quality of the NIST sphere fitting algorithm,
we will describe it in this section.
The shortest distance between a random point and the surface of a sphere is the orthogonal distance. We want to minimize the sum of
squares of the orthogonal distances between a given set of 3D points and the surface of the best-fitted sphere.
The objective function is given by the orthogonal error function:
\begin{align}
  F = \sum_{i=1}^{n}f^2_i = \sum_{i=1}^{n}(\sqrt{(x_i-x)^2+(y_y-y)^2+(z_i-z)^2} - r)^2 \label{eq:ortho}
\end{align}
where $(x, y, z)$ is the unknown centroid coordinate, $r$ is the known radius and $(x_i, y_i, z_i)$
are the points obtained from the depth image. We refer to this as the orthogonal constrained fit, because the equation is constrained by the radius being known.
This non-linear equation can then be solved using the Levenberg–Marquardt algorithm \cite{DonaldW}.

To initialize the Levenberg–Marquardt algorithm, an initial centroid guess needs to be provided.
A good guess will reduce runtime, but also minimize the risk of finding a non-global local minimum.
This is important for the orthogonal error function since it has two local minima~\cite[p.~3544]{FranaszekM2009FStR}.
As shown in Figure \ref{fig:nist}, the median distance $d$ between the origin of the camera plane and $N$ points on the
sphere closest to the origin is calculated. This makes it important that a reasonable segmentation
of the sphere has been achieved before running the algorithm and more specifically that the sphere is
not occluded. Normally in the NIST algorithm, points are selected from the dataset that fall within a distance of $d + 0.5r$ from the origin, where $r$ is the radius of the sphere.
The resulting dataset is considered as an initial segmentation of data belonging to the sphere, such that any background elements are filtered out.
The guess for the initial centroid is then provided by the so-called \textbf{closest-point method}, in which the 5\% of the closest points to the camera are selected and used to perform the constrained fit using Levenberg–Marquardt.
% The guess for the initial center is then provided by minimizing equation \ref{eq:ortho} in expanded form, using the segmented data points:
% \begin{align}
%   F = \sum_{i=1}^{n}f^2_i &= \sum_{i=1}^{n}(\sqrt{(x_i-x)^2+(y_y-y)^2+(z_i-z)^2} - r)^2 \\
%   &= \sum_{i=1}^{n}((x_i-x)^2+(y_y-y)^2+(z_i-z)^2 - r^2)^2 \Rightarrow \\
%   f_i &=-2x_ix-2y_iy-2z_iz + (x_i^2 + y_i^2 + z_i^2) + (x^2 + y^2 + z^2-r^2)
% \end{align}
% We solve this as a linear least squares problem (IS IT LINEAR WHEN R KNOWN?) $AX=B$ where:
% \begin{align}
%   A = \begin{bmatrix}
%     -2x_1 & -2y_1 & -2z_1 & 1 \\
%     \vdots & \vdots & \vdots & \vdots \\
%     -2x_n & -2y_n & -2z_n & 1
%   \end{bmatrix},
%   X = \begin{bmatrix}
%     x \\
%     y \\
%     z \\
%     \rho
%   \end{bmatrix},
%   B = \begin{bmatrix}
%     -x_1^2-y_1^2-z_1^2+r^2 \\
%     \vdots \\
%     -x_n^2-y_n^2-z_n^2+r^2
%   \end{bmatrix}
% \end{align}
% and $\rho = (x^2+y^2+z^2-r^2)$.


%% Initialization
\citet{RachakondaPrem2017Mact} notes that other methods could be used to get the initial guess and we have
observed improved accuracy by deprojecting the 2D center of the
circle into 3D space, and then simply adding the known radius to its depth component. This gives a point inside the sphere that is close to the centroid, assuming no occlusion. This is also faster than performing the closest-point method, making it an obvious choice since it is both faster and more accurate.

% Two criteria in each iteration: Angle and distance from current centroid. Points outside of the sphere will not be included because they are segmented away (or at most a few of them) as a preprocessing step.

% Inliers vs. outliers, outlier if residual > 3* stddev(residuals)

With an initial guess obtained, a vector $OC_0$ is constructed between the origin and the initial sphere centroid $C_0$.
Each datapoint $P_i$ is included in the calculation, if the angle $\beta_i$ between the vector $C_0P_i$ and $C_0O$
is less than $60^{\circ}$ and the distance between the points and the line $C_0O$ is less than or equal to
$r \cdot \sin(60)$, i.e. within a cylinder of radius $r \cdot \sin(60)$ originating in the camera origin as shown in Figure \ref{fig:nist}.
\begin{figure}[ht]
  \centering
  \includegraphics[width=1\textwidth]{images/nist.png}
  \caption{Visual representation of $C_0P_i$ and $C_0O$ and $\beta_i$. Blue dots are point cloud points, the red dot is $P_i$.}\label{fig:nist}
\end{figure}
An orthogonal constrained fit is performed on the resultant dataset. Points are marked as outliers from
this dataset when the absolute values of the corresponding residuals exceed three times the standard deviation of the residuals.
The inliers are used to calculate the sphere centroid using the
Levenberg–Marquardt method and Equation \ref{eq:ortho}. This step is then repeated five more times,
with each prior estimate of the centroid serving as this initial guess in subsequent iterations. The choice of five iterations is heuristically chosen by \citet{RachakondaPrem2017Mact}, as the point at which no further improvement is likely to occur. An alternative would be to use the size of the residuals as a stopping criterion.
The use of the cone and cylindrical region ensures that the points on
the outer periphery of the sphere are excluded. These points are usually of lower accuracy,
either because they do not belong to the sphere at all, or because they are taken at an extreme angle.

\subsubsection{Alternative Sphere Fitting Algorithms}
Given that non-linear fitting methods such as Levenberg–Marquardt can be slow, we will briefly discuss why we chose this approach, instead of others available to us. We are prevented from using unconstrained
sphere fitting algorithms, where the radius is also estimated since the noise in the depth data is likely to result
in radius estimates that deviate substantially from the true radius, thus giving poor centroid estimates.
This limits us to the constrained case with a known radius, where nonlinear solvers need to be applied.
A popular choice for nonlinear problems is the Gauss-Newton algorithm, but
compared to Levenberg–Marquardt, Gauss-Newton is known to exhibit lower robustness with respect
to starting point, making us prefer Levenberg–Marquardt\cite{RachakondaPrem2017Mact}\cite{mathworks}.

There are also other options for minimizing the objective function from eq.~\eqref{eq:ortho}. This can be achieved by utilizing any function minimizers, such as \texttt{scipy.optimize.fmin} which relies
on the simplex algorithm. Unfortunately, many of these minimizers (including simplex) are not resistant
to local minima, and a correct centroid estimate can therefore not be guaranteed.

Lastly, we could use an approximated centroid solution, such as presented in the \lstinline{PySoRo} software library,
where the mean surface point is found on the sphere, from the camera's perspective, after which the centroid is found
by adding the true radius to the depth, taking perspective into account. While this approach is theoretically sound,
it is very sensitive to noise in the sphere surface depth data, since this noise will be directly transferred to the centroid estimate.

We prefer our method because it is more robust to noisy outliers and it fits to the radius constraint. This gives more accurate centroids, which is important when they are used for camera calibration. If the centroids are too inaccurate, it will propagate to the calculated transformation and cause misalignment.

\subsection{Limitations and Requirements}
Given our choice of method, there are a number of things to keep in mind, in order for the spheres to be easily recognized. These requirements need to be satisfied by all cameras.
\begin{itemize}
  \item The spheres should ideally not be placed in front of objects of similar colors, since this will make the circle detector less likely to pick up the circle, due to poor contrast.
  \item The spheres should not be partially occluded since we rely on circle detection to detect the spheres.
  \item Based on our experience, the accuracy of the sphere centroids suffers when the spheres are more than 150cm from the camera. The noise is too heavy at this distance.
  \item Spheres should not be placed along the left image edge since depth data is not available in this region, see Chapter~\ref{section:cameras}.
  \item Since the identification is based on the color of the spheres, lighting should be neutral in color (i.e. white), so as not to shift the color of the spheres into different areas of the color spectrum.
  \item In practice, it is hard to robustly tell more than a few colors apart, which effectively limits the number of colored spheres that can be placed and unambiguously identified. We have employed four colors, namely red, green, blue, and yellow.
\end{itemize}
Figure \ref{fig:exampleSphere} shows examples of good and poor calibration setups.
\begin{figure}[H]
  \centering
  \begin{subfigure}[t]{.5\textwidth}
    \centering
    \includegraphics[width=.8\linewidth]{images/sphere_bad.jpg}
    \caption{Poor calibration setup. Not enough contrast between the red sphere and the background. Yellow sphere too far away. Green sphere occluded.}
    \label{fig:badSphere}
  \end{subfigure}%
  \begin{subfigure}[t]{.5\textwidth}
    \centering
    \includegraphics[width=.8\linewidth]{images/sphere_good.jpg}
    \caption{Good calibration setup.}
    \label{fig:goodSphere}
  \end{subfigure}
  \caption{Examples of good and poor calibration setups of the colored spheres.}
  \label{fig:exampleSphere}
\end{figure}